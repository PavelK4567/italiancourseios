//
//  PageStatistic.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseStatistic.h"
#import "ProgressFactory.h"

@interface PageStatistic : BaseStatistic

@property (nonatomic, assign) NSInteger pageOrderID;
@property (nonatomic, assign) NSInteger numberOfCorrectAnswers;
@property (nonatomic, assign) NSInteger numberOfIncorrectAnswers;
@property (nonatomic, assign) NSInteger numberOfSkippedQuestions;
@property (nonatomic, assign) NSInteger numberOfNotAnsweredQuestions;
@property (nonatomic, assign) NSInteger numberOfTotalQuestions;
@property (nonatomic)         BOOL      isExposure;
@property (nonatomic)         BOOL      pageStatus;
@property (nonatomic)         BOOL      isVisited;

+ (BaseStatistic *)buildStatisticFromObject:(id)dataObject
                                childObject:(NSArray *)childObject
                              statisticType:(StatisticType)type;
@end
