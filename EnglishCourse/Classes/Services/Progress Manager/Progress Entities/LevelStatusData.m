//
//  LevelData.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/26/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LevelStatusData.h"
#import "UnitData.h"

@implementation LevelStatusData

#pragma mark - init

- (id)initWithDict:(NSDictionary *)dict {
    
    if (self = [super init]) {
        self.levelId        = [[dict objectForKey:@"levelID"] integerValue];
        self.maxUnitId      = [[dict objectForKey:@"maxUnitID"] integerValue];
        self.contentVersion = [dict  objectForKey:@"contentVersion"];
        self.diploma        = [[dict objectForKey:@"diploma"] boolValue];
        self.score          = [[dict objectForKey:@"score"] integerValue];
        self.unitDataArray  = [LevelStatusData createUnitDataStructure:[dict objectForKey:@"units"]];
    }
    
    return self;
}


#pragma mark - createUnitDataStructure

+ (NSMutableArray *)createUnitDataStructure:(NSArray *)unitsData
{

    NSMutableArray *units = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < unitsData.count; i++) {
        
        id unitData = [unitsData objectAtIndex:i];
        if(unitData) {
            
            UnitData *unit = [[UnitData alloc] initWithUnitId:[[unitData objectForKey:@"unitID"] integerValue] progress:[[unitData objectForKey:@"progress"] integerValue] achived:[[unitData objectForKey:@"achieved"] boolValue]];
            if(unit)
                [units addObject:unit];
        }
    }
    
    return units;
}

#pragma mark -
@end
