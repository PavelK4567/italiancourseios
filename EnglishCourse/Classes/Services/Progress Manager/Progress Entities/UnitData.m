//
//  UnitData.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/26/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "UnitData.h"

@implementation UnitData

- (id)initWithUnitId:(NSInteger)unitId
            progress:(NSInteger)progress
             achived:(BOOL)achived
{
    
    self          = [self init];
    self.unitID   = unitId;
    self.progress = progress;
    self.achieved = achived;
    return self;
}
@end
