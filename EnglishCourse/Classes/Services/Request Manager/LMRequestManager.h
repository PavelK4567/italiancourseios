//  LMRequestManager.h
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


typedef enum {
  kRequestLoginUserAPICall,
  kRequestLoginTokenAPICall,
  kRequestShowTokenAPICall,
  kRequestConfirmLoginTokenAPICall,
  kRequestGetUserStatusAPICall,
  kRequestGetUserNativeStatusAPICall,
  kRequestSubscriptionForNativeUserAPICall,
  kRequestBillingForNativeUserAPICall,
  kRequestLogoutUserAPICall,
  kRequestNuanceAPICall,
} ELMRequestType;


@interface LMRequestManager : NSObject

@property (nonatomic, strong) NSString *groupID;
@property (nonatomic, strong) NSString *contentID;

- (ASIHTTPRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers;


@end


#ifdef REQUEST_SINGLETON

@interface LMRequestManager ()
+ (id)sharedInstance;
@end

#define REQUEST_MANAGER ((LMRequestManager *)[LMRequestManager sharedInstance])
#endif
