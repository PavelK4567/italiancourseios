//
//  LMApiRequestManager.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 1/14/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMApiRequestManager.h"


@implementation LMApiRequestManager

#ifdef API_REQUEST_SINGLETON
SINGLETON_GCD(LMApiRequestManager)
#endif


#pragma mark - create request

- (LMRequest *)post:(API_REQUEST_TYPE)type headers:(NSDictionary *)headers
{
    NSURL *url = [self urlForType:type headers:headers];
    LMRequest *request;
    if (url) {
        request = [LMRequest requestWithURL:url usingCache:[ASIDownloadCache sharedCache] andCachePolicy:ASIAskServerIfModifiedCachePolicy];
        NSArray *keys = [headers allKeys];
        for (NSString *key in keys) {
            [request addRequestHeader:key value:headers[key]];
            [request setRequestType:type];
            return request;
        }
    }
    return 0;
}


#pragma mark - create url for request

- (NSURL *)urlForType:(API_REQUEST_TYPE)type headers:(NSDictionary *)headers
{
    NSString *path = nil;
    switch (type) {
            
        case kRequestUpdateProgress:
            path = [NSString stringWithFormat:kUpdateProgressUrl,kApiECBaseUrl, [headers objectForKey:kUserMSISDN],[headers objectForKey:kGroupID],[headers objectForKey:kContentID],[headers objectForKey:kUnitID],[headers objectForKey:kUnitScore],[headers objectForKey:kUnitAchieved],[headers objectForKey:KProgress]];
            break;
        case kSendDiploma:
            path = [NSString stringWithFormat:kSendDiplomaUrl,kApiECBaseUrl, [headers objectForKey:kUserMSISDN],[headers objectForKey:kGroupID],[headers objectForKey:kContentID],[headers objectForKey:kLevelId],[headers objectForKey:kSalutation],[headers objectForKey:kFirstName],[headers objectForKey:kLastName],[headers objectForKey:kEmail],[headers objectForKey:kResend]];
            break;
        case kDiplomaDetails:
            path = [NSString stringWithFormat:kDiplomaDetailsUrl,kApiECBaseUrl, [headers objectForKey:kUserMSISDN],[headers objectForKey:kGroupID],[headers objectForKey:kContentID]];
            break;
        case kUserProgression:
            path = [NSString stringWithFormat:kUserProgressionUrl,kApiECBaseUrl, [headers objectForKey:[kUserMSISDN lowercaseString]],[headers objectForKey:kContentID],[headers objectForKey:kPhoneModel],[headers objectForKey:kOsType],[headers objectForKey:kOsVersion],[headers objectForKey:kLevelId],[headers objectForKey:kGroupID],[headers objectForKey:kLessonId],[headers objectForKey:kSkipped],[headers objectForKey:kCorrect],[headers objectForKey:kinCorrect]];
            break;
        case kPlacementTest:
            path = [NSString stringWithFormat:kPlacementtestUrl,kApiECBaseUrl, [headers objectForKey:[kUserMSISDN lowercaseString]],[headers objectForKey:kContentID],[headers objectForKey:kGroupId],[headers objectForKey:kPhoneModel],[headers objectForKey:kOsType],[headers objectForKey:kOsVersion],[headers objectForKey:kCompleted],[headers objectForKey:kGrade],[headers objectForKey:kRecommendedLevel],[headers objectForKey:kDateTime]];
            break;
        case kSessionStatistic:
            path = [NSString stringWithFormat:kSessionStatisticUrl,kApiECBaseUrl, [headers objectForKey:[kUserMSISDN lowercaseString]],[headers objectForKey:kGroupID],[headers objectForKey:kContentID],[headers objectForKey:kDeviceId],[headers objectForKey:kStartDate],[headers objectForKey:kEndDate]];
            break;
            
            default:
            break;
    }
    if (path) {
        return [NSURL URLWithString:path];
    }
    return nil;

}


#pragma mark - updateProgressForUser

- (void)updateProgressForUser:(NSString *)msisdn
                      groupID:(NSString *)groupId
                    contentID:(NSString *)contentId
                       unitID:(NSString *)unitId
                    unitScore:(NSString *)unitScore
                  unitAchived:(NSString *)unitAchived
                     progress:(NSString *)progress
              CompletionBlock:(ApiResultBlock)completionBlock
{
    
    NSArray *values = [NSArray arrayWithObjects:msisdn,groupId,contentId,unitId,unitScore,unitAchived,progress ,nil];
    NSArray *keys   = [NSArray arrayWithObjects:kUserMSISDN,kGroupID,kContentID,kUnitID,kUnitScore,kUnitAchieved,KProgress, nil];

    
    __weak LMRequest *request = [self createApiRequest:kRequestUpdateProgress headerKeys:keys headerValues:values];
    
    [self executeApiRequest:request
        withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
        completionBlock(asiHttpRequest,error,responseCode,responseDictionary);
    }];
}


#pragma mark - send diploma

- (void)sendDiplomaForUser:(NSString *)msisdn
                   groupID:(NSString *)groupId
                 contentID:(NSString *)contentId
                   levelId:(NSString *)levelId
                salutation:(NSString *)salutation
                 firstName:(NSString *)firstName
                  lastName:(NSString *)lastName
                     email:(NSString *)email
                    resend:(NSString *)resend
           CompletionBlock:(ApiResultBlock)completionBlock
{

    NSArray *values      = [NSArray arrayWithObjects:msisdn,groupId,contentId,levelId,salutation,firstName,lastName,email,resend,nil];
    NSArray *keys        = [NSArray arrayWithObjects:kUserMSISDN,kGroupID,kContentID,kLevelId,kSalutation,kFirstName,kLastName,kEmail,kResend, nil];

    __weak LMRequest *request = [self createApiRequest:kSendDiploma headerKeys:keys headerValues:values];
   
    [self executeApiRequest:request withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
        completionBlock(asiHttpRequest,error,responseCode,responseDictionary);
    }];
}


#pragma mark - diploma details

- (void)diplomaDetailsForUser:(NSString *)msisdn
                      groupID:(NSString *)groupId
                    contentID:(NSString *)contentId
              CompletionBlock:(DiplomaDetailsResultBlock)completionBlock
{

    NSArray *values      = [NSArray arrayWithObjects:msisdn,groupId,contentId,nil];
    NSArray *keys        = [NSArray arrayWithObjects:kUserMSISDN,kGroupID,kContentID,nil];

    __weak LMRequest *request = [self createApiRequest:kDiplomaDetails headerKeys:keys headerValues:values];
    
    [self executeApiRequest:request withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
        completionBlock(asiHttpRequest,error,responseDictionary);
    }];
}


#pragma mark - user progression

- (void)userProgression:(NSString *)msisdn
              contentId:(NSString *)contentId
             phoneModel:(NSString *)phoneModel
                 osType:(NSString *)osType
              osVersion:(NSString *)osVersion
                levelId:(NSString *)levelId
                groupId:(NSString *)groupId
              lessionId:(NSString *)lessionId
                skipped:(NSString *)skipped
                correct:(NSString *)correct
              inCorrect:(NSString *)inCorrect
        CompletionBlock:(ApiResultBlock)completionBlock
{
    
    NSArray *values = [NSArray arrayWithObjects:msisdn,contentId,groupId,phoneModel,osType,osVersion,levelId,lessionId,skipped,correct,inCorrect,nil];
    NSArray *keys   = [NSArray arrayWithObjects:kUserMSISDN,kContentID,kGroupID,kPhoneModel,kOsType,kOsVersion,kLevelId,kLessonId,kSkipped,kCorrect,kinCorrect,nil];
    
    __weak LMRequest *request = [self createApiRequest:kUserProgression headerKeys:keys headerValues:values];

    [self executeApiRequest:request withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
    {
        completionBlock(asiHttpRequest,error,responseCode,responseDictionary);
    }];
}


#pragma mark - sessionStatisticForUser

- (void)sessionStatisticForUser:(NSString *)msisdn
                        groupId:(NSString *)groupId
                      contentId:(NSString *)contentId
                       deviceId:(NSString *)deviceId
                      startDate:(NSString *)startDate
                        endDate:(NSString *)endDate
                CompletionBlock:(ApiResultBlock)completionBlock
{
  
    NSArray *values = [NSArray arrayWithObjects:msisdn,contentId,groupId,deviceId,startDate,endDate, nil];
    NSArray *keys   = [NSArray arrayWithObjects:kUserMSISDN,kContentID,kGroupID,kDeviceId,kStartDate,kEndDate,nil];
    
    __weak LMRequest *request = [self createApiRequest:kSessionStatistic headerKeys:keys headerValues:values];
    
    [self executeApiRequest:request withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
     {
         completionBlock(asiHttpRequest,error,responseCode,responseDictionary);
     }];
}


#pragma mark - placementTestForUser

- (void)placementTestForUser:(NSString *)msisdn
                   contentId:(NSString *)contentId
                     groupId:(NSString *)groupId
                  phoneModel:(NSString *)phoneModel
                      osType:(NSString *)osType
                   osVersion:(NSString *)osVersion
                   completed:(NSString *)completed
                       grade:(NSString *)grade
            recommendedLevel:(NSString *)recommendedLevel
                    dateTime:(NSString *)datetime
             CompletionBlock:(ApiResultBlock)completionBlock
{
    if(!contentId){
        contentId = @"16";
    }
    if(!groupId){
        groupId = @"16";
    }
    
    NSArray *values = [NSArray arrayWithObjects:msisdn,contentId,groupId,phoneModel,osType,osVersion,completed,grade,recommendedLevel,datetime,nil];
    NSArray *keys   = [NSArray arrayWithObjects:kUserMSISDN,kContentID,kGroupID,kPhoneModel,kOsType,kOsVersion,kCompleted,kGrade,kRecommendedLevel,kDateTime,nil];
    
    __weak LMRequest *request = [self createApiRequest:kPlacementTest headerKeys:keys headerValues:values];
    
    [self executeApiRequest:request withCompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
     {
         completionBlock(asiHttpRequest,error,responseCode,responseDictionary);
     }];

}


#pragma mark - createApiRequest

- (LMRequest *)createApiRequest:(API_REQUEST_TYPE)type
                     headerKeys:(NSArray *)keys
                   headerValues:(NSArray *)values

{
    if(keys && values) {
        NSDictionary *header = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        LMRequest *request   = [self post:type headers:header];
        [request setHeader:header];
    
        return request;
    }
    else return nil;
}


#pragma mark - executeApiRequest

- (void)executeApiRequest:(LMRequest *)request
      withCompletionBlock:(ApiResultBlock)completionBlock
{
    __weak LMRequest *weakRequest = request;
    __weak typeof(self) weakSelf  = self;
    
    if(!request.requestHeaders) {
    
        weakRequest = [self createApiRequest:request.requestType  headerKeys:request.header.allKeys headerValues:request.header.allValues];
    }
    
    if(![self isRequestSaved:request])
        [self saveRequest:request];
    
    [weakRequest setCompletionBlock:^{
        if(!weakRequest.error) {
            
            NSError *error;
            NSMutableDictionary *response = [NSJSONSerialization
                                             JSONObjectWithData:weakRequest.responseData
                                             options:NSJSONReadingMutableContainers
                                             error:&error];
            
            completionBlock(weakRequest,weakRequest.error,weakRequest.responseStatusCode,response);
            
            DLog(@"**************Request with URL:%@ is executed",weakRequest.url);
        }
        
        [weakSelf removeRequest:request];
    }];
    
    [weakRequest setFailedBlock:^{
        completionBlock(weakRequest,weakRequest.error,-1,nil);
        [weakSelf saveRequest:weakRequest];
    }];
    
    if(weakRequest)
        [weakRequest startAsynchronous];

}


#pragma mark - removeRequest

- (void)removeRequest:(LMRequest *)request
{
   
    for(LMRequest *requestItem in [self getRequestList]) {
        
        NSMutableArray *requestArray = [self getRequestList];
        
        if([requestItem.header isEqualToDictionary:request.header]) {
            [requestArray removeObject:requestItem];

            NSData *requestsData = [NSKeyedArchiver archivedDataWithRootObject:requestArray];
            [[NSUserDefaults standardUserDefaults] setObject:requestsData forKey:@"requests"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            DLog(@"**************Request with URL:%@ is deleted",request.url);
        }
    }
}

#pragma mark - removeAllRequests

- (void)removeAllRequests
{
    
    NSMutableArray *requestArray = [[NSMutableArray alloc] initWithCapacity:0];
    requestArray = [NSMutableArray new];
    NSData *requestsData = [NSKeyedArchiver archivedDataWithRootObject:requestArray];
    [[NSUserDefaults standardUserDefaults] setObject:requestsData forKey:@"requests"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - saveRequest

- (void)saveRequest:(LMRequest *)request
{
    NSMutableArray *requestArray = [self getRequestList];

    if(!requestArray) {
        requestArray = [NSMutableArray new];
    }
    
    BOOL isSaved = NO;
    for(LMRequest *oldRequest in requestArray) {
        
        if(oldRequest.header == request.header)
            isSaved = YES;
    }
    
    if(!isSaved)
        [requestArray addObject:request];

    NSData *requestsData = [NSKeyedArchiver archivedDataWithRootObject:requestArray];
    [[NSUserDefaults standardUserDefaults] setObject:requestsData forKey:@"requests"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - getRequestList

- (NSMutableArray *)getRequestList
{
    NSData *progressData = [[NSUserDefaults standardUserDefaults] objectForKey:@"requests"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:progressData];
}


#pragma mark - isRequestSaved

- (BOOL)isRequestSaved:(LMRequest *)request
{
    BOOL status = NO;
    
    NSMutableArray *requestArray = [self getRequestList];
    
    for (LMRequest *requestItem in requestArray) {
        if([requestItem.header isEqualToDictionary:request.header]) {
            status = YES;
        }
    }
    
    return status;
    
}


#pragma mark - request queue

- (void)doNetworkOperations
{

    [[self networkQueue] cancelAllOperations];
    

    [self setNetworkQueue:[ASINetworkQueue queue]];
    [[self networkQueue] setDelegate:self];
    [[self networkQueue] setRequestDidFinishSelector:@selector(requestFinished:)];
    [[self networkQueue] setRequestDidFailSelector:@selector(requestFailed:)];
    [[self networkQueue] setQueueDidFinishSelector:@selector(queueFinished:)];
    
    int i;
    
    NSMutableArray *requestList = [self getRequestList];
    
    for (i = 0; i < requestList.count; i++) {
        LMRequest *request = requestList[i];
        LMRequest *newRequest = [self createApiRequest:request.requestType headerKeys:request.header.allKeys headerValues:request.header.allValues];
        [[self networkQueue] addOperation:newRequest];
    }
    
    [[self networkQueue] go];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [self removeRequest:(LMRequest *)request];
    
    if ([[self networkQueue] requestsCount] == 0) {

        [self setNetworkQueue:nil];
    }

    DLog(@"**************Request finished");
}

- (void)requestFailed:(ASIHTTPRequest *)request
{

    if ([[self networkQueue] requestsCount] == 0) {
        [self setNetworkQueue:nil];
    }

    DLog(@"%@ Request failed",request.url);
}


- (void)queueFinished:(ASINetworkQueue *)queue
{

    if ([[self networkQueue] requestsCount] == 0) {
        [self setNetworkQueue:nil]; 
    }
    
    [self removeAllRequests];
    DLog(@"Queue finished");
}


#pragma mark - checkApiResponseStatus

- (BOOL)checkApiResponseStatus:(NSInteger)status
{
    
  switch (status) {
    case KAApiTokenStatusSuccess:
      return YES;
      break;
      
    case KAApiInvalidToken:
      return NO;
      break;
      
    default:
      return NO;
      break;
  }
  return NO;
}

#pragma mark -

@end
