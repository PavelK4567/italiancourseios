//  LMRequestManager.m
//  Created by Dimitar Tasev on 30.06.14.
//  Copyright (c) 2014 La Mark. All rights reserved.


#import "LMRequestManager.h"
#import <ASIHTTP/ASIDownloadCache.h>


@implementation LMRequestManager


#ifdef REQUEST_SINGLETON
SINGLETON_GCD(LMRequestManager)
#endif


- (ASIHTTPRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSURL *url = [self urlForType:type headers:headers];
	ASIHTTPRequest *request;
	if (url) {
		request = [ASIHTTPRequest requestWithURL:url usingCache:[ASIDownloadCache sharedCache] andCachePolicy:ASIAskServerIfModifiedCachePolicy];
		NSArray *keys = [headers allKeys];
		for (NSString *key in keys) {
			[request addRequestHeader:key value:headers[key]];
			return request;
		}
	}
	return 0;
}

- (NSURL *)urlForType:(ELMRequestType)type headers:(NSDictionary *)headers {
	NSString *path = 0;
	switch (type) {
     
    case kRequestLoginTokenAPICall:
      self.groupID = @"16";
      self.contentID = @"16";
      path = [NSString stringWithFormat:@"%@/sendLoginToken?msisdn=%@&groupID=%@&contentID=%@", kApiBaseUrl, headers[kUserMSISDN],self.groupID,self.contentID];
      break;
      
    case kRequestShowTokenAPICall:
      path = [NSString stringWithFormat:@"http://la-mark-il.com:8081/LMWeb/pictionary/getToken?msisdn=%@&groupID=%@", headers[kUserMSISDN],self.groupID];
      if(STAGING){
      path = [NSString stringWithFormat:@"http://kantoo.com/LMWeb/pictionary/getToken?msisdn=%@&groupID=%@", headers[kUserMSISDN],self.groupID];
      }
      break;
      
    case kRequestConfirmLoginTokenAPICall:
        self.groupID = @"16";
        self.contentID = @"16";
        path = [NSString stringWithFormat:@"%@/confirmLoginToken?msisdn=%@&groupID=%@&contentID=%@&phoneModel=%@&osType=IOS&osVersion=%@&token=%@", kApiBaseUrl, [headers objectForKey:kUserMSISDN],
              self.groupID, self.contentID,
              [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
              [[UIDevice currentDevice] systemVersion],
              [headers objectForKey:kUserCode]];
      break;

    case kRequestGetUserStatusAPICall:
        self.groupID = @"16";
        self.contentID = @"16";
      path =
      [NSString stringWithFormat:@"%@/status?msisdn=%@&groupID=%@&contentID=%@", kApiECBaseUrl, [headers objectForKey:kUserMSISDN],self.groupID,self.contentID];
      break;
      
    case kRequestGetUserNativeStatusAPICall:
            self.groupID = @"16";
            self.contentID = @"16";
      path =
       [NSString stringWithFormat:@"%@/status?msisdn=%@&groupID=%@&contentID=%@", kApiECBaseUrl, [headers objectForKey:kUserMSISDN],self.groupID,self.contentID];
      break;
    case kRequestSubscriptionForNativeUserAPICall:
            self.groupID = @"16";
            self.contentID = @"16";
            path = [NSString stringWithFormat:@"%@/confirmLoginTokenSTO?msisdn=%@&siteID=106&productID=16&groupID=%@&contentID=%@&source=CourseAPP&phoneModel=%@&osType=ios&osVersion=%@",
       kApiBaseUrl,
       [headers objectForKey:kUserMSISDN],
       self.groupID,
       self.contentID,
       [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
       [[UIDevice currentDevice] systemVersion]
       ];

      break;
    case kRequestBillingForNativeUserAPICall:
        self.groupID = @"16";
        self.contentID = @"16";
        path = [NSString stringWithFormat:@"%@/confirmLoginTokenSTO?msisdn=%@&siteID=106&productID=16&groupID=%@&contentID=%@&source=CourseAPP&phoneModel=%@&osType=ios&osVersion=%@",
                kApiBaseUrl,
                [headers objectForKey:kUserMSISDN],
                self.groupID,
                self.contentID,
                [[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"],
                [[UIDevice currentDevice] systemVersion]
                ];

      break;
    case kRequestLogoutUserAPICall:
      path = 0;
      break;
    case kRequestNuanceAPICall:
      
        self.groupID = @"16";
        self.contentID = @"16";
        path = [NSString stringWithFormat:@"%@?uid=%@&msisdn=%@&userid=%@&groupID=%@&contentID=%@&deviceID=%@&words=%@",kApiNuanceUrl,[headers objectForKey:kUserDeviceId],[headers objectForKey:kUserMSISDN],[headers objectForKey:kUserMSISDN],[REQUEST_MANAGER groupID],[REQUEST_MANAGER contentID],[headers objectForKey:kUserDeviceId],[self encodeToPercentEscapeString: [headers objectForKey:kSearchWords]]];      
      break;
    default:
      break;
	}
	if (path) {
		return [NSURL URLWithString:path];
	}
	return 0;
}

- (NSString *)encodeToPercentEscapeString:(NSString *)string {
  return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL,
                                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ", kCFStringEncodingUTF8));
}

@end
