    //
//  LMDownloadManager.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 1/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMDownloadManager.h"
#import "SimplePingHelper.h"

#define kMaxNumberOfLevels 3

#define kLevelOne @"0"
#define kLevelTwo @"1"
#define kLevelThree @"2"

#define LEVELS_ARRAY @[kLevelOne, kLevelTwo, kLevelThree]

@interface LMDownloadManager (Private)

- (NSMutableArray *)findRequestsPer:(NSInteger)sectionIndex unitIndex:(NSInteger)unitIndex;
- (BOOL)compareInstancesArrays:(NSArray *)instancesArray instanceRequests:(NSArray *)instanceRequests;
- (void)compareAndExecuteInstancesArrays:(NSArray *)instancesArray instanceRequests:(NSArray *)instanceRequests;

@end

@implementation LMDownloadManager

+ (LMDownloadManager *)sharedInstance
{
    static LMDownloadManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [LMDownloadManager new];
        }
    });
    return instance;
}

- (void)loadRequests
{
    if (self.requestsData == nil) {
        //self.requestsData = [self loadRequestsData];
    }
}

- (void)createEmptyRequestsStructure
{
    if (self.requestsData == nil) {
        self.requestsData = [NSMutableDictionary dictionaryWithObject:[NSMutableDictionary dictionaryWithCapacity:kMaxNumberOfLevels] forKey:[USER_MANAGER takeContentVersion]];
    }
}

- (void)setCurrentLevelForSync:(NSInteger)currentLevel levelVersion:(NSString *)levelVersion
{
    self.currentLevel = currentLevel;
    self.levelVersion = levelVersion;
    
    DLog(@"CURRENT LEVEL SET:%ld",(long)self.currentLevel);
    [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"> CURRENT LEVEL SET:%ld\n", (long)self.currentLevel]];
}

- (BOOL)checkLevelDataForLevelIndex:(NSInteger)levelIndex
{
    BOOL levelDataExist = NO;
    
    if (self.requestsData[[USER_MANAGER takeContentVersion]][levelIndex]) {
        levelDataExist = YES;
    }
    
    return levelDataExist;
}

- (BOOL)checkExistingRequestData:(NSInteger)levelOrder
{
    return (self.requestsData && self.requestsData[[USER_MANAGER takeContentVersion]] && self.requestsData[[USER_MANAGER takeContentVersion]][[NSString stringWithFormat:@"%ld",(long)levelOrder]]);
}


- (void)createRequestForInstanceFor:(NSString *)contentVersion
                         levelIndex:(NSInteger)levelIndex
                       sectionIndex:(NSInteger)sectionIndex
                          unit:(Unit *)unit
                            orderID:(NSInteger)orderID
                              pages:(NSInteger)pages
                         instanceID:(NSInteger)instanceID
                             synced:(BOOL)synced
{
    
    if(!self.requestQueue) {
        self.requestQueue = [NSMutableArray new];
    }
    
    InstanceRequest *instanceReq = [InstanceRequest new];
    instanceReq.requestType = REQUEST_INSTANCE;
    instanceReq.instanceID = instanceID;
    instanceReq.instanceOrder = orderID;
    instanceReq.levelIndex = levelIndex;
    instanceReq.sectionIndex = sectionIndex;
    instanceReq.unit = unit;
    instanceReq.isSynced = synced;
    //instanceReq.unitIndex = unitIndex;
    
    instanceReq.levelOrder = (levelIndex + 1);
    instanceReq.levelVersion = self.levelVersion;
    instanceReq.pages = pages;
    instanceReq.delegate = self;
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[NSString stringWithFormat:@"%@",contentVersion]];
    if(!instancesReqDict){
        [self createEmptyRequestsStructure];
        instancesReqDict = self.requestsData[[NSString stringWithFormat:@"%@",contentVersion]];
    }
    NSString *levelsString = [NSString stringWithFormat:@"%ld",(long)levelIndex];
    
    NSMutableArray *instancesArray = instancesReqDict[levelsString];
    
    if (!instancesArray) {
        
        instancesArray = [NSMutableArray arrayWithCapacity:3];
        instancesReqDict[levelsString] = instancesArray;
        
    }
    
    [instancesArray addObject:instanceReq];
    
    instanceReq.nextIndex = [instancesArray count];
    
    [self.requestQueue addObject:instanceReq];
}

- (void)executeNextRequest
{
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    NSString *levelsString = [NSString stringWithFormat:@"%ld",(long)self.currentLevel];
    
    NSMutableArray *instancesArray = instancesReqDict[levelsString];
    
    BOOL isExecutingInstance = NO;
    
    for (int i = 0; i < [self.requestQueue count]; i++) {
        
        LMBaseRequest *requestForExecution = self.requestQueue[i];
        
        if (![requestForExecution isRequestFinished]) {
            
            NSString *logString = [NSString stringWithFormat:@"INSTANCE INDEX:%d IS CALLED FOR EXECUTION FOR INSTANCE ID:%ld", i, (long)[((InstanceRequest *)requestForExecution) instanceID]];
            
            DLog(logString);
            [[Utilities sharedInstance] appendLogFileWithString:logString];
            
            if (requestForExecution.numberOfTries > 0) {
                requestForExecution.numberOfTries = 0;
            }
            
            requestForExecution.numberOfTries++;
            [requestForExecution executeRequest];
            
            isExecutingInstance = YES;
            break;
        }
        
    }
    
    if (!isExecutingInstance) {
        [self cancelRequestsExecution];
    }
    
}

- (BOOL)checkCompletedUnit:(NSArray *)downloadedInstances sectionIndex:(NSInteger)sectionIndex unitIndex:(NSInteger)unitIndex
{
    BOOL isUnitCompleted = NO;
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    NSString *levelsString = [NSString stringWithFormat:@"%ld",(long)self.currentLevel];
    
    NSMutableArray *instancesArray = instancesReqDict[levelsString];
    
    NSArray *requestsPerUnit = [self findRequestsPer:sectionIndex unitIndex:unitIndex];
    
    DLog(@"UNIT COMPLETE TEST: Instnaces Array:%ld Current Index:%ld Next Index:%ld", (unsigned long)[instancesArray count], self.iteratorIndex, self.iteratorIndex + 1);
    [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"> UNIT COMPLETE TEST: Instnaces Array:%ld Current Index:%ld Next Index:%ld\n", (unsigned long)[instancesArray count], self.iteratorIndex, self.iteratorIndex + 1]];
    
    NSInteger numberOfDownloadedInstances = [downloadedInstances count];
    NSInteger numberOfInstanceRequests = [requestsPerUnit count];
    
    if (numberOfDownloadedInstances == numberOfInstanceRequests && [self compareInstancesArrays:downloadedInstances instanceRequests:requestsPerUnit]) {
        //NSLog(@"UNIT COMPLETED WITH INDEX: %ld", unitIndex);
        isUnitCompleted = YES;
        
        //[self compareAndExecuteInstancesArrays:downloadedInstances instanceRequests:requestsPerUnit];
        
    }
    
    return isUnitCompleted;
}

- (BOOL)compareInstancesArrays:(NSArray *)instancesArray instanceRequests:(NSArray *)instanceRequests
{
    BOOL instancesAreSame = NO;
    
    NSInteger numberOfSameInstances = 0;
    
    for (int i = 0; i < [instancesArray count]; i++) {
        
        BaseInstance *downloadInstance = instancesArray[i];
        
        for (int j = 0; j < [instanceRequests count]; j++) {
            
            InstanceRequest *instanceReq = instanceRequests[j];
            
            if (downloadInstance.mID == instanceReq.instanceID && instanceReq.isRequestFinished) {
                numberOfSameInstances++;
                break;
            }
            
        }
        
    }
    
    if (numberOfSameInstances == [instanceRequests count]) {
        instancesAreSame = YES;
    }
    
    //NSLog(@"NUMBER OF SAME INSTANCES IS:%ld FROM %ld", numberOfSameInstances, [instanceRequests count]);
    
    return instancesAreSame;
}

- (void)compareAndExecuteInstancesArrays:(NSArray *)instancesArray instanceRequests:(NSArray *)instanceRequests
{
    
    //NSLog(@">>compareAndExecuteInstancesArrays");
    
    BOOL instancesAreSame = NO;
    
    NSInteger numberOfSameInstances = 0;
    NSMutableArray *notExecutedArray = [NSMutableArray arrayWithCapacity:2];
    
    for (int i = 0; i < [instanceRequests count]; i++) {
        
        InstanceRequest *instanceReq = instanceRequests[i];
        
        for (int j = 0; j < [instancesArray count]; j++) {
            
            BaseInstance *downloadInstance = instancesArray[j];
            
            if (instanceReq.instanceID == downloadInstance.mID) {
                instancesAreSame = YES;
                break;
            }
            
        }
        
        if (!instancesAreSame) {
            [notExecutedArray addObject:instanceReq];
            //[instanceReq executeRequest];
        }
     
        instancesAreSame = NO;
        
    }
    
    if ([notExecutedArray count] > 0) {
        
        DLog(@">>MISSING INSTANCES WILL BE EXECUTED");
        
        for (InstanceRequest *instanceReq in notExecutedArray) {
            [instanceReq executeRequest];
        }
    }
    
}

- (void)saveRequestsData:(NSDictionary *)requestsData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"reqdata"];
    
    [NSKeyedArchiver archiveRootObject:requestsData toFile:cachedFilePath];
}

- (NSDictionary *)loadRequestsData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"reqdata"];
    
    return [NSKeyedUnarchiver unarchiveObjectWithFile:cachedFilePath];
}

- (BOOL)checkDownloadProgress
{
    NSInteger levelsCompleted = 0;
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    for (NSString *levelIndex in LEVELS_ARRAY) {
        
        NSMutableArray *instancesArray = instancesReqDict[levelIndex];
        
        NSInteger indexCounter = 0;
        
        for (LMBaseRequest *executedReq in instancesArray) {
            
            if ([executedReq isRequestFinished]) {
                indexCounter++;
            }
            
        }
        
        if (indexCounter == [instancesArray count] && (instancesArray != nil && [instancesArray count] > 0)) {
            levelsCompleted++;
        }
        
    }
    
    return (levelsCompleted == [LEVELS_ARRAY count]);
    
}


- (BOOL)checkInstanceExecutionEnded:(NSInteger)levelID
{
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    NSMutableArray *instancesArray = instancesReqDict[LEVELS_ARRAY[levelID]];

    NSInteger indexCounter = 0;
    
    for (LMBaseRequest *executedReq in instancesArray) {
        
        if ([executedReq isRequestFinished] && ![executedReq isRequestRunning]) {
            indexCounter++;
        }
        
    }
    
    return (indexCounter == [instancesArray count] );
}

- (BOOL)isManagerExecutingRequests
{
    return self.isExecuting;
}

- (void)startRequestsExecution
{
    self.isExecuting = YES;
    
    
    [self configureReachability];

}

- (void)cancelRequestsExecution
{
    self.isExecuting = NO;
}

- (void)requestDataCleanUp
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *cachedFilePath = [documentsDirectory stringByAppendingPathComponent:@"reqdata"];
    
    self.iteratorIndex = -1;
    
    [[Utilities sharedInstance] removeFileFromPath:cachedFilePath];
}

- (void)listAllRequestsWithProp
{
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];

    NSMutableArray *instancesArray = instancesReqDict[LEVELS_ARRAY[self.currentLevel]];
    
    for (InstanceRequest *instanceReq in instancesArray) {
        [instanceReq printDataAsString];
    }
}

- (NSMutableArray *)findRequestsPer:(NSInteger)sectionIndex unitIndex:(NSInteger)unitIndex
{
    NSMutableArray *requests = nil;
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    NSMutableArray *instancesArray = instancesReqDict[LEVELS_ARRAY[self.currentLevel]];
    
    for (int i = 0; i < [instancesArray count]; i++) {
        
        InstanceRequest *instanceReq = instancesArray[i];
        
        if (instanceReq.sectionIndex == sectionIndex && instanceReq.unitIndex == unitIndex) {
            if (requests == nil) {
                requests = [NSMutableArray new];
            }
            
            [requests addObject:instanceReq];
        }
        
    }
    
    return requests;
}

- (void)executedNotExecutedInstances:(NSArray *)sections
{
    
    NSMutableDictionary *instancesReqDict = self.requestsData[[USER_MANAGER takeContentVersion]];
    
    NSMutableArray *instancesArray = instancesReqDict[LEVELS_ARRAY[self.currentLevel]];
    
    for (int i = 0; i < [sections count]; i++) {
        
        Section *section = sections[i];
        
        NSArray *unitsFromSection = [section unitsArray];
        
        for (int j = 0; j < [unitsFromSection count]; j++) {
            
            Unit *unit = unitsFromSection[j];
            
            if ([unit.instancesArray count] < unit.numberOfInstances) {
                NSArray *requestsPerUnit = [self findRequestsPer:i unitIndex:j];
                [self compareAndExecuteInstancesArrays:unit.instancesArray instanceRequests:requestsPerUnit];
            }
            
        }
        
    }
    
    
    
    
    
    
}

- (void)executeLevelDataRequestForLevelID:(NSInteger)levelOrder
{
    [self createEmptyRequestsStructure];
    
    LevelDataRequest *levelDataReq = [LevelDataRequest new];
    levelDataReq.requestType = REQUEST_LEVEL_DATA;
    levelDataReq.levelOrder = (levelOrder + 1);
    levelDataReq.levelVersion = self.levelVersion;
    levelDataReq.delegate = self;
    
    [levelDataReq executeRequest];
}

- (void)executeLevelRequestForLevelID:(NSInteger)levelOrder
{
    LevelRequest *levelReq = [LevelRequest new];
    levelReq.requestType = REQUEST_LEVEL;
    levelReq.levelOrder = (levelOrder + 1);
    levelReq.levelVersion = self.levelVersion;
    levelReq.delegate = self;
    
    [levelReq executeRequest];
}

- (void)executeCourseInfoRequest
{
    CourseInfoRequest *courseInfoReq = [CourseInfoRequest new];
    courseInfoReq.requestType = REQUEST_COURSE_INFO;
    courseInfoReq.levelVersion = [USER_MANAGER takeContentVersion];
    courseInfoReq.delegate = self;
    
    [courseInfoReq executeRequest];
}

- (void)executeCourseRequest
{
    CourseRequest *courseReq = [CourseRequest new];
    courseReq.requestType = REQUEST_COURSE;
    courseReq.levelVersion = [USER_MANAGER takeContentVersion];
    courseReq.delegate = self;
    
    [courseReq executeRequest];
}

- (void)executeCharactersRequest
{
    CharactersRequest *charactersReq = [CharactersRequest new];
    charactersReq.requestType = REQUEST_CHARACTERS;
    charactersReq.levelVersion = [USER_MANAGER takeContentVersion];
    charactersReq.delegate = self;
    
    [charactersReq executeRequest];
}

#pragma mark - configureReachability

- (void)configureReachability
{
    if (!self.isObserverRegistered) {
    
        Reachability *reach = [Reachability reachabilityWithHostName:@"http://google.com"];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reachabilityChanged:)
                                                     name:kReachabilityChangedNotification
                                                   object:nil];
        [reach startNotifier];
    
        self.isObserverRegistered = YES;
        
    }
}

#pragma mark - reachabilityChanged

- (void)reachabilityChanged:(NSNotification *)notification
{
    Reachability *localReachability = [notification object];
    
    if ([localReachability isReachable] && !self.isExecuting) {
        [self startRequestsExecution];
        [self executeNextRequest];
    }

}

- (void)requestDidFinishWithExecution:(LMBaseRequest *)request withError:(BOOL)withError errorMessage:(NSString *)errorMessage
{
    //request.isRequestFinished = (!withError) ? YES : NO;
    if (withError) {
        DLog(@"REEXECUTING REQUEST: %@", [request generateURL]);
        [[Utilities sharedInstance] appendLogFileWithString:[NSString stringWithFormat:@"> REEXECUTING REQUEST: %@\n", [request generateURL]]];
        
        if (request.numberOfTries < kNumberOFTries) {
            request.numberOfTries++;
            [request executeRequest];
        } else {
            [self cancelRequestsExecution];
            [SimplePingHelper ping:@"216.58.209.164" target:self sel:@selector(pingResult:)];

          
        }
        
    } else {
        
        switch (request.requestType) {
            case REQUEST_LEVEL:
                request.isRequestRunning = NO;
                request.isRequestFinished = YES;
                [self.delegate levelRequestDidFinishWithExecution:(((LevelRequest *)request).levelOrder - 1) withError:withError];
                break;
            case REQUEST_LEVEL_DATA:
                request.isRequestRunning = NO;
                request.isRequestFinished = YES;
                [self.delegate levelDataRequestDidFinishWithExecution:(((LevelDataRequest *)request).levelOrder - 1) withError:withError];
                break;
            case REQUEST_INSTANCE:
//                [self.delegate instanceRequestDidFinishWithExecution:(((InstanceRequest *)request).levelOrder - 1)
//                                                        sectionIndex:((InstanceRequest *)request).sectionIndex
//                                                           unitIndex:((InstanceRequest *)request).unitIndex
//                                                       instanceOrder:((InstanceRequest *)request).instanceOrder
//                                                               pages:((InstanceRequest *)request).pages
//                                                           nextIndex:((InstanceRequest *)request).nextIndex
//                                                              synced:((InstanceRequest *)request).isSynced
//                                                           withError:withError];
                [self.delegate instanceRequestDidFinishWithExecution:(((InstanceRequest *)request).levelOrder - 1)
                                                        sectionIndex:((InstanceRequest *)request).sectionIndex
                                                           unitIndex:((InstanceRequest *)request).unitIndex
                                                                unit:((InstanceRequest *)request).unit
                                                       instanceOrder:((InstanceRequest *)request).instanceOrder
                                                               pages:((InstanceRequest *)request).pages
                                                           nextIndex:((InstanceRequest *)request).nextIndex
                                                              synced:((InstanceRequest *)request).isSynced
                                                           withError:withError];

                break;
            case REQUEST_COURSE_INFO:
                request.isRequestRunning = NO;
                request.isRequestFinished = YES;
                [self.delegate courseInfoRequestDidFinishWithExecution:withError];
                break;
            case REQUEST_COURSE:
                request.isRequestRunning = NO;
                request.isRequestFinished = YES;
                [self.delegate courseRequestDidFinishWithExecution:withError];
                break;
            case REQUEST_CHARACTERS:
                request.isRequestRunning = NO;
                request.isRequestFinished = YES;
                [self.delegate charactersRequestDidFinishWithExecution:withError];
                break;
            default:
                break;
        }
        
    }
    
}



#pragma mark SimplePingHelper
- (void)pingResult:(NSNumber*)success {
  /* if (![ReachabilityHelper reachable]) {
   [self.delegate syncDidFinishWithExecution:YES cause:kAlertNoConnection];
   } else {
   [self.delegate syncDidFinishWithExecution:YES cause:kAlertServerUnreachable];
   }*/
  if (success.boolValue) {
    [self.delegate syncDidFinishWithExecution:YES cause:kAlertServerUnreachable];
  } else {
    [self.delegate syncDidFinishWithExecution:YES cause:kAlertNoConnection];
  }
}
@end
