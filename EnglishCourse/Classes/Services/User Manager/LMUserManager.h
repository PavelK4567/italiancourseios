//  LMUserManager.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUser.h"
#import "PurchaseManager.h"


@interface LMUserManager : NSObject


typedef void (^ManagerResultBlock)(NSError *error);
typedef void (^UserStatusResultBlock)(NSNumber *userStatus);
typedef void (^IsLoginResultBlock)(BOOL isUserLogin);


@property (nonatomic) BOOL isUserRegister;

- (BOOL)processAutologin;
- (BOOL)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code;
- (void)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code WithCompletitionBlock:(IsLoginResultBlock)completitionBlock;

- (void)makeLoginTokenWithMSISDN:(NSString *)msisdn WithCompletitionBlock:(UserStatusResultBlock)completitionBlock;


- (BOOL)takeLoginTokenWithMSISDN:(NSString *)msisdn;
- (NSNumber *)showMeTokenForMSISDN:(NSString *)msisdn;
- (void)registerNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)loadNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock;
- (void)buyPurchases;
- (void)logOutUser;

-(void) loginNoCodeWithMSISDN: (NSString *) msisdn;
- (void)makeBillingForNativeUserWithCompletitionBlock:(ManagerResultBlock)completitionBlock;

- (NSString *)isNextBillingDateDay;
- (BOOL)isAuthenticated;
- (BOOL)isGuest;
- (BOOL)isActive;
- (BOOL)isInitialized;
- (BOOL)isNative;
- (BOOL)chekUserStatus:(int)currentUserStatus;

- (BOOL)canOpenLesson;
- (void)UserStatus;
- (KAUserStatus)getUserStatus;
- (NSString *)userMsisdn;
- (NSString *)userPhone;

- (void)initializeWithGuest;
- (void)initializeWithUser;
- (void)initializeWithUser:(LMUser *)user;
- (void)initializeWithNativeUser;

- (void)setGuestStatus;

- (NSString *)statusDescription:(KAUserStatus)status;
- (NSString *)loginConfigPath:(LMUser *)user;
- (NSString *)defaultPrefix;
- (NSString *)defaultMSISDN;
- (NSString *)userMsisdnString;
- (NSString *)userPhoneWithAndroidFormat;

- (NSInteger)numberOfCredits;
- (void)setNumberOfCredits:(NSInteger)tempCredits;

- (void)takeCredit;

- (NSString *)deviceIdentifier;
- (NSString *)takeStringFromDate;
- (NSString *)takeNativeDateStringFromDate;
- (NSString*)nextBillingDateString;
- (NSString *)takeNativeDateStringForSettings;

-(void)setLastLoginTime;
-(BOOL)loginBefore3Days;
-(NSInteger)userWeeklyUsage;

-(NSArray *)takeLevelData;
-(NSNumber *)takeCurrentLevel;
-(NSString *)takeContentVersion;

@end


#ifdef USER_SINGLETON

@interface LMUserManager ()
+ (id)sharedInstance;
@end

#define USER_MANAGER ((LMUserManager *)[LMUserManager sharedInstance])
#endif
