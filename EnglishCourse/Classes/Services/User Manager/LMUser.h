//  LMUser.h
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


@interface LMUser : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *msisdn;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *countryCode;
@property (strong, nonatomic) NSString *areaCode;
@property (strong, nonatomic) NSDate *billingDate;
@property (assign, nonatomic) KAUserStatus status;

@property (assign, nonatomic) NSInteger numberOfCredits;
@property (assign, nonatomic) NSInteger isNative;

@property (assign, nonatomic) NSInteger totalCount;
@property (assign, nonatomic) NSInteger weeklyCount;
@property (assign, nonatomic) NSInteger availableCount;
@property (strong, nonatomic) NSDate *availableStartDate;
@property (strong, nonatomic) NSDate *availableEndDate;

@property (strong, nonatomic) NSArray *levelData;
@property (strong, nonatomic) NSString *contentVersion;
@property (assign, nonatomic) NSNumber *currentLevel;

- (id)initWithMSISDN:(NSString *)msisdn;

- (id)initWithContentsOfFile:(NSString *)filePath;

- (void)synchronizeToFile;
- (void)synchronizeNativeUserToFile;

- (id)initNativeUser:(NSString *)msisdn;

- (void)saveStatusData;
- (void)loadStatusData;

@end
