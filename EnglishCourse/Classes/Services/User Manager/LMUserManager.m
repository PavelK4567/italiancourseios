//  LMUserManager.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMUserManager.h"
//#import "RageKAInAppPurchasesHelper.h"

@interface LMUserManager () {
  BOOL isLoggedInAutomatically;
  BOOL isLoggedIn;
  BOOL isMSISDNUser;
  NSString *tokenString;
  NSNumber *userStatus;
  NSNumber *userToken;
}

@property(strong, nonatomic) LMUser *user;

- (BOOL)lastLoginUserType;
- (NSString *)lastLoginConfigPath;

@end


@implementation LMUserManager


#ifdef USER_SINGLETON
SINGLETON_GCD(LMUserManager)
#endif


- (id)init
{
  self = [super init];
  if (self) {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kUserManagerInitialized]) {
      if ([self lastLoginUserType]) {
        [self initializeWithUser];
      } else {
        NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
        if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
          [self initializeWithNativeUser];
        }else{
          [self initializeWithGuest];
        }
      }
    } else {
    }
  }
  return self;
}
- (void)logOutUser
{
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDNPath];
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidMSISDN];
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:kLastValidUniqueID];
  [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kLastValidLoginUser];
  if(![self isNative]){
  [[NSUserDefaults standardUserDefaults] setObject:@NO forKey:kUserManagerInitialized];
  }
  [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)initializeWithNativeUser
{
  self.user = nil;
  self.user = [[LMUser alloc] initNativeUser:[SDCloudUserDefaults stringForKey:kMonthlyPurchaseId]];
  [[NSUserDefaults standardUserDefaults] setObject:[SDCloudUserDefaults stringForKey:kMonthlyPurchaseId] forKey:kUserMSISDN];
  [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserCode];
  [[NSUserDefaults standardUserDefaults] synchronize];
  [self.user synchronizeNativeUserToFile];
    
}

- (void)initializeWithGuest
{
  self.user = [[LMUser alloc] initWithMSISDN:[[self defaultPrefix] stringByAppendingString:kUserManagerDefault]];
  [[NSUserDefaults standardUserDefaults] setObject:self.user.msisdn forKey:kUserMSISDN];
  [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserCode];
  [[NSUserDefaults standardUserDefaults] synchronize];
  [self.user synchronizeToFile];
}

- (void)initializeWithUser
{
  DLog(@" objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  //NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
  
    [self initializeWithUser:[[LMUser alloc] initWithMSISDN:[[NSUserDefaults standardUserDefaults] objectForKey:kLastValidMSISDN]]];
    [[NSUserDefaults standardUserDefaults] setObject:self.user.msisdn forKey:kUserMSISDN];
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kUserCode];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)initializeWithUser:(LMUser *)user
{
  self.user = user;
  [self.user synchronizeToFile];
}
- (void)setGuestStatus
{
  self.user.status = KAUserGuest;
}
- (BOOL)processAutologin
{
   /* NSLog(@"00/2/2 %@",[self getDateObjectFromString:@"00/2/2"]);
    NSLog(@"90/2/2 %@",[self getDateObjectFromString:@"90/2/2"]);
    NSLog(@"80/2/2 %@",[self getDateObjectFromString:@"80/2/2"]);
    NSLog(@"70/2/2 %@",[self getDateObjectFromString:@"70/2/2"]);
    NSLog(@"60/2/2 %@",[self getDateObjectFromString:@"60/2/2"]);
    NSLog(@"50/2/2 %@",[self getDateObjectFromString:@"50/2/2"]);
    NSLog(@"40/2/2 %@",[self getDateObjectFromString:@"40/2/2"]);
    NSLog(@"30/2/2 %@",[self getDateObjectFromString:@"30/2/2"]);
    NSLog(@"20/2/2 %@",[self getDateObjectFromString:@"20/2/2"]);
   */ 
  if ([self isInitialized]) {
    if([self isNative]){
      NSDictionary *headers = @{ kUserMSISDN : self.user.msisdn, kUserCode : @"", kUserDeviceId : self.user.msisdn};
      if (![ReachabilityHelper reachable]) {
        [UIAlertView alertWithCause:kAlertNoInternetConnection];
        self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
        self.user.numberOfCredits = 0;
        [self.user loadStatusData];
        self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
        if ([self isNextBillingDatePassed]) {
          self.user.status = KAUserStatusCanceled;
        }
        isLoggedInAutomatically = [self chekUserStatus:self.user.status];
        isLoggedIn = YES;
        return YES;
      }
      ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestGetUserNativeStatusAPICall headers:headers];
      __weak ASIHTTPRequest *request = loginRequest;
      [loginRequest setCompletionBlock:^{
        if (request.responseStatusCode == 200 && !request.error) {
          NSError *error = 0;
          NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
          if (responseDictionary && [responseDictionary objectForKey:kApiStatus] && [self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
            self.user.status = (KAUserStatus) [[responseDictionary objectForKey:kApiUserStatus] integerValue];
              if([responseDictionary objectForKey:kApiNextBillingDate])
                  self.user.billingDate = [self getDateObjectFromString:[responseDictionary objectForKey:kApiNextBillingDate]];
            self.user.numberOfCredits = [[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue];
            self.user.levelData = [responseDictionary objectForKey:kApiLevelData];
            self.user.currentLevel = [responseDictionary objectForKey:kApiUserCurrentLevel];
            self.user.contentVersion = [responseDictionary objectForKey:kApiUserContentVersion];
            
            [SDCloudUserDefaults setObject:self.user.billingDate forKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
            /*if ([self isNextBillingDatePassed]) {
              self.user.status = KAUserStatusCanceled;
            }*/
            [SDCloudUserDefaults synchronize];
            [self.user synchronizeNativeUserToFile];
            [self.user saveStatusData];
            isLoggedInAutomatically = [self chekUserStatus:self.user.status];
            isLoggedIn = YES;
            return;
          }
        }else{
          [UIAlertView alertWithCause:kAlertNoInternetConnection];
          self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
          self.user.numberOfCredits = 0;
          [self.user loadStatusData];
          self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
          isLoggedInAutomatically = YES;
          isLoggedIn = YES;
          return;
          
        }
        isLoggedInAutomatically = NO;
        isLoggedIn = NO;
      }];
      [loginRequest setFailedBlock:^{
        //isLoggedInAutomatically = NO;
        self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
        self.user.numberOfCredits = 0;
        [self.user loadStatusData];
        self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
        isLoggedInAutomatically = YES;
        isLoggedIn = YES;
        if([ReachabilityHelper reachable]) {
          [UIAlertView alertWithCause:kAlertServerUnreachable];
        }
        else {
          [UIAlertView alertWithCause:kAlertNoConnection];
        }
      }];
      [loginRequest startSynchronous];
      return isLoggedInAutomatically;

    }else{
      NSDictionary *headers =
      @{ kUserMSISDN : self.user.msisdn };
      DLog(@"[ReachabilityHelper reachable] %@", [ReachabilityHelper reachable] ? @"YES" : @"NO");
      if (![ReachabilityHelper reachable]) {
        [UIAlertView alertWithCause:kAlertNoInternetConnection];
        self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
        self.user.numberOfCredits = 0;
        [self.user loadStatusData];
        self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
        isLoggedInAutomatically = YES;
        isLoggedIn = YES;
        return YES;
      }
      ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestGetUserStatusAPICall headers:headers];
      __weak ASIHTTPRequest *request = loginRequest;
      [loginRequest setCompletionBlock:^{
        if (request.responseStatusCode == 200 && !request.error) {
          NSError *error = 0;
          NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
          if (responseDictionary && [responseDictionary objectForKey:kApiStatus] && [self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
            userStatus = [responseDictionary objectForKey:kApiUserStatus];
              if([responseDictionary objectForKey:kApiNextBillingDate])
                  self.user.billingDate = [self getDateObjectFromString:[responseDictionary objectForKey:kApiNextBillingDate]];
            self.user.numberOfCredits = [[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue];
            self.user.levelData = [responseDictionary objectForKey:kApiLevelData];
            self.user.currentLevel = [responseDictionary objectForKey:kApiUserCurrentLevel];
            self.user.contentVersion = [responseDictionary objectForKey:kApiUserContentVersion];
            [self.user saveStatusData];
              
            self.user.status = (KAUserStatus)[userStatus intValue];
            isLoggedInAutomatically = [self chekUserStatus:[userStatus intValue]];
            isLoggedIn = YES;
            return;
          }
        }else{
            [UIAlertView alertWithCause:kAlertNoInternetConnection];
            self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
            self.user.numberOfCredits = 0;
            self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
            [self.user loadStatusData];
            isLoggedInAutomatically = YES;
            isLoggedIn = YES;
            return;

        }
        
        isLoggedInAutomatically = NO;
        isLoggedIn = NO;
      }];
      [loginRequest setFailedBlock:^{
        //isLoggedInAutomatically = NO;
        self.user.billingDate = [[NSUserDefaults standardUserDefaults] objectForKey:kLastValidBillingDate];
        self.user.numberOfCredits = 0;
        [self.user loadStatusData];
        self.user.status = (KAUserStatus)[[NSUserDefaults standardUserDefaults] integerForKey : kLastValidUserStatus];
        isLoggedInAutomatically = YES;
        isLoggedIn = YES;
        if([ReachabilityHelper reachable]) {
          [UIAlertView alertWithCause:kAlertServerUnreachable];
        }
        else {
          [UIAlertView alertWithCause:kAlertNoConnection];
        }
      }];
      [loginRequest startSynchronous];
      return isLoggedInAutomatically;
    }
  }
  return NO;
}
    
-(void) loginNoCodeWithMSISDN: (NSString *) msisdn {
    [self chekLastLoginUser:msisdn];
    self.user = [[LMUser alloc] initWithMSISDN:msisdn];
    [self initializeWithUser:self.user];
    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
    [self.user synchronizeToFile];
    isLoggedIn = YES;
}

- (BOOL)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code
{
  NSDictionary *headers = @{ kUserMSISDN : msisdn, kUserCode : code };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestConfirmLoginTokenAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
        [self chekLastLoginUser:msisdn];
        self.user = [[LMUser alloc] initWithMSISDN:msisdn];
        [self initializeWithUser:self.user];
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
        [self.user synchronizeToFile];
        isLoggedIn = YES;
        return;
      } else {
        [UIAlertView alertWithCause:kAlertCodeNotMatchMSISDN];
      }
    }
    isLoggedIn = NO;
  }];
  [loginRequest setFailedBlock:^{ isLoggedIn = NO; }];
  [loginRequest startSynchronous];
  return isLoggedIn;
}
//
- (void)loginWithMSISDN:(NSString *)msisdn code:(NSString *)code WithCompletitionBlock:(IsLoginResultBlock)completitionBlock
{
  NSDictionary *headers = @{ kUserMSISDN : msisdn, kUserCode : code };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestConfirmLoginTokenAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
        [self chekLastLoginUser:msisdn];
        self.user = [[LMUser alloc] initWithMSISDN:msisdn];
        [self initializeWithUser:self.user];
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
        [self.user synchronizeToFile];
        isLoggedIn = YES;
        completitionBlock(isLoggedIn);
        return;
      }
      else
      {
        [UIAlertView alertWithCause:kAlertCodeNotMatchMSISDN];
        completitionBlock(false);
      }
    }
    else completitionBlock(false);
  }];
  [loginRequest setFailedBlock:^{completitionBlock(false);}];
  [loginRequest startAsynchronous];
}
// system send token with sms
- (BOOL)takeLoginTokenWithMSISDN:(NSString *)msisdn
{
  NSDictionary *headers = @{ kUserMSISDN : msisdn, kUserDeviceId : [self deviceIdentifier] };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestLoginTokenAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      // tokenString = request.responseData;
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]])
        userStatus = [responseDictionary objectForKey:kApiUserStatus];
      return;
    }
  }];
  [loginRequest setFailedBlock:^{ isLoggedInAutomatically = NO; }];
  [loginRequest startSynchronous];
  return [self chekUserStatus:[userStatus intValue]];
}

- (void)makeLoginTokenWithMSISDN:(NSString *)msisdn WithCompletitionBlock:(UserStatusResultBlock)completitionBlock
{
  NSDictionary *headers = @{ kUserMSISDN : msisdn, kUserDeviceId : [self deviceIdentifier] };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestLoginTokenAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      // tokenString = request.responseData;
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      
      if ([self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]){
        userStatus = [responseDictionary objectForKey:kApiUserStatus];
        
        completitionBlock(userStatus);
        return;
      }
      else completitionBlock(0);
    }
    else completitionBlock(0);
  }];
  [loginRequest setFailedBlock:^{
    completitionBlock(0);
  }];
  [loginRequest startAsynchronous];
  
}

// only for test to see token on LMLoginCodeViewController
- (NSNumber *)showMeTokenForMSISDN:(NSString *)msisdn
{
  NSDictionary *headers = @{ kUserMSISDN : msisdn, kUserDeviceId : [self deviceIdentifier] };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestShowTokenAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      // tokenString = request.responseData;
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]])
        userToken = [responseDictionary objectForKey:kApiToken];
      return;
    }
  }];
  [loginRequest setFailedBlock:^{ isLoggedInAutomatically = NO; }];
  [loginRequest startSynchronous];
  return userToken;
}

// for setup billingDate
//and for login with tocken
- (void)loadUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
  NSDictionary *headers = @{ kUserMSISDN : self.user.msisdn};
  if ([self isGuest]) {
    headers = @{ kUserMSISDN : @"guest" };
  }
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestGetUserStatusAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if (responseDictionary && [responseDictionary objectForKey:kApiStatus] && [self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
        self.user.status = (KAUserStatus)[userStatus intValue];
          if([responseDictionary objectForKey:kApiNextBillingDate])
              self.user.billingDate = [self getDateObjectFromString:[responseDictionary objectForKey:kApiNextBillingDate]];
          
        self.user.numberOfCredits = [[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue];
        self.user.levelData = [responseDictionary objectForKey:kApiLevelData];
        self.user.currentLevel = [responseDictionary objectForKey:kApiUserCurrentLevel];
        self.user.contentVersion = [responseDictionary objectForKey:kApiUserContentVersion];
        [self.user saveStatusData];
        completitionBlock(request.error);
        return;
      }
        else completitionBlock(request.error);
    }
      else completitionBlock(request.error);
  }];
  [loginRequest setFailedBlock:^{completitionBlock(request.error);}];
  [loginRequest startAsynchronous];
}

// for native user registration
- (void)registerNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
  NSDictionary *headers = @{ kUserMSISDN : self.user.msisdn, kUserCode : @"", kUserDeviceId : self.user.msisdn };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestSubscriptionForNativeUserAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([[responseDictionary objectForKey:kApiStatus] intValue]== 0) {
        //[SDCloudUserDefaults setObject:[responseDictionary objectForKey:kApiUserUniqueID] forKey:@"uid"];
        [SDCloudUserDefaults setObject:self.user.msisdn forKey:@"uid"];
        [SDCloudUserDefaults synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
        completitionBlock(request.error);
        return;
      }
      else completitionBlock(request.error);
    }
    else completitionBlock(request.error);
  }];
  [loginRequest setFailedBlock:^{completitionBlock(request.error);}];
  [loginRequest startAsynchronous];
}

// for native user billing
- (void)makeBillingForNativeUserWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
  
  NSDictionary *headers = @{ kUserMSISDN : [SDCloudUserDefaults stringForKey:@"uid"], kUserCode : @"", kUserDeviceId : [SDCloudUserDefaults stringForKey:@"uid"] };
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestBillingForNativeUserAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if ([[responseDictionary objectForKey:kApiStatus] intValue]== 0) {
        //[SDCloudUserDefaults setObject:[responseDictionary objectForKey:kApiUserUniqueID] forKey:@"uid"];
        [SDCloudUserDefaults setObject:self.user.msisdn forKey:@"uid"];
        [SDCloudUserDefaults synchronize];
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
        completitionBlock(request.error);
        return;
      }
      else completitionBlock(request.error);
    }
    else completitionBlock(request.error);
  }];
  [loginRequest setFailedBlock:^{completitionBlock(request.error);}];
  [loginRequest startAsynchronous];
}
// for native user read status
- (void)loadNativeUserValuesWithCompletitionBlock:(ManagerResultBlock)completitionBlock
{
  NSDictionary *headers = @{ kUserMSISDN : self.user.msisdn, kUserCode : @"", kUserDeviceId : self.user.msisdn};
  ASIHTTPRequest *loginRequest = [REQUEST_MANAGER post:kRequestGetUserNativeStatusAPICall headers:headers];
  __weak ASIHTTPRequest *request = loginRequest;
  [loginRequest setCompletionBlock:^{
    if (request.responseStatusCode == 200 && !request.error) {
      NSError *error = 0;
      NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:request.responseData options:0 error:&error];
      if (responseDictionary && [responseDictionary objectForKey:kApiStatus] && [self chekApiStatus:[[responseDictionary objectForKey:kApiStatus] intValue]]) {
        self.user.status = (KAUserStatus) [[responseDictionary objectForKey:kApiUserStatus] integerValue];
          if([responseDictionary objectForKey:kApiNextBillingDate])
              self.user.billingDate = [self getDateObjectFromString:[responseDictionary objectForKey:kApiNextBillingDate]];
        self.user.numberOfCredits = [[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue];
        self.user.levelData = [responseDictionary objectForKey:kApiLevelData];
        self.user.currentLevel = [responseDictionary objectForKey:kApiUserCurrentLevel];
        self.user.contentVersion = [responseDictionary objectForKey:kApiUserContentVersion];
        
        [SDCloudUserDefaults setObject:self.user.billingDate forKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
        /*if ([self isNextBillingDatePassed]) {
          self.user.status = KAUserStatusCanceled;
        }*/
        [SDCloudUserDefaults synchronize];
        [self.user synchronizeNativeUserToFile];
        [self.user saveStatusData];
        completitionBlock(request.error);
        return;
      }
      else completitionBlock(request.error);
    }
    else completitionBlock(request.error);
  }];
  [loginRequest setFailedBlock:^{completitionBlock(request.error);}];
  [loginRequest startAsynchronous];
}
- (void)buyPurchases
{

}

- (BOOL)chekApiStatus:(int)currentStatus
{
  switch (currentStatus) {
  case KAApiTokenStatusSuccess:
    return YES;
    break;

  case KAApiInvalidToken:
    return NO;
    break;

  default:
    return NO;
    break;
  }
  
  return NO;
}

- (BOOL)AuthenticatedUserStatus:(int)currentUserStatus
{
  switch (currentUserStatus) {
  case KAUserStatusNotRegistered:
    return NO;
    break;

  case KAUserStatusActive:
    return YES;
    break;

  case KAUserStatusDisabled:
    return YES;
    break;

  case KAUserStatusCanceled:
    return NO;
    break;

  case KAUserStatusToBeCanceled:
    return YES;
    break;

  default:
    return NO;
    break;
  }
}

- (BOOL)chekUserStatus:(int)currentUserStatus
{
  // DLog(@"USER STATUS:%@ ", [self statusDescription:self.user.status]);
  switch (currentUserStatus) {
  case KAUserStatusNotRegistered:
    //[UIAlertView alertWithCause:kAlertNotRegisteredUser];
    return NO;
    break;

  case KAUserStatusActive:
    return YES;
    break;

  case KAUserStatusDisabled:
    [UIAlertView alertWithCause:kAlertDisabledUser];
    return NO;
    break;

  case KAUserStatusCanceled:
      if ([USER_MANAGER isNative]) {
        //[UIAlertView alertWithCause:kAlertCanceledNativeUser];
      }else{
        [UIAlertView alertWithCause:kAlertCanceledUser];
      }
      return NO;
      break;

  case KAUserStatusToBeCanceled:
    return YES;
    break;

  default:
    return NO;
    break;
  }
}

- (void)UserStatus
{
  DLog(@"USER STATUS:%@ ", [self statusDescription:self.user.status]);
}

- (KAUserStatus)getUserStatus
{
  return self.user.status;
}

- (NSString *)userMsisdn
{
  return self.user.msisdn;
}
- (NSString *)userMsisdnString
{
  if([self isGuest]){
    return @"GUEST";
  }
  return self.user.msisdn;
}
- (NSString *)userPhone
{
  return [self.user.msisdn substringFromIndex:2];
}

- (NSString *)userPhoneWithAndroidFormat
{
    NSRange firstNumRange = NSMakeRange (2, 2);
    NSString *firstNumbers = [self.user.msisdn substringWithRange:firstNumRange];
    NSString *secondNumbers = [self.user.msisdn substringFromIndex:4];
    NSString *finalNumbers = [NSString stringWithFormat:@"%@-%@",firstNumbers,secondNumbers];
    return finalNumbers;
}

- (BOOL)isAuthenticated
{
  return [self AuthenticatedUserStatus:self.user.status];
  //return [self AuthenticatedUserStatus:self.user.status] && (![self isNextBillingDatePassed]);
}

- (BOOL)isActive{
  return (self.user.status == KAUserStatusActive) ? YES : NO;
}
- (BOOL)isGuest
{
  return (self.user.status == KAUserGuest) ? YES : NO;
}
- (BOOL)isNative
{
  return self.user.isNative;
}
- (BOOL)isInitialized
{
  return [[[NSUserDefaults standardUserDefaults] objectForKey:kUserManagerInitialized] boolValue];
}

- (BOOL)isNextBillingDatePassed
{
  if (!self.user.billingDate)
    return NO;
  return [[NSDate date] timeIntervalSinceDate:self.user.billingDate] > 0;
}

- (NSString*)isNextBillingDateDay
{
  if (!self.user.billingDate)
    return @"";
  //60*60*24 = 86400
  return [NSString stringWithFormat:@"%.0f",-1*  [[NSDate date] timeIntervalSinceDate:self.user.billingDate]/86400];
}
- (NSString*)nextBillingDateString
{
  if (!self.user.billingDate)
    return @"";
  //60*60*24 = 86400
   return [NSString stringWithFormat:@"%.0f",1+(-1*  [[NSDate date] timeIntervalSinceDate:self.user.billingDate]/86400)];
}
#pragma mark - User credits


- (BOOL)canOpenLesson
{
  //if([self isGuest])
  //  return YES;
  
  return (self.user.numberOfCredits > 0);
}

- (void)setNumberOfCredits:(NSInteger)tempCredits
{
  self.user.numberOfCredits = tempCredits;
}

- (NSInteger)numberOfCredits
{
  return self.user.numberOfCredits;
  // for camFind
}



- (void)takeCredit
{
  self.user.numberOfCredits--;
}



- (NSString *)takeNativeDateStringForSettings{
  NSDate *date = [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
  int daysToAdd = -1;
  NSDate *newDate1 = [date dateByAddingTimeInterval:60*60*24*daysToAdd];
  NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
  [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
  return [dateFormatter2 stringFromDate:newDate1];
}

- (NSString *)takeNativeDateStringFromDate{
  NSDate *date = [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
  NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
  [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
  return [dateFormatter2 stringFromDate:date];
}
- (NSString *)takeStringFromDate {
  NSDate *date = self.user.billingDate;
  NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc] init];
  [dateFormatter2 setDateFormat:@"dd/MM/yy"]; // 2012-12-11T13:26:40Z
  return [dateFormatter2 stringFromDate:date];
}

#pragma mark - chek Last Login User
- (void)chekLastLoginUser:(NSString *)tempUserMSISDN
{
  if (self.user && [self.user.msisdn isEqualToString:tempUserMSISDN]) {
    DLog(@"Old User");
  } else {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kLastValidMSISDN]) {
      DLog(@"NEW User");
      [self resetUserData];
    }
  }
}

- (void)resetUserData
{
}

#pragma mark - Helpers

- (NSString *)statusDescription:(KAUserStatus)status
{
  switch (status) {
  case KAUserGuest:
    return Localized(kUserStatusGuestString);
    break;

  case KAUserStatusNotRegistered:
    return Localized(kUserStatusNotRegistered);
    break;

  case KAUserStatusActive:
    return Localized(kUserStatusActiveString);
    break;

  case KAUserStatusToBeCanceled:
    return Localized(kUserStatusToBeDeactivatedString);
    break;

  case KAUserStatusCanceled:
    return Localized(kUserStatusDeactivatedString);
    break;

  case KAUserStatusDisabled:
    return Localized(kUserStatusDisabledString);
    break;
  }
  return @"";
}

- (id)getDateObjectFromString:(NSString *)dateString
{
    if(dateString==(id) [NSNull null] || [dateString length]==0 || [dateString isEqualToString:@""])
    {
        return nil;
    }
    if([dateString isEqual: [NSNull null]]){
        return nil;
    }
    NSString *newDate = [NSString stringWithFormat:@"20%@",dateString];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    //[dateFormatter setDateFormat:SERVER_DATETIME_FORMAT];
    return [dateFormatter dateFromString:newDate];
}

#pragma mark - In app
-(void)makeInApp{
}
#pragma mark - Private

- (NSString *)deviceIdentifier
{
  static NSString *deviceIdentifier = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{ deviceIdentifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; });
  return deviceIdentifier;
}

- (NSString *)defaultPrefix
{
  return @"55";
}

- (NSString *)defaultMSISDN
{
  return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

- (NSString *)defaultUniqueID
{
  return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

- (BOOL)lastLoginUserType
{
  return [[[NSUserDefaults standardUserDefaults] objectForKey:kLastValidLoginUser] boolValue];
}

- (NSString *)loginConfigPath:(LMUser *)user
{
  NSString *group = (user.status == KAUserGuest) ? @"guest" : @"user";
  return [[NSString documentsDir] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@.plist", group, self.user.msisdn]];
}

- (NSString *)lastLoginConfigPath
{
  return [[NSUserDefaults standardUserDefaults] stringForKey:kLastValidMSISDNPath];
}
#pragma mark chek for nofications 
-(void)setLastLoginTime{
  if([self isActive]){
    NSDate* now = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:now forKey:kLastTimeActiveUserLoginDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}
//we not use
-(BOOL)loginBefore3Days{
  NSDate* lastLoginDate = [[NSUserDefaults standardUserDefaults]  objectForKey:kLastTimeActiveUserLoginDate];
  if(!lastLoginDate){
    return NO;
  }
  //NSTimeInterval aa = [lastLoginDate timeIntervalSinceNow] ;
  if ([lastLoginDate timeIntervalSinceNow] < -1*D_DAY*7)
  {
    return YES;
    
  }
  else
  {
    return NO;
  }
}
-(NSInteger)userWeeklyUsage{
  //TODO: make Calculations
  return 35;
}

-(NSArray *)takeLevelData{
  return self.user.levelData;
}
-(NSNumber *)takeCurrentLevel{
  if(!self.user)
    return 0;
  if(!self.user.currentLevel)
    return 0;
  return self.user.currentLevel;
}
-(NSString *)takeContentVersion{
  if(self.user && self.user.contentVersion)
  return self.user.contentVersion;
  
  return @"0";
}

@end
