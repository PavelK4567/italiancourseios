//  LMAudioManager.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMAudioManager.h"
#import <AVFoundation/AVFoundation.h>


@interface LMAudioManager () <AVAudioPlayerDelegate> {
	BOOL _audioEnabled;
  AVAudioSession *audioSession;
  AVAudioRecorder *audioRecorder;
  NSDictionary *recorderConfiguration;
}

@property (strong, nonatomic) AVAudioPlayer *backgroundPlayer;
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) NSBundle *sounds;

@end


@implementation LMAudioManager


#ifdef AUDIO_SINGLETON
SINGLETON_GCD(LMAudioManager)
#endif


- (id)init {
	self = [super init];
	if (self) {
		_audioEnabled = YES;
		_backgroundPlayer = 0;
		_audioPlayer = 0;
		_sounds = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"Sounds" ofType:@"bundle"]];
    recorderConfiguration = @{
                              AVFormatIDKey: [NSNumber numberWithInt:kAudioFormatLinearPCM],
                              AVEncoderAudioQualityKey:[NSNumber numberWithInt:AVAudioQualityMedium],
                              AVEncoderBitRateKey : @16000,
                              AVLinearPCMBitDepthKey: @16,
                              AVNumberOfChannelsKey: @1,
                              AVSampleRateKey: @16000.,
                              AVLinearPCMIsFloatKey: @NO
                              };
    audioSession = [AVAudioSession sharedInstance];
	}
	return self;
}

- (void)setEnabled:(BOOL)enabled {
	_audioEnabled = enabled;
	/*if (_audioEnabled) {
   [self playBackgroundMusic];
   } else {
   [self pauseBackgroundMusic];
   }*/
}

- (void)playBackgroundMusic {
	if (_audioEnabled) {
		NSError *error = 0;
		NSString *backgroundMusicPath = [self.sounds pathForResource:kSoundTypeBackground ofType:kSoundFileType];
		NSURL *backgroundMusicURL = [NSURL fileURLWithPath:backgroundMusicPath];
    
		self.backgroundPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
		[self.backgroundPlayer setNumberOfLoops:-1];
		[self.backgroundPlayer play];
	}
}

- (void)pauseBackgroundMusic {
	if (self.backgroundPlayer) {
		[self.backgroundPlayer stop];
		[self setBackgroundPlayer:0];
	}
}

- (void)playSound:(ESoundType)type {
	if (_audioEnabled) {
		if (![_sounds isLoaded]) {
			[_sounds load];
		}
    
		NSString *effectPath = 0;
		switch (type) {
			case kSoundButtonTap:
				effectPath = [self.sounds pathForResource:kSoundTypeButtonTap ofType:kSoundFileType];
				break;
        
			case kSoundSwipe:
				effectPath = [self.sounds pathForResource:kSoundTypeSwipe ofType:kSoundFileType];
				break;
        
			case kSoundError:
				effectPath = [self.sounds pathForResource:kSoundTypeError ofType:kSoundFileType];
				break;
        
			case kSoundSuccess:
				effectPath = [self.sounds pathForResource:kSoundTypeSuccess ofType:kSoundFileType];
				break;
        
			case kSoundRecord:
        effectPath = [[NSBundle mainBundle] pathForResource:@"Tone_5.1_-_Rec_begin_tone.wav" ofType:@""];
				//effectPath = [self.sounds pathForResource:kSoundTypeResults ofType:kSoundFileType];
				break;
      case kSoundStopRecord:
        effectPath = [[NSBundle mainBundle] pathForResource:@"Tone_5.1_-_Rec_begin_tone.wav" ofType:@""];
        //effectPath = [[NSBundle mainBundle] pathForResource:@"Tone_5.3_-_Rec_end_tone-2.wav" ofType:@""];
        //effectPath = [self.sounds pathForResource:kSoundTypeResults ofType:kSoundFileType];
        break;
        
			case kSoundDeletePicture:
				effectPath = [self.sounds pathForResource:kSoundTypeDeletePicture ofType:kSoundFileType];
				break;
        
			case kSoundTakePicture:
				effectPath = [self.sounds pathForResource:kSoundTypeTakePicture ofType:kSoundFileType];
				break;
		}
    
		NSURL *effectURL = [NSURL fileURLWithPath:effectPath];
		SystemSoundID soundID;
		AudioServicesCreateSystemSoundID((__bridge CFURLRef)effectURL, &soundID);
		AudioServicesPlaySystemSound(soundID);
	}
}

- (BOOL)playAudio:(NSURL *)path {
  NSError *error = 0;
  [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
	//if (!_audioEnabled)
		//return NO;
	if (self.audioPlayer.isPlaying) {
		DLog(@"Cannot play %@ since another file is playing at the moment.", path);
		return NO;
	}
  
	if (_audioEnabled) {
		self.backgroundPlayer.volume /= 5;
	}
  
	//NSError *error = 0;
	self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:path error:&error];
	[self.audioPlayer setDelegate:self];
	[self.audioPlayer setNumberOfLoops:0];
    
	[self.audioPlayer play];
  
	if (error) {
		[self.audioPlayer stop];
		[self.audioPlayer setDelegate:0];
		[self setAudioPlayer:0];
	}
  
	return !error;
}

- (BOOL)playAudioFromDocumentPath:(NSString *)fileName {
    
    [self stopPlaying];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSURL *urlNew = [NSURL URLWithString:dataPath];
    [self playAudio:urlNew];
    return 0;
}

- (BOOL)playAudioFromDocumentPath:(NSString *)fileName componentSender:(id)sender {
    
    [self stopPlaying];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSURL *urlNew = [NSURL URLWithString:dataPath];
    [self playAudio:urlNew fromSender:sender];
    return 0;
    
}

- (double)getSoundDuration:(NSString *)fileName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:fileName];
    NSURL *urlNew = [NSURL URLWithString:dataPath];

    NSError *error = 0;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
 
    
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:urlNew error:&error];
    [self.audioPlayer setDelegate:self];
    [self.audioPlayer setNumberOfLoops:0];
    
    return [self.audioPlayer duration];
    
}

- (BOOL)playAudio:(NSURL *)path fromSender:(id)sender {
    
    NSError *error = 0;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    //if (!_audioEnabled)
    //return NO;
    if (self.audioPlayer.isPlaying) {
        DLog(@"Cannot play %@ since another file is playing at the moment.", path);
        return NO;
    }
    
    if (_audioEnabled) {
        self.backgroundPlayer.volume /= 5;
    }
    
    
    //NSError *error = 0;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:path error:&error];
    [self.audioPlayer setDelegate:self];
    [self.audioPlayer setNumberOfLoops:0];
    
    NSInteger sec = [self.audioPlayer duration];
    NSInteger sDuration = sec%60;
    NSInteger repeatCount;
    
//    NSLog(@"Duration ---- %ld", sDuration);
    
    if (sDuration == 0) {
        sDuration = 1;
        repeatCount = 2;
    }
    else if (sDuration == 1) {
        
        repeatCount = 2;
    }
    else {
        
        repeatCount = sDuration/2;
    }
    
    
    if ([sender isKindOfClass:[UIButton class]]) {
        
        UIButton *btn = (UIButton *)sender;
        
        btn.imageView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"ic_small_sound2"], [UIImage imageNamed:@"ic_small_sound"], [UIImage imageNamed:@"ic_small_sound2"], [UIImage imageNamed:@"ic_small_sound"], nil];
        btn.imageView.animationDuration = sDuration;
        [btn.imageView setAnimationRepeatCount:repeatCount];
        [btn.imageView startAnimating];
        
    }
    else if ([sender isKindOfClass:[UIImageView class]]) {
        
        UIImageView *imgSoundButton = (UIImageView *)sender;
        
        imgSoundButton.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"ic_small_sound2"], [UIImage imageNamed:@"ic_small_sound"], [UIImage imageNamed:@"ic_small_sound2"], [UIImage imageNamed:@"ic_small_sound"],  nil];
        imgSoundButton.animationDuration = sDuration;
        [imgSoundButton setAnimationRepeatCount:repeatCount];
        [imgSoundButton startAnimating];
        
    }

    [self.audioPlayer play];
    
    if (error) {
        [self.audioPlayer stop];
        [self.audioPlayer setDelegate:0];
        [self setAudioPlayer:0];
    }
    
    return !error;
}


- (BOOL)stopPlaying {
    if ([self.audioPlayer isPlaying]) {
        [self.audioPlayer stop];
        
        [self.delegate playbackComplete];
    }
    return 0;
}

#pragma mark - AVAudioPlayerDelegate

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
  [self.delegate playbackComplete];
	if (player == self.audioPlayer) {
		if (_audioEnabled) {
			self.backgroundPlayer.volume *= 5;
		}
	}
}
#pragma mark - Recording

- (void)record:(NSString *)path {
  NSError *error = 0;
  NSURL *recordingPath = [NSURL fileURLWithPath:path];
  NSError *error2 = nil;
  
  [audioSession setCategory:AVAudioSessionCategoryRecord error:&error];
  if (error) {
    DLog(@"%@", @"Error configuring recording session.");
    return;
  }
  audioRecorder = [[AVAudioRecorder alloc] initWithURL:recordingPath settings:recorderConfiguration error:&error];
  if (error) {
    [self invalidateRecorder];
    DLog(@"%@", @"Error configuring recorder.");
    return;
  }
  self.recordingStatus = kAudioActive;
  
  [audioRecorder setDelegate:self];
  [audioRecorder record];
  BOOL success = [recordingPath setResourceValue: [NSNumber numberWithBool: YES]
                  
                                          forKey: NSURLIsExcludedFromBackupKey error: &error2];
  if(!success){
    NSLog(@"Error excluding %@ from backup %@", [recordingPath lastPathComponent], error2);
  }
}

-(void)deleteRecording {
  [audioRecorder deleteRecording];
}

- (void)stopRecording {
  if(audioRecorder){
    [audioRecorder stop];
  }
  if(self.delegate ){
    [self.delegate recordingComplete];
    [self.delegate recordingCompleteWithRecordFile:audioRecorder.url];
  }
  [self invalidateRecorder];
}
- (void)cancelRecording {
  [audioRecorder stop];
  [self invalidateRecorder];
}
- (void)invalidateRecorder {
  audioRecorder.delegate = 0;
  audioRecorder = 0;
  self.recordingStatus = kAudioIdle;
}
#pragma  mark - AVAudioRecorderDelegate

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag
{
  [self.delegate recordingComplete];
  [self invalidateRecorder];
}

- (void)audioRecorderEncodeErrorDidOccur:(AVAudioRecorder *)recorder error:(NSError *)error
{
  [self invalidateRecorder];
  DLog(@"Recording stopped due to decoding error.\n[%ld] %@", (long)error.code, error.description);
}

#if TARGET_OS_IPHONE

- (void)audioRecorderBeginInterruption:(AVAudioRecorder *)recorder
{
  DLog(@"%@", @"Recording interrupted.");
}

- (void)audioRecorderEndInterruption:(AVAudioRecorder *)recorder withOptions:(NSUInteger)flags
{
  DLog(@"%@", @"Recording resumed.");
  [audioRecorder record];
}

#endif

#pragma Mark default sound
- (void)playRightSound
{
  if (_audioEnabled) {
    
      NSString *strCorrectFeedbackSound = [DATA_SERVICE dataModel].courseData.properties.correctFeedbackSound;
      [self playAudioFromDocumentPath:strCorrectFeedbackSound];
  }
  else {
  
      [self.delegate playbackComplete];
  }

}

-(void)playWrongSound
{
  if (_audioEnabled) {
    NSString *strIncorrectFeedbackSound = [DATA_SERVICE dataModel].courseData.properties.incorrectFeedbackSound;
    [self playAudioFromDocumentPath:strIncorrectFeedbackSound];
  }else{
    [self.delegate playbackComplete];
  }
}

- (void)playInitialSound
{
    NSString *initialSoundPath = [[NSBundle mainBundle] pathForResource:@"silence" ofType:@"mp3"];
    [self playAudio:[NSURL URLWithString:initialSoundPath]];
}
@end
