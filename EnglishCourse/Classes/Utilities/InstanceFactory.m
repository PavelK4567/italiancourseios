//
//  InstanceFactory.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/24/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "InstanceFactory.h"

@implementation InstanceFactory

+ (BaseInstance *)createInstanceForType:(NSInteger)type instanceDataDict:(NSDictionary *)instanceDataDict {
    
    BaseInstance *instance = nil;
    
    switch (type) {
        case kInstanceSimplePageProfile:
            instance = [SimplePageProfile createInstanceWithData:instanceDataDict];
            break;
        
        case kInstanceSimplePageTextAndVideo:
            instance = [SimplePageTextAndVideo createInstanceWithData:instanceDataDict];
            break;
        
        case kInstanceSimplePageBubbleDialog:
            instance = [SimplePageBubbleDialog createInstanceWithData:instanceDataDict];
            break;
            
        case kInstanceMultipleChoiceTextProgressiveChat:
            instance = [MultipleChoiceTextProgressiveChat createInstanceWithData:instanceDataDict];
            break;
        
        case kInstanceMultipleChoiceImage:
            instance = [MultipleChoiceImage createInstanceWithData:instanceDataDict];
            break;
        
        case kInstanceMultipleChoiceText:
            instance = [MultipleChoiceText createInstanceWithData:instanceDataDict];
            break;
            
        case kInstanceMissingWords:
            instance = [FillTheMissingWords createInstanceWithData:instanceDataDict];
            break;
            
        case kInstanceWriting:
            instance = [Writing createInstanceWithData:instanceDataDict];
            break;
            
        case kInstancePresentationDialog:
            instance = [FramesPresentationDialog createInstanceWithData:instanceDataDict];
            break;
            
        case kInstancePractice:
            instance = [Practice createInstanceWithData:instanceDataDict];
            break;
            
        default:
            break;
    }
    
    
    return instance;
    
}

@end
