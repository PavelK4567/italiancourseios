//
//  Utilities.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities

+ (Utilities *)sharedInstance {
    
    static Utilities *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (instance == nil) {
            instance = [Utilities new];
        }
    });
    
    return instance;
    
}
    
- (bool) isFirstRun {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults boolForKey: @"firstRun"]) {
        [defaults setBool: YES forKey: @"firstRun"];
        [defaults synchronize];
        return YES;
    } else {
        return NO;
    }
}

- (void)extractDownloadedArchive:(NSString *)filePath delegate:(id<SSZipArchiveDelegate>)delegate {
    
    [self removeAllTempFiles];
    
    [SSZipArchive unzipFileAtPath:filePath toDestination:NSTemporaryDirectory() delegate:delegate];
    
}

- (void)extractInitialData:(id<SSZipArchiveDelegate>)delegate {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    NSString *modelDataArchive = [[NSBundle mainBundle] pathForResource:@"publishEC" ofType:@"zip"];
    if(STAGING){
      modelDataArchive = [[NSBundle mainBundle] pathForResource:@"publishECP" ofType:@"zip"];
    }
    [self createModelDataRootFolder];
  
      [SSZipArchive unzipFileAtPath:modelDataArchive toDestination:dataPath delegate:delegate];
}

- (void)extractArchiveAtPath:(NSString *)dataPath {
    
    [self removeAllTempFiles];
    
    [SSZipArchive unzipFileAtPath:dataPath toDestination:NSTemporaryDirectory() delegate:nil];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDirectoryEnumerator* enumerator = [[NSFileManager defaultManager] enumeratorAtPath:NSTemporaryDirectory()];
    
    for (NSString *file in enumerator) {
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath: [NSString stringWithFormat:@"%@/%@",NSTemporaryDirectory(),file] isDirectory: &isDirectory];
        if (!isDirectory) {
            
            if (![[file pathExtension] isEqualToString:@"json"]) {
                [self moveFileObjectWithName:file fromLocation:NSTemporaryDirectory() toLocation:documentsDirectory];
            }
            
        }
        
    }
    
}

- (NSDictionary *)extractArchiveAndReturnData:(NSString *)dataPath {
  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *tmpPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderASITmpFolder];
    
    NSDictionary *dataDict = nil;
    
    [self removeAllTempFiles];
    
    [[Utilities sharedInstance] appendLogFileWithString:dataPath];
    
    [SSZipArchive unzipFileAtPath:dataPath toDestination:tmpPath delegate:nil];
    
    NSDirectoryEnumerator* enumerator = [[NSFileManager defaultManager] enumeratorAtPath:tmpPath];
    
    for (NSString *file in enumerator) {
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath: [NSString stringWithFormat:@"%@/%@",tmpPath,file] isDirectory: &isDirectory];
        if (!isDirectory) {
            
            if ([[file pathExtension] isEqualToString:@"json"]) {
                NSString *jsonFileToParse = [NSString stringWithFormat:@"%@/%@",tmpPath,file];
                
                NSData *dataFromFile = [[NSFileManager defaultManager] contentsAtPath:jsonFileToParse];
                NSError *error;
                
                if (dataFromFile) {
                    dataDict = [NSJSONSerialization JSONObjectWithData:dataFromFile options:NSJSONReadingMutableLeaves error:&error];
                }
            } else {
                [self moveFileObjectWithName:file fromLocation:tmpPath toLocation:documentsDirectory];
            }
            
        }
        
    }

    return  dataDict;
}

- (void)listAllFilesInDir:(NSString *)path {
    
    NSString *pathToTraverse = path;
    
    NSDirectoryEnumerator* enumerator = [[NSFileManager defaultManager] enumeratorAtPath:pathToTraverse];
    
    for (NSString *file in enumerator) {
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath: [NSString stringWithFormat:@"%@/%@",pathToTraverse,file] isDirectory: &isDirectory];
        if (!isDirectory) {
            DLog(@"FILE:%@ WITH EXTENSION:%@",file, [file pathExtension]);
            
            if ([[file pathExtension] isEqualToString:@"zip"]) {

            }
            
        } else {
            DLog(@"DIR:%@",file);
            [self listAllFilesInDir: file];
        }
    }

}

- (void)listAllFilesInBundle {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    NSArray *folders = [[NSFileManager defaultManager] subpathsAtPath:dataPath];
    
    for (NSString *file in folders) {
        BOOL isDirectory = NO;
        [[NSFileManager defaultManager] fileExistsAtPath: [NSString stringWithFormat:@"%@/%@",dataPath,file] isDirectory: &isDirectory];
        if (!isDirectory) {
            DLog(@"FILE:%@ WITH EXTENSION:%@",file, [file pathExtension]);
            
            if ([[file pathExtension] isEqualToString:@"zip"]) {

            }
            
        } else {
            DLog(@"DIR:%@",file);
            [self listAllFilesInDir: file];
        }

    }        
        
}

- (BOOL)removeFileFromPath:(NSString *)filePath {
    
    DLog(@"[DEBUG] removeFileFromPath -> File Path:%@", filePath);

    NSFileManager *manager = [NSFileManager defaultManager];
    
    BOOL isErrorPresent = NO;
    
    if ([manager fileExistsAtPath:filePath]) {
        NSError *error;
        //[manager removeItemAtPath:filePath error:&error];
        
        if (error) {
            isErrorPresent = YES;
        }
        
    }
    
    return isErrorPresent;
    
}

- (void)removeAllTempFiles {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderASITmpFolder];
    
    DLog(@"[DEBUG] removeAllTempFiles -> Data Path:%@", dataPath);
    
    NSArray* tempFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dataPath error:NULL];
    
    for (NSString *file in tempFiles) {
    
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", dataPath, file] error:NULL];
    
    }
    
}

- (void)removeAllFilesFromPath:(NSString *)folderPath {
    
    NSArray* tempFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:NULL];
    
    for (NSString *file in tempFiles) {
        
        NSString *fileToBeRemoved = [folderPath stringByAppendingPathComponent:file];
        
        [[NSFileManager defaultManager] removeItemAtPath:fileToBeRemoved error:NULL];
        
    }
    
}

- (void)removeAllFilesFromDocumentDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    [self removeAllFilesFromPath:documentsDirectory];
}

- (void)removeInitialData {
    
    NSString *modelDataArchive = [[NSBundle mainBundle] pathForResource:@"publishECP" ofType:@"zip"];
    
    [[NSFileManager defaultManager] removeItemAtPath:modelDataArchive error:NULL];
    
}

- (void)createModelDataRootFolder {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }

}

- (BOOL)checkIfASITmpFolderCreated
{
    BOOL isCreated = NO;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderASITmpFolder];
    
    isCreated = [self directoryExistAtPath:dataPath];
    
    return isCreated;
}

- (void)createASITmpFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderASITmpFolder];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
}

- (NSString *)getASITmpFolder
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderASITmpFolder];
    
    return dataPath;
}

- (void)moveFileObjectWithName:(NSString *)fileName fromLocation:(NSString *)fromLocation toLocation:(NSString *)toLocation {
    
    NSString *fileNameWithLocationFrom = [NSString stringWithFormat:@"%@/%@",fromLocation, fileName];
    NSString *fileNameWithLocationTo = [NSString stringWithFormat:@"%@/%@",toLocation, fileName];
    
    DLog(@"[DEBUG] moveFileObjectWithName -> From Location:%@ To Location:%@", fromLocation, toLocation);
    
    NSError *error;
    
    [[NSFileManager defaultManager] moveItemAtPath:fileNameWithLocationFrom toPath:fileNameWithLocationTo error:&error];
    
    [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:fileNameWithLocationTo]];
    
    //TODO: Return error message with delegate
    
}

- (void)removeModelDataRootFolderWithContent {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:kDocumentsFolderModelData];
    
    for (NSString *levelFolderName in LEVELS_FOLDERS_ARRAY) {
        NSString *levelPath = [dataPath stringByAppendingPathComponent:levelFolderName];
        DLog(@"[DEBUG] removeModelDataRootFolderWithContent -> removeFileFromPath Level Path:%@", levelPath);
        [self removeFileFromPath:levelPath];
    }
    DLog(@"[DEBUG] removeModelDataRootFolderWithContent -> removeAllFilesFromPath Level Path:%@", dataPath);
    [self removeAllFilesFromPath:dataPath];
}

- (NSDictionary *)createDictionaryFromJSONAtPath:(NSString *)filePath
{
    
    NSDictionary *dataDict = nil;
    
    NSData *dataFromFile = [[NSFileManager defaultManager] contentsAtPath:filePath];
    NSError *error;
    
    if (dataFromFile) {
        dataDict = [NSJSONSerialization JSONObjectWithData:dataFromFile options:NSJSONReadingMutableLeaves error:&error];
    }
    
    return dataDict;
    
}

- (uint64_t)getFreeDiskspace {
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        DLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        DLog(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

- (void)appendLogFileWithString:(NSString *)log {
    
    if (kDebugLOG == YES) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSError *error;
        
        NSString *logData = [NSString stringWithContentsOfFile:[documentsDirectory stringByAppendingPathComponent:@"log.data"] encoding:NSUTF8StringEncoding error:&error];
        
        if (error) {
            logData = log;
        } else {
            logData = [logData stringByAppendingString:log];
        }
        
        [logData writeToFile:[documentsDirectory stringByAppendingPathComponent:@"log.data"] atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
    }
    
}

- (BOOL)fileExistAtPath:(NSString *)filePath
{
    BOOL fileExist = NO;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        fileExist = YES;
    }
    
    return fileExist;
}

- (BOOL)directoryExistAtPath:(NSString *)filePath
{
    BOOL fileExist = NO;
    
    BOOL isDirectory = YES;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    if (isDirectory) {
        fileExist = YES;
    }
    
    return fileExist;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL

{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    
    
    NSError *error = nil;
    
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                    
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    
    if(!success){
        
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        
    }
    
    return success;
    
}

#pragma mark -
#pragma mark - SSZipArchiveDelegate

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    DLog(@"Unziped path:%@",unzippedPath);
    
    [self listAllFilesInDir:NSTemporaryDirectory()];
}

@end
