//
//  Constants.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/18/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#ifndef EnglishCourse_Constants_h
#define EnglishCourse_Constants_h
#endif

/* TODO: Zare add title here */

#define RED   0
#define GREEN 1
#define BLUE  2
#define ALPHA 3

#define kNumberOFTries 100

#define kDebugLOG NO



#define kInitialCoursePropJSON @"courseProp.json"
#define kInitialCourseZIP @"course.zip"
#define kInitialCourseJSON @"course.json"
#define kInitialCharactersZIP @"characters.zip"
#define kInitialCharactersJSON @"characters.json"
#define kInitialLevelJSON @"level.json"
#define kInitialLevelDataJSON @"levelData.json"

#define kInitialLevelDataZIP @"levelData.zip"

#define kDocumentsFolderModelData @"ModelData"
#define kDocumentsFolderASITmpFolder @"ASITmp"

#define kInitialLevelOne @"1"
#define kInitialLevelTwo @"2"
#define kInitialLevelThree @"3"

#define LEVELS_FOLDERS_ARRAY @[kInitialLevelOne, kInitialLevelTwo, kInitialLevelThree]

#define kCourse @"course"

#define kCharacters @"characters"
#define kCharacter @"character"
#define kCharacterID @"characterID"
#define kId @"id"
#define kColor @"color"
#define kName @"name"

#define kLevelData @"levelData"
#define kMD5 @"MD5"
#define kType @"type"

#define kSections @"sections"
#define kSectionID @"sectionID"
#define kSectionOrder @"sectionOrder"
#define kSectionName @"sectionName"
#define kUnits @"units"

#define kUntouchedImage @"untouchedImage"
#define kTouchedImage @"touchedImage"
#define kOrder @"order"
#define kOrderNr @"orderNr"
#define kTeaser @"teaser"
#define kGrammarCount @"grammarCount"
#define kInstances @"instances"
#define kWordCount @"wordCount"
#define kPages @"pages"

#define kInstanceID @"instanceID"
#define kUnitID @"unitID"

#define kLevelOrder @"levelOrder"

#define kSectionDescription @"sectionDescription"

#define kDescription @"description"
#define kImage @"image"
#define kAudio @"audio"
#define kSound @"sound"
#define kVideo @"video"
#define kVideoPreview @"videoPreview"
#define kText @"text"
#define kTranslation @"translation"

#define kIDprop @"ID"
#define kInstructionText @"instructionText"
#define kInstructionSound @"instructionSound"
#define kCorrectFeedback @"correctFeedback"
#define kCorrectFeedbackSound @"correctFeedbackSound"
#define kIncorrectFeedback @"incorrectFeedback"
#define kIncorrectFeedbackSound @"incorrectFeedbackSound"
#define kQuestionAudio @"questionAudio"
#define kQuestionText @"questionText"

#define kLongVersion @"longVersion"

#define kLeftCharacterID @"leftCharacterID"
#define kRightCharacterID @"rightCharacterID"

#define kLevels @"levels"
#define kSegments @"segments"

#define kQuestion @"question"
#define kAnswers @"answers"
#define kAnswer @"answer"
#define kIsAnswer @"isAnswer"
#define kIsCorrect @"isCorrect"
#define kIsLongLayout @"isLongLayout"
#define kCaseSensitive @"caseSensitive"

#define kWords @"words"
#define kWord @"word"
#define kNativeText @"nativeText"
#define kTargetText @"targetText"

#define kPractiseSection @"practice_section"
#define kExposureSection @"exposure_section"
#define kPractices @"practices"
#define kLayout @"layout"
#define kCorrectAnswers @"correctAnswers"

#define kHeader @"header"
#define kBody @"body"
#define kFooter @"footer"

#define kSectionTitle @"sectionTitle"
#define kSectionText @"sectionText"

#define kTitle @"title"

#define kAlign @"align"

#define kDataExtracted @"dataExtracted"
#define kDataCompleteDownload @"dataCompleteDownload"
#define kSyncStarted @"syncStarted"
#define kSyncFinished @"syncFinished"

#define kCourseData @"courseData"
#define kCharactersData @"charactersData"

#define kUnitPerformaceThreshold @"unitPerformaceThreshold"
#define kScoreFirstTime @"scoreFirstTime"
#define kUsageTime @"usageTime"
#define kUnitsCreditsApple @"unitsCreditsApple"
#define kUnitsCreditsGuest @"unitsCreditsGuest"
#define kScoreDiploma @"scoreDiploma"
#define kUnitsCreditsVivo @"unitsCreditsVivo"
#define kScoreWrongAnswer @"scoreWrongAnswer"
#define kScoreSecondTime @"scoreSecondTime"
#define kFbLink @"fbLink"
#define kPlacementTestDescription @"placementTestDescription"

#define kInitial @"initial"

#define kInstanceSimplePageProfile 0
#define kInstanceSimplePageTextAndVideo 1
#define kInstanceSimplePageBubbleDialog 2
#define kInstanceMultipleChoiceTextProgressiveChat 3
#define kInstanceMultipleChoiceImage 4
#define kInstanceMultipleChoiceText 5
#define kInstanceMissingWords 7
#define kInstanceWriting 9
#define kInstancePresentationDialog 10
#define kInstancePractice 11

#define kSimpleDialogConversationIconWidth 52
#define kSimpleDialogConversationIconHeight 52
#define kSimpleDialogConversationIconSpace 2

#define kChatItemInitialHeight 52

#define kSpeakerImageWidth 22
#define kSpeakerImageHeight 22

#define kPotentialAnswerPadding 5
#define kPotentialAnswerHeight 33

#define kTitleLabelDefaultHeight 46

#define kContentVersion @"contentVersion"
#define kLevelVersion @"levelVersion"
#define kRequestsData @"requestsData"
#define kInitialContentDownload @"initialContentDownload"

#define kInitialModelGenerated @"initialModelGenerated"

/* MissingWordView */

#define kScrollViewHeight 35.0
#define kTagListHeight 80.0
#define kMissingWordButtonInnerPadding 10.0
#define kScrollViewWidth self.mWidth
#define kZeroPointValue 0.0
#define kMissingWordButtonPadding 8.0
#define kMissingWordButtonCornerRadius 6.0
#define kWordPattern @"\\<[[a-zA-Z0-9\\w\\s\\'’]'+]*\\>"
#define kUnderlineWord @"__________"
#define kZeroPositionValue 0
#define kButtonColor @"e6d8e8"
#define kQestionBackground @"DFDFDF"
#define kButtonBorderWidth .5
#define kWordsSpacing 6.0
#define kMissingWordButtonHeight 35.0
#define kButtonInnerPadding 3.0
#define kQestionsPadding 5.0
#define kDefaultPadding 5.0



/* Progres Model */

#define kGuestUserMSISDN @"55000000000"
#define kPercentSuccess 80.0

#define kPageControlHeight 49

/* MultipleChoiceImage Objects */

#import "MultipleChoiceImageExposureObject.h"
#import "MultipleChoiceImagePracticeObject.h"

/* Practice Questions */

#import "PracticeQuestionObject.h"

/* UIImageView categories */

#import "UIImageView+DocumentsPath.h"
#import "UIImage+DocumentsPath.h"

/* UINavigationController Category */

#import "UINavigationController+Orientation.h"

/* width of UIImage different vehicles */

#define widthImageSimpleProfile 320
#define heightImageSimpleProfile 221

#define widthImageMultiChoiceText 320
#define heightImageMultiChoiceText 185
