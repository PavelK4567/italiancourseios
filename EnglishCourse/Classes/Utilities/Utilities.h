//
//  Utilities.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/7/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import "SSZipArchive.h"


@protocol UtilitiesDelegate <NSObject>

- (void)returnProgressInfoWithErrorMessage:(BOOL)hasError message:(NSString *)message;

@end

@interface Utilities : NSObject

+ (Utilities *)sharedInstance;

- (bool) isFirstRun;
    
- (void)extractDownloadedArchive:(NSString *)filePath delegate:(id<UtilitiesDelegate>)delegate;
- (void)extractInitialData:(id<SSZipArchiveDelegate>)delegate;
- (NSDictionary *)extractArchiveAndReturnData:(NSString *)dataPath;
- (void)extractArchiveAtPath:(NSString *)dataPath;

- (void)listAllFilesInDir:(NSString *)path;
- (void)listAllFilesInBundle;
- (BOOL)removeFileFromPath:(NSString *)filePath;

- (void)removeAllTempFiles;
- (void)removeAllFilesFromPath:(NSString *)folderPath;

- (void)removeInitialData;

- (void)createModelDataRootFolder;
- (void)removeModelDataRootFolderWithContent;
- (void)removeAllFilesFromDocumentDirectory;
- (void)moveFileObjectWithName:(NSString *)fileName fromLocation:(NSString *)fromLocation toLocation:(NSString *)toLocation;

- (void)appendLogFileWithString:(NSString *)log;

- (BOOL)fileExistAtPath:(NSString *)filePath;

- (BOOL)directoryExistAtPath:(NSString *)filePath;

- (BOOL)checkIfASITmpFolderCreated;

- (void)createASITmpFolder;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

- (NSString *)getASITmpFolder;

- (NSDictionary *)createDictionaryFromJSONAtPath:(NSString *)filePath;

- (uint64_t)getFreeDiskspace;

@end
