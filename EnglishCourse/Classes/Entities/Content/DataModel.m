//
//  DataModel.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 12/4/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "DataModel.h"
#import "DataService.h"

@implementation DataModel

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    
    self.courseData = [aDecoder decodeObjectForKey:kCourseData];
    self.charactersData = [aDecoder decodeObjectForKey:kCharactersData];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.courseData forKey:kCourseData];
    [aCoder encodeObject:self.charactersData forKey:kCharactersData];
    
}

- (void)saveModel {
    [[DataService sharedInstance] saveDataModel:self];
}

- (void)deleteModel
{
    [[DataService sharedInstance] deleteDataModel];
}

@end
