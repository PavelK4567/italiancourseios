//
//  SimplePageBubbleDialog.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseInstance.h"
#import "SegmentBubbleDialog.h"

@interface SimplePageBubbleDialog : BaseInstance

@property (nonatomic, strong) NSArray *segmentsArray;

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict;

@end
