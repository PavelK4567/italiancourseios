//
//  Practice.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Practice.h"

@implementation Practice

- (id)initWithCoder:(NSCoder *)aDecode {
    
    self = [super initWithCoder:aDecode];
    
    self.practiceSections = [aDecode decodeObjectForKey:@"practiceSections"];
    self.isInitial = [aDecode decodeBoolForKey:@"initial"];
    self.title = [aDecode decodeObjectForKey:@"title"];

    self.mID = [aDecode decodeIntegerForKey:@"mID"];
    self.type = [aDecode decodeIntegerForKey:@"type"];
    
    self.refObjectsArray = [aDecode decodeObjectForKey:@"refObjectsArray"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.practiceSections forKey:@"practiceSections"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];

    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
    [aCoder encodeObject:self.refObjectsArray forKey:@"refObjectsArray"];
    
}

+ (Practice *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    Practice *instance = nil;
    
    if (instanceDataDict) {
        instance = [Practice new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        
        NSArray *practisesArray = instanceDataDict[kPractices];
        
        NSMutableArray *practiseSectionsDataArray = nil;
        
        if (practisesArray && [practisesArray count] > 0) {
            practiseSectionsDataArray = [NSMutableArray arrayWithCapacity:1];
            
            for (NSDictionary *practiseDict in practisesArray) {
                PractiseSection *practiceInstance = [PractiseSection createInstanceWithData:practiseDict];
                
                [practiseSectionsDataArray addObject:practiceInstance];
            }
            
            instance.practiceSections = [practiseSectionsDataArray copy];
            
            instance.refObjectsArray = practiseSectionsDataArray;
            
            DLog(@"Practice instance ID:%ld REF OBJ:%lu", (long)instance.mID, (unsigned long)[instance.refObjectsArray count]);
        }
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.type = [instanceDataDict[kType] integerValue];
        
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *practicesArray = ((Practice *)self.tempInstance).practiceSections;
    
    NSArray *segmentsArray = self.practiceSections;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        PractiseSection *practiceInstance = segmentsArray[index];
        PractiseSection *tempSegment = practicesArray[index];
        
        [practiceInstance removeDifferentInstanceResources:tempSegment];
        
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *practicesArrayArray = instanceDict[kPractices];
    
    NSArray *segmentsArray = self.practiceSections;
    
    for (int index = 0; index < [segmentsArray count]; index++) {
        
        PractiseSection *practiceInstance = segmentsArray[index];
        NSDictionary *segmentDict = practicesArrayArray[index];
        
        [practiceInstance removeUnusedResources:segmentDict];
        
    }
    
}

- (void)replaceWithInstance:(Practice *)instance {
    [super replaceWithInstance:instance];

    self.practiceSections =  [instance.practiceSections copy];
    self.isInitial = instance.isInitial;
    self.title =  instance.title;
}

@end
