//
//  ExposureSection.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseEntity.h"

@interface ExposureSection : BaseEntity

@property (nonatomic, copy) NSString *instructionText;
@property (nonatomic, copy) NSString *instructionSound;

+ (ExposureSection *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeUnusedResources:(NSDictionary *)instanceDict;
- (void)removeDifferentInstanceResources:(ExposureSection *)tempSection;

@end
