//
//  WordMissingWords.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "WordMissingWords.h"

@implementation WordMissingWords

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.word = [aDecoder decodeObjectForKey:@"word"];
    self.translation = [aDecoder decodeObjectForKey:@"translation"];
    self.isCorrect = [aDecoder decodeBoolForKey:@"isCorrect"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.word forKey:@"word"];
    [aCoder encodeObject:self.translation forKey:@"translation"];
    [aCoder encodeBool:self.isCorrect forKey:@"isCorrect"];
    
}

+ (WordMissingWords *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    WordMissingWords *instance = nil;
    
    if (instanceDataDict) {
        instance = [WordMissingWords new];
        
        instance.word = instanceDataDict[kWord];
        instance.translation = instanceDataDict[kTranslation];
        instance.isCorrect = [instanceDataDict[kIsCorrect] boolValue];
    }
    
    return instance;
}

@end
