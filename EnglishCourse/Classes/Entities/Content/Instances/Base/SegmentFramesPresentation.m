//
//  SegmentFramesPresentation.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SegmentFramesPresentation.h"

@implementation SegmentFramesPresentation

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.characterID = [aDecoder decodeIntegerForKey:@"characterID"];
    self.allign = [aDecoder decodeBoolForKey:@"allign"];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
  
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.characterID forKey:@"characterID"];
    [aCoder encodeBool:self.allign forKey:@"allign"];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    [aCoder encodeObject:self.text forKey:@"text"];
  
}

+ (SegmentFramesPresentation *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    SegmentFramesPresentation *instance = nil;
    
    if (instanceDataDict) {
        instance = [SegmentFramesPresentation new];
        
        instance.text = instanceDataDict[kText];
        instance.audio = instanceDataDict[kAudio];
        instance.allign = [instanceDataDict[kAlign] boolValue];
        instance.image = instanceDataDict[kImage];
        instance.characterID = [instanceDataDict[kCharacter] integerValue];
        instance.orderNumber = [instanceDataDict[kOrderNr] integerValue];
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(SegmentFramesPresentation *)tempPresentation
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempPresentation.audio isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
    if (![tempPresentation.image isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kImage] isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    

}

@end
