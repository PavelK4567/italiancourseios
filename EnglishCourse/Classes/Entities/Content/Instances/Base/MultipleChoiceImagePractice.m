//
//  MultipleChoiceImagePractice.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceImagePractice.h"

@implementation MultipleChoiceImagePractice

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.orderNumber = [aDecoder decodeIntegerForKey:@"orderNumber"];
    self.question = [aDecoder decodeObjectForKey:@"question"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    self.answer = [aDecoder decodeIntegerForKey:@"answer"];
    self.correctFeedback = [aDecoder decodeObjectForKey:@"correctFeedback"];
    self.incorrectFeedback = [aDecoder decodeObjectForKey:@"incorrectFeedback"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.orderNumber forKey:@"orderNumber"];
    [aCoder encodeObject:self.question forKey:@"question"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    [aCoder encodeInteger:self.answer forKey:@"answer"];
    [aCoder encodeObject:self.correctFeedback forKey:@"correctFeedback"];
    [aCoder encodeObject:self.incorrectFeedback forKey:@"incorrectFeedback"];
    
}

+ (MultipleChoiceImagePractice *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    MultipleChoiceImagePractice *instance = nil;
    
    if (instanceDataDict) {
        instance = [MultipleChoiceImagePractice new];
        
        instance.orderNumber = [instanceDataDict[kOrderNr] integerValue];
        instance.question = instanceDataDict[kQuestion];
        instance.audio = instanceDataDict[kAudio];
        instance.answer = [instanceDataDict[kAnswer] integerValue];
        instance.correctFeedback = [Feedback createInstanceWithData:instanceDataDict[kCorrectFeedback]];
        instance.incorrectFeedback = [Feedback createInstanceWithData:instanceDataDict[kIncorrectFeedback]];
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources:(MultipleChoiceImagePractice *)tempPractice
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![tempPractice.audio isEqualToString:self.audio]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
    
    [self.correctFeedback removeDifferentResources:tempPractice.correctFeedback.audio];
    [self.incorrectFeedback removeDifferentResources:tempPractice.incorrectFeedback.audio];
    
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
    [self.correctFeedback removeUnusedResources:instanceDict[kCorrectFeedback]];
    [self.incorrectFeedback removeUnusedResources:instanceDict[kIncorrectFeedback]];
}

@end
