//
//  SegmentFramesPresentation.h
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "BaseSegment.h"

@interface SegmentFramesPresentation : BaseSegment

@property (nonatomic, assign) NSInteger characterID;
@property (nonatomic, assign) BOOL      allign;
@property (nonatomic, copy) NSString  *image;
@property (nonatomic, copy) NSString  *audio;

+ (SegmentFramesPresentation *)createInstanceWithData:(NSDictionary *)instanceDataDict;

- (void)removeDifferentInstanceResources:(SegmentFramesPresentation *)tempPresentation;
- (void)removeUnusedResources:(NSDictionary *)instanceDict;

@end
