//
//  Answer.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "Answer.h"

@implementation Answer

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [self init];
    self.orderNumber = [aDecoder decodeIntegerForKey:@"orderNumber"];
    self.isCorrect = [aDecoder decodeBoolForKey:@"isCorrect"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.orderNumber forKey:@"orderNumber"];
    [aCoder encodeBool:self.isCorrect forKey:@"isCorrect"];
    [aCoder encodeObject:self.text forKey:@"text"];
    
}

+ (Answer *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    Answer *answer = nil;
    
    if (instanceDataDict) {
        answer = [Answer new];
        
        answer.orderNumber = [instanceDataDict[kOrderNr] integerValue];
        answer.isCorrect = [instanceDataDict[kIsCorrect] boolValue];
        answer.text = instanceDataDict[kText];
    }
    
    return answer;
}

@end
