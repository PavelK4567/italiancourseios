//
//  SimplePageProfile.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SimplePageProfile.h"

@implementation SimplePageProfile

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    self.image = [aDecoder decodeObjectForKey:@"image"];
    self.audio = [aDecoder decodeObjectForKey:@"audio"];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.image forKey:@"image"];
    [aCoder encodeObject:self.audio forKey:@"audio"];
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    SimplePageProfile *instance = nil;
    
    if (instanceDataDict) {
        instance = [SimplePageProfile new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.instructionSound = instanceDataDict[kInstructionSound];
        
        instance.image = instanceDataDict[kImage];
        instance.audio = instanceDataDict[kAudio];
        instance.text = instanceDataDict[kText];
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeDifferentInstanceResources
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((SimplePageProfile *)self.tempInstance).image isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![((SimplePageProfile *)self.tempInstance).audio isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
    if (![((SimplePageProfile *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![instanceDict[kImage] isEqualToString:self.image]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.image];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![instanceDict[kAudio] isEqualToString:self.audio]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.audio];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
    if (![instanceDict[kInstructionSound] isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }
}
- (void)replaceWithInstance:(SimplePageProfile *)instance {
    [super replaceWithInstance:instance];
    self.image = instance.image;
    self.audio = instance.audio;
    self.text = instance.text;
    self.isInitial = instance.isInitial;
    self.title = instance.title;

}
@end
