//
//  SimplePageTextAndVideo.m
//  EnglishCourse
//
//  Created by Zarko Popovski on 11/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "SimplePageTextAndVideo.h"

@implementation SimplePageTextAndVideo

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    if (!aDecoder) {
        
        DLog(@"aDecoder");
    }
    self = [super initWithCoder:aDecoder];
    self.text = [aDecoder decodeObjectForKey:@"text"];
    self.video = [aDecoder decodeObjectForKey:@"video"];
    self.videoPreview = [aDecoder decodeObjectForKey:@"videoPreview"];
    self.isInitial = [aDecoder decodeBoolForKey:@"initial"];
    self.title = [aDecoder decodeObjectForKey:@"title"];
    
    self.mID = [aDecoder decodeIntegerForKey:@"mID"];
    self.type = [aDecoder decodeIntegerForKey:@"type"];
    
    return self;
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    if (!aCoder) {
        
        DLog(@"aCoder");
    }
    [super encodeWithCoder:aCoder];
    [aCoder encodeObject:self.text forKey:@"text"];
    [aCoder encodeObject:self.video forKey:@"video"];
    [aCoder encodeObject:self.videoPreview forKey:@"videoPreview"];
    [aCoder encodeBool:self.isInitial forKey:@"initial"];
    [aCoder encodeObject:self.title forKey:@"title"];
    
    [aCoder encodeInteger:self.mID forKey:@"mID"];
    [aCoder encodeInteger:self.type forKey:@"type"];
    
}

+ (BaseInstance *)createInstanceWithData:(NSDictionary *)instanceDataDict {
    SimplePageTextAndVideo *instance = nil;
    
    if (instanceDataDict) {
        instance = [SimplePageTextAndVideo new];
        
        instance.mID = [instanceDataDict[kInstanceID] integerValue];
        instance.unitID = [instanceDataDict[kUnitID] integerValue];
        instance.order = [instanceDataDict[kOrder] integerValue];
        instance.type = [instanceDataDict[kType] integerValue];
        instance.name = instanceDataDict[kName];
        instance.instructionText = instanceDataDict[kInstructionText];
        
        instance.text = instanceDataDict[kText];
        instance.video = instanceDataDict[kVideo];
        instance.videoPreview = instanceDataDict[kVideoPreview];
        
        instance.isInitial = [instanceDataDict[kInitial] boolValue];
        
        instance.title = instanceDataDict[kTitle];
        
        instance.tempDataObj = nil;
    }
    
    return instance;
}

- (void)removeUnusedResources:(NSDictionary *)instanceDict
{

}
- (void)removeDifferentInstanceResources
{
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if (![((SimplePageTextAndVideo *)self.tempInstance).video isEqualToString:self.video]) {
        NSString *imageForRemove = [documentsDirectory stringByAppendingPathComponent:self.video];
        [[Utilities sharedInstance] removeFileFromPath:imageForRemove];
    }
    
    if (![((SimplePageTextAndVideo *)self.tempInstance).text isEqualToString:self.text]) {
        NSString *audioForRemove = [documentsDirectory stringByAppendingPathComponent:self.text];
        [[Utilities sharedInstance] removeFileFromPath:audioForRemove];
    }
    
    if (![((SimplePageTextAndVideo *)self.tempInstance).instructionSound isEqualToString:self.instructionSound]) {
        NSString *soundForRemove = [documentsDirectory stringByAppendingPathComponent:self.instructionSound];
        [[Utilities sharedInstance] removeFileFromPath:soundForRemove];
    }*/
}

- (void)replaceWithInstance:(SimplePageTextAndVideo *)instance {
    [super replaceWithInstance:instance];
    self.text = instance.text;
    self.video = instance.video;
    self.videoPreview = instance.videoPreview;
    self.isInitial = instance.isInitial;
    self.title = instance.title;
}
@end
