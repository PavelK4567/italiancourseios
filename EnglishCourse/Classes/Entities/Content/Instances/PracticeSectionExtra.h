//
//  PracticeSectionExtra.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/14/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PractiseSection.h"

@interface PracticeSectionExtra : PractiseSection

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger length;

@property (nonatomic, assign) NSInteger instanceNumber;
@property (nonatomic, assign) NSInteger pageNumber;
@property (nonatomic, assign) BOOL isExposure;

+ (PracticeSectionExtra *)createInstanceWithData:(PractiseSection *)instanceDataDict withIndex:(NSInteger)index andLength:(NSInteger)length;

@end
