//
//  Element.h
//
//  Created by action item on 1/14/10.
//  Copyright 2010 action-item. All rights reserved.
//

@interface Element : NSObject <NSCoding> {
    int _errorCode;
    NSString *_errorDesc;
}

+ (NSArray *)deserializeJSONRoot:(id)jsonRoot elementKey:(NSString *)elementKey classHandler:(Class)classHandler;

- (id)getObjectFromKey:(id)element key:(NSString *)key;
- (id)getObjectFromKey:(id)element key:(NSString *)key defaultValue:(id)defaultValue;
- (id)getDateObjectFromKey:(id)element key:(NSString *)key;
- (void)fillWithDictionaryElement:(id)element; // override this in subclasses

- (BOOL)hasError;
- (NSError *)error;

@property(nonatomic, unsafe_unretained) int errorCode;
@property(nonatomic, strong) NSString *errorDesc;

@end
