//
//  TestLogic.h
//  K1000
//
//  Created by Action Item on 5/8/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

//#import "BaseLogicObject.h"

#import "TestCategory.h"

#define APP_FINISHED_EXTRACTING_PRELOADED_TESTS @"APP_FINISHED_EXTRACTING_PRELOADED_TESTS"
#define APP_FINISHED_EXTRACTING_TEST @"APP_FINISHED_EXTRACTING_TEST"

@interface TestLogic : NSObject {
    BOOL _isExtractingPreloadedTests;
    NSMutableDictionary *_tests;
    NSMutableArray *_unsentTests;
}
@property (strong, nonatomic) NSBundle *bundle;

// paths
- (NSString *)testsPath:(BOOL)forceCreate;
- (NSString *)testsZipFilePath;

- (TestCategory *)testCategoryOfType:(CategoryType)categoryType;
- (NSArray *)allTests;
- (NSArray *)filterQuestionsFromEachCategory:(int)maxQuestions;

@end
