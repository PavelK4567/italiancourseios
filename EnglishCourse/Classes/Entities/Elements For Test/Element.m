//
//  Element.m
//
//  Created by action item on 1/14/10.
//  Copyright 2010 action-item. All rights reserved.
//

#import "Element.h"

@implementation Element

@synthesize errorDesc = _errorDesc;
@synthesize errorCode = _errorCode;

+ (NSArray *)deserializeJSONRoot:(id)jsonRoot elementKey:(NSString *)elementKey classHandler:(Class)classHandler
{
	if (!jsonRoot)
		return nil;
	if ([jsonRoot isKindOfClass:[NSArray class]])
	{
		NSMutableArray *items = [NSMutableArray array];
		for (id element in jsonRoot)
		{
			Element *newItem = [[classHandler alloc] init];
			[newItem fillWithDictionaryElement:element];
			[items addObject:newItem];
		}
        return items;
	}
	if ([jsonRoot isKindOfClass:[NSDictionary class]])
	{
		if (elementKey)
            return [[self class] deserializeJSONRoot:[jsonRoot objectForKey:elementKey] elementKey:nil classHandler:classHandler];
		else
		{
			NSMutableArray *items = [NSMutableArray array];
			Element *newItem = [[classHandler alloc] init];
			[newItem fillWithDictionaryElement:jsonRoot];

			[items addObject:newItem];
			return items;
		}
	}

	return nil;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
	if (self = [self init])
	{
    }
	return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
}

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@: errorCode: [%d] errorDesc: [%@]", NSStringFromClass([self class]), _errorCode, _errorDesc];
}

- (id)getObjectFromKey:(id)element key:(NSString *)key defaultValue:(id)defaultValue
{
	id value = [element objectForKey:key];
	if (!value || [value isKindOfClass:[NSNull class]])
    {
        key = [NSString stringWithFormat:@"%@_%@", NSStringFromClass([self class]).lowercaseString, key];
        value = [element objectForKey:key];
        if (!value || [value isKindOfClass:[NSNull class]])
            return defaultValue;
    }
	return value;
}

- (id)getObjectFromKey:(id)element key:(NSString *)key
{
	return [self getObjectFromKey:element key:key defaultValue:nil];
}

- (id)getDateObjectFromKey:(id)element key:(NSString *)key
{
    NSString *stringObject = [self getObjectFromKey:element key:key];

    // Purpose: Return a string of the specified date-time in UTC (Zulu) time zone in ISO 8601 format.
    // Example: 2013-10-25T06:59:43.431Z
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormatter setDateFormat:SERVER_DATETIME_FORMAT];
    return [dateFormatter dateFromString:stringObject];

    //    NSString *stringObject = [self getObjectFromKey:element key:key];
    //    NSDate *date = [NSDate dateWithStringFormat:SERVER_DATETIME_FORMAT dateAsString:stringObject];
    //    if (!date)
    //        date = [NSDate dateWithStringFormat:SERVER_DATETIME_FORMAT2 dateAsString:stringObject];
    //    return date;
}

- (void)fillWithDictionaryElement:(id)element
{
	_errorCode = [[self getObjectFromKey:element key:@"status"] intValue];
	_errorDesc = nil;

    if (_errorCode != ERROR_CODE_SUCCESS /* 0 */)
    {
        switch (_errorCode)
        {
            case 1:
                _errorDesc = @"Token is invalid";
                break;
            case 100:
                _errorDesc = @"Technical error";
                break;
            case 101:
                _errorDesc = @"Invalid param";
                break;
        }
    }
}

- (BOOL)hasError
{
    return _errorCode != ERROR_CODE_SUCCESS;
}

- (NSError *)error
{
    if (![self hasError])
        return nil;
    //return [self errorVar([_errorDesc length] ? _errorDesc : @"Unknown server error", _errorCode) ];
  return  [[NSError alloc]initWithDomain:@"ERROR" code:0 userInfo:nil];
}


@end

