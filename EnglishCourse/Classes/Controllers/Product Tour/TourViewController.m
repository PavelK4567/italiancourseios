//
//  TourViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "TourViewController.h"
#import "LevelSelectionViewController.h"

@interface TourViewController (){
  NSInteger numOfPage;
}

@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) UIPageControl *mainPageControl;
@end


@implementation TourViewController

- (void)viewDidLoad {
  [super viewDidLoad];
 
  self.view.backgroundColor = [UIColor clearColor];
  UIView* backView = [[UIView alloc] initWithFrame:self.view.frame];
  //backView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
  backView.backgroundColor = [UIColor whiteColor];
  [self.view insertSubview:backView atIndex:0];
  numOfPage = 6;
  self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH, LM_DEMO_SCROLL_HEIGHT)];
  //[self.mainScrollView scrollRectToVisible:CGRectMake(0, 0, 320, LM_SCROLL_HEIGHT) animated:YES];
  [self.mainScrollView setBackgroundColor:[UIColor clearColor]];
  [self.mainScrollView setDelegate:self];
  [self.mainScrollView setContentMode:UIViewContentModeCenter];
  self.mainScrollView.userInteractionEnabled = YES;
  self.mainScrollView.contentSize = CGSizeMake(LM_WIDTH*numOfPage, LM_DEMO_SCROLL_HEIGHT);
  //[self.mainScrollView setScrollEnabled:NO];
  [self.mainScrollView setShowsHorizontalScrollIndicator:FALSE];
  [self.mainScrollView setShowsVerticalScrollIndicator:FALSE];
  self.mainScrollView.scrollsToTop = NO;
  self.mainScrollView.pagingEnabled = YES;
  [self.view addSubview:self.mainScrollView];
  
  self.mainPageControl = [[UIPageControl alloc] init];
  self.mainPageControl.frame = CGRectMake(0,self.view.mHeight-60,self.view.mWidth,60);
  
  self.mainPageControl.numberOfPages = numOfPage;
  self.mainPageControl.currentPage = 0;
  self.mainPageControl.backgroundColor = [UIColor clearColor];
  self.mainPageControl.currentPageIndicatorTintColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"];
  self.mainPageControl.pageIndicatorTintColor = [APPSTYLE colorForType:@"Italy_Main_Grey_Color_3"];
  [self.view addSubview:self.mainPageControl];
  
  UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  button.tintColor = [UIColor blackColor];
  [button addTarget:self
             action:@selector(closeProductTour:)
   forControlEvents:UIControlEventTouchUpInside];
  //[button setTitle:@"X" forState:UIControlStateNormal];
  [APPSTYLE applyIcon:@"ic_close_black" toButton:button];
  button.frame = CGRectMake(1.0, 1.0, 44.0, 44.0);
  [self.view addSubview:button];
  
  if(IS_IPHONE4){
    [self.mainPageControl setFrame:CGRectMake(0,self.view.mHeight-40,self.view.mWidth,60)];
  }
  
}


- (void)viewWillDisappear:(BOOL)animated {
  [self.navigationController setNavigationBarHidden:NO animated:animated];
  [[UIApplication sharedApplication] setStatusBarHidden:NO];
  [super viewWillDisappear:animated];
}
- (void)viewWillAppear:(BOOL)animated {
  [self.navigationController setNavigationBarHidden:YES animated:animated];
  [[UIApplication sharedApplication] setStatusBarHidden:YES];
  [super viewWillAppear:animated];
  [self addImages];
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)addImages{
    UIImage *imgA = [UIImage imageNamed:@"tutorial_img_1"];
    UIImage *imgB = [UIImage imageNamed:@"tutorial_img_2"];
    
    
    UIImageView *holderA = [[UIImageView alloc] initWithImage:imgA];
    [holderA setOrigin:CGPointZero];
    [holderA setSize:CGSizeMake(self.view.size.width, self.view.size.height)];
    [self.mainScrollView addSubview:holderA];
    
    UIImageView *holderB = [[UIImageView alloc] initWithImage:imgB];
    [holderB setOrigin:CGPointMake(LM_WIDTH, 0)];
    [holderB setSize:CGSizeMake(self.view.size.width, self.view.size.height)];
    [self.mainScrollView addSubview:holderB];
    
    for(int i=0; i<4;i++){
        //for(int j=0; j<3;j++){
        NSString *nameOfImage = [NSString  stringWithFormat:@"demo%d",i+1];
        if(IS_IPHONE5){
            nameOfImage = [NSString  stringWithFormat:@"demo%d_iphone5",i+1];
        }
        UIImageView *tempImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:nameOfImage]];
        [tempImageView setOrigin:CGPointMake((LM_WIDTH*(i + 2)), 0)];
        [self.mainScrollView addSubview:tempImageView];
        //}
    }
}
#pragma mark UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  CGFloat pageWidth = self.mainScrollView.frame.size.width;
  float fractionalPage = self.mainScrollView.contentOffset.x / pageWidth;
  NSInteger page = lround(fractionalPage);
  self.mainPageControl.currentPage = page; 
}

#pragma mark ProductTour
-(void) closeProductTour:(UIButton*)sender
{
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"Demonstration"]){
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Demonstration"];
    LevelSelectionViewController *controler = [LevelSelectionViewController new];
    [APP_DELEGATE mainController].navigationItem.title = @" ";
    [[APP_DELEGATE mainController].navigationController pushViewController:controler animated:NO];
  }
  [self dismissViewControllerAnimated:NO  completion:nil];
  
}

#pragma mark Orientations
- (NSUInteger)supportedInterfaceOrientations {
  
  return UIInterfaceOrientationMaskPortrait;
  
}

- (BOOL)shouldAutorotate {
  
  return NO;
}

@end
