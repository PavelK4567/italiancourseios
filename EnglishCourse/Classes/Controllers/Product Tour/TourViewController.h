//
//  TourViewController.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/29/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TourViewController : UIViewController<UIScrollViewDelegate>

@end
