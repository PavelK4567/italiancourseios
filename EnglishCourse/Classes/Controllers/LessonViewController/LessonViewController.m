    //
//  LessonViewController.m
//  ECPractices
//
//  Created by Dimitar Shopovski on 12/1/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//
//Dimitar wrote this yees


#import "LessonViewController.h"

@interface LessonViewController ()

@end

@implementation LessonViewController

@synthesize arrayDataSource;
@synthesize viewControllers, lmPagingControl;
@synthesize currentPage;
@synthesize isViedoActive;

- (void)loadData {
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.numberOfLines = 0;
    self.lblPoints.text = [NSString stringWithFormat:@"%ld\nPONTOS", (long)currentLevelStatistics.score];
    [self.lblPoints sizeToFit];
    
    self.lblPoints.x = self.viewNavBar.mWidth-self.lblPoints.mWidth-5;
    self.lblPoints.center = CGPointMake(self.lblPoints.center.x, self.btnCloseTitle.center.y);
    
    [PROGRESS_MANAGER setIsUnitOpen:PROGRESS_MANAGER.currentLevelIndex section:PROGRESS_MANAGER.currentSectionIndex unit:PROGRESS_MANAGER.currentUnitIndex];
}

- (void)viewDidLoad {
  [super viewDidLoad];
    [self refreshContentWithStartPage:YES];
    
    [self.viewNavBar.layer setShadowOffset:CGSizeMake(0, 0.5)];
    [self.viewNavBar.layer setShadowRadius:0.5];
    [self.viewNavBar.layer setShadowColor:[UIColor colorWithRed:118.0/255.0 green:66.0/255.0 blue:154.0/255.0 alpha:1.0].CGColor];
    [self.viewNavBar.layer setShadowOpacity:0.8];
    [self.viewNavBar setBackgroundColor:[APPSTYLE colorForType:@"Italy_Main_Red_Color"]];
    [self.view bringSubviewToFront:self.viewNavBar];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self.lblLessonTitle setTextColor:[UIColor whiteColor]];
    [self.lblLessonTitle setFont:[UIFont systemFontOfSize:14.0]];
    [self.lblPoints setTextColor:[UIColor whiteColor]];
    [self.lblPoints setFont:[UIFont systemFontOfSize:12.0]];
    
    self.scrollViewInstanceContainer.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
    
}


- (void)configureUI {

    
}


#pragma mark - 
#pragma mark - Refresh the containter

- (void)refreshContentWithStartPage:(BOOL)isStartPage {
    
    self.lblLessonTitle.hidden = YES;
    self.lmPagingControl.hidden = YES;

    [self addActivityIndicator];
    
    [self clearMainScrollView];
  
    
    [PROGRESS_MANAGER setIsUnitVisited:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
    
    [self performSelector:@selector(showTheContentAfterDelay:) withObject:[NSNumber numberWithBool:isStartPage] afterDelay:0.25];
    
    
    
//    DLog(@"not guest -> Max index - %@", [PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]]);

}

- (void)showTheContentAfterDelay:(id)state {
    
    NSNumber *num = (NSNumber *)state;
    BOOL isStartPage = [num boolValue];
    
    unitScore = 0;
    
    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:[PROGRESS_MANAGER currentSectionIndex]];
    Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:[PROGRESS_MANAGER currentUnitIndex]];
    self.arrayDataSource = [currentLesson instancesArray];
    self.lessonInfo = currentLesson;
    
    
    self.lblLessonTitle.text = [NSString stringWithFormat:@"Aula %ld: %@", (long)[self getTheOrderOfLessonInTheLevel:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]+1], [currentLesson name]];
    
    if (isStartPage) {
        
        self.currentPage = 0;
        
    }
    else {
        
        self.currentPage = [PROGRESS_MANAGER getLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
        
    }
    
    lastPage = self.currentPage;
    
    NSInteger dotNumber;
    NSArray *arrayRearranged = [self rearrangeTheArray:self.arrayDataSource];
    
    if (self.currentPage == [arrayRearranged count]) {
        isSummaryPage = YES;
        self.arrayDataSource = arrayRearranged;
        dotNumber = [self.arrayDataSource count];
    }
    else {

        dotNumber = lastPage;
//        dotNumber = [self convertProgressValueToDotNumber:lastPage array:self.arrayDataSource];
        self.arrayDataSource = arrayRearranged;

    }

    NSInteger dotsNumber = [self getNumberOfDots:self.arrayDataSource];
    
    [self createControl:dotsNumber withInstances:self.arrayDataSource andCurrentPage:dotNumber];
    
    
    self.lblLessonTitle.hidden = NO;
    self.lmPagingControl.hidden = NO;
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.numberOfLines = 0;
    self.lblPoints.text = [NSString stringWithFormat:@"%ld\nPONTOS", (long)currentLevelStatistics.score];
    [self.lblPoints sizeToFit];
    
    self.lblPoints.x = self.viewNavBar.mWidth-self.lblPoints.mWidth-5;
    self.lblPoints.center = CGPointMake(self.lblPoints.center.x, self.btnCloseTitle.center.y);
    
    isSummaryPage = NO;
    isViedoActive = NO;
    isRefreshInProgress = YES;
    self.scrollViewInstanceContainer.scrollEnabled = NO;
    
    self.currentPage = dotNumber;
    lastPage = self.currentPage;
    
    [self initialScroll:self.arrayDataSource];
    
    [self setNextButtonTitle:self.currentPage];
    
    [self removeActivityIndicator];

    [self setTheMaximumLessonUserCanAccess];
}

#pragma mark - Rearrange the array

- (NSArray *)rearrangeTheArray:(NSArray *)inputArray {
        
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
    
    int index = 0;
    int numberOfInstances = 0;
    BaseInstance *instance = nil;
    
    for (int i=0; i<inputArray.count; i++) {
        
        instance = (BaseInstance *)[inputArray objectAtIndex:i];
        [instance setInstanceNumber:i];
        
        if ([instance isKindOfClass:[MultipleChoiceImage class]]) {
            
            //TO DO: write the code
            
            if ([(MultipleChoiceImage *)instance exposureSection] != nil) {
                
                MultipleChoiceImageExposureObject *exposureObject = [MultipleChoiceImageExposureObject new];
                
                exposureObject.instructionText = [[(MultipleChoiceImage *)instance exposureSection] instructionText];
                exposureObject.type = [(MultipleChoiceImage *)instance type];
                exposureObject.instructionSound = [[(MultipleChoiceImage *)instance exposureSection] instructionSound];
                exposureObject.wordMultipleChoiceImagesArray = [(MultipleChoiceImage *)instance wordMultipleChoiceImagesArray];
                exposureObject.title = [(MultipleChoiceImage *)instance title];
                [exposureObject setInstanceNumber:i];
                [exposureObject setPageNumber:0];
                [exposureObject setIsExposure:YES];
                
                [tempArray addObject:exposureObject];
                index++;
                numberOfInstances++;
                
            }
            
            
            NSArray *arrayPracticeSections = (NSArray *)[[(MultipleChoiceImage *)instance practiceSection] practiceSections];
            MultipleChoiceImagePractice *practice = nil;
            
            for (int j=0; j<arrayPracticeSections.count; j++) {
                
                practice = (MultipleChoiceImagePractice *)[arrayPracticeSections objectAtIndex:j];
                
                MultipleChoiceImagePracticeObject *practiceObject = [MultipleChoiceImagePracticeObject new];
                
                practiceObject.instructionText = [[(MultipleChoiceImage *)instance exposureSection] instructionText];
                practiceObject.instructionSound = [[(MultipleChoiceImage *)instance exposureSection] instructionSound];
                practiceObject.wordMultipleChoiceImagesArray = [(MultipleChoiceImage *)instance wordMultipleChoiceImagesArray];
                practiceObject.question = practice.question;
                practiceObject.answer = practice.answer;
                practiceObject.audio = practice.audio;
                practiceObject.correctFeedback = practice.correctFeedback;
                practiceObject.incorrectFeedback = practice.correctFeedback;
                practiceObject.type = [(MultipleChoiceImage *)instance type];

                
                [practiceObject setInstanceNumber:i];
                [practiceObject setPageNumber:j+1];
                
                [tempArray addObject:practiceObject];
                
            }
            
            index+=arrayPracticeSections.count;
            numberOfInstances+=arrayPracticeSections.count;
            
        }
        else {
            
            [instance setPageNumber:0];
            [tempArray addObject:instance];
            index++;
            numberOfInstances++;

        }
    
    }

    return tempArray;
    
}

- (NSInteger)getNumberOfDots:(NSArray *)inputArray {
    
    int numberOfInstances = 0;
    
    for (BaseEntity *instance in inputArray) {
        
        if ([instance isKindOfClass:[MultipleChoiceImage class]]) {
            
            if ([(MultipleChoiceImage *)instance exposureSection] != nil) {
                
                numberOfInstances++;
                
            }
            
            NSArray *arrayPracticeSections = (NSArray *)[[(MultipleChoiceImage *)instance practiceSection] practiceSections];
            numberOfInstances+=arrayPracticeSections.count;
            
        }
        else if ([instance isKindOfClass:[Practice class]]) {
            
            numberOfInstances++;
            
        }
        else {

            numberOfInstances++;
            
        }
        
    }
    
    return numberOfInstances;
    
}

#pragma mark - Convert the progress number into dot number

- (NSInteger)convertProgressValueToDotNumber:(NSInteger)progressNumber array:(NSArray *)arraySource {
    
    NSInteger dotNumber = 0;
    BaseInstance *baseInstance;
    
    for (int i=0; i<progressNumber; i++) {
        
        baseInstance = [arraySource objectAtIndex:i];
        if (baseInstance.type == kInstanceMultipleChoiceImage) {
            
            if ([(MultipleChoiceImage *)baseInstance exposureSection] != nil) {
                
                dotNumber+=1;
            }
            
            MultipleChoiceImagePracticeSection *practiceSection = [(MultipleChoiceImage *)baseInstance practiceSection];
            NSArray *arrayPractices = [practiceSection practiceSections];
            dotNumber+=[arrayPractices count];
        }
        else {
            
            dotNumber+=1;
        }
        
    }
    
    return dotNumber;
}


#pragma mark - Close lesson controller

- (IBAction)closeLessonController:(id)sender {
    
    //set the current page in progress manager
    
    if (isSummaryPage) {
        
        [PROGRESS_MANAGER setLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instanceIndex:kNumberOfPages];
    }
    else {
        
//        BaseInstance *currentInstanceInfo = [self.arrayDataSource objectAtIndex:currentPage];

        [PROGRESS_MANAGER setLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instanceIndex:self.currentPage];
    }
    
    if (currentPage < kNumberOfPages && currentPage != -1) {
        
        ECVehicleDisplay *instance = (ECVehicleDisplay *)[self.viewControllers objectAtIndex:currentPage];
        
        if ([instance respondsToSelector:@selector(disableVehicleInteractions)]) {
            
            [instance disableVehicleInteractions];
            
        }
        
        if ([instance isKindOfClass:[SimplePageTextAndVideoScreen class]]) {
            [((SimplePageTextAndVideoScreen *)instance) stopPlaying];
          
        } else if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
            [((MultipleChoiceTextProgressiveChatScreen *)instance) stopPlaying];
        }

    }
    
    if ([PROGRESS_MANAGER isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]]) {
        
        [self updateProgres:false level:[PROGRESS_MANAGER currentLevelIndex] module:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] isSummary:isSummaryPage];
    }
    
    else {
        [self updateProgres:false level:[PROGRESS_MANAGER currentLevelIndex] module:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] isSummary:isSummaryPage];
    }
    
    NSDictionary *lessonProgressionData = [PROGRESS_MANAGER lessonSummary:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
    
    NSInteger skippedQuestions = [[lessonProgressionData objectForKey:@"numberOfSkippedQuestions"] integerValue];
    NSInteger correctAnswers = [[lessonProgressionData objectForKey:@"numberOfCorrectAnswers"] integerValue];
    NSInteger incorrectAnswers = [[lessonProgressionData objectForKey:@"numberOfIncorrectAnswers"] integerValue];
    
    [self updateUserProgression:[NSString stringWithFormat:@"%ld", (long)skippedQuestions] withCorrect:[NSString stringWithFormat:@"%ld", (long)correctAnswers] withIncorrect:[NSString stringWithFormat:@"%ld", (long)incorrectAnswers] moduleIndex:[PROGRESS_MANAGER currentSectionIndex] unitIndex:[PROGRESS_MANAGER currentUnitIndex]];
    
    [self removAllObserversInVehicles];
    
    [self clearMainScrollView];
    
    [APP_DELEGATE backToLevelPage];
    
//    DLog(@"CLOSE MAX - %@", [PROGRESS_MANAGER maxUnitIndexForLevel:[PROGRESS_MANAGER currentLevelIndex]]);

    
}

- (void)closeFinishLevel {
    
    //set the current page in progress manager
    
    if (isSummaryPage) {
        
        [PROGRESS_MANAGER setLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instanceIndex:kNumberOfPages];
    }
    else {
        
        //        BaseInstance *currentInstanceInfo = [self.arrayDataSource objectAtIndex:currentPage];
        
        [PROGRESS_MANAGER setLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instanceIndex:self.currentPage];
    }
    
    if (currentPage < kNumberOfPages && currentPage != -1) {
        
        ECVehicleDisplay *instance = (ECVehicleDisplay *)[self.viewControllers objectAtIndex:currentPage];
        
        if ([instance respondsToSelector:@selector(disableVehicleInteractions)]) {
            
            [instance disableVehicleInteractions];
            
        }
        
        
        if ([instance isKindOfClass:[SimplePageTextAndVideoScreen class]]) {
            [((SimplePageTextAndVideoScreen *)instance) stopPlaying];

        } else if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
            [((MultipleChoiceTextProgressiveChatScreen *)instance) stopPlaying];
        }
        
    }
    
    if ([PROGRESS_MANAGER isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]]) {
        
        [self updateProgres:true level:[PROGRESS_MANAGER currentLevelIndex] module:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] isSummary:YES];
    }
    
    NSDictionary *lessonProgressionData = [PROGRESS_MANAGER lessonSummary:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
    
    NSInteger skippedQuestions = [[lessonProgressionData objectForKey:@"numberOfSkippedQuestions"] integerValue];
    NSInteger correctAnswers = [[lessonProgressionData objectForKey:@"numberOfCorrectAnswers"] integerValue];
    NSInteger incorrectAnswers = [[lessonProgressionData objectForKey:@"numberOfIncorrectAnswers"] integerValue];
    
    [self updateUserProgression:[NSString stringWithFormat:@"%ld", (long)skippedQuestions] withCorrect:[NSString stringWithFormat:@"%ld", (long)correctAnswers] withIncorrect:[NSString stringWithFormat:@"%ld", (long)incorrectAnswers] moduleIndex:[PROGRESS_MANAGER currentSectionIndex] unitIndex:[PROGRESS_MANAGER currentUnitIndex]];
    
    [self removAllObserversInVehicles];
    
    [self clearMainScrollView];
    
    [APP_DELEGATE backToLevelPageAfterFinishThePrevious];
    
}

#pragma mark - Scroll view functions

- (void)loadScrollViewWithPage:(NSInteger)page {
    
    if (page < 0) return;
    if (page >= kNumberOfPages) return;

    
    UIView *instance = [self.viewControllers objectAtIndex:page];
    
    BaseInstance *currentInstanceInfo = [self.arrayDataSource objectAtIndex:page];
    
    if (instance == (UIView *)[NSNull null]) {
        
        if ([currentInstanceInfo isKindOfClass:[SimplePageProfile class]]) {
            
            instance = [[SimplePageProfileScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(SimplePageProfileScreen *)instance setDictVehicleInfo:(SimplePageProfile *)currentInstanceInfo];
            [(SimplePageProfileScreen *)instance drawGUI];
            [(SimplePageProfileScreen *)instance initializeElements];

            
        }
        else if ([currentInstanceInfo isKindOfClass:[Writing class]]) {
            
            instance = [[WritingVehicleScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(WritingVehicleScreen *)instance setDictVehicleInfo:(Writing *)currentInstanceInfo];
            [(ECVehicleDisplay *)instance feedbackView].correctFeedback = [(Writing *)currentInstanceInfo correctFeedback];
            [(ECVehicleDisplay *)instance feedbackView].incorrectFeedback = [(Writing *)currentInstanceInfo incorrectFeedback];
            
            [(WritingVehicleScreen *)instance drawGUI];
            [(WritingVehicleScreen *)instance initializeElements];
            
            if (page == self.currentPage) {
                
                [self performSelector:@selector(writingScreenDelay:) withObject:instance afterDelay:0.25];
                
            }

        }
        else if ([currentInstanceInfo isKindOfClass:[MultipleChoiceText class]]) {
            
            instance = [[ECMultipleChoiceSimple alloc] initWithFrame:LM_VEHICLE_RECT];
            [(ECMultipleChoiceSimple *)instance setDictVehicleInfo:(MultipleChoiceText *)currentInstanceInfo];
            [(ECVehicleDisplay *)instance feedbackView].correctFeedback = [(MultipleChoiceText *)currentInstanceInfo correctFeedback];
            [(ECVehicleDisplay *)instance feedbackView].incorrectFeedback = [(MultipleChoiceText *)currentInstanceInfo incorrectFeedback];
            
            [(ECMultipleChoiceSimple *)instance initializeElements];

            
        }
        else if ([currentInstanceInfo isKindOfClass:[SimplePageBubbleDialog class]]) {
            
            instance = [[SimplePageDialogScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(SimplePageDialogScreen *)instance setDictVehicleInfo:(SimplePageBubbleDialog *)currentInstanceInfo];
            
            [((SimplePageDialogScreen *)instance) initData];

        }
        else if ([currentInstanceInfo isKindOfClass:[SimplePageTextAndVideo class]]) {
            
            instance = [[SimplePageTextAndVideoScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(SimplePageTextAndVideoScreen *)instance setDictVehicleInfo:(SimplePageTextAndVideo *)currentInstanceInfo];
            [(SimplePageTextAndVideoScreen *)instance drawGUI];
            [(SimplePageTextAndVideoScreen *)instance initData];
            
        }
        else if ([currentInstanceInfo isKindOfClass:[FramesPresentationDialog class]]) {
            
            instance = [[FramesPresentationDialogScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(FramesPresentationDialogScreen *)instance setDictVehicleInfo:(FramesPresentationDialog *)currentInstanceInfo];
            [((FramesPresentationDialogScreen *)instance) loadData];
        }
        else if ([currentInstanceInfo isKindOfClass:[MultipleChoiceTextProgressiveChat class]]) {
            
            instance = [[MultipleChoiceTextProgressiveChatScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(MultipleChoiceTextProgressiveChatScreen *)instance setDictVehicleInfo:(MultipleChoiceTextProgressiveChat *)currentInstanceInfo];

            [((MultipleChoiceTextProgressiveChatScreen *)instance) setDelegate:self];
            
            [(MultipleChoiceTextProgressiveChatScreen *)instance setupInstance];
            [(MultipleChoiceTextProgressiveChatScreen *)instance initData];
            
            if ([PROGRESS_MANAGER isInstanceFinishedWithCorrectAnswer:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber pageNumber:page]) {
                
                ((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed = YES;
            }
            
        }
        else if ([currentInstanceInfo isKindOfClass:[MultipleChoiceImageExposureObject class]]) {
            
            instance = [[MultipleChoiceImageExposureScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(MultipleChoiceImageExposureScreen *)instance setDictVehicleInfo:(MultipleChoiceImageExposureObject *)currentInstanceInfo];
            [(MultipleChoiceImageExposureScreen *)instance initializeElements];

            [(MultipleChoiceImageExposureScreen *)instance setExposureDelegate:self];
            
            if (page == self.currentPage) {
                
                [self performSelector:@selector(startExposureAfterDelay:) withObject:instance afterDelay:0.25];
                
            }

        }
        else if ([currentInstanceInfo isKindOfClass:[FillTheMissingWords class]]) {

                instance = [[MissingWordContainerViewL1 alloc] initWithFrame:LM_VEHICLE_RECT];
                [((MissingWordContainerViewL1 *)instance) setDictVehicleInfo:currentInstanceInfo];
                [(MissingWordContainerViewL1 *)instance feedbackView].correctFeedback = [(FillTheMissingWords *)currentInstanceInfo correctFeedback];
                [((MissingWordContainerViewL1 *)instance) setRenderCompletedViews:NO];
                [((MissingWordContainerViewL1 *)instance) loadDataForIndex:0];
        
        }
        else if ([currentInstanceInfo isKindOfClass:[MultipleChoiceImagePracticeObject class]]) {
            
            instance = [[MultipleChoiceImagePracticeScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(MultipleChoiceImagePracticeScreen *)instance setDictVehicleInfo:(MultipleChoiceImagePracticeObject *)currentInstanceInfo];
            [(ECVehicleDisplay *)instance feedbackView].correctFeedback = [(MultipleChoiceImagePracticeObject *)currentInstanceInfo correctFeedback];
            [(ECVehicleDisplay *)instance feedbackView].incorrectFeedback = [(MultipleChoiceImagePracticeObject *)currentInstanceInfo incorrectFeedback];
            [(MultipleChoiceImagePracticeScreen *)instance initializeElements];

            
            if (page == self.currentPage) {
                
                [self performSelector:@selector(multiChoiceImagePracticeDelay:) withObject:instance afterDelay:0.25];
                
            }

        }
        else if ([currentInstanceInfo isKindOfClass:[Practice class]]) {
            
            instance = [[PracticeQuestionScreen alloc] initWithFrame:LM_VEHICLE_RECT];
            [(PracticeQuestionScreen *)instance setDictVehicleInfo:(Practice *)currentInstanceInfo];
        }
        else {
            
            instance = [[SimplePageProfileScreen alloc] initWithFrame:LM_VEHICLE_RECT];
        }
        
        if (page == self.currentPage) {
            
            if ([instance isKindOfClass:[PracticeQuestionScreen class]]) {
                
            }
            else {
                
                if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
                        [self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];

                }
                
                
                NSInteger pageNumber;
                if (currentInstanceInfo.type == kInstanceMultipleChoiceImage && currentInstanceInfo.pageNumber !=0) {
                    
                    pageNumber = currentInstanceInfo.pageNumber-1;
                }
                else {
                    
                    pageNumber = currentInstanceInfo.pageNumber;
                    
                }
                if (![PROGRESS_MANAGER isInstanceVisited:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber page:pageNumber]) {
                    
                    
                    if ([self isTheInstancePresentation:currentInstanceInfo]) {
                        
                        [self updateInstanceStatistics:currentInstanceInfo withStatus:-1];
                    }
                    else {
                        
                        [self updateInstanceStatistics:currentInstanceInfo withStatus:0];
                        
                    }
                    
                }
            }
            
        }
        
        
        [(ECVehicleDisplay *)instance setInstanceOrder:page];
        [(ECVehicleDisplay *)instance setVehicleDelegate:self];
        if ([instance isKindOfClass:[PracticeQuestionScreen class]]) {
            
            [instance setBackgroundColor:[APPSTYLE colorForType:@"Bg_Multichoice_Practice"]];
            
        }
        else {
        
            instance.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
        
        }
        
        [self.scrollViewInstanceContainer addSubview:instance];
        [self.viewControllers replaceObjectAtIndex:page withObject:instance];
        
        ////////
        //show layout for completed
        
        if ([instance respondsToSelector:@selector(showLayoutForCompletedInstance)]) {
            
            NSInteger pageNumber;
            if (currentInstanceInfo.type == kInstanceMultipleChoiceImage && currentInstanceInfo.pageNumber != 0) {
                
                pageNumber = currentInstanceInfo.pageNumber-1;
            }
            else {
                
                pageNumber = currentInstanceInfo.pageNumber;
                
            }
            
            if (currentInstanceInfo.type == kInstanceMultipleChoiceImage) {
                
                if ([PROGRESS_MANAGER isPageFinishedWithCorrectAnswer:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber pageNumber:pageNumber]) {
                    
                    [(ECVehicleDisplay *)instance setIsCompleted:YES];
                    [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
                }
                
            }
            else {
                
                if ([PROGRESS_MANAGER isInstanceFinishedWithCorrectAnswer:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber pageNumber:pageNumber]) {
                    
                    if (currentInstanceInfo.type != kInstanceMultipleChoiceTextProgressiveChat) {
                        [(ECVehicleDisplay *)instance setIsCompleted:YES];
                        [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
                    } else {
                        ((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed = YES;
                    }
                    
                }
            }
        }
        
        ///////
        
    }
    else {
        
        if ([(ECVehicleDisplay *)instance feedbackView] != nil && ![instance isKindOfClass:[PracticeQuestionScreen class]])
            [(ECVehicleDisplay *)instance feedbackView].hidden = YES;
        
        if ([instance respondsToSelector:@selector(resetTheInstance)] && ![(ECVehicleDisplay *)instance isCompleted] && ![instance isKindOfClass:[PracticeQuestionScreen class]]) {
            
            [(ECVehicleDisplay *)instance resetTheInstance];
        }
        if ([instance isKindOfClass:[PracticeQuestionScreen class]]) {
            
            [(ECVehicleDisplay *)instance performSelector:@selector(resetTheInstance) withObject:nil afterDelay:0.5];

        }
        
        if ([(ECVehicleDisplay *)instance isKindOfClass:[WritingVehicleScreen class]]) {
            
            if (self.currentPage == page && !isSummaryPage) {
                
                [self writingScreenDelay:(BaseInstance *)instance];
                
            }
        }
        
        if ([instance respondsToSelector:@selector(showLayoutForCompletedInstance)]) {

            NSInteger pageNumber;
            if (currentInstanceInfo.type == kInstanceMultipleChoiceImage && currentInstanceInfo.pageNumber != 0) {
                
                pageNumber = currentInstanceInfo.pageNumber-1;
            }
            else {
                
                pageNumber = currentInstanceInfo.pageNumber;
                
            }
            
            if (currentInstanceInfo.type == kInstanceMultipleChoiceImage) {
                
                if ([PROGRESS_MANAGER isPageFinishedWithCorrectAnswer:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber pageNumber:pageNumber]) {
                    
                    [(ECVehicleDisplay *)instance setIsCompleted:YES];
                    [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
                }
                else {
                    
                    if ([(ECVehicleDisplay *)instance respondsToSelector:@selector(resetTheInstance)]) {
                        
                        if ([(ECVehicleDisplay *)instance isCompleted]) {
                            
                            [(ECVehicleDisplay *)instance resetTheInstance];
                        }
                        
                    }
                    
                }
                
                
            }
            else {
                
                if ([PROGRESS_MANAGER isInstanceFinishedWithCorrectAnswer:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:currentInstanceInfo.instanceNumber pageNumber:pageNumber]) {
                    
                    [(ECVehicleDisplay *)instance setIsCompleted:YES];
                    
                    if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]] && ((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed && !((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                        ((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
                    }
                    
                    [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
                }
                else {
                    
                    if ([(ECVehicleDisplay *)instance respondsToSelector:@selector(resetTheInstance)]) {
                        
                        if ([(ECVehicleDisplay *)instance isCompleted]) {
                            
                            [(ECVehicleDisplay *)instance resetTheInstance];
                        }
                        
                    }
                    
                }
            }
        }
        
    }
    
}

- (void)startExposureAfterDelay:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }
    
    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    
    if ([currentBaseInstance isKindOfClass:[MultipleChoiceImageExposureScreen class]] && [instance isEqual:currentBaseInstance]) {
        
        if (![(ECVehicleDisplay *)instance isCompleted]) {
            [(MultipleChoiceImageExposureScreen *)instance startAnimatingTheElements];
        }
    
    }
}

- (void)multiChoiceImagePracticeDelay:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }
    
    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    
    if ([currentBaseInstance isKindOfClass:[MultipleChoiceImagePracticeScreen class]] && [instance isEqual:currentBaseInstance]) {
    
        if ([(MultipleChoiceImagePracticeScreen *)instance respondsToSelector:@selector(playSoundQuestion:)]) {
         
            [(MultipleChoiceImagePracticeScreen *)instance playSoundQuestion:nil];
            
        }
    }
}

- (void)multiChoiceTextProgressiveChatDelay:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }

    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    
//    if (((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed && ((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted) {
//        ((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
//    }
    
    if ([currentBaseInstance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]] && [instance isEqual:currentBaseInstance]) {
  
        [((MultipleChoiceTextProgressiveChatScreen *)instance) playInitialSound];
        
    }
}

- (void)multiChoiceTextProgressiveChatDelayNotCompleted:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }
    
    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    
    if ([currentBaseInstance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]] && [instance isEqual:currentBaseInstance]) {
        ((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed = YES;
        [((MultipleChoiceTextProgressiveChatScreen *)instance) playInitialSound];
    }
}

- (void)writingScreenDelay:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }
    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    if ([currentBaseInstance isKindOfClass:[WritingVehicleScreen class]] && [currentBaseInstance isEqual:instance]) {
        [((WritingVehicleScreen *)instance) playIntroSound:nil];
        [((WritingVehicleScreen *)instance) showKeyboard];
        
    }

}

- (void)simplePageScreenDelay:(BaseInstance *)instance {
    
    if (self.currentPage >= kNumberOfPages) {
        return;
    }
    
    BaseInstance *currentBaseInstance = [self.viewControllers objectAtIndex:self.currentPage];
    
    if ([currentBaseInstance isKindOfClass:[SimplePageProfileScreen class]] && [instance isEqual:currentBaseInstance]) {

        [((SimplePageProfileScreen *)instance) playIntroSound:nil];
    }
}

#pragma mark - ScrollView Delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    NSLog(@"scrollViewDidScroll");
//    if (scrollView == _scrollViewInstanceContainer) {
//                
//        if (isRefreshInProgress) {
//            return;
//        }
////        if (isSummaryPage) {
////            return;
////        }
//        if (isPracticeActive) {
//            return;
//        }
//        if (pageControlUsed) {
//            return;
//        }
//        
//        int page;
//        CGFloat pageWidth = self.scrollViewInstanceContainer.frame.size.width;
//        page = floor((self.scrollViewInstanceContainer.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//        
//        
//        if (lastPage!=page && lastPage != -1) {
//            
//            if (!isSummaryPage) {
//                
//                if (lastPage < kNumberOfPages) {
//                ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:lastPage];
//                
//                if ([instance respondsToSelector:@selector(disableVehicleInteractions)]) {
//                    
//                    [instance disableVehicleInteractions];
//                    
//                }
//                
//                if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceImageExposureScreen class]]) {
//                    
//                    if (![(ECVehicleDisplay *)instance isCompleted] && [(ECVehicleDisplay *)instance animationInProgress]) {
//                        
//                        [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
//                        
//                    }
//                    
//                } else if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
//                    
//                    if (![(ECVehicleDisplay *)instance isCompleted]) {
//                        
//                        [(MultipleChoiceTextProgressiveChatScreen *)instance stopPlaying];
//                        
//                    }
//                    
//                } else if ([(ECVehicleDisplay *)instance isKindOfClass:[WritingVehicleScreen class]]) {
//                    
//                    [[(WritingVehicleScreen *)instance txtTranslationText] resignFirstResponder];
//                    
//                } else if ([instance isKindOfClass:[SimplePageTextAndVideoScreen class]]) {
//                    
//                    if ([((SimplePageTextAndVideoScreen *)instance) isVideoPlaying]) {
//                        
//                        [((SimplePageTextAndVideoScreen *)instance) stopPlaying];
//                        
//                    }
//                }
//                }
//            }
//            
//            lastPage=page;
//            
//            
//            self.currentPage = page;
//            
//            [self setNextButtonTitle:self.currentPage];
//            
//            if (self.currentPage < 0 || self.currentPage>kNumberOfPages) {
//                return;
//            }
//            
//            [self loadScrollViewWithPage:self.currentPage-1];
//            if (self.currentPage < kNumberOfPages) {
//                [self loadScrollViewWithPage:self.currentPage];
//                [self loadScrollViewWithPage:self.currentPage+1];
//            }
//            [self.lmPagingControl refreshControlWithCurrentPage:self.currentPage];
//            
//            
//            if (page == kNumberOfPages) {
//                
//                if (!isSummaryPage) {
//                    
//                    isSummaryPage = YES;
//                    [self createSummaryPageWithIndex:page];
//                    
//                }
//            }
//            else {
//                
//                if (page<kNumberOfPages) {
//                    
//                    ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:page];
//                    
//                    if ((NSNull *)instance != [NSNull null]) {
//                        
//                        if (![instance isKindOfClass:[PracticeQuestionScreen class]]) {
//                            
//                            if (instance.feedbackView)
//                                [(ECVehicleDisplay *)instance feedbackView].hidden = YES;
//                            
//                            if ([instance isKindOfClass:[MultipleChoiceImageExposureScreen class]]) {
//                                
//                                if (!instance.isCompleted) {
//                                    
//                                    [self performSelector:@selector(startExposureAfterDelay:) withObject:instance afterDelay:0.25];
//                                    
//                                }
//                            }
//                        }
//                        
//                        if ([instance isKindOfClass:[MultipleChoiceImagePracticeScreen class]]) {
//                            [self performSelector:@selector(multiChoiceImagePracticeDelay:) withObject:instance afterDelay:0.25];
//                        }
//                        
//                        if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
//                            [self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
//                            if (![(ECVehicleDisplay *)instance isCompleted]) {
//                                
//                                [(LMCustomPagingControll *)scrollView setUserInteractionEnabled:YES];
//                                //                    [self disableScrolling:nil];
//                                
//                            }
//                        }
//                        
//                        if ([instance isKindOfClass:[WritingVehicleScreen class]]) {
//                            
//                            [self performSelector:@selector(writingScreenDelay:) withObject:instance afterDelay:0.0];
//                            
//                        }
//                        if ([instance isKindOfClass:[SimplePageProfileScreen class]]) {
//                            [self performSelector:@selector(simplePageScreenDelay:) withObject:instance afterDelay:0.25];
//                        }
//                        
//                    }
//                    
//                    
//                    
//                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
//                        
//                        if (page < kNumberOfPages) {
//                            
//                            BaseInstance *baseInstance = [self.arrayDataSource objectAtIndex:page];
//                            NSInteger pageNumber;
//                            if (baseInstance.type == kInstanceMultipleChoiceImage && baseInstance.pageNumber !=0) {
//                                
//                                pageNumber = baseInstance.pageNumber-1;
//                            }
//                            else {
//                                
//                                pageNumber = baseInstance.pageNumber;
//                                
//                            }
//                            
//                            if (![(ECVehicleDisplay *)[self.viewControllers objectAtIndex:page] isCompleted] && ![PROGRESS_MANAGER isInstanceVisited:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:baseInstance.instanceNumber page:pageNumber]) {
//                                
//                                if ([self isTheInstancePresentation:baseInstance]) {
//                                    
//                                    [self updateInstanceStatistics:baseInstance withStatus:-1];
//                                    
//                                }
//                                else {
//                                    
//                                    [self updateInstanceStatistics:baseInstance withStatus:0];
//                                    
//                                }
//                            }
//                            
//                        }
//                        
//                    });
//                    
//                    
//                }//end page<kNumberOfPages
//                
//                
//                
//            }//end else
//
//        
//        }
//        else {
//            return;
//        }
//        
//        
//    }
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
//    pageControlUsed = NO;
//    
//    if (isSummaryPage) {
//        
//        CGFloat pageWidth = self.scrollViewInstanceContainer.frame.size.width;
//        int page = floor((self.scrollViewInstanceContainer.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
//
//        if (page != kNumberOfPages) {
//            
//            lastPage = page;
//            self.currentPage = page;
//            isSummaryPage = NO;
//
//        }
//        
//        [self setNextButtonTitle:self.currentPage];
//    }
//    if (isPracticeActive) {
//        
//        [self loadScrollViewWithPage:self.currentPage-1];
//        [self loadScrollViewWithPage:self.currentPage];
//        [self loadScrollViewWithPage:self.currentPage+1];
//        
//        
//        isPracticeActive = NO;
//        
//        [self.lmPagingControl refreshControlWithCurrentPage:self.currentPage];
//
//    }
    
}

- (void)initialScroll:(NSArray *)arraySource {
    
    kNumberOfPages = [arraySource count];
    
    if (self.viewControllers == nil) {
        self.viewControllers = [NSMutableArray new];
    }
    else {
        
        [self.viewControllers removeAllObjects];
    }
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (unsigned i = 0; i < kNumberOfPages; i++) {
        
        [controllers addObject:[NSNull null]];
        
    }
    self.viewControllers = controllers;
    
    if (self.scrollViewInstanceContainer == nil) {
        
        self.scrollViewInstanceContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, LM_WIDTH, LM_SCROLL_HEIGHT)];
        [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(0, 0, 320, LM_SCROLL_HEIGHT) animated:YES];
        
        [self.scrollViewInstanceContainer setBackgroundColor:[UIColor whiteColor]];
        [self.scrollViewInstanceContainer setDelegate:self];
        [self.scrollViewInstanceContainer setBouncesZoom:YES];
        [self.scrollViewInstanceContainer setContentMode:UIViewContentModeCenter];
        self.scrollViewInstanceContainer.userInteractionEnabled = YES;
        
        self.scrollViewInstanceContainer.contentSize = CGSizeMake(LM_WIDTH*(kNumberOfPages+1), LM_SCROLL_HEIGHT);
        
        [self.scrollViewInstanceContainer setScrollEnabled:NO];
        [self.scrollViewInstanceContainer setShowsHorizontalScrollIndicator:FALSE];
        [self.scrollViewInstanceContainer setShowsVerticalScrollIndicator:FALSE];
        
        self.scrollViewInstanceContainer.scrollsToTop = NO;
        self.scrollViewInstanceContainer.pagingEnabled = YES;
        
        UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeActionScorllContainer:)];
        [leftSwipe setDirection:UISwipeGestureRecognizerDirectionLeft];
        leftSwipe.delegate = self;
        [self.scrollViewInstanceContainer addGestureRecognizer:leftSwipe];
        
        UISwipeGestureRecognizer *rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeActionScorllContainer:)];
        [rightSwipe setDirection:UISwipeGestureRecognizerDirectionRight];
        rightSwipe.delegate = self;
        [self.scrollViewInstanceContainer addGestureRecognizer:rightSwipe];

        self.scrollViewInstanceContainer.scrollEnabled = NO;
        
        [self.view addSubview:self.scrollViewInstanceContainer];
        
    }
    else {
        
        self.scrollViewInstanceContainer.contentSize = CGSizeMake(LM_WIDTH*(kNumberOfPages+1), LM_SCROLL_HEIGHT);

    }
    
    
    if (self.currentPage == kNumberOfPages) {
        
        isSummaryPage = YES;
        [self createSummaryPageWithIndex:kNumberOfPages];
//        [[self.lmPagingControl btnNext] setTitle:Localized(@"T202") forState:UIControlStateNormal];
        [self.lmPagingControl refreshControlWithCurrentPage:kNumberOfPages-1];
    }
    
    
    [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(self.currentPage*LM_WIDTH, 0, LM_WIDTH, LM_SCROLL_HEIGHT) animated:NO];

    [self loadScrollViewWithPage:self.currentPage-1];
    [self loadScrollViewWithPage:self.currentPage];
    [self loadScrollViewWithPage:self.currentPage+1];
    
    
//    for (int i=0; i<[arraySource count]; i++) {
//        
//        [self loadScrollViewWithPage:i];
//        
//    }

    [self performSelector:@selector(enableScrollingAfterRefresh) withObject:nil afterDelay:0.5];
    
}

#pragma mark - Gesture Delegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
  if ([gestureRecognizer.view isKindOfClass:[UIScrollView class]]){
    UIScrollView *tempScrollView = (UIScrollView*)gestureRecognizer.view;
    tempScrollView.scrollEnabled = NO;
  }
    
    if (gestureRecognizer.view.tag == 6969) {
     
        return NO;
        
    }
    //return NO;
  
  return YES;
}



- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
  return NO;
}

///////////
- (void)enableScrollingAfterRefresh {
    
    isRefreshInProgress = NO;
    [self performSelector:@selector(enableUserInteractionAfterDelay) withObject:nil afterDelay:0.25];
}

- (void)enableUserInteractionAfterDelay {
    
//    self.scrollViewInstanceContainer.scrollEnabled = YES;

}

- (void)clearMainScrollView {
    
    [self.scrollViewInstanceContainer.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

//    for (UIView *subview in self.scrollViewInstanceContainer.subviews) {
//        
//        [subview removeFromSuperview];
//        
//    }
    

}

- (void)createControl:(NSInteger)numberOfPages withInstances:(NSArray *)instances andCurrentPage:(NSInteger)currentPageIndex {
        
    if (self.lmPagingControl == nil) {
        
        self.lmPagingControl = [[LMCustomPagingControll alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-kPageControlHeight, LM_WIDTH, kPageControlHeight) userProgress:currentPageIndex instances:self.arrayDataSource andDots:numberOfPages];
        self.lmPagingControl.delegate = self;
        
        [self.view addSubview:self.lmPagingControl];
    }
    else {
        
        [self.lmPagingControl recreateControllWithCurrentPage:currentPageIndex instances:self.arrayDataSource andDots:numberOfPages];
        
    }
    
}


#pragma mark - LMCustomPagingControllDelegate implementation

- (IBAction)nextButtonAction:(id)sender withSpeed:(float)speed {
    
    pageControlUsed = YES;
    [self.lmPagingControl.btnNext setUserInteractionEnabled:NO];
    //take credits
    
    if (isSummaryPage) {
        
        [self jumpToNewLesson:nil];
        
    }
    else {
        
        LMCustomPagingControll *lmPageControl = (LMCustomPagingControll *)sender;
        
        ///Show completed instance
        ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:self.lmPagingControl.pageCurrent];
        
        if ([instance respondsToSelector:@selector(disableVehicleInteractions)]) {
            
            [instance disableVehicleInteractions];
            
        }
        
        if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceImageExposureScreen class]]) {
            
            if (![(ECVehicleDisplay *)instance isCompleted] && [(ECVehicleDisplay *)instance animationInProgress]) {
                
                [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
                
            }
            
        } else if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
            
            if (![(ECVehicleDisplay *)instance isCompleted]) {
                
                [(MultipleChoiceTextProgressiveChatScreen *)instance stopPlaying];
              
            }
            
        } else if ([(ECVehicleDisplay *)instance isKindOfClass:[WritingVehicleScreen class]]) {
            
            [[(WritingVehicleScreen *)instance txtTranslationText] resignFirstResponder];
            
        } else if ([instance isKindOfClass:[SimplePageTextAndVideoScreen class]]) {
            
            if ([((SimplePageTextAndVideoScreen *)instance) isVideoPlaying]) {
                
                [((SimplePageTextAndVideoScreen *)instance) stopPlaying];
                
                ((SimplePageTextAndVideoScreen *)instance).previewImage.hidden = NO;
            }
        }
        
            
        NSInteger cPage = lmPageControl.pageCurrent+1;
        lastPage = cPage;
        
        if (cPage == kNumberOfPages) {
            
            isSummaryPage = YES;
            [self createSummaryPageWithIndex:cPage];
//            [[self.lmPagingControl btnNext] setTitle:Localized(@"T202") forState:UIControlStateNormal];
            [self.lmPagingControl refreshControlWithCurrentPage:cPage-1];
            
        }
        else {
            
            isSummaryPage = NO;

            BaseInstance *baseInstance = [self.arrayDataSource objectAtIndex:cPage];
            NSInteger pageNumber;
            if (baseInstance.type == kInstanceMultipleChoiceImage && baseInstance.pageNumber !=0) {
                
                pageNumber = baseInstance.pageNumber-1;
            }
            else {
                
                pageNumber = baseInstance.pageNumber;
                
            }
            if (![PROGRESS_MANAGER isInstanceVisited:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] instance:baseInstance.instanceNumber page:pageNumber] && ![(ECVehicleDisplay *)[self.viewControllers objectAtIndex:cPage] isCompleted]) {
                
                if ([self isTheInstancePresentation:baseInstance]) {
                    
                    [self updateInstanceStatistics:baseInstance withStatus:-1];
                    
                }
                else {
                    
                    [self updateInstanceStatistics:baseInstance withStatus:0];

                }
            }
            else {
                
//                if ([self isTheInstancePresentation:baseInstance]) {
//                    
//                    [self updateInstanceStatistics:baseInstance withStatus:-1];
//                    
//                }
//                else {
//                    
//                    [self updateInstanceStatistics:baseInstance withStatus:0];
//                    
//                }

            }
            
            
            ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:cPage];
            
            if ((NSNull *)instance != [NSNull null] && ![instance isKindOfClass:[PracticeQuestionScreen class]]) {
                
                if (instance.feedbackView)
                    [(ECVehicleDisplay *)instance feedbackView].hidden = YES;
                
                if ([instance isKindOfClass:[MultipleChoiceImageExposureScreen class]]) {
                    
                    if (!instance.isCompleted) {
                    
                        [self performSelector:@selector(startExposureAfterDelay:) withObject:instance afterDelay:0.25];
                    
                    }
                    else {
                        
                            [(LMCustomPagingControll *)sender setUserInteractionEnabled:YES];
//                            [self disableScrolling:nil];
                        
                        }
                    }
            }
            
            
                if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
                    
                    [self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
                    if (![(ECVehicleDisplay *)instance isCompleted]) {
                        
                        if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted) {
                            
                            //((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
                            
                            
                            //[((MultipleChoiceTextProgressiveChatScreen *)instance) clearProgressiveChat];
                            //[((MultipleChoiceTextProgressiveChatScreen *)instance) initData];
                            
                            if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                                [((MultipleChoiceTextProgressiveChatScreen *)instance) resetProgressiveChat];
                                [((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
                            }
                            
//                            if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed)
//                            {
                            
                            //}
                            
                            
                            //[self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
                        }
                        
                        [(LMCustomPagingControll *)sender setUserInteractionEnabled:YES];
                        
//                        [self disableScrolling:nil];
                        
                    } else {
                        
                        if (((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed && !((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                            ((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
                        }
                        
                        if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                            
                            [((MultipleChoiceTextProgressiveChatScreen *)instance) resetProgressiveChat];
                            
                            [((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
                        }
                        
                    }
                    //!((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed
                }
            
                if ([instance isKindOfClass:[MultipleChoiceImagePracticeScreen class]]) {
                    [self performSelector:@selector(multiChoiceImagePracticeDelay:) withObject:instance afterDelay:0.25];
                }
            
                if ([instance isKindOfClass:[WritingVehicleScreen class]]) {
                    [self performSelector:@selector(writingScreenDelay:) withObject:instance afterDelay:0.0];
                }
                if ([instance isKindOfClass:[SimplePageProfileScreen class]]) {
                    [self performSelector:@selector(simplePageScreenDelay:) withObject:instance afterDelay:0.25];
                }
            }
        
        if (cPage < kNumberOfPages) {
            
            self.currentPage = cPage;
            
        }
        
        [self setNextButtonTitle:cPage];
        
            [self loadScrollViewWithPage:cPage-1];
            [self loadScrollViewWithPage:cPage];
            [self loadScrollViewWithPage:cPage+1];
        
            [self setNextButtonTitle:cPage];

        
        
        
            [UIView animateWithDuration:speed animations:^{
                
                [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(cPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height) animated:NO];
                
            } completion:^(BOOL finished) {
                
                [self.lmPagingControl.btnNext setUserInteractionEnabled:YES];
                pageControlUsed = NO;
                
            }];
            
            [(ECVehicleDisplay *)instance feedbackView].hidden = YES;
            [[(ECVehicleDisplay *)instance feedbackView] settTheInfoViewOnTheOriginalPosition];
            
        
        [self.lmPagingControl refreshControlWithCurrentPage:cPage];

    
    }
}


- (void)previousButtonSelected:(id)sender withSpeed:(float)speed {
    
    pageControlUsed = YES;
    
    LMCustomPagingControll *lmPageControl = (LMCustomPagingControll *)sender;
    
    if (lmPageControl.pageCurrent-1 < 0 && !isSummaryPage) {
        return;
    }
    NSInteger cPage;
    
    if (isSummaryPage) {
        
        isSummaryPage = NO;
        cPage = lmPagingControl.pageCurrent;
        lastPage = cPage;
        [UIView animateWithDuration:speed
                              delay:0.0
                            options:0
                         animations:^{
                             [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(cPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height) animated:NO];
                         }
                         completion:^(BOOL finished) {
                             
                             self.lmPagingControl.btnPrevious.userInteractionEnabled = YES;
                             pageControlUsed = NO;

                         }];
    }
    else {
        
        ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:self.lmPagingControl.pageCurrent];
        
        if ([instance respondsToSelector:@selector(disableVehicleInteractions)]) {
            
            [instance disableVehicleInteractions];
            
        }
        
        if ([(ECVehicleDisplay *)instance isKindOfClass:[SimplePageTextAndVideoScreen class]]) {

            if ([((SimplePageTextAndVideoScreen *)instance) isVideoPlaying]) {
                
                [((SimplePageTextAndVideoScreen *)instance) stopPlaying];
                
                ((SimplePageTextAndVideoScreen *)instance).previewImage.hidden = NO;
            }
            
        }
        
        if ([(ECVehicleDisplay *)instance isKindOfClass:[WritingVehicleScreen class]]) {
            
            [[(WritingVehicleScreen *)instance txtTranslationText] resignFirstResponder];
            
        }

        cPage = lmPageControl.pageCurrent-1;
        lastPage = cPage;
        
        [self loadScrollViewWithPage:cPage-1];
        [self loadScrollViewWithPage:cPage];
        
        [UIView animateWithDuration:speed
                              delay:0.0
                            options:0
                         animations:^{

                            [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(cPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height) animated:NO];
                         }
                         completion:^(BOOL finished) {
                             
                             [(LMCustomPagingControll *)self.lmPagingControl setUserInteractionEnabled:YES];
                             [(LMCustomPagingControll *)self.lmPagingControl.btnPrevious setUserInteractionEnabled:YES];
                             pageControlUsed = NO;
                             
                        }];
    }
    
    self.currentPage = cPage;
    
    [self.lmPagingControl refreshControlWithCurrentPage:cPage];
    
    ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:cPage];
    
    if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceImageExposureScreen class]]) {
        
        if (![(ECVehicleDisplay *)instance isCompleted] && [(ECVehicleDisplay *)instance animationInProgress]) {
            
            [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];
            
        }
        if (!instance.isCompleted) {
            
            [self performSelector:@selector(startExposureAfterDelay:) withObject:instance afterDelay:0.25];
            
        }
    }
    else if ([(ECVehicleDisplay *)instance isKindOfClass:[WritingVehicleScreen class]]) {
        
        if (!instance.isCompleted) {
            
            [[(WritingVehicleScreen *)instance txtTranslationText] becomeFirstResponder];
            _actualTextView = [(WritingVehicleScreen *)instance txtTranslationText];
            
        }
        else {
            
            [(ECVehicleDisplay *)instance showLayoutForCompletedInstance];

        }
    }
    else if ([instance isKindOfClass:[MultipleChoiceImagePracticeScreen class]]) {
        [self performSelector:@selector(multiChoiceImagePracticeDelay:) withObject:instance afterDelay:0.25];
    } else if ([(ECVehicleDisplay *)instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
        [self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
        if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted) {
            [((MultipleChoiceTextProgressiveChatScreen *)instance) resetProgressiveChat];
            
            [((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
            
            //[self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
        } else {
            //((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
            
            if (((MultipleChoiceTextProgressiveChatScreen *)instance).isReplayed && !((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                ((MultipleChoiceTextProgressiveChatScreen *)instance).isCompleted = NO;
            }
            
            if (!((MultipleChoiceTextProgressiveChatScreen *)instance).isDisplayed) {
                
                [((MultipleChoiceTextProgressiveChatScreen *)instance) resetProgressiveChat];
                
                [((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
            }
            
        }
        
        [(LMCustomPagingControll *)sender setUserInteractionEnabled:YES];
        
    }
    
    if ((NSNull *)instance != [NSNull null]) {
        
        if ([instance isCompleted])
            [[self.lmPagingControl btnNext] setTitle:Localized(@"T200") forState:UIControlStateNormal];
        else
            [[self.lmPagingControl btnNext] setTitle:Localized(@"T201") forState:UIControlStateNormal];
    }

    
    
}

#pragma mark - Delegate methods diable/enable scroll, page control buttons

- (void)enableScrolling:(id)sender {
    
    DLog(@"Scroll enabled");
//    self.isScrollEnable = YES;
//    [self.scrollViewInstanceContainer setScrollEnabled:self.isScrollEnable];
    
    
}

- (void)disableScrolling:(id)sender {
    
    DLog(@"Scroll disabled");

    self.isScrollEnable = NO;
    [self.self.scrollViewInstanceContainer setScrollEnabled:self.isScrollEnable];
    
}

- (void)enablePageControlButtons {
    
    DLog(@"Page control buttons enabled");

    self.lmPagingControl.btnNext.userInteractionEnabled = YES;
    self.lmPagingControl.btnPrevious.userInteractionEnabled = YES;
    [(LMCustomPagingControll *)self.lmPagingControl setUserInteractionEnabled:YES];

}

- (void)disablePageControlButtons {
    
    DLog(@"Page control buttons disabled");

    self.lmPagingControl.btnNext.userInteractionEnabled = NO;
    self.lmPagingControl.btnPrevious.userInteractionEnabled = NO;
    
}

#pragma mark - Vehicle delegate methods

- (void)changeNavigationButtons:(id)sender {
    
    [lmPagingControl.btnNext setTitle:Localized(@"T200") forState:UIControlStateNormal];
    self.scrollViewInstanceContainer.userInteractionEnabled = YES;
//    self.scrollViewInstanceContainer.scrollEnabled = YES;
}

- (void)setIsPracticeVehicleActive:(BOOL)isActive {
    
    isPracticeActive = isActive;
    
}


#pragma mark - Instruction Delegate Mehods
- (void)addPoints:(int)points isLastAnswer:(BOOL)isLastAnswer {
    
    unitScore+=points;
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.numberOfLines = 0;
    
    DLog(@"(addPoints:(int)points isLastAnswer:(BOOL)isLastAnswer) Current points - %ld + new points - %d", (long)currentLevelStatistics.score, points);
    
    [self updateLabelWithPoints:currentLevelStatistics.score+points];
    
    [PROGRESS_MANAGER updateLevelScore:[PROGRESS_MANAGER currentLevelIndex] score:points];
    
    
    BaseInstance *currentBaseInstance = (BaseInstance *)[self.arrayDataSource objectAtIndex:self.currentPage];
    if (isLastAnswer) {
        
        [self updateInstanceStatistics:currentBaseInstance withStatus:1];

    }
 

}


#pragma mark - add points for page

- (void)addPoints:(int)points forPage:(int)pageNumber status:(int)status {
    
    unitScore+=points;

    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.numberOfLines = 0;
    
    //NSLog(@"(addPoints:(int)points forPage:(int)pageNumber status:(int)status <> Current points - %ld + new points - %d", (long)currentLevelStatistics.score, points);
    
    [self updateLabelWithPoints:currentLevelStatistics.score+points];
    
    [PROGRESS_MANAGER updateLevelScore:[PROGRESS_MANAGER currentLevelIndex] score:points];
    
    
    BaseInstance *currentBaseInstance = (BaseInstance *)[self.arrayDataSource objectAtIndex:self.currentPage];
    [self updatePageStatistics:currentBaseInstance page:pageNumber withStatus:status];

}

-(void)updatePoints:(id)sender {
    
    DLog(@"Update points");

//    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
//    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
//    self.lblPoints.numberOfLines = 0;
//    self.lblPoints.text = [NSString stringWithFormat:@"%ld\nPONTOS", (long)currentLevelStatistics.score];
//    [self.lblPoints sizeToFit];
//    
//    self.lblPoints.x = self.viewNavBar.width-self.lblPoints.width-5;
//    self.lblPoints.center = CGPointMake(self.lblPoints.center.x, self.btnCloseTitle.center.y);
    
}


- (void)updateLabelWithPoints:(NSInteger)points {
    
    self.lblPoints.numberOfLines = 0;
    self.lblPoints.text = [NSString stringWithFormat:@"%ld\nPONTOS", (long)points];
    [self.lblPoints sizeToFit];
    
    self.lblPoints.x = self.viewNavBar.mWidth-self.lblPoints.mWidth-5;
    self.lblPoints.center = CGPointMake(self.lblPoints.center.x, self.btnCloseTitle.center.y);

    
}

- (void)addPointsWithIncorrectAnswer:(int)points {
    
    unitScore+=points;
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.numberOfLines = 0;
    
    //NSLog(@"(addPointsWithIncorrectAnswer:(int)points Current points - %ld + new points - %d", (long)currentLevelStatistics.score, points);
    
    [self updateLabelWithPoints:currentLevelStatistics.score+points];
    
    [PROGRESS_MANAGER updateLevelScore:[PROGRESS_MANAGER currentLevelIndex] score:points];
    
    [self finishInstanceWithIncorrectAnswer];
    
}

- (void)finishInstanceWithIncorrectAnswer {
    
    BaseInstance *baseInstance = (BaseInstance *)[self.arrayDataSource objectAtIndex:self.currentPage];
    [self updateInstanceStatistics:baseInstance withStatus:2];
}

#pragma mark - Update instance stats

- (void)updateInstanceStatistics:(BaseInstance *)baseInstance withStatus:(int)status {
    
    int skipped = 0;
    int correct = 0;
    int incorrect = 0;
    BOOL statusOfInstance = YES;
    
    if (status == 0) {
        //skipped
        skipped = 1;
        statusOfInstance = NO;
    }
    else if (status == 1) {
        //corrcet
        correct = 1;
    }
    else if (status == 2) {
        //incorrect
        incorrect = 1;
    }
    
    
    if (baseInstance.pageNumber != 0) {
        
        if ([baseInstance isKindOfClass:[Practice class]] && skipped == 1) {
            
            [PROGRESS_MANAGER updateInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex]
                                                      section:[PROGRESS_MANAGER currentSectionIndex]
                                                         unit:[PROGRESS_MANAGER currentUnitIndex]
                                                     instance:baseInstance.instanceNumber
                                       numberOfCorrectAnswers:correct numberOfIncorrectAnswers:incorrect
                                     numberOfSkippedQuestions:skipped
                                 numberOfNotAnsweredQuestions:0
                                       numberOfTotalQuestions:0
                                                       status:statusOfInstance
                                                  isFirstTime:NO];

        }
        else {
            
            [PROGRESS_MANAGER updatePageStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex]
                                                  section:[PROGRESS_MANAGER currentSectionIndex]
                                                     unit:[PROGRESS_MANAGER currentUnitIndex]
                                                 instance:baseInstance.instanceNumber
                                                     page:baseInstance.pageNumber-1
                                   numberOfCorrectAnswers:correct
                                 numberOfIncorrectAnswers:incorrect
                                 numberOfSkippedQuestions:skipped
                             numberOfNotAnsweredQuestions:0
                                   numberOfTotalQuestions:0
                                               pageStatus:statusOfInstance
                                               isExposure:NO];
        }
    }
    else {
                
        [PROGRESS_MANAGER updateInstanceStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex]
                                                  section:[PROGRESS_MANAGER currentSectionIndex]
                                                     unit:[PROGRESS_MANAGER currentUnitIndex]
                                                 instance:baseInstance.instanceNumber
                                   numberOfCorrectAnswers:correct
                                 numberOfIncorrectAnswers:incorrect
                                 numberOfSkippedQuestions:skipped
                             numberOfNotAnsweredQuestions:0
                                   numberOfTotalQuestions:0
                                                   status:statusOfInstance
                                              isFirstTime:NO];
        
    }
    
}

#pragma mark - Update page stats

- (void)updatePageStatistics:(BaseInstance *)baseInstance page:(int)pageIndex withStatus:(int)status {
    
    int skipped = 0;
    int correct = 0;
    int incorrect = 0;
    BOOL statusOfInstance = YES;
    
    if (status == 0) {
        //skipped
        skipped = 1;
        statusOfInstance = NO;
    }
    else if (status == 1) {
        //corrcet
        correct = 1;
    }
    else if (status == 2) {
        //incorrect
        incorrect = 1;
    }
    
    [PROGRESS_MANAGER updatePageStatisticForLevel:[PROGRESS_MANAGER currentLevelIndex]
                                          section:[PROGRESS_MANAGER currentSectionIndex]
                                             unit:[PROGRESS_MANAGER currentUnitIndex]
                                         instance:baseInstance.instanceNumber
                                             page:pageIndex
                           numberOfCorrectAnswers:correct
                         numberOfIncorrectAnswers:incorrect
                         numberOfSkippedQuestions:skipped
                     numberOfNotAnsweredQuestions:0
                           numberOfTotalQuestions:0
                                       pageStatus:statusOfInstance
                                       isExposure:NO];

    
}


#pragma mark - Delegate methods for practice questions auto scroll

- (void)gotoNextPage {
    
    NSInteger cPage = self.currentPage+1;
    
    if (cPage == kNumberOfPages) {
        
//        isSummaryPage = YES;
//        [self createSummaryPageWithIndex:kNumberOfPages];
        [lmPagingControl.btnNext setTitle:Localized(@"T200") forState:UIControlStateNormal];
    }
    else {
    
        [self loadScrollViewWithPage:cPage];
        [self loadScrollViewWithPage:cPage+1];
        [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(cPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height) animated:YES];

    }
    
    
}

- (void)gotoPreviousPage {
    
    

}

#pragma mark - Create Summary Page

- (void)createSummaryPageWithIndex:(NSInteger)summaryIndexPage {
    
    LessonSummary *lessonSummary = [LessonSummary loadFromNib];
    lessonSummary.lessonSummaryDelegate = self;
    
    lessonSummary.frame = CGRectMake(summaryIndexPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height);
    [lessonSummary setContentSize:CGSizeMake(LM_WIDTH, lessonSummary.imgBubble.frame.origin.y+lessonSummary.imgBubble.frame.size.height+10)];
    
    NSString *strLessonNumber = [NSString stringWithFormat:@"%ld", [self getTheOrderOfLessonInTheLevel:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]+1]];
    
    if ([PROGRESS_MANAGER isLessonCompleted:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]]) {
        
        lessonSummary.stringPerformanceTitle = Localized(@"T480.1");
        lessonSummary.stringPerformanceDescription = [NSString stringWithFormat:Localized(@"T480.2"), strLessonNumber, self.lessonInfo.name];
        lessonSummary.isSuccessful = YES;
        
        [self updateProgres:true level:[PROGRESS_MANAGER currentLevelIndex] module:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] isSummary:NO];

        [PROGRESS_MANAGER updateProgressForUserLevel:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] unitAchived:YES];
    }

    else {
        
        lessonSummary.stringPerformanceTitle = Localized(@"T481.1");
        lessonSummary.stringPerformanceDescription = [NSString stringWithFormat:Localized(@"T481.2"), strLessonNumber, [self.lessonInfo name]];
        lessonSummary.isSuccessful = NO;
        [self updateProgres:false level:[PROGRESS_MANAGER currentLevelIndex] module:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex] isSummary:NO];
      
    }
    
    //////////////////////////////////////////////////////////
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    
    NSInteger levelScore = currentLevelStatistics.score;
    NSInteger levelIndex = [PROGRESS_MANAGER currentLevelIndex];
    NSInteger lessonIndex = [PROGRESS_MANAGER currentUnitIndex];
    NSInteger moduleIndex = [PROGRESS_MANAGER currentSectionIndex];
    
    [lessonSummary setStatisticsValue:levelIndex module:moduleIndex lesson:lessonIndex points:levelScore];
    
    NSInteger currentLevelIndex = [PROGRESS_MANAGER currentLevelIndex];
    NSInteger currentModuleIndex = [PROGRESS_MANAGER currentSectionIndex];
    NSInteger currentLessonIndex = [PROGRESS_MANAGER currentUnitIndex];
    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];

    if (!(currentLessonIndex+1 < currentSection.unitsArray.count)) {
        
        if (!(currentModuleIndex + 1 < self.arrayLevelDataSource.count))
            [lessonSummary setIsLastLesson:YES];
    
    }
    else
        [lessonSummary setIsLastLesson:NO];
    
    if([PROGRESS_MANAGER areAllLessonsVisitedInLevel:PROGRESS_MANAGER.currentLevelIndex])
       [lessonSummary setIsFirstTime:NO];
    else
        [lessonSummary setIsFirstTime:YES];
    
    
    [self.scrollViewInstanceContainer addSubview:lessonSummary];
    
    
    NSDictionary *dictData = [PROGRESS_MANAGER lessonSummary:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
    
    DLog(@"dict - %@", dictData);
    
    lessonSummary.sumarryPageData = dictData;
    

    ////nnnew
    
    if (currentLessonIndex+1 < currentSection.unitsArray.count) {
        
        //we are still in the module
        currentLessonIndex = currentLessonIndex+1;
        Section *currtSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
        Unit *currLesson = (Unit *)[currtSection.unitsArray objectAtIndex:currentLessonIndex];
        
        lessonSummary.wordCount = self.lessonInfo.wordCount;
        lessonSummary.grammarCount = self.lessonInfo.grammarCount;
        
        lessonSummary.lessonInfo = currLesson;

        
    }
    else {
        
        if (currentModuleIndex+1 < self.arrayLevelDataSource.count) {
            
            //1
            //check if the module is last in the level
            
            DLog(@"New module - first lesson");
            
            currentModuleIndex+=1;
            currentLessonIndex = 0;
            
            Section *currtSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
            Unit *currLesson = (Unit *)[currtSection.unitsArray objectAtIndex:currentLessonIndex];

            lessonSummary.wordCount = self.lessonInfo.wordCount;
            lessonSummary.grammarCount = self.lessonInfo.grammarCount;

            lessonSummary.lessonInfo = currLesson;
            
        }
        else {
            
            //2
            //if not load new module first lesson
            
            if (currentLevelIndex+1 < [[DATA_SERVICE findAllLevelsForCourse] count]) {
                
                currentLevelIndex+=1;
                currentModuleIndex = 0;
                currentLessonIndex = 0;
                
                Section *currtSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
                Unit *currLesson = (Unit *)[currtSection.unitsArray objectAtIndex:currentLessonIndex];
                
                lessonSummary.wordCount = self.lessonInfo.wordCount;
                lessonSummary.grammarCount = self.lessonInfo.grammarCount;

                
                [lessonSummary setLessonInfo:currLesson andTeaserText:[NSString stringWithFormat:Localized(@"T499"), [PROGRESS_MANAGER takeCurentLevelName:levelIndex]]];
                
            }
            else {
                
                Section *currtSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
                Unit *currLesson = (Unit *)[currtSection.unitsArray objectAtIndex:currentLessonIndex];
                
                lessonSummary.wordCount = self.lessonInfo.wordCount;
                lessonSummary.grammarCount = self.lessonInfo.grammarCount;

                [lessonSummary setLessonInfo:currLesson andTeaserText:[NSString stringWithFormat:Localized(@"T499"), [PROGRESS_MANAGER takeCurentLevelName:levelIndex]]];

            }
            
        }
    }

    //newww



    [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(summaryIndexPage*LM_WIDTH, 0, LM_WIDTH, self.scrollViewInstanceContainer.frame.size.height) animated:YES];
    
    [self.lmPagingControl setUserInteractionEnabled:YES];
    [self.lmPagingControl.btnNext setUserInteractionEnabled:YES];
    
}

#pragma mark - Lesson Summary Delegate

- (void)jumpToNewLesson:(id)sender {
    
    [self.lmPagingControl.btnNext setUserInteractionEnabled:YES];
    
    NSInteger previousLevelIndex = [PROGRESS_MANAGER currentLevelIndex];
    NSInteger previousModuleIndex = [PROGRESS_MANAGER currentSectionIndex];
    NSInteger previousLessonIndex = [PROGRESS_MANAGER currentUnitIndex];

    NSInteger currentLevelIndex = [PROGRESS_MANAGER currentLevelIndex];
    NSInteger currentModuleIndex = [PROGRESS_MANAGER currentSectionIndex];
    NSInteger currentLessonIndex = [PROGRESS_MANAGER currentUnitIndex];

    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
    
    if (currentLessonIndex+1 < currentSection.unitsArray.count) {
        
        //we are still in the module
        
        currentLessonIndex = currentLessonIndex+1;
        self.currentIndex = [NSIndexPath indexPathForRow:currentLessonIndex inSection:currentModuleIndex];
//        [[self.lmPagingControl btnNext] setTitle:@"SKIP" forState:UIControlStateNormal];
        
        DLog(@"We are still in the module - change the lesson");

    }
    else {
        
        //module is finished
        
        if (currentModuleIndex+1 < self.arrayLevelDataSource.count) {
            
            //1
            //check if the module is last in the level

            DLog(@"New module - first lesson");
            
            currentModuleIndex+=1;
            currentLessonIndex = 0;
            
            self.currentIndex = [NSIndexPath indexPathForRow:0 inSection:currentModuleIndex];


        }
        else {
            
            //2
            //if not load new module first lesson

            if (currentLevelIndex+1 < [[DATA_SERVICE findAllLevelsForCourse] count]) {
                
                [PROGRESS_MANAGER resetInstanceValues:[PROGRESS_MANAGER currentLevelIndex]+1 section:0 unit:0];
                
                currentLevelIndex+=1;
                currentModuleIndex = 0;
                currentLessonIndex = 0;
                
                
                //new
                
                [PROGRESS_MANAGER setLastVisitedInstance:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex instanceIndex:self.currentPage];
                self.currentPage = 0;
                pageControlUsed = NO;
                [self.lmPagingControl refreshControlWithCurrentPage:0];
                [self setNextButtonTitle:0];
                //////////////////////////////////////
                
                [PROGRESS_MANAGER resetInstanceValues:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex];
                
//                [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:currentLevelIndex index:currentModuleIndex];
//                [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:currentLevelIndex index:currentLessonIndex];
                
                [PROGRESS_MANAGER setCurrentLevelIndex:currentLevelIndex];
                [PROGRESS_MANAGER saveCurrentLevelIndex:currentLevelIndex forUser:[USER_MANAGER userMsisdn]];
                [PROGRESS_MANAGER setCurrentSectionIndex:currentModuleIndex];
                [PROGRESS_MANAGER setCurrentUnitIndex:currentLessonIndex];
                
                isSummaryPage = NO;
                
                //end new
                                
                self.currentIndex = [NSIndexPath indexPathForRow:0 inSection:0];
                
                [self closeFinishLevel];
                
                return;
                
            }
            else {
                
//                [self closeLessonController:nil];
                [APP_DELEGATE openLevelSelectionAfterFinishingLevels];
                
                return;
            }

            
            DLog(@"Level was finished");

        }
    }
    
    if(![USER_MANAGER isGuest]) {
        
        //vivo user or INApp user
        
        Section *nextSection = (Section *)[self.arrayLevelDataSource objectAtIndex:self.currentIndex.section];
        Unit *nextLesson = (Unit *)[nextSection.unitsArray objectAtIndex:self.currentIndex.row];
        
        NSArray *arrayInstances = [nextLesson instancesArray];
        
        if (!arrayInstances || [arrayInstances count] < nextLesson.numberOfInstances) {
            
            if (!arrayInstances) {
                if (![ReachabilityHelper reachable]) {
                    [UIAlertView alertWithCause:kAlertNoConnection];
                } else {
                    if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                        [UIAlertView alertWithCause:kAlertOutOfStorage];
                    } else {
                        [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                    }
                }
            } else if ([arrayInstances count] < nextLesson.numberOfInstances) {
                if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                    [UIAlertView alertWithCause:kAlertOutOfStorage];
                } else {
                    [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                }
            }
            
        }
        else {
            
            //save progress
            
            BOOL isNextLessonVisited = [PROGRESS_MANAGER isUnitVisited:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex];
            
            if([USER_MANAGER canOpenLesson] && !isNextLessonVisited)
            {
                
                NSString *unitIdString = [NSString stringWithFormat:@"%ld",nextLesson.unitID];
                [API_REQUEST_MANAGER updateProgressForUser:[USER_MANAGER userMsisdnString]
                                                   groupID:[REQUEST_MANAGER groupID]
                                                 contentID:[REQUEST_MANAGER contentID]
                                                    unitID:unitIdString
                                                 unitScore:@"0"
                                               unitAchived:@"false"
                                                  progress:@"0"
                                           CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
                 {
                     if(responseDictionary && [responseDictionary objectForKey:kApiStatus] && [API_REQUEST_MANAGER checkApiResponseStatus:[[responseDictionary objectForKey:kApiStatus] integerValue]])
                     {
                         [USER_MANAGER setNumberOfCredits:[[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue]];
                     }
                     else
                     {
                         [UIAlertView alertWithCause:kAlertServerUnreachable];
                         return;
                     }
                 }
                 ];
            }
            else
            {
                if(![USER_MANAGER canOpenLesson]) {
                    
                    [UIAlertView alertWithCause:kAlertNoMoreCreditsForActiveUser];
                    return;
                }
            }

            
            DLog(@"Before update module - %ld, lesson - %ld", PROGRESS_MANAGER.currentSectionIndex, PROGRESS_MANAGER.currentUnitIndex);
            
            [self updateProgres:false level:previousLevelIndex module:previousModuleIndex unit:previousLessonIndex isSummary:YES];
            
            NSDictionary *lessonProgressionData = [PROGRESS_MANAGER lessonSummary:[PROGRESS_MANAGER currentLevelIndex] section:previousModuleIndex unit:previousLessonIndex];
            
            NSInteger skippedQuestions = [[lessonProgressionData objectForKey:@"numberOfSkippedQuestions"] integerValue];
            NSInteger correctAnswers = [[lessonProgressionData objectForKey:@"numberOfCorrectAnswers"] integerValue];
            NSInteger incorrectAnswers = [[lessonProgressionData objectForKey:@"numberOfIncorrectAnswers"] integerValue];
            
            [self updateUserProgression:[NSString stringWithFormat:@"%ld", (long)skippedQuestions] withCorrect:[NSString stringWithFormat:@"%ld", (long)correctAnswers] withIncorrect:[NSString stringWithFormat:@"%ld", (long)incorrectAnswers] moduleIndex:previousModuleIndex unitIndex:previousLessonIndex];
            
            
            //////////////////////////////////////
            BaseInstance *instance = [nextLesson.instancesArray lastObject];
            
            //DLog(@"CLICK CHECK -> UNIT ID:%d NEED INSTNACES:%d DOWNLOADED INSTNACES:%d FROM REQUESTED:%d", unit.unitID, unit.numberOfInstances, [unit.instancesArray count], [unit.instancesInfosArray count]);
            
            if (![PROGRESS_MANAGER isUnitAvailable:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex] && !instance.isInitial) {
                
                [PROGRESS_MANAGER unitInstancesDownloadCompleted:currentLevelIndex sectionIndex:currentModuleIndex unitIndex:currentLessonIndex];
                
                if (![PROGRESS_MANAGER isUnitAvailable:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex] && !instance.isInitial) {
                
                    if ([[Utilities sharedInstance] getFreeDiskspace] <= ((100 * 1024) * 1024)) {
                        [UIAlertView alertWithCause:kAlertOutOfStorage];
                    } else {
                        [UIAlertView alertWithCause:KAlertInstanceNotDownloadedYet];
                    }
                    return;
                }
                
                
            }
            
            [PROGRESS_MANAGER setLastVisitedInstance:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex instanceIndex:self.currentPage];
            self.currentPage = 0;
            pageControlUsed = NO;
            [self.lmPagingControl refreshControlWithCurrentPage:0];
            [self setNextButtonTitle:0];
            //////////////////////////////////////
            
            [PROGRESS_MANAGER resetInstanceValues:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex];
            
            [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:currentLevelIndex index:currentModuleIndex];
            [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:currentLevelIndex index:currentLessonIndex];
            
            [PROGRESS_MANAGER setCurrentLevelIndex:currentLevelIndex];
            [PROGRESS_MANAGER saveCurrentLevelIndex:currentLevelIndex forUser:[USER_MANAGER userMsisdn]];
            [PROGRESS_MANAGER setCurrentSectionIndex:currentModuleIndex];
            [PROGRESS_MANAGER setCurrentUnitIndex:currentLessonIndex];

            self.lblLessonTitle.hidden = YES;
            self.lmPagingControl.hidden = YES;
            
            
            [self removAllObserversInVehicles];
            
            [self refreshContentWithStartPage:YES];
        }
        
        
        
    }
    else {
        
        //guest user
        
        NSIndexPath *maxAvailForGuest = [self findMaxValuesForGuest:[LMHelper getNumberOfGestCredits] arrayLessons:self.arrayLevelDataSource];
        NSInteger moduleAvailForGuest = maxAvailForGuest.section;
        NSInteger lessonAvailForGuest = maxAvailForGuest.row;
        
        if (currentModuleIndex > moduleAvailForGuest || (currentModuleIndex == moduleAvailForGuest && currentLessonIndex > lessonAvailForGuest)) {

            
            UIAlertView *tempAlert = [UIAlertView alertViewWithTitle:@""
                                                             message:Localized(@"T462")
                                                   cancelButtonTitle:Localized(@"T511")
                                                   otherButtonTitles:@[Localized(@"T463")]
                                                           onDismiss: ^(int buttonIndex) {
                                                               if (buttonIndex == -1){
                                                                   
                                                               }else if (buttonIndex == 0){
                                                                   [APP_DELEGATE buildLoginStack];
                                                               }
                                                           }
                                                            onCancel: ^{
                                                                
                                                            }];
            [tempAlert show];
            
            
        }
        else {
            
            [PROGRESS_MANAGER resetInstanceValues:currentLevelIndex section:currentModuleIndex unit:currentLessonIndex];
            
            [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:currentLevelIndex index:currentModuleIndex];
            [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:currentLevelIndex index:currentLessonIndex];
            
            [PROGRESS_MANAGER saveCurrentLevelIndex:currentLevelIndex forUser:[USER_MANAGER userMsisdn]];
            [PROGRESS_MANAGER saveCurrentModulIndex:currentModuleIndex forUser:[USER_MANAGER userMsisdn]];
            [PROGRESS_MANAGER saveCurrentUnitIndex:currentLessonIndex forUser:[USER_MANAGER userMsisdn]];
            
            [PROGRESS_MANAGER setCurrentLevelIndex:currentLevelIndex];
            [PROGRESS_MANAGER setCurrentSectionIndex:currentModuleIndex];
            [PROGRESS_MANAGER setCurrentUnitIndex:currentLessonIndex];

            self.currentPage = 0;
            pageControlUsed = NO;
            [self.lmPagingControl refreshControlWithCurrentPage:0];
            [self setNextButtonTitle:0];
            
            self.lblLessonTitle.hidden = YES;
            self.lmPagingControl.hidden = YES;
            
            [self removAllObserversInVehicles];

            [self refreshContentWithStartPage:YES];

        }

        
        
    }
    
    
}


- (void)jumpToFirstSkippedOrWrongAnswered:(id)sender {
    
    pageControlUsed = YES;
    NSInteger indexOfFirstSkipped = [PROGRESS_MANAGER getTheFirstSkippedOrWrongAnsweredInstance:[PROGRESS_MANAGER currentLevelIndex] section:[PROGRESS_MANAGER currentSectionIndex] unit:[PROGRESS_MANAGER currentUnitIndex]];
    
    self.currentPage = indexOfFirstSkipped;
    lastPage = indexOfFirstSkipped;
    isSummaryPage = NO;
    
//    [UIView animateWithDuration:0.25 animations:^{
    
        [self.scrollViewInstanceContainer scrollRectToVisible:CGRectMake(indexOfFirstSkipped*LM_WIDTH_INSTACE, 0, LM_WIDTH_INSTACE, LM_SCROLL_HEIGHT) animated:NO];
        
//    } completion:^(BOOL finished) {
    
        [self.lmPagingControl updateDotAtIndex:(int)kNumberOfPages-1];
        [self.lmPagingControl.btnNext setUserInteractionEnabled:YES];
    
//    }];
    
    [self performSelector:@selector(jumpToFirstSkippedOrWrongAnsweredDelay) withObject:nil afterDelay:0.25];
    
    
    UIView *instance = [self.viewControllers objectAtIndex:indexOfFirstSkipped];
    
    if ([instance isKindOfClass:[MultipleChoiceTextProgressiveChatScreen class]]) {
        [((MultipleChoiceTextProgressiveChatScreen *)instance) resetProgressiveChat];
        [((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
        
        [self performSelector:@selector(multiChoiceTextProgressiveChatDelay:) withObject:instance afterDelay:0.25];
        
        //[((MultipleChoiceTextProgressiveChatScreen *)instance) drawInstance];
    }
    
    [self loadScrollViewWithPage:indexOfFirstSkipped-1];
    [self loadScrollViewWithPage:indexOfFirstSkipped];
    [self loadScrollViewWithPage:indexOfFirstSkipped+1];
    
    [self.lmPagingControl refreshControlWithCurrentPage:indexOfFirstSkipped];

    [self setNextButtonTitle:indexOfFirstSkipped];

    
    
    
}

-(void)jumpToFirstSkippedOrWrongAnsweredDelay {
    
    pageControlUsed = NO;

}

- (void)jumpToFirstLessonWithoutStar:(NSInteger)level
                             section:(NSInteger)section
                                unit:(NSInteger)unit

{
    
    [PROGRESS_MANAGER resetInstanceValues:level section:section unit:unit];
    
    [PROGRESS_MANAGER saveCurrentLevelIndex:level forUser:[USER_MANAGER userMsisdn]];
    [PROGRESS_MANAGER saveCurrentModulIndex:section forUser:[USER_MANAGER userMsisdn]];
    [PROGRESS_MANAGER saveCurrentUnitIndex:unit forUser:[USER_MANAGER userMsisdn]];
    
    [PROGRESS_MANAGER setCurrentLevelIndex:level];
    [PROGRESS_MANAGER setCurrentSectionIndex:section];
    [PROGRESS_MANAGER setCurrentUnitIndex:unit];
    
    self.currentPage = 0;
    pageControlUsed = NO;
    [self.lmPagingControl refreshControlWithCurrentPage:0];
    [self setNextButtonTitle:0];
    
    self.lblLessonTitle.hidden = YES;
    self.lmPagingControl.hidden = YES;
    
    [PROGRESS_MANAGER setLastVisitedSectionIndexForLevel:level index:section];
    [PROGRESS_MANAGER setLastVisitedUnitIndexForLevel:level index:unit];
    
    [PROGRESS_MANAGER setCurrentSectionIndex:section];
    [PROGRESS_MANAGER setCurrentUnitIndex:unit];
    
    [self refreshContentWithStartPage:YES];
}


#pragma mark - MultipleChoiceTextProgressiveChatDelegate implementation

- (void)didFinishWithExecution
{
    [self enableScrolling:nil];
}



#pragma mark - Check is the instance only presentation

- (BOOL)isTheInstancePresentation:(BaseInstance *)instance {
    
    if ([instance isKindOfClass:[PracticeSectionExtra class]] || [instance isKindOfClass:[MultipleChoiceImagePracticeObject class]]) {
        return NO;
    }
    if ([instance isKindOfClass:[MultipleChoiceImageExposureObject class]]) {
        return YES;
    }
    
    if ([instance isKindOfClass:[SimplePageProfile class]] || [instance isKindOfClass:[SimplePageTextAndVideo class]] || [instance isKindOfClass:[SimplePageBubbleDialog class]]) {
        
        return YES;
    }
    
    return NO;
}

#pragma mark - Orientation

- (NSUInteger)supportedInterfaceOrientations {
    
    if(isViedoActive) {
        
        return UIInterfaceOrientationMaskAllButUpsideDown;
        
    }
    else {
    
        return UIInterfaceOrientationMaskPortrait;
    
    }
}

#pragma mark
- (void)updateProgres:(BOOL)unitAchived
                level:(NSInteger)levelIndex
               module:(NSInteger)moduleIndex
                 unit:(NSInteger)unitIndex
            isSummary:(BOOL)isSummary
{
  
    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:moduleIndex];
    Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:unitIndex];
    
    double unitProgress = [PROGRESS_MANAGER getLessonProgression:levelIndex section:moduleIndex unit:unitIndex];
    
    if ([USER_MANAGER isActive]) {
    
      NSString *unitIdString = [NSString stringWithFormat:@"%ld", (long)currentLesson.unitID];
      
      [API_REQUEST_MANAGER updateProgressForUser:[USER_MANAGER userMsisdnString]
                                         groupID:[REQUEST_MANAGER groupID]
                                       contentID:[REQUEST_MANAGER contentID]
                                          unitID:unitIdString
                                       unitScore:isSummary ? @"0" : [NSString stringWithFormat:@"%ld", (long)unitScore]
                                     unitAchived:unitAchived ? @"true" : @"false"
                                        progress:[NSString stringWithFormat:@"%d", (int)unitProgress]
                                 CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
       {
         if(responseDictionary && [responseDictionary objectForKey:kApiStatus] && [API_REQUEST_MANAGER checkApiResponseStatus:[[responseDictionary objectForKey:kApiStatus] integerValue]])
         {
           [USER_MANAGER setNumberOfCredits:[[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue]];
             //unitScore = 0;
         }
         else
         {
             if([ReachabilityHelper reachable]) {
                 [UIAlertView alertWithCause:kAlertServerUnreachable];
             }
             else {
                 [UIAlertView alertWithCause:kAlertNoConnection];
             }
         }
       }];
      
    }
    else
    {
    
    }
}

-(void)updateUserProgression:(NSString*)skipped withCorrect:(NSString*)correct withIncorrect:(NSString*)incorrect moduleIndex:(NSInteger)moduleIndex unitIndex:(NSInteger)unitIndex {
  
    DLog(@"Section - %ld, unit - %ld", moduleIndex, unitIndex);
    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:moduleIndex];
    Unit *currentLesson = (Unit *)[currentSection.unitsArray objectAtIndex:unitIndex];
  
    NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
    LevelData *level = [arrayCourseLevels objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
  
    if([USER_MANAGER isActive])
    {
    
        NSString *unitIdString = [NSString stringWithFormat:@"%ld",(long)currentLesson.unitID];
        NSString *levelIdString = [NSString stringWithFormat:@"%ld",(long)level.levelID];
    
        [API_REQUEST_MANAGER userProgression:[USER_MANAGER userMsisdnString]
                               contentId:[REQUEST_MANAGER contentID]
                              phoneModel:[[[UIDevice currentDevice] model] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
                                  osType:@"IOS"
                               osVersion:[[UIDevice currentDevice] systemVersion]
                                 levelId:levelIdString
                                 groupId:[REQUEST_MANAGER groupID]
                               lessionId:unitIdString
                                 skipped:skipped
                                 correct:correct
                               inCorrect:incorrect
                         CompletionBlock:
     
     ^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary)
     {
       if(responseDictionary && [responseDictionary objectForKey:kApiStatus] && [API_REQUEST_MANAGER checkApiResponseStatus:[[responseDictionary objectForKey:kApiStatus] integerValue]])
       {
         //[USER_MANAGER setNumberOfCredits:[[responseDictionary objectForKey:kApiUserNumberOfCredits] integerValue]];
       }
       else
       {
           if([ReachabilityHelper reachable]) {
               [UIAlertView alertWithCause:kAlertServerUnreachable];
           }
           else {
               [UIAlertView alertWithCause:kAlertNoConnection];
           }
       }
     }
     ];
    
  }
}
#pragma mark - Should Autorotate

- (BOOL)shouldAutorotate {
  
    if (isViedoActive) {
        
        return YES;
    }

    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BaseInstance *)getBaseInstanceForIndex:(NSInteger)index {
    
    for (BaseInstance *baseInstance in self.arrayDataSource) {
        
        if (baseInstance.instanceNumber == index-1) {
            
            return baseInstance;
        }
        
    }
    
    return nil;
}

#pragma mark -
#pragma mark - get first lesson number

- (NSInteger)getTheNumberOfFirstLessonForLevel:(NSInteger)levelIndex {
    
    NSArray *arrayLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
    
    LevelData *level;
    
    NSInteger startNumber = 0;
    
    for (int i=0; i<levelIndex; i++) {
        
        level = [arrayLevels objectAtIndex:i];
        
        for (Section *section in [(LevelData *)level sectionsArray]) {
            
            startNumber+=section.unitsArray.count;
        }
    }
    
    return startNumber;
}

#pragma mark -
#pragma mark - Help method to return number of lesson

- (NSInteger)getTheOrderOfLessonInTheLevel:(NSInteger)section unit:(NSInteger)unit {
    
    if (section == 0) {
        
        return unit+[self getTheNumberOfFirstLessonForLevel:[PROGRESS_MANAGER currentLevelIndex]];
        
    }
    
    NSInteger lessonOrderIndex = 0;
    Section *currentSection;
    
    for (int i=0; i<section; i++) {
        
        currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:i];
        lessonOrderIndex += [currentSection.unitsArray count];
    }
    
    lessonOrderIndex+=unit;
    
    lessonOrderIndex+=[self getTheNumberOfFirstLessonForLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    return lessonOrderIndex;
}


#pragma mark - 
#pragma mark - Set maximum lesson user can open

- (void)setTheMaximumLessonUserCanAccess {
    
    NSInteger currentModuleIndex = [PROGRESS_MANAGER currentSectionIndex];
    NSInteger currentLessonIndex = [PROGRESS_MANAGER currentUnitIndex];
    
    Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
    
    if (currentLessonIndex+1 < currentSection.unitsArray.count) {
        
        //we are still in the module
        currentLessonIndex+=1;
        
        Section *nextSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
        Unit *nextLesson = (Unit *)[nextSection.unitsArray objectAtIndex:currentLessonIndex];
        
        [PROGRESS_MANAGER setMaxUnitForLevel:[PROGRESS_MANAGER currentLevelIndex] maxUnitId:nextLesson.unitID];

    }
    else {
        
        //module is finished
        
        if (currentModuleIndex+1 < self.arrayLevelDataSource.count) {
            
            //1
            //check if the module is last in the level
            
            currentModuleIndex +=1;
            currentLessonIndex = 0;
            
            Section *nextSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
            Unit *nextLesson = (Unit *)[nextSection.unitsArray objectAtIndex:currentLessonIndex];
            
            [PROGRESS_MANAGER setMaxUnitForLevel:[PROGRESS_MANAGER currentLevelIndex] maxUnitId:nextLesson.unitID];

            
        }
        else {
            
            //2
            //if not load new module first lesson
            
            DLog(@"Level finished");
            
        }
    }
    
}

#pragma mark -
#pragma mark - Set next buttton title

- (void)setNextButtonTitle:(NSInteger)index {
    
    if (index > kNumberOfPages || index < 0) {
        return;
    }
    if (index == kNumberOfPages) {
        
        NSInteger currentModuleIndex = [PROGRESS_MANAGER currentSectionIndex];
        
        if (currentModuleIndex+1 < self.arrayLevelDataSource.count) {
            
            //1
            //check if the module is last in the level
            
        }
        else {
            
            //2
            //if not load new module first lesson
            
            
            //NSInteger currentLevelIndex = [PROGRESS_MANAGER currentLevelIndex];
            
            
//            if (currentLevelIndex+1 < [[DATA_SERVICE findAllLevelsForCourse] count]) {
            
                Section *currentSection = (Section *)[self.arrayLevelDataSource objectAtIndex:currentModuleIndex];
                
                if ([PROGRESS_MANAGER currentUnitIndex]+1 >= [[currentSection unitsArray] count]) {
                    
                    [[self.lmPagingControl btnNext] setTitle:Localized(@"T203") forState:UIControlStateNormal];
                    return;

                }
                
//            }
//            else {
//                
//                [[self.lmPagingControl btnNext] setTitle:Localized(@"T423") forState:UIControlStateNormal];
//                
//                return;
//            }
            
        }
        
        
        [[self.lmPagingControl btnNext] setTitle:Localized(@"T202") forState:UIControlStateNormal];
        return;
    }
    
    ECVehicleDisplay *instance = [self.viewControllers objectAtIndex:index];
    
    if ((NSNull *)instance == [NSNull null])
        return;
    
    if ([instance isKindOfClass:[MultipleChoiceImageExposureScreen class]] || [instance isKindOfClass:[SimplePageTextAndVideoScreen class]] || [instance isKindOfClass:[SimplePageProfileScreen class]] || [instance isKindOfClass:[SimplePageDialogScreen class]]) {
        
        [[self.lmPagingControl btnNext] setTitle:Localized(@"T200") forState:UIControlStateNormal];
        return;
    }
    if ([(ECVehicleDisplay *)instance isCompleted]) {
        
        [[self.lmPagingControl btnNext] setTitle:Localized(@"T200") forState:UIControlStateNormal];
    }
    else {
        [[self.lmPagingControl btnNext] setTitle:Localized(@"T201") forState:UIControlStateNormal];
    }
}


#pragma mark - Left swipe action 

- (void)leftSwipeActionScorllContainer:(id)sender {
    
    if (isSummaryPage)
        return;
    
    [self nextButtonAction:self.lmPagingControl withSpeed:0.5];
    
    
}

- (void)rightSwipeActionScorllContainer:(id)sender {
    
    [self previousButtonSelected:self.lmPagingControl withSpeed:0.5];
    
}


#pragma mark - Destroy observers from video vehicle

- (void)removAllObserversInVehicles {
    
    for (ECVehicleDisplay *vehicle in self.viewControllers) {
        
        if (vehicle && [vehicle isKindOfClass:[SimplePageTextAndVideoScreen class]]) {
            
            [((SimplePageTextAndVideoScreen *)vehicle) disappearSimplePageTextAndVideo];

        }
        
    }
    
}


#pragma mark - Vehicle delegate action

- (void)closeFeedbackandSwipePage:(int)side {
    
    if (side == 1) {
        
        [self performSelector:@selector(leftSwipeActionScorllContainer:) withObject:nil afterDelay:0.2];
    }
    else if (side == 0) {
        
        [self performSelector:@selector(rightSwipeActionScorllContainer:) withObject:nil afterDelay:0.2];

    }
    else {
        
        
    }
    
}

#pragma mark - Convert integer to indexPath

- (NSIndexPath *)findMaxValuesForGuest:(NSInteger)maxAvail arrayLessons:(NSArray *)arrayLessons {
    
    NSInteger tempValue = kParam001;
    
    for (int i=0; i<arrayLessons.count; i++) {
        
        Section *currentSection = (Section *)[arrayLessons objectAtIndex:i];
        
        if (tempValue <= currentSection.unitsArray.count) {
            
            return [NSIndexPath indexPathForRow:tempValue-1 inSection:i];
        }
        
        tempValue-=currentSection.unitsArray.count;
        
    }
    
    return [NSIndexPath indexPathForRow:0 inSection:0];
}


@end
