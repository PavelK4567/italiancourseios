//
//  ModuleCertificateView.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/16/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "ModuleCertificateView.h"

@implementation ModuleCertificateView


#pragma mark - 

- (void)awakeFromNib
{
    [super awakeFromNib];
}


#pragma mark - bindGUI

- (void)bindGUI
{
    
    [congratulationsLabel setText:Localized(@"T601")];
    [certificateText setText:[NSString stringWithFormat:Localized(@"T600"),self.modulNumber]];
    [alertView setHidden:YES];
    [facebookButton setTitle:[NSString stringWithFormat:@"  %@",Localized(@"T620")] forState:UIControlStateNormal];
        
    [self layoutGUI];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    [containerView setHidden:NO];
    [alertView setHidden:YES];
    
    containerView.layer.cornerRadius = 5.0;
    facebookButton.layer.cornerRadius = 5.0;
    [closeButton setTintColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"]];
    
    
}


- (void)layoutPopup
{

    [containerView setHidden:YES];
    [alertView setHidden:NO];
    [alertView setY:100];
    
}

#pragma mark - Show on view

- (void)showAnimated:(BOOL)animated
{
    [self showAnimated:animated completionBlock:nil];
}


- (void)showAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock
{
    if (_shown == YES) return;
    
    _shown = YES;
    
    if (animated)
    {
        self.superview.userInteractionEnabled = YES;
        
        [UIView animateWithDuration:0.35
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.alpha = 1;
                         }
                         completion:^(BOOL finished) {
                             self.superview.window.userInteractionEnabled = YES;
                             
                             if (completionBlock)
                                 completionBlock();
                         }];
    }
    else
    {
        self.alpha = 1;
        
        if (completionBlock)
            completionBlock();
    }
}


- (void)hideAnimated:(BOOL)animated
{
    [self hideAnimated:YES completionBlock:nil];
}


- (void)hideAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;
{
    if (_shown == NO) return;
    
    _shown = NO;
    
    if (animated)
    {
        self.superview.userInteractionEnabled = NO;
        
        [UIView animateWithDuration:0.35
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             self.superview.userInteractionEnabled = YES;
                             
                             if (completionBlock)
                                 completionBlock();
                         }];
    }
    else
    {
        self.alpha = 0;
        
        if (completionBlock)
            completionBlock();
    }
}

- (IBAction)hidePopup:(id)sender
{
    [self hideAnimated:YES];
}


#pragma mark - Share

- (IBAction)shareAction:(id)sender
{
    LevelData *levelData                         = DATA_SERVICE.dataModel.courseData.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
    SLComposeViewController *shareViewController = [SLComposeViewController new];
    shareViewController                          = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    
    [shareViewController setInitialText:levelData.fbLink];
    [self hideAnimated:YES];
    [APP_DELEGATE.lessonController presentViewController:shareViewController animated:YES completion:nil];
}


#pragma mark - setters

- (void)setAlertText:(NSString *)alertText
{
    _alertText = alertText;
}

- (void)setCongradulationsText:(NSString *)congradulationsText
{
    _congradulationsText = congradulationsText;
    _alertText = nil;
}

- (void)setModulNumber:(NSInteger)modulNumber
{
    _modulNumber = modulNumber;
    
    [self bindGUI];
}

#pragma mark -

@end
