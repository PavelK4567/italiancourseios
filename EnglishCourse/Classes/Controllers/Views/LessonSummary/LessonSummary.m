//
//  LessonSummary.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/8/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LessonSummary.h"
#import "LevelCompletitionViewController.h"
#import "ModuleCertificateView.h"
#import "LMHelper.h"

@implementation LessonSummary
@synthesize imgDiplomaEndLine, imgDiplomaIcon, imgBubble, imgLearnedTimeIcon, imgLearnedWordsIcon, imgPerformanceChart;
@synthesize imgPerformanceMap, imgPerformanceStatus, imgPerformnaceEndLine, imgTeaserCharacter;
@synthesize lblDiplomaDescription, lblGrammarPoints, lblGrammarPointsStatic, lblPerformanceDescription, lblPerformanceTitle;
@synthesize lblTeaserText, lblWeekMinutes, lblWeekMinutesRecomended, lblWordsLearned, lblWordsLearnedStatic;
@synthesize btnDiplomaAction;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    //Interface design
    
    [APPSTYLE applyStyle:@"Label_Progress_Title" toLabel:self.lblPerformanceTitle];
    [APPSTYLE applyStyle:@"Font_Summary_Regular" toLabel:self.lblPerformanceDescription];
    
    [APPSTYLE applyStyle:@"Button_Diploma_Action" toButton:self.btnDiplomaAction];
    [self.btnDiplomaAction setBackgroundColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"]];
    
    [APPSTYLE applyStyle:@"Font_Summary_Regular" toLabel:self.lblGrammarPointsStatic];
    [APPSTYLE applyStyle:@"Font_Summary_Light" toLabel:self.lblWeekMinutesRecomended];
    [APPSTYLE applyStyle:@"Font_Summary_Regular" toLabel:self.lblWordsLearnedStatic];
    
    [APPSTYLE applyStyle:@"Font_Summary_Regular" toLabel:self.lblDiplomaDescription];
    
    
    self.imgPerformnaceEndLine.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Grey_Color_3"];
    self.imgDiplomaEndLine.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Grey_Color_3"];
    
    self.imgBubble.image = [[UIImage imageWithImage:[UIImage imageNamed:@"img_bubble_r.png"] color:[UIColor colorFromHexString:@"#A5BAC2"]] stretchableImageWithLeftCapWidth:21 topCapHeight:14];
    
    [self.imgTeaserCharacter.layer setCornerRadius:4];
    [self.imgTeaserCharacter.layer setMasksToBounds:YES];
    
    [APPSTYLE applyStyle:@"Font_Summary_Light_Teaser" toLabel:self.lblTeaserText];
    
    
    self.lblWordsLearned.text = [NSString stringWithFormat:@"%ld", self.wordCount];
    self.lblGrammarPoints.text = [NSString stringWithFormat:@"%ld", self.grammarCount];
    
    self.lblWeekMinutes.text = [NSString stringWithFormat:Localized(@"T491"), (int)[PROGRESS_MANAGER weekPracticeTime]];
    self.lblWeekMinutesRecomended.text = [NSString stringWithFormat:Localized(@"T491.1"), kParam041];
    
    self.lblWordsLearnedStatic.text = Localized(@"T499.1");
    self.lblGrammarPointsStatic.text = Localized(@"T499.2");
    
    [self.btnDiplomaAction.layer setCornerRadius:3.0];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionMoveIn;
    transition.subtype = kCATransitionFromLeft;
    
    self.isLastLesson = [PROGRESS_MANAGER isLastLessonInLevel:PROGRESS_MANAGER.currentLevelIndex section:PROGRESS_MANAGER.currentSectionIndex unit:PROGRESS_MANAGER.currentUnitIndex];
    
    LevelStatistic *levelStatistic = PROGRESS_MANAGER.userStatistic.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
    ModuleCertificateView *mcv;
    
    
    if ([PROGRESS_MANAGER checkForModuleCertificate:[PROGRESS_MANAGER currentSectionIndex] inLevel:[PROGRESS_MANAGER currentLevelIndex]]) {
        
        if(![LMHelper isModuleSaved:[USER_MANAGER userMsisdn] modul:[PROGRESS_MANAGER currentSectionIndex] level:[PROGRESS_MANAGER currentLevelIndex]]) {
            mcv = [ModuleCertificateView getFromNib];
            
            if(!mcv.isShown) {
                [mcv setModulNumber:[PROGRESS_MANAGER currentSectionIndex] + 1];
                [APP_DELEGATE.window addSubview:mcv];
                [mcv showAnimated:YES];
                
                [LMHelper saveModulCertificate:[PROGRESS_MANAGER currentSectionIndex] level:[PROGRESS_MANAGER currentLevelIndex] user:[USER_MANAGER userMsisdn]];
            }
            
        }
    }
    
    if([PROGRESS_MANAGER areAllLessonsVisitedInLevel:PROGRESS_MANAGER.currentLevelIndex]) {
        if(![PROGRESS_MANAGER haveEnoughPointsInLevel:PROGRESS_MANAGER.currentLevelIndex]) {
            
            //practice more
            if(![LMHelper isLevelCompletitionSaved:[USER_MANAGER userMsisdn] type:0 level:PROGRESS_MANAGER.currentLevelIndex] && self.isLastLesson) {
                [LMHelper saveLevelCompletition:[USER_MANAGER userMsisdn] type:0 level:PROGRESS_MANAGER.currentLevelIndex];
                [APP_DELEGATE.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromLeft];
                LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
                [levelVC setDiplomaPoints:levelStatistic.score];
                [levelVC setDelegate:self];
                [APP_DELEGATE.navigationController presentViewController:levelVC animated:(mcv.isShown ? NO : YES) completion:nil];
            }
        }
        
        else {
            
            //diploma with honor
            if([PROGRESS_MANAGER areAllLessonsVisitedInLevel:PROGRESS_MANAGER.currentLevelIndex] && [PROGRESS_MANAGER haveEnoughPointsInLevel:PROGRESS_MANAGER.currentLevelIndex] && [PROGRESS_MANAGER haveEnoughStarsInLevel:PROGRESS_MANAGER.currentLevelIndex]){
                
                if(![LMHelper isLevelCompletitionSaved:[USER_MANAGER userMsisdn] type:1 level:PROGRESS_MANAGER.currentLevelIndex]) {
                    [LMHelper saveLevelCompletition:[USER_MANAGER userMsisdn] type:1 level:PROGRESS_MANAGER.currentLevelIndex];
                    [APP_DELEGATE.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromLeft];
                    LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
                    [levelVC setNumberOfLessons:-1];
                    [levelVC setDiplomaPoints:-1];
                    [APP_DELEGATE.navigationController presentViewController:levelVC animated:YES completion:nil];
                }
            }
            
            else
            {
                //diploma without honor
                if(![LMHelper isLevelCompletitionSaved:[USER_MANAGER userMsisdn] type:2 level:PROGRESS_MANAGER.currentLevelIndex]) {
                    [LMHelper saveLevelCompletition:[USER_MANAGER userMsisdn] type:2 level:PROGRESS_MANAGER.currentLevelIndex];
                    [APP_DELEGATE.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromLeft];
                    LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
                    [levelVC setNumberOfLessons:[PROGRESS_MANAGER numberOfLessonsInLevel:PROGRESS_MANAGER.currentLevelIndex]];
                    [APP_DELEGATE.navigationController presentViewController:levelVC animated:YES completion:nil];
                }
            }
            
        }
        
    }
    
    else
    {
        
        if(self.isLastLesson) {
            [APP_DELEGATE.navigationController.view.layer addAnimation:transition forKey:kCATransitionFromLeft];
            LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
            [levelVC setDiplomaPoints:levelStatistic.score];
            [levelVC setDelegate:self];
            [APP_DELEGATE.navigationController presentViewController:levelVC animated:(mcv.isShown ? NO : YES) completion:nil];
        }
    }
}


- (void)setLessonInfo:(Unit *)lessonInfo {
    
    Character *character = [DATA_SERVICE findCharacterByID:[lessonInfo characterID]];
    
    //    NSBubbleData *bubbleData = [NSBubbleData dataWithText:lessonInfo.teaser date:nil type:BubbleTypeMine];
    
    NSBubbleData *bubbleData = [NSBubbleData dataWithText:lessonInfo.teaser date:nil type:BubbleTypeMine index:0 chatType:SIMPLE_CHAT answersArray:nil audioSample:nil drawAnswered:NO];
    
    bubbleData.isAnswer = NO;
    bubbleData.showAvatar = YES;
    bubbleData.avatar = [UIImage imageFromDocumentsResourceFile:character.image];
    bubbleData.colorInHEX = character.color;
    
    [self setNewInfo:bubbleData];
    
    //    self.wordCount = lessonInfo.wordCount;
    //    self.grammarCount = lessonInfo.grammarCount;
    
}

- (void)setLessonInfo:(Unit *)lessonInfo andTeaserText:(NSString *)stringTeaser {
    
    if (stringTeaser) {
        
        NSBubbleData *bubbleData = [NSBubbleData dataWithText:stringTeaser date:nil type:BubbleTypeMine index:0 chatType:SIMPLE_CHAT answersArray:nil audioSample:nil drawAnswered:NO];
        
        bubbleData.isAnswer = NO;
        bubbleData.showAvatar = NO;
        bubbleData.colorInHEX = @"#fae2ca";
        
        [self setNewInfo:bubbleData];
        
    }
    else {
        
        Character *character = [DATA_SERVICE findCharacterByID:[lessonInfo characterID]];
        
        //    NSBubbleData *bubbleData = [NSBubbleData dataWithText:lessonInfo.teaser date:nil type:BubbleTypeMine];
        
        NSBubbleData *bubbleData = [NSBubbleData dataWithText:lessonInfo.teaser date:nil type:BubbleTypeMine index:0 chatType:SIMPLE_CHAT answersArray:nil audioSample:nil drawAnswered:NO];
        
        bubbleData.isAnswer = NO;
        bubbleData.showAvatar = YES;
        bubbleData.avatar = [UIImage imageFromDocumentsResourceFile:character.image];
        bubbleData.colorInHEX = character.color;
        
        [self setNewInfo:bubbleData];
    }
    
    //    self.wordCount = lessonInfo.wordCount;
    //    self.grammarCount = lessonInfo.grammarCount;
}

- (void)setNewInfo:(NSBubbleData *)data {
    
    if (self.bubbleView == nil) {
        
        CGRect frame = data.view.frame;
        frame.origin.y = 515;
        
        self.bubbleView = [[LMBubbleView alloc] initWithFrame:CGRectMake(0, 515, frame.size.width, frame.size.height)];
        
        self.bubbleView.bubbleType = BUBBLE_SUMMARY;
        
        if (data.showAvatar) {
            
            self.bubbleView.avatarImage.hidden = NO;
            self.bubbleView.showAvatar = YES;
            
        }
        else {
            
            self.bubbleView.avatarImage.hidden = YES;
            
        }
        
        [self.bubbleView setDataInternal:data];
        [self addSubview:self.bubbleView];
        
        [self setContentSize:CGSizeMake(LM_WIDTH, self.bubbleView.frame.origin.y+self.bubbleView.frame.size.height+20)];
    }
    else {
        
        [self.bubbleView setDataInternal:data];
        if (data.showAvatar) {
            
            self.bubbleView.showAvatar = YES;
            
            self.bubbleView.avatarImage.hidden = NO;
            
        }
        else {
            
            self.bubbleView.avatarImage.hidden = YES;
            
        }
        [self setContentSize:CGSizeMake(LM_WIDTH, self.bubbleView.frame.origin.y+self.bubbleView.frame.size.height+20)];
        
    }
}

- (void)setSumarryPageData:(NSDictionary *)sumarryPageData {
    
    if (sumarryPageData == nil) {
        
        float correctAnswersPercent = 0.0;
        float incorrectAnswersPercent = 0.0;
        
        if (self.progressCustomView == nil) {
            
            
            self.progressCustomView = [ProgressCustomView loadFromNib];
            self.progressCustomView.frame = CGRectMake(0, self.lblPerformanceDescription.y+self.lblPerformanceDescription.mHeight+5, 320, 80);
            
            NSArray *arrayColors = @[[UIColor colorWithRed:165.0/255.0 green:211.0/255.0 blue:143.0/255.0 alpha:1.0],
                                     [UIColor colorWithRed:232.0/255.0 green:125.0/255.0 blue:126.0/255.0 alpha:1.0],
                                     [UIColor colorWithRed:179.0/255.0 green:177.0/255.0 blue:182.0/255.0 alpha:1.0]
                                     ];
            [self.progressCustomView.progressLabel setBackBorderWidth: 20.0];
            [self.progressCustomView.progressLabel setFrontBorderWidth: 19.0];
            [self.progressCustomView.progressLabel setIsMultipart:YES];
            
            [self.progressCustomView.progressLabel setArrayColorsMultiparts:arrayColors];
            
            [self addSubview:self.progressCustomView];
            
        }
        
        
        [self.progressCustomView.progressLabel setArrayParts:@[@(correctAnswersPercent/100.0),
                                                               @((correctAnswersPercent+incorrectAnswersPercent)/100.0),
                                                               @(100.0/100.0)]];
        
    }
    else {
        
        NSInteger correctAnswers = [[sumarryPageData valueForKey:@"numberOfCorrectAnswers"] integerValue];
        NSInteger incorrectAnswers = [[sumarryPageData valueForKey:@"numberOfIncorrectAnswers"] integerValue];
        NSInteger skippedAnswers = [[sumarryPageData valueForKey:@"numberOfSkippedQuestions"] integerValue];
        NSInteger totalQuestions = [[sumarryPageData valueForKey:@"numberOfTotalQuestions"] integerValue];
        //old version
        //    NSInteger totalQuestions = correctAnswers + incorrectAnswers + skippedAnswers;
        
        float correctAnswersPercent = (float)correctAnswers*100.0/(float)totalQuestions;
        float incorrectAnswersPercent = (float)incorrectAnswers*100.0/(float)totalQuestions;
        float skippedAnswersPercent = (float)skippedAnswers*100.0/(float)totalQuestions;
        
        
        DLog(@"Correct percent - %f", correctAnswersPercent);
        DLog(@"Incorrect percent - %f", incorrectAnswersPercent);
        DLog(@"Skipped percent - %f", skippedAnswersPercent);
        
        
        if (self.progressCustomView == nil) {
            
            
            self.progressCustomView = [ProgressCustomView loadFromNib];
            self.progressCustomView.frame = CGRectMake(0, self.lblPerformanceDescription.y+self.lblPerformanceDescription.mHeight+5, 320, 80);
            
            NSArray *arrayColors = @[[UIColor colorWithRed:165.0/255.0 green:211.0/255.0 blue:143.0/255.0 alpha:1.0],
                                     [UIColor colorWithRed:232.0/255.0 green:125.0/255.0 blue:126.0/255.0 alpha:1.0],
                                     [UIColor colorWithRed:179.0/255.0 green:177.0/255.0 blue:182.0/255.0 alpha:1.0]
                                     ];
            [self.progressCustomView.progressLabel setBackBorderWidth: 20.0];
            [self.progressCustomView.progressLabel setFrontBorderWidth: 19.0];
            [self.progressCustomView.progressLabel setIsMultipart:YES];
            
            [self.progressCustomView.progressLabel setArrayColorsMultiparts:arrayColors];
            
            [self addSubview:self.progressCustomView];
            
        }
        
        
        [self.progressCustomView.progressLabel setArrayParts:@[@(correctAnswersPercent/100.0),
                                                               @((correctAnswersPercent+incorrectAnswersPercent)/100.0),
                                                               @(100.0/100.0)]];
    }
    
}

#pragma mark - Setter for progression

- (void)setStringPerformanceTitle:(NSString *)stringPerformanceTitle {
    
    self.lblPerformanceTitle.numberOfLines = 0;
    self.lblPerformanceTitle.text = stringPerformanceTitle;
    [self.lblPerformanceTitle sizeToFit];
    
    self.lblPerformanceTitle.center = CGPointMake(LM_WIDTH_INSTACE/2, self.lblPerformanceTitle.center.y);
    
}

- (void)setStringPerformanceDescription:(NSString *)stringPerformanceDescription {
    
    self.lblPerformanceDescription.numberOfLines = 0;
    self.lblPerformanceDescription.text = stringPerformanceDescription;
    [self.lblPerformanceDescription sizeToFit];
    self.lblPerformanceDescription.center = CGPointMake(LM_WIDTH_INSTACE/2, self.lblPerformanceDescription.center.y);
    
}

- (void)setIsSuccessful:(BOOL)isSuccessful {
    
    if (isSuccessful) {
        
        [self.imgPerformanceStatus setImage:[UIImage imageNamed:@"ic_star_big"]];
    }
    else {
        
        [self.imgPerformanceStatus setImage:[UIImage imageNamed:@"ic_star2_big"]];
    }
}

- (void)setStringBtnDiplomaAction:(NSString *)stringBtnDiplomaAction {
    
    CGSize stringsize = [stringBtnDiplomaAction sizeWithAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue" size:15]}];
    
    if ([stringBtnDiplomaAction isEqualToString:@"-"]) {
        
        self.btnDiplomaAction.hidden = YES;
    }
    else {
        
        self.btnDiplomaAction.hidden = NO;
    }
    
    self.btnDiplomaAction.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.btnDiplomaAction setTitle:stringBtnDiplomaAction forState:UIControlStateNormal];
    
    btnDiplomaAction.frame = CGRectMake(btnDiplomaAction.frame.origin.x, lblDiplomaDescription.origin.y+lblDiplomaDescription.frame.size.height+10, stringsize.width+22, 33);
    
    //    imgDiplomaIcon.y = self.lblDiplomaDescription.y;
    
    imgDiplomaEndLine.y = self.btnDiplomaAction.y+self.btnDiplomaAction.mHeight + 15;
    imgLearnedWordsIcon.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblWordsLearned.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblWordsLearnedStatic.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblGrammarPoints.y = lblWordsLearnedStatic.y + lblWordsLearnedStatic.mHeight + 2;
    lblGrammarPointsStatic.y = lblWordsLearnedStatic.y + lblWordsLearnedStatic.mHeight + 2;
    
    
    imgLearnedTimeIcon.y = imgLearnedWordsIcon.y+imgLearnedWordsIcon.mHeight+20;
    lblWeekMinutes.y = imgLearnedTimeIcon.y;
    lblWeekMinutesRecomended.y = lblWeekMinutes.y + lblWeekMinutes.mHeight + 2;
}

- (void)setStringDiplomaAction:(NSString *)stringDiplomaAction {
    
    lblDiplomaDescription.numberOfLines = 0;
    lblDiplomaDescription.text = stringDiplomaAction;
    [lblDiplomaDescription sizeToFit];
    
    btnDiplomaAction.frame = CGRectMake(btnDiplomaAction.frame.origin.x, lblDiplomaDescription.origin.y+lblDiplomaDescription.frame.size.height+10, btnDiplomaAction.mWidth, btnDiplomaAction.mHeight);
    
    imgDiplomaEndLine.y = self.btnDiplomaAction.y+self.btnDiplomaAction.mHeight + 15;
    imgLearnedWordsIcon.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblWordsLearned.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblWordsLearnedStatic.y = imgDiplomaEndLine.y+imgDiplomaEndLine.mHeight + 15;
    lblGrammarPoints.y = lblWordsLearnedStatic.y + lblWordsLearnedStatic.mHeight + 2;
    lblGrammarPointsStatic.y = lblWordsLearnedStatic.y + lblWordsLearnedStatic.mHeight + 2;
    
    imgLearnedTimeIcon.y = imgLearnedWordsIcon.y+imgLearnedWordsIcon.mHeight+20;
    lblWeekMinutes.y = imgLearnedTimeIcon.y;
    lblWeekMinutesRecomended.y = lblWeekMinutes.y + lblWeekMinutes.mHeight + 2;
}

- (void)setStatisticsValue:(NSInteger)levelIndex
                    module:(NSInteger)moduleIndex
                    lesson:(NSInteger)lessonIndex
                    points:(NSInteger)levelScore {
    
    NSArray *arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];
    LevelData *levelData = [arrayCourseLevels objectAtIndex:levelIndex];
    NSString *levelName = levelData.levelName;
    
    
    //    DLog(@"3 conditions: isLevelCompleted - %d; isLessonCompleted - %d, levelScore < 1500 - %d", [PROGRESS_MANAGER isLevelCompleted:levelIndex], [PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex], levelScore < kParam043?true:false);
    
    
    NSInteger startedLessons = [PROGRESS_MANAGER numOfStartLesson:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    NSInteger numberOfLessons = [[DataService sharedInstance] numOfLessonInLevel:[PROGRESS_MANAGER currentLevelIndex]];
    
    BOOL isLevelCompleted = startedLessons == numberOfLessons;
    
    if (!isLevelCompleted && ![PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore < kParam043) {
        
        self.stringBtnDiplomaAction = Localized(@"T488");
        self.stringDiplomaAction = Localized(@"T482");
        self.typeOfAction = 2;
        
    }
    else if (!isLevelCompleted && [PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore < kParam043) {
        
        self.stringBtnDiplomaAction = Localized(@"T489");
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T483"), kParam043-levelScore, levelName];
        self.typeOfAction = 1;
    }
    else if (!isLevelCompleted && [PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore >= kParam043) {
        
        self.stringBtnDiplomaAction = Localized(@"T489");
        //        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T484"), [PROGRESS_MANAGER getNumberOfLessonsToCompleteLevel:levelIndex], levelData.levelName];
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T484"), numberOfLessons-startedLessons, levelData.levelName];
        
        self.typeOfAction = 1;
        
    }
    else if (!isLevelCompleted && ![PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore >= kParam043) {
        
        self.stringBtnDiplomaAction = Localized(@"T488");
        self.stringDiplomaAction = Localized(@"T485");
        self.typeOfAction = 2;
    }
    else if (isLevelCompleted && levelScore >= kParam043) {
        
        self.stringBtnDiplomaAction = [NSString stringWithFormat:Localized(@"T490"), levelData.levelName];
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T486"), levelData.levelName];
        self.typeOfAction = 3;
    }
    else if (isLevelCompleted && [PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore < kParam043) {
        
        //no button
        self.stringBtnDiplomaAction = @"-";
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T487"), kParam043-levelScore, levelName];
    }
    else if (isLevelCompleted && ![PROGRESS_MANAGER isLessonCompleted:levelIndex section:moduleIndex unit:lessonIndex] && levelScore < kParam043) {
        
        self.stringBtnDiplomaAction = Localized(@"T488");
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T487"), kParam043-levelScore, levelName];
        self.typeOfAction = 2;
    }
    else if (!isLevelCompleted && levelScore >= kParam043) {
        
        self.stringBtnDiplomaAction = [NSString stringWithFormat:Localized(@"T490"), levelData.levelName];
        self.stringDiplomaAction = [NSString stringWithFormat:Localized(@"T486"), levelData.levelName];
        self.typeOfAction = 3;
    }
    
    
    
    
    
}

#pragma -
#pragma mark - Next button

- (IBAction)nextButtonAction:(id)sender {
    
    if (self.typeOfAction == 1) {
        
        [self.lessonSummaryDelegate jumpToNewLesson:self];
        
    }
    else if (self.typeOfAction == 2) {
        
        [self.lessonSummaryDelegate jumpToFirstSkippedOrWrongAnswered:self];
        
    }
    else if (self.typeOfAction == 3) {
        
        [self levelCompletitionScreenShow:self];
        
    }
}

- (void)levelCompletitionScreenShow:(id)sender {
    
    //    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    //    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    //
    //
    //    LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
    //    [levelVC setDiplomaPoints:currentLevelStatistics.score];
    //    [APP_DELEGATE.navigationController presentViewController:levelVC animated:YES completion:nil];
    
    //diploma with honor
    if([PROGRESS_MANAGER areAllLessonsVisitedInLevel:PROGRESS_MANAGER.currentLevelIndex] && [PROGRESS_MANAGER haveEnoughPointsInLevel:PROGRESS_MANAGER.currentLevelIndex] && [PROGRESS_MANAGER haveEnoughStarsInLevel:PROGRESS_MANAGER.currentLevelIndex]){
        LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
        [levelVC setNumberOfLessons:-1];
        [levelVC setDiplomaPoints:-1];
        [APP_DELEGATE.navigationController presentViewController:levelVC animated:YES completion:nil];
    }
    
    else
    {
        //diploma without honor
        
        LevelCompletitionViewController *levelVC = [[LevelCompletitionViewController alloc] initWithNibName:@"LevelCompletitionViewController" bundle:nil];
        [levelVC setNumberOfLessons:[PROGRESS_MANAGER numberOfLessonsInLevel:PROGRESS_MANAGER.currentLevelIndex]];
        [APP_DELEGATE.navigationController presentViewController:levelVC animated:YES completion:nil];
    }
    
    
}


#pragma mark - practiceMore from LevelCompletitionViewController

- (void)practiceMoreInLevel:(NSInteger)level
                    section:(NSInteger)section
                       unit:(NSInteger)unit
{
    [self.lessonSummaryDelegate jumpToFirstLessonWithoutStar:level section:section unit:unit];
}

@end
