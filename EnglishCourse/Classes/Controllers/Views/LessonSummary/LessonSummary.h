//
//  LessonSummary.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 1/8/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgressCustomView.h"
#import "LevelCompletitionViewController.h"

@protocol LessonSummaryDelegate <NSObject>

@required

- (void)jumpToNewLesson:(id)sender;
- (void)jumpToFirstSkippedOrWrongAnswered:(id)sender;
- (void)jumpToFirstLessonWithoutStar:(NSInteger)level
                             section:(NSInteger)section
                                unit:(NSInteger)unit;

@end

@interface LessonSummary : UIScrollView <PracticeMoreDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgPerformanceStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblPerformanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPerformanceDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgPerformanceMap;
@property (weak, nonatomic) IBOutlet UIImageView *imgPerformanceChart;
@property (weak, nonatomic) IBOutlet UIImageView *imgPerformnaceEndLine;


@property (weak, nonatomic) IBOutlet UIImageView *imgDiplomaIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDiplomaDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnDiplomaAction;
@property (weak, nonatomic) IBOutlet UIImageView *imgDiplomaEndLine;

@property (weak, nonatomic) IBOutlet UIImageView *imgLearnedWordsIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgLearnedTimeIcon;

@property (weak, nonatomic) IBOutlet UILabel *lblWordsLearned;
@property (weak, nonatomic) IBOutlet UILabel *lblWordsLearnedStatic;
@property (weak, nonatomic) IBOutlet UILabel *lblGrammarPointsStatic;
@property (weak, nonatomic) IBOutlet UILabel *lblGrammarPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblWeekMinutes;
@property (weak, nonatomic) IBOutlet UILabel *lblWeekMinutesRecomended;

@property (weak, nonatomic) IBOutlet ProgressCustomView *progressCustomView;

@property (weak, nonatomic) IBOutlet UIImageView *imgTeaserCharacter;
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblTeaserText;

@property (strong, nonatomic) LMBubbleView *bubbleView;

@property (nonatomic, weak) id<LessonSummaryDelegate> lessonSummaryDelegate;

- (IBAction)nextButtonAction:(id)sender;

@property (nonatomic, assign) NSDictionary *sumarryPageData;

@property (nonatomic, assign) NSInteger wordCount;
@property (nonatomic, assign) NSInteger grammarCount;


////////////////////////

@property (nonatomic, assign) Unit *lessonInfo;
@property (nonatomic, assign) NSString *stringPerformanceTitle;
@property (nonatomic, assign) NSString *stringPerformanceDescription;
@property (nonatomic, assign) BOOL isSuccessful;
@property (nonatomic, assign) BOOL isFirstTime;
@property (nonatomic, assign) NSString *stringDiplomaAction;
@property (nonatomic, assign) NSString *stringBtnDiplomaAction;
@property (nonatomic, assign) NSInteger typeOfAction;
@property (nonatomic) BOOL isLastLesson;

- (void)setStatisticsValue:(NSInteger)levelIndex
                    module:(NSInteger)moduleIndex
                    lesson:(NSInteger)lessonIndex
                    points:(NSInteger)levelScore;

- (void)levelCompletitionScreenShow:(id)sender;

- (void)setLessonInfo:(Unit *)lessonInfo andTeaserText:(NSString *)stringTeaser;

@end
