//
//  InstructionView.m
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/25/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "InstructionView.h"

@implementation InstructionView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        [self.viewForGestures setBackgroundColor:[UIColor clearColor]];
        [self.viewInstructionTextBackground setBackgroundColor:[UIColor colorWithRed:72.0/255.0 green:61.0/255.0 blue:139.0/255.0 alpha:1.0]];
        
        self.txtInstructionText.textAlignment = NSTextAlignmentCenter;
        [self.txtInstructionText setTextColor:[UIColor whiteColor]];
        [self.txtInstructionText setFont:[UIFont systemFontOfSize:14.0]];
        [self.txtInstructionText setBackgroundColor:[UIColor clearColor]];
        [self.txtInstructionText setEditable:NO];
        self.txtInstructionText.alpha = 0.0;
        
        
//        [self.txtInstructionText setTextContainerInset:UIEdgeInsetsZero];
//        [self.txtInstructionText addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];

        [self.viewInstructionTextBackground addSubview:self.txtInstructionText];
        
        
        self.originFrame = CGRectMake(LM_WIDTH-39, 0, 39, 39);
        
        [self addSubview:self.viewInstructionTextBackground];
        
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeTheInstructionView:)];
        [self.viewForGestures addGestureRecognizer:self.tapGestureRecognizer];
        
        self.swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeTheInstructionView:)];
        [self.swipeGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown];
        [self.viewForGestures addGestureRecognizer:self.swipeGestureRecognizer];
        
        [self.viewForGestures setUserInteractionEnabled:NO];
        
    }
    
    return self;
}


- (void)awakeFromNib {
    
    self.originFrame = CGRectMake(LM_WIDTH-39, 0, 39, 39);
    
    [self setBackgroundColor:[UIColor clearColor]];
    
    [self.viewForGestures setBackgroundColor:[UIColor clearColor]];
    [self.viewInstructionTextBackground setBackgroundColor:[UIColor colorWithRed:72.0/255.0 green:61.0/255.0 blue:139.0/255.0 alpha:1.0]];
    
    [self.btnInstructionButton setBackgroundColor:[UIColor colorWithRed:72.0/255.0 green:61.0/255.0 blue:139.0/255.0 alpha:1.0]];

    self.txtInstructionText.textAlignment = NSTextAlignmentCenter;
    [self.txtInstructionText setTextColor:[UIColor whiteColor]];
    [self.txtInstructionText setFont:[UIFont systemFontOfSize:14.0]];
    [self.txtInstructionText setBackgroundColor:[UIColor clearColor]];
    [self.txtInstructionText setEditable:NO];
    self.txtInstructionText.alpha = 0.0;
    
    
    //        [self.txtInstructionText setTextContainerInset:UIEdgeInsetsZero];
    //        [self.txtInstructionText addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeTheInstructionView:)];
    [self.viewForGestures addGestureRecognizer:self.tapGestureRecognizer];
    
    self.swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeTheInstructionView:)];
    [self.swipeGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionUp | UISwipeGestureRecognizerDirectionDown];
    [self.viewForGestures addGestureRecognizer:self.swipeGestureRecognizer];
    
    [self.viewForGestures setUserInteractionEnabled:NO];
}



#pragma mark - Gestire recognizer method

- (void)closeTheInstructionView:(id)sender {
    
    DLog(@"Gesture recognizer called");
    
    if (self.isExpanded)
        [self collapseInstructionAfterGestureEvent];
    
}


#pragma mark - Expand and collapse methods

- (IBAction)toogleInstructionView:(id)sender {
    
    if (!self.isExpanded) {
        
        [self.viewForGestures setUserInteractionEnabled:YES];

        [self expandInstructionView];
        //self.txtInstructionText.text = @"Some instructions for the vehicle, Some instructions for the vehicle, Some instructions for the vehicle, Some instructions for the vehicle";

    }
    else {
        
        [self collapseInstructionView];
    }
    
    self.isExpanded = !self.isExpanded;
    
}

- (void)openInstructionView {
    
    [self.viewForGestures setUserInteractionEnabled:YES];
    [self expandInstructionView];
    self.isExpanded = YES;
}

- (void)expandInstructionView {
    
    [self.txtInstructionText scrollRectToVisible:CGRectMake(0, 0, 20, 20) animated:NO];
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.frame = CGRectMake(0, self.frame.origin.y, LM_WIDTH, self.frame.size.height);
        self.txtInstructionText.alpha = 1.0;

        
    } completion:^(BOOL finished) {
        
        [self finishExpanding];
        
    }];

}

- (void)finishExpanding {
    
    UIFont *font = self.txtInstructionText.font;
    NSString *strTextToTranslate = self.txtInstructionText.text;
    
    
    NSAttributedString *attributedText = [[NSAttributedString alloc]
                                          initWithString:strTextToTranslate
                                          attributes:@{ NSFontAttributeName: [UIFont systemFontOfSize:14.0]}];
    
    const CGRect paragraphRect = [attributedText boundingRectWithSize:CGSizeMake(self.txtInstructionText.frame.size.width, 300)
                                                              options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                              context:nil];
    CGSize contentSize = paragraphRect.size;
    
    
    float numberOfLines = contentSize.height / font.lineHeight;

    
    float fontLineHeight = [self.txtInstructionText font].lineHeight;
    
    [UIView animateWithDuration:0.4 animations:^{
        
        if (numberOfLines == 1 || [self.txtInstructionText.text length] == 0) {
            
            self.txtInstructionText.center = CGPointMake(self.txtInstructionText.center.x, self.viewInstructionTextBackground.mHeight/2);
        }
        else if (numberOfLines == 2) {
           self.viewInstructionTextBackground.height = fontLineHeight*2 + 4;
            self.txtInstructionText.center = CGPointMake(self.txtInstructionText.center.x, (fontLineHeight*2 + 4)/2);
        }
        else {
            self.viewInstructionTextBackground.height = fontLineHeight*3 + 4;
            self.txtInstructionText.center = CGPointMake(self.txtInstructionText.center.x, (fontLineHeight*3 + 4)/2);
        }
        
        self.height = 38+5+LM_SCROLL_HEIGHT;
        self.x = 0;
        
    } completion:^(BOOL finished) {

    }];
}


- (void)collapseInstructionView {
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.viewInstructionTextBackground.height = 34;
        self.height = 39;
        
    } completion:^(BOOL finished) {
        
        [self finishCollapsing];
        
    }];
}

- (void)finishCollapsing {

    [UIView animateWithDuration:0.4 animations:^{
        
        self.frame = self.originFrame;
        self.isExpanded = NO;
        
    }];
}

#pragma mark - Collaps after gesture event

- (void)collapseInstructionAfterGestureEvent {
    
    [self.viewForGestures setUserInteractionEnabled:NO];

    [UIView animateWithDuration:0.4 animations:^{
        
        self.viewInstructionTextBackground.height = 34;
        self.height = 39;

        
    } completion:^(BOOL finished) {
        
        [self finishCollapsingAfterGestureEvent];
        
    }];
}

- (void)finishCollapsingAfterGestureEvent {
    
    [UIView animateWithDuration:0.4 animations:^{
        
        self.frame = self.originFrame;
        self.isExpanded = NO;
        
    }];
}


#pragma mark - Play sound

- (void)playInstructionSound:(NSString *)instructionSound {
    
    self.strInstructionSound = instructionSound;
    
    //Audio manager will play the sound
    
}

- (void)setTheInstructionText:(NSString *)instructionText {
    
    self.strInstructionText = instructionText;
    self.txtInstructionText.text = instructionText;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *txtview = object;
    CGFloat topoffset = ([txtview bounds].size.height - [txtview contentSize].height * [txtview zoomScale])/2.0;
    topoffset = ( topoffset < 0.0 ? 0.0 : topoffset );
    txtview.contentOffset = (CGPoint){.x = 0, .y = -topoffset};
}

- (void)dealloc {
    
    [self.txtInstructionText removeObserver:self forKeyPath:@"contentSize"];
    

}

@end
