//
//  FramesPresentationDialogScreen.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "FramesPresentationDialogScreen.h"

#import "ZipFile.h"
#import "ZipException.h"
#import "FileInZipInfo.h"
#import "ZipWriteStream.h"
#import "ZipReadStream.h"

#define heightForLineInProgressView 3

@implementation FramesPresentationDialogScreen{
  BOOL isRecordingImage;
  BOOL automaticPlay;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    
        /*UILabel *lblTest = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 300, 40)];
        lblTest.text = @"Test: Frames presentation dialog";
        [self addSubview:lblTest];
        */
        [self setBackgroundColor:[UIColor orangeColor]];
        [self makeFirstScreen];
    }
  
    return self;
}

-(void)makeFirstScreen{
  int upSpace = 20;
  if (IS_IPHONE4) {
    upSpace = 30;
  }
  baseView = [[UIView alloc] initWithFrame:CGRectMake(0, [self.viewTitleLabel mHeight]-upSpace, [self mWidth] , [self mHeight])];
  baseView.backgroundColor = [UIColor clearColor];
  [self addSubview:baseView];
  
  mainImageView =[[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-320)/2, 10, 320, 162)];
  if (IS_IPHONE4) {
    [mainImageView setFrame:CGRectMake((self.frame.size.width-320)/2, 5, 320, 162)];
  }
  mainImageView.backgroundColor = [UIColor blueColor];
  [baseView addSubview:mainImageView];
  
  profileSoundButton = [UIButton buttonWithType:UIButtonTypeCustom];
  profileSoundButton.frame = CGRectMake(6, 198-upSpace, 33, 33);
  soundPlay = NO;
  [profileSoundButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
  [profileSoundButton addTarget:self action:@selector(playFrameSound:) forControlEvents:UIControlEventTouchUpInside];
  profileSoundButton.hidden = NO;
  [baseView addSubview:profileSoundButton];
  
  
  progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
  progressView.progressTintColor=[APPSTYLE colorForType:@"Italy_Main_Red_Color"];
  //[[progressView layer]setCornerRadius:10.0f];
  //[[progressView layer]setBorderWidth:2.0f];
  [[progressView layer]setMasksToBounds:TRUE];
  progressView.clipsToBounds = YES;
  [[progressView layer]setFrame:CGRectMake(40, 213-upSpace, 268, 4)];
  //[[progressView layer]setBorderColor:[UIColor blackColor].CGColor];
  progressView.trackTintColor = [APPSTYLE colorForType:@"Gray_light"];
  [progressView setProgress: 0 animated:YES];
  [baseView addSubview:progressView];
  
  /*textView = [[UIView alloc]initWithFrame:CGRectMake(0, 235, self.frame.size.width, 47)];
  textView.backgroundColor = [UIColor lightGrayColor];
  [baseView addSubview:textView];*/
  
  mainScroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 235-upSpace, self.frame.size.width, 47)];
  mainScroll.backgroundColor = [UIColor whiteColor];//[APPSTYLE colorForType:@"Gray_light"];
  [baseView addSubview:mainScroll];

  
  continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
  continueButton.frame = CGRectMake((340-128)/2, 303-upSpace, 128, 39);
  [APPSTYLE applyTitle:Localized(@"T128") toButton:continueButton];
  [continueButton setHidden:YES];
  [baseView addSubview:continueButton];
  continueButton.layer.cornerRadius = 4.0;
  //continueButton.layer.borderWidth = 1;
  continueButton.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"];
  
  recordAgainButton = [UIButton buttonWithType:UIButtonTypeCustom];
  recordAgainButton.frame = CGRectMake(24, 303-upSpace, 128, 39);
  [APPSTYLE applyTitle:Localized(@"T127") toButton:recordAgainButton];
  [recordAgainButton setHidden:YES];
  [baseView addSubview:recordAgainButton];
  recordAgainButton.layer.cornerRadius = 4.0;
  recordAgainButton.layer.borderWidth = 1;
  recordAgainButton.backgroundColor = [UIColor whiteColor];
  [recordAgainButton.layer setBorderColor: [APPSTYLE colorForType:@"Italy_Main_Green_Color"].CGColor];
  [recordAgainButton setTitleColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"] forState:UIControlStateNormal];
  
  //for long version
  ////////
  progressViewForLongVersion = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
  progressViewForLongVersion.progressTintColor =[APPSTYLE colorForType:@"Italy_Main_Red_Color"];
  [[progressViewForLongVersion layer]setMasksToBounds:TRUE];
  progressViewForLongVersion.clipsToBounds = YES;
  [[progressViewForLongVersion layer]setFrame:CGRectMake(35, 213-upSpace, 268, 4)];
  progressViewForLongVersion.trackTintColor = [APPSTYLE colorForType:@"Gray_light"];
  [progressViewForLongVersion setProgress: 0 animated:YES];
  [baseView addSubview:progressViewForLongVersion];
  [progressViewForLongVersion setHidden:YES];
}

- (void)loadData
{
  curentFrameDialog = 0;
  currentGameStatus = PlaybackIntroStatus;
  [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T120")];

  //[baseView setFrame:CGRectMake(0, [self.viewTitleLabel height], [self width] , [self height])];
  segmentsArray =  [(FramesPresentationDialog *)self.dictVehicleInfo segmentsArray];
  [self setDataForNewStep];
  
  allCharactersArray = [[DataService sharedInstance] findAllCharacters];
  charactersArray = [[NSMutableArray alloc]init];
  
  //make buttons in progressView
  progresButtonsArray = [NSMutableArray arrayWithCapacity:[segmentsArray count]];
  int buttonDistance = progressView.frame.size.width / [segmentsArray count];
  maxFrameDialog = (int)[segmentsArray count]-1;
  for(int i = 0; i < [segmentsArray count]; i++){
    //take characters for this page
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:i];
//    Character *character = allCharactersArray[(tempSegmentFramesPresentation.characterID - 1)];
    Character *character = [DATA_SERVICE findCharacterByID:tempSegmentFramesPresentation.characterID];
    if([charactersArray count]>0){
      Character *lastCharacter = [charactersArray objectAtIndex:0];
      if (lastCharacter.mID  != character.mID) {
        if([charactersArray count]<2)
        [charactersArray addObject:character];
      }
    }else{
      [charactersArray addObject:character];
    }
    UIButton *tempProgresButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tempProgresButton.frame = CGRectMake(progressView.x+buttonDistance*i, progressView.y - ((4+progressView.mHeight)/2), heightForLineInProgressView, 12);
    tempProgresButton.tag = i;
    [tempProgresButton addTarget:self action:@selector(progresButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:tempProgresButton];
    if(i!=0)
      tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Gray_light"];
    [progresButtonsArray addObject:tempProgresButton];
    
    UIButton *tempBigButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tempBigButton.frame = CGRectMake(progressView.x+buttonDistance*i, progressView.y - 16, 33, 33);
    tempBigButton.tag = i;
    [tempBigButton addTarget:self action:@selector(progresButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:tempBigButton];
    tempBigButton.backgroundColor = [UIColor clearColor];
    
    /*NSString *audioUrl = [self recordingPathForIndex:i order:self.instanceOrder];
    DLog(@"recordingCompleteWithRecordFile %@",audioUrl);
      if ([[NSFileManager defaultManager] fileExistsAtPath:audioUrl]) {
        [[NSFileManager defaultManager] removeItemAtPath:audioUrl error:nil];
      }*/
   //[self.viewTitleLabel setTitleForFramePresentation:[(FramesPresentationDialog *)self.dictVehicleInfo sectionTitle]];
    
  }
  //chek if is simple
  if(![(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
    
    Character *lastCharacter = [charactersArray objectAtIndex:0];
    charactersArray = [[NSMutableArray alloc]init];
    [charactersArray addObject:lastCharacter];
    [self goToSpeakingRecordingModeStatus:nil];
    
  }
  
}


- (void)loadDataForLongVersion
{
  
  progresButtonsArrayForLongVersion = [[NSMutableArray alloc]init];
  numOfRecordForLongVersion = 0;
  
  for(int i = 0; i < [segmentsArray count]; i++){
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:i];
//    Character *character = allCharactersArray[(tempSegmentFramesPresentation.characterID - 1)];
    Character *character = [DATA_SERVICE findCharacterByID:tempSegmentFramesPresentation.characterID];
    if(selectedCharacter.mID  == character.mID) {
      numOfRecordForLongVersion ++;
    }
    NSString *audioUrl = [self recordingPathForIndex:i order:self.instanceOrder];
    if ([[NSFileManager defaultManager] fileExistsAtPath:audioUrl]) {
       DLog(@"remove Record File: %@",audioUrl);
      [[NSFileManager defaultManager] removeItemAtPath:audioUrl error:nil];
    }
  }
  
  int buttonDistance = progressView.frame.size.width / numOfRecordForLongVersion;
  for(int j = 0; j < numOfRecordForLongVersion; j++){
    UIButton *tempProgresButton = [UIButton buttonWithType:UIButtonTypeCustom];
    tempProgresButton.frame = CGRectMake(progressViewForLongVersion.x+buttonDistance*j, progressViewForLongVersion.y - ((4+progressViewForLongVersion.mHeight)/2), heightForLineInProgressView, 12);
    //tempProgresButton.tag = j;
    //[tempProgresButton addTarget:self action:@selector(progresButtonPress:) forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:tempProgresButton];
    if(j!=0)
      tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Gray_light"];
    
    [progresButtonsArrayForLongVersion addObject:tempProgresButton];
  }
  curentFrameDialogForLongVersion = 0;
  [progressViewForLongVersion setHidden:NO];
  [progressView setHidden:YES];
  for(int i=0;i<[progresButtonsArray count];i++){
    UIButton *tempProgresButton = [progresButtonsArray objectAtIndex:i];
    [tempProgresButton setHidden:YES];
  }
}



- (void)removeAllSubviewsFromScroll {
  while (mainScroll.subviews.count) {
    UIView* child = mainScroll.subviews.lastObject;
    [child removeFromSuperview];
  }
}
#pragma mark set interface for new step
-(void)setDataForNewStep{
  [self removeAllSubviewsFromScroll];
  if([segmentsArray count]){
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:curentFrameDialog];
    [mainImageView setImage:[UIImage imageFromDocumentsResourceFile:[tempSegmentFramesPresentation image]]];
    wordsFull = [[tempSegmentFramesPresentation text] componentsSeparatedByString:@" "];
    /*NSMutableArray *wordsMutable = [NSMutableArray new];
     for (NSString *word in wordsFull) {
     [wordsMutable addObject:[[word punctuationFree] lowercaseString]];
     }*/
    int count = 0;
    for(int i = 0; i < [wordsFull count]; i++){
      UILabel *label =  [[UILabel alloc] initWithFrame: CGRectMake(count,15,0,0)];
      label.text = [wordsFull objectAtIndex:i];
      [label sizeToFit];
      count+= label.frame.size.width + 3;
      [mainScroll addSubview:label];
    }
    //UIscrollview center alignment
    float topInset = 0;
    float sideInset = (self.frame.size.width - count) / 2.0;
    if (sideInset < 0.0) sideInset = 0.0;
    [mainScroll setContentInset:UIEdgeInsetsMake(topInset, sideInset, -topInset, -sideInset)];
    if(count > 320){
      mainScroll.contentSize =  CGSizeMake(count,mainScroll.frame.size.height );
    }else{
      mainScroll.contentSize =  CGSizeMake(320,mainScroll.frame.size.height );
    }
  }
  [self setColorForProgressViewButtons];
}
-(void)setColorForProgressViewButtons{
  for(int i=0;i<[progresButtonsArray count];i++){
    UIButton *tempProgresButton = [progresButtonsArray objectAtIndex:i];
    if(i <= curentFrameDialog ){
      tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];
    }else{
       tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Gray_light"];
    }
    //first button is not visible
    if(i ==0){
      tempProgresButton.backgroundColor = [UIColor clearColor];
    }
  }
  
}
#pragma mark set interface for current status after playbackComplete
-(void)chekProgresStatus{
  if(currentGameStatus == PlaybackIntroStatus){
    [progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
    if(curentFrameDialog < maxFrameDialog){
       curentFrameDialog += 1;
      [self setDataForNewStep];
      [self playFrameSound:0];
    }else{
      [continueButton setHidden:NO];
      [continueButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
      [continueButton addTarget:self action:@selector(goToCharacterSelectionStatus:) forControlEvents:UIControlEventTouchUpInside];
    }
  }else if(currentGameStatus == SpeakingRecordingModeStatus){
    if([recorderButton isHidden])
      [progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
    if(curentFrameDialog < maxFrameDialog){
      if([recorderButton isHidden]){
        curentFrameDialog += 1;
        [self setDataForNewStep];
        [self chekCharacterIfIsRecordable];
      }
    }else{
     // [self goToDialogSummaryMode];
    }
  }if(currentGameStatus == DialogSummaryStatus){
    [progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
    if(curentFrameDialog < maxFrameDialog){
      curentFrameDialog += 1;
      [self setDataForNewStep];
      [self playFrameSound:0];
    }
  }
  
}
- (void)goToCharacterSelectionStatus:(id)sender {
  [mainScroll setHidden:YES];
  [mainImageView setHidden:YES];
  [progressView setHidden:YES];
  [profileSoundButton setHidden:YES];
  [continueButton  setHidden:YES];
  for(int i=0;i<[progresButtonsArray count];i++){
    UIButton *tempProgresButton = [progresButtonsArray objectAtIndex:i];
    [tempProgresButton setHidden:YES];
  }
  [continueButton addTarget:self action:@selector(goToSpeakingRecordingModeStatus:) forControlEvents:UIControlEventTouchUpInside];
  currentGameStatus = CharacterSelectionStatus;
  
  [self.viewTitleLabel setTitleForFramePresentation:[(FramesPresentationDialog *)self.dictVehicleInfo sectionTitle] ];
  
  
  /*charactersView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
  charactersView.backgroundColor = [UIColor clearColor];
  [baseView addSubview:charactersView];
  */
  self.viewTitleLabel.hidden = YES;
  
  charactersView= [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, [self mWidth], [self mHeight])];
  charactersView.backgroundColor = [UIColor clearColor];
  [self addSubview:charactersView];
  /////
  CustomTitleView *titleView = [[CustomTitleView alloc] initWithFrame:CGRectMake(0, 0, LM_WIDTH_INSTACE, 82)];
  [titleView setTitleForFramePresentation:[(FramesPresentationDialog *)self.dictVehicleInfo sectionTitle] ];
  titleView.tag = 101;
  [charactersView addSubview:titleView];
  //////////
  for(int i=0;i<[charactersArray count];i++){
    Character *lastCharacter = [charactersArray objectAtIndex:i];
    UIButton *CharacterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CharacterButton.frame = CGRectMake(20+(i*140), 71+60, 124, 124);
    if (IS_IPHONE4) {
      CharacterButton.frame = CGRectMake(20+(i*140), 71+40, 124, 124);
    }
    
    [CharacterButton setImage:[UIImage imageFromDocumentsResourceFile:lastCharacter.image] forState:UIControlStateNormal];
    [CharacterButton addTarget:self action:@selector(goToSpeakingRecordingModeStatus:) forControlEvents:UIControlEventTouchUpInside];
    CharacterButton.tag = i;
    [charactersView addSubview:CharacterButton];
    CharacterButton.imageView.layer.cornerRadius = CharacterButton.mHeight/2;
    
    
    UILabel *CharacterLabel = [[UILabel alloc]initWithFrame:CGRectMake(20+(i*140),200+60, 124, 40)];
    if (IS_IPHONE4) {
      CharacterLabel.frame = CGRectMake(20+(i*140),200+40, 124, 40);
    }
    CharacterLabel.textAlignment = NSTextAlignmentCenter;
    CharacterLabel.textColor = [UIColor lightGrayColor];
    CharacterLabel.text = lastCharacter.name;
    [charactersView addSubview:CharacterLabel];
    
  }
  
  UILabel *tempInfoLabel = [[UILabel alloc]initWithFrame:CGRectMake(5,260+105, 320-10, 240)];
  if (IS_IPHONE4) {
    tempInfoLabel.frame = CGRectMake(5,260+40, 320-10, 240);
  }
  tempInfoLabel.textAlignment = NSTextAlignmentCenter;
  tempInfoLabel.numberOfLines = 0;
  tempInfoLabel.textColor = [UIColor lightGrayColor];
  [charactersView addSubview:tempInfoLabel];
  /*tempInfoLabel.text =[NSString stringWithFormat:@"%@\n %@ \n %@",
  [(FramesPresentationDialog *)self.dictVehicleInfo sectionText],
                        [(FramesPresentationDialog *)self.dictVehicleInfo sectionText],
                        [(FramesPresentationDialog *)self.dictVehicleInfo sectionText]];*/
  tempInfoLabel.text = [(FramesPresentationDialog *)self.dictVehicleInfo sectionText];
  [tempInfoLabel sizeToFit];
  
  charactersView.contentSize =  CGSizeMake(charactersView.frame.size.width,tempInfoLabel.frame.origin.y + tempInfoLabel.frame.size.height );
  //;
}

- (void)goToSpeakingRecordingModeStatus:(id)sender {
  self.viewTitleLabel.hidden = NO;
  [charactersView removeFromSuperview];
  
  curentFrameDialog = 0;
  [self setDataForNewStep];
  
  //take selected Character
  if(sender){
    UIButton *CharacterButton = (UIButton*)sender;
      
      
    selectedCharacter = [charactersArray objectAtIndex:CharacterButton.tag];
  }else{
    selectedCharacter = [charactersArray objectAtIndex:0];
  }
  
  
  
  
  [mainScroll setHidden:NO];
  [mainImageView setHidden:NO];
  [progressView setHidden:NO];
  [profileSoundButton setHidden:NO];
  
  if([(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
    for(int i=0;i<[progresButtonsArray count];i++){
      UIButton *tempProgresButton = [progresButtonsArray objectAtIndex:i];
      [tempProgresButton setHidden:NO];
    }
  }
  [progressView setProgress:0];
  currentGameStatus = SpeakingRecordingModeStatus;
  [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
  //draw new recorderButton
  int recorderButtonHeight = 62;
  int recorderButtonWidth = 45;
  if (IS_IPHONE4) {
    recorderButtonHeight = 48;
    recorderButtonWidth = 33;
  }
  recorderButton = [UIButton buttonWithType:UIButtonTypeCustom];
  int upSpace = 20;
  if (IS_IPHONE4) {
    upSpace = 30;
  }
  recorderButton.frame = CGRectMake(138,291-upSpace, recorderButtonWidth, recorderButtonHeight);
  [recorderButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateNormal];
  [recorderButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
  
  [continueButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [baseView addSubview:recorderButton];
  [self chekCharacterIfIsRecordable];
  
  nuanseImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_nuance"]];
  [baseView addSubview:nuanseImage];
  nuanseImage.x = 254;
  nuanseImage.y = 291-upSpace;
  if([(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
    [self loadDataForLongVersion];
  }
}

#pragma mark red record button press
- (void)chekSentence:(id)sender {
 [profileSoundButton setEnabled:YES];
  
  [self.vehicleDelegate enableScrolling:0];
  [self.vehicleDelegate enablePageControlButtons];
  
  [stopRecordSoundTimer invalidate];
  stopRecordSoundTimer = nil;
  [recorderButton setHidden:YES];
  [nuanseImage setHidden:YES];
  [recordAgainButton setHidden:NO];
  [recordAgainButton setUserInteractionEnabled:NO];
  [recordAgainButton addTarget:self action:@selector(recordAgain:) forControlEvents:UIControlEventTouchUpInside];
  [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  
  [continueButton setHidden:NO];
  [continueButton setUserInteractionEnabled:NO];
  int upSpace = 20;
  if (IS_IPHONE4) {
    upSpace = 30;
  }
  [continueButton setFrame:CGRectMake(39+132, 303-upSpace, 128, 39)];
  [continueButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [continueButton addTarget:self action:@selector(goToNextSentence:) forControlEvents:UIControlEventTouchUpInside];
  [AUDIO_MANAGER stopRecording];
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionForFramePresentation];
    [AUDIO_MANAGER setDelegate:nil];
    return;
  }
  [self.vehicleDelegate addActivityIndicatorForFramePresent];
  
  [AUDIO_MANAGER stopPlaying];
  [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Tone_5.3_-_Rec_end_tone-2.wav" ofType:@""]]];
  [AUDIO_MANAGER setDelegate:nil];
  
  // [self removeAllSubviewsFromScroll];
  [recorderButton setHidden:YES];
  [continueButton setHidden:YES];
  [recordAgainButton setHidden:YES];
}

#pragma mark swich play or record on SpeakingRecordingModeStatus
-(void)chekCharacterIfIsRecordable{
  if(![(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
    numberOfTry = 0;
    [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
    [recorderButton setHidden:NO];
    [continueButton setHidden:YES];
    [recordAgainButton setHidden:YES];
    [recorderButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateNormal];
    [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recorderButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
    
    [nuanseImage setHidden:NO];
    
    return;
  }
  [self.viewTitleLabel setTitleForFramePresentation:@" "];
  SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:curentFrameDialog];
//  Character *character = allCharactersArray[(tempSegmentFramesPresentation.characterID - 1)];
  Character *character = [DATA_SERVICE findCharacterByID:tempSegmentFramesPresentation.characterID];
  if(selectedCharacter.mID  != character.mID) {
    curentFrameDialog += 1;
    if([segmentsArray count ] > curentFrameDialog){
      [self setDataForNewStep];
      [progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
      [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
    }else{
      [self goToDialogSummaryMode];
      [stopRecordSoundTimer invalidate];
      stopRecordSoundTimer = nil;
      [recorderButton setHidden:YES];
      [nuanseImage setHidden:YES];
      
      return;
    }
    /*[recorderButton setHidden:YES];
    [nuanseImage setHidden:YES];
    [self playFrameSound:0];*/
  }else{
    
    numberOfTry = 0;
    [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
    [recorderButton setHidden:NO];
    [continueButton setHidden:YES];
    [recordAgainButton setHidden:YES];
    [recorderButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateNormal];
    [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recorderButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
    [nuanseImage setHidden:NO];
    
    
  }
}

#pragma mark continue(rec mode) button press

- (void)goToNextSentence:(id)sender {
  //add points
  /*if(numberOfTry == 1){
    [self.vehicleDelegate addPoints:kParam010 * numberOfCorrectWords isLastAnswer:YES];
  }else if(numberOfTry == 2){
     [self.vehicleDelegate addPoints:kParam011 * numberOfCorrectWords isLastAnswer:YES];
  }else{
    [self.vehicleDelegate updatePoints:sender];
    if(numberOfCorrectWords <= 0)numberOfCorrectWords=1;
     [self.vehicleDelegate addPoints:kParam012 * numberOfCorrectWords isLastAnswer:YES];
  }*/
  numberOfTry = 0;
  [progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
  if(curentFrameDialog < maxFrameDialog){
    curentFrameDialog += 1;
    [self setDataForNewStep];
    
    [continueButton setHidden:YES];
    [recordAgainButton setHidden:YES];
    [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
    [recorderButton setHidden:NO];
    [recorderButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateNormal];
    [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [recorderButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
   
    [nuanseImage setHidden:NO];
    //set progres
    //for second progresView
    if([(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
      curentFrameDialogForLongVersion += 1;
      [progressViewForLongVersion setProgress:(((progressViewForLongVersion.progress * numOfRecordForLongVersion) + 1) / numOfRecordForLongVersion)];
      for(int i=0;i<[progresButtonsArrayForLongVersion count];i++){
        UIButton *tempProgresButton = [progresButtonsArrayForLongVersion objectAtIndex:i];
        if(i <= curentFrameDialogForLongVersion ){
          tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];
        }else{
          tempProgresButton.backgroundColor = [APPSTYLE colorForType:@"Gray_light"];
        }
        //first button is not visible
        if(i ==0){
          tempProgresButton.backgroundColor = [UIColor clearColor];
        }
      }
    }
    //end set progres
    [self chekCharacterIfIsRecordable];
  }else{
    [self goToDialogSummaryMode];
  }
}
-(void)goToDialogSummaryMode{
  ////
  if([(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
    [progressView setHidden:NO];
    [progressViewForLongVersion setHidden:YES];
    if([(FramesPresentationDialog *)self.dictVehicleInfo longVersion]){
      [progressViewForLongVersion setHidden:YES];
      for(int i=0;i<[progresButtonsArrayForLongVersion count];i++){
        UIButton *tempProgresButton = [progresButtonsArrayForLongVersion objectAtIndex:i];
        [tempProgresButton setHidden:YES];
      }
    }
    for(int i=0;i<[progresButtonsArray count];i++){
      UIButton *tempProgresButton = [progresButtonsArray objectAtIndex:i];
      [tempProgresButton setHidden:NO];
    }
  }
  /////
  currentGameStatus = DialogSummaryStatus;
  [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T129")];
  //[self playFrameSound:0];
  int upSpace = 20;
  if (IS_IPHONE4) {
    upSpace = 30;
  }
  [continueButton setFrame:CGRectMake((340-128)/2, 303-upSpace, 128, 39)];
  [continueButton setHidden:NO];
  [continueButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [continueButton addTarget:self action:@selector(startDialogSummary:) forControlEvents:UIControlEventTouchUpInside];

  [recordAgainButton setHidden:YES];
  [self startDialogSummary:0];
  
  
}
- (void)startDialogSummary:(id)sender {
  [self.vehicleDelegate changeNavigationButtons:self];
  //[self.viewTitleLabel setTitleForFramePresentation:Localized(@"T120")];
  //[baseView setFrame:CGRectMake(0, [self.viewTitleLabel height], [self width] , [self height])];
  
  [continueButton setHidden:YES];
  int upSpace = 20;
  if (IS_IPHONE4) {
    upSpace = 30;
  }
  [continueButton setFrame:CGRectMake((340-128)/2, 303-upSpace, 128, 39)];
  [continueButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  curentFrameDialog = 0;
  [self setDataForNewStep];
  [progressView setProgress:0];
  [continueButton setHidden:YES];
  [recordAgainButton setHidden:YES];
  //[self playFrameSound:0];
  [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(playFrameSoundOnTime) userInfo:nil repeats:NO];

  
  automaticPlay = NO;
  curentFrameDialog = 0;
  [progressView setProgress:0];
  [self setDataForNewStep];
  soundPlay = NO;
  [AUDIO_MANAGER setDelegate:nil];
  [AUDIO_MANAGER stopPlaying];
}
-(void)playFrameSoundOnTime{
  [self playFrameSound:profileSoundButton];
  
}
#pragma mark buttons function
- (void)playFrameSound:(id)sender {
  if(!sender) soundPlay = NO;//make pause only for button press
  if(sender)automaticPlay = YES;
  if(currentGameStatus == PlaybackIntroStatus){
    [self.viewTitleLabel setTitleForFramePresentation:@" "];
    
  }
  if((currentGameStatus == DialogSummaryStatus) || (currentGameStatus == PlaybackIntroStatus)){
    if(progressView.progress == 1){
      automaticPlay = YES;
      curentFrameDialog = 0;
      [progressView setProgress:0];
      [self setDataForNewStep];
    }
  }
  if(soundPlay){
    soundPlay = NO;
    [AUDIO_MANAGER setDelegate:nil];
    [AUDIO_MANAGER stopPlaying];
    [profileSoundButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
  }else{
    soundPlay = YES;
    [profileSoundButton setImage:[UIImage imageNamed:@"ic_pause"] forState:UIControlStateNormal];
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:curentFrameDialog];
    
    if(currentGameStatus != DialogSummaryStatus){
      [AUDIO_MANAGER playAudioFromDocumentPath:[tempSegmentFramesPresentation audio]];
    }else{
      NSString *audioUrl = [self recordingPathForIndex:curentFrameDialog order:self.instanceOrder];
      if(automaticPlay){//not automatic play
        if ([[NSFileManager defaultManager] fileExistsAtPath:audioUrl]) {
          [AUDIO_MANAGER playAudio:[NSURL URLWithString: audioUrl]];
        }else{
          [AUDIO_MANAGER playAudioFromDocumentPath:[tempSegmentFramesPresentation audio]];
        }
      }else{
        [profileSoundButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
      }
      
    }
    
    
    [AUDIO_MANAGER setDelegate:self];
  }
  
}
- (void)progresButtonPress:(id)sender {
  DLog(@"progresButtonPress");
 
  if(sender){
    
    UIButton *pressButton = (UIButton*)sender;
    if(currentGameStatus != SpeakingRecordingModeStatus){
      curentFrameDialog = (int)pressButton.tag;
      int segmentsArrayCount = (int)[segmentsArray count];
      float progres = ((float)curentFrameDialog / (float)segmentsArrayCount);
      [progressView setProgress:progres];
      [self setDataForNewStep];
      [self playFrameSound:0];
    }
  }
}

#pragma mark record button press
-(void)recordSound:(id)sender {
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        if (granted) {
            NSLog(@"Permission granted");
            [profileSoundButton setEnabled:NO];
            ///
            [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Tone_5.1_-_Rec_begin_tone.wav" ofType:@""]]];
            [AUDIO_MANAGER setDelegate:nil];
            [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(afterRecordSound) userInfo:nil repeats:NO];
        }
        else {
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Microphone Restriction"
                                                              message:Localized(@"O Kantoo não tem acesso ao microfone. Para ativá-lo, entre em “Configurações” no seu celular e ligue o microfone.")
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            message.tag = 1;
            [message show];
            NSLog(@"O Kantoo não tem acesso ao microfone. Para ativá-lo, entre em “Configurações” no seu celular e ligue o microfone.");
            
        }
    }];
   
    /*
  //[AUDIO_MANAGER playSound:kSoundRecord];
  [profileSoundButton setEnabled:NO];
  ///
  [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Tone_5.1_-_Rec_begin_tone.wav" ofType:@""]]];
  [AUDIO_MANAGER setDelegate:nil];
  [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(afterRecordSound) userInfo:nil repeats:NO];
     
     */

}

-(void)afterRecordSound {
  if (![ReachabilityHelper reachable]) {
    [UIAlertView alertWithCause:kAlertNoInternetConnectionForFramePresentation];
    return;
  }
  [self.vehicleDelegate disableScrolling:0];
  [self.vehicleDelegate disablePageControlButtons];
  stopRecordSoundTimer =  [NSTimer scheduledTimerWithTimeInterval:kParam048 target:self selector:@selector(stopRecordSound) userInfo:nil repeats:NO];
  [AUDIO_MANAGER setDelegate:self];
  [AUDIO_MANAGER record:[self recordingPathForIndex:curentFrameDialog order:self.instanceOrder]];
  [recorderButton setImage:[UIImage imageNamed:@"ic_mic2"] forState:UIControlStateNormal];
  [self performSelector:@selector(bounce) withObject:0 afterDelay:0.250];
  [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [recorderButton addTarget:self action:@selector(chekSentence:) forControlEvents:UIControlEventTouchUpInside];
  
}
-(void)stopRecordSound{
  
  [stopRecordSoundTimer invalidate];
  stopRecordSoundTimer = nil;
  
  if(currentGameStatus == SpeakingRecordingModeStatus){
    if(![recorderButton isHidden]){
      [self chekSentence:0];
    }
  }
}
- (NSString *)recordingPathForIndex:(int)path order:(NSInteger)order {
  return [[self recordingPath] stringByAppendingPathComponent:
          [NSString stringWithFormat:@"record%ld_%d.lpcm", (long)order,path]];
}
- (NSString *)recordingPath {
  //return [NSString documentsDir];
  NSString *recordingPathUrlString;// =[[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];
  if ([USER_MANAGER isGuest]){
    recordingPathUrlString = [[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];
  }else{
    recordingPathUrlString = [[NSString documentsDir] stringByAppendingPathComponent:@"Recordings"];
  }
  if (![[NSFileManager defaultManager] fileExistsAtPath:recordingPathUrlString]) {
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:recordingPathUrlString withIntermediateDirectories:NO attributes:nil error:&error];
  }
  return recordingPathUrlString;
}

-(void)recordAgain:(id)sender {
  //stopRecordSoundTimer =  [NSTimer scheduledTimerWithTimeInterval:kParam048 target:self selector:@selector(stopRecordSound) userInfo:nil repeats:NO];
  if(stopRecordSoundTimer)
    [stopRecordSoundTimer invalidate];
  stopRecordSoundTimer = nil;
  
  [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T122")];
  
  
  [continueButton setHidden:YES];
  [recordAgainButton setHidden:YES];
  [recorderButton setHidden:NO];
  
  [recorderButton setImage:[UIImage imageNamed:@"ic_mic"] forState:UIControlStateNormal];
  [recorderButton addTarget:self action:@selector(recordSound:) forControlEvents:UIControlEventTouchUpInside];
  
  [self setDataForNewStep];
  /*[AUDIO_MANAGER setDelegate:self];
  [AUDIO_MANAGER record:[self recordingPathForIndex:curentFrameDialog order:self.instanceOrder]];
  [recorderButton setImage:[UIImage imageNamed:@"ic_mic2"] forState:UIControlStateNormal];
  [self performSelector:@selector(bounce) withObject:0 afterDelay:0.250];
  [recorderButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
  [recorderButton addTarget:self action:@selector(chekSentence:) forControlEvents:UIControlEventTouchUpInside];*/
  
}

#pragma mark - AVWrapperDelegate

- (void)recordingComplete {
  DLog(@"recordingComplete");
}

- (void)playbackItemComplete {
  //[progressView setProgress:(((progressView.progress * [segmentsArray count]) + 1) / [segmentsArray count])];
}

- (void)playbackComplete {
  soundPlay = NO;
  [profileSoundButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
  [AUDIO_MANAGER setDelegate:nil];
  if(currentGameStatus != SpeakingRecordingModeStatus){
    [self chekProgresStatus];
  }
  if((currentGameStatus == DialogSummaryStatus) || (currentGameStatus == PlaybackIntroStatus)){
    if(progressView.progress == 1){
      automaticPlay = NO;
      curentFrameDialog = 0;
      /*if(currentGameStatus != PlaybackIntroStatus){
        [progressView setProgress:0];
        [self setDataForNewStep];
      }*/
    }
  }
  
}

- (void)playbackPaused {
  // do nothing
}

- (void)recordingCompleteWithRecordFile:(NSURL *)audioUrl {
  DLog(@"recordingCompleteWithRecordFile %@",audioUrl);
  [self confirmAudio:audioUrl];
  
  //[AUDIO_MANAGER playAudio:audioUrl];
  //[AUDIO_MANAGER setDelegate:self];
 
}
#pragma mark send audio to nuance
- (void)confirmAudio:(NSURL *)audioUrl {
  
   /* [AUDIO_MANAGER stopPlaying];
    [AUDIO_MANAGER playAudio:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Tone_5.3_-_Rec_end_tone-2.wav" ofType:@""]]];
    [AUDIO_MANAGER setDelegate:nil];
  */
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    if (![ReachabilityHelper reachable]) {
      [UIAlertView alertWithCause:kAlertNoInternetConnectionForFramePresentation];
      return;
    }
    [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T123")];
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:curentFrameDialog];
    NSString *words = [ [tempSegmentFramesPresentation text] stringByReplacingOccurrencesOfString:@"’" withString:@"'"];
    
    NSDictionary *headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER deviceIdentifier],kSearchWords:words };
    if ([USER_MANAGER isGuest]) {
      headers = @{ kUserMSISDN : @"guest", kUserDeviceId : [USER_MANAGER deviceIdentifier],kSearchWords:words };
    }
    if ([USER_MANAGER isNative]) {
      headers = @{ kUserMSISDN : [USER_MANAGER userMsisdn], kUserDeviceId : [USER_MANAGER userMsisdn],kSearchWords:words };
    }
    __weak FramesPresentationDialogScreen *instance = self;
    __weak ASIHTTPRequest *confirmImageRequest = [REQUEST_MANAGER post:kRequestNuanceAPICall headers:headers];
    ///////// MAke zip
    NSString *documentsDir = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *filePath = [documentsDir stringByAppendingPathComponent:@"test.zip"];
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:NULL];
    ZipFile *zipFile = [[ZipFile alloc] initWithFileName:filePath mode:ZipFileModeCreate];
    
    ZipWriteStream *stream1 =
    [zipFile writeFileInZipWithName:@"abc.lpcm" fileDate:[NSDate dateWithTimeIntervalSinceNow:-86400.0] compressionLevel:ZipCompressionLevelBest];
    [stream1 writeData:[[NSData alloc] initWithContentsOfURL:audioUrl]];
    [stream1 finishedWriting];
    [zipFile close];
    
    NSString *fileP = [NSString stringWithFormat:@"file://%@", filePath];
    NSData *zipData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:fileP]];
    int bytes = zipData.length;
    NSMutableData *body = [[NSMutableData alloc] initWithCapacity:bytes];
    //__weak NSMutableData *body = [NSMutableData data];
    //__weak NSMutableData *body = [[NSMutableData alloc]initWithData:[NSData dataWithData:zipData]];
    [body appendData:[NSData dataWithData:zipData]];
    
    ///////////
    [confirmImageRequest setPostBody:[body mutableCopy]];
    //__weak ASIHTTPRequest *request = confirmImageRequest;
    [confirmImageRequest setShouldContinueWhenAppEntersBackground:NO];
    [confirmImageRequest setTimeOutSeconds:10];
    
    [confirmImageRequest setCompletionBlock: ^{
      
      [instance.vehicleDelegate removeActivityIndicator];
      [continueButton setHidden:NO];
      [recordAgainButton setHidden:NO];
      [recordAgainButton setUserInteractionEnabled:YES];
      [continueButton setUserInteractionEnabled:YES];
      if (confirmImageRequest.responseStatusCode == 200 && !confirmImageRequest.error) {
        NSError *error = 0;
        NSMutableDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:confirmImageRequest.responseData options:0 error:&error];
        NSArray *responseArray = [responseDictionary objectForKey:kPreferencesCheckedList];
        [instance chekResults:responseArray];
        if([responseArray count] == 0)
        {
          [UIAlertView alertWithCause:kAlertNoResultFromCamFindMessage];
          [[NSFileManager defaultManager] removeItemAtURL:audioUrl  error:nil];
        }
      }
      else {
        [UIAlertView alertWithCause:kAlertCamFindAPITechnicalError];
        //[instance.navigationController popViewControllerAnimated:YES];
      }
    }];
    
    [confirmImageRequest setFailedBlock: ^{
      DLog(@"Failed Block ");
      [recordAgainButton setUserInteractionEnabled:YES];
      [continueButton setUserInteractionEnabled:YES];
      [continueButton setHidden:NO];
      [recordAgainButton setHidden:NO];
      [instance.vehicleDelegate removeActivityIndicator];
    }];
    [confirmImageRequest startAsynchronous];
  });
}
- (void)chekResults:(NSArray *)responseArray {
  numberOfTry +=1 ;
  numberOfCorrectWords = 0;
  
  [recordAgainButton setUserInteractionEnabled:YES];
  [continueButton setUserInteractionEnabled:YES];
  
  
  [self removeAllSubviewsFromScroll];
  if([segmentsArray count]>0){
    SegmentFramesPresentation *tempSegmentFramesPresentation = [segmentsArray objectAtIndex:curentFrameDialog];
    [mainImageView setImage:[UIImage imageFromDocumentsResourceFile:[tempSegmentFramesPresentation image]]];
    wordsFull = [[tempSegmentFramesPresentation text] componentsSeparatedByString:@" "];
    
    int count = 0;
    BOOL haveWrongWord = NO;
    for(int i = 0; i < [wordsFull count]; i++){
      //add text
      NSString *word = [wordsFull objectAtIndex:i];
      if([word isEqualToString: @""] ||  [word isEqualToString: @" "]){
        break;
      }
      UILabel *label =  [[UILabel alloc] initWithFrame: CGRectMake(count,15,0,0)];
      label.text = [wordsFull objectAtIndex:i];
      [label sizeToFit];
      
      
      //add sign
      NSDictionary *object;
      if([responseArray count]> i){
        object = [responseArray objectAtIndex:i];
      }else{
        object = @{kPreferencesflag:@0};
      }
      UIImageView *imageSign = [[UIImageView alloc] initWithFrame: CGRectMake(count+(label.frame.size.width/2),3,10,10)];
      if([[object objectForKey:kPreferencesflag] isEqualToNumber:@1]){
        [imageSign setFrame:CGRectMake(count+(label.frame.size.width/2),3,13,9)];
        [imageSign setImage:[UIImage imageNamed:@"ic_v_green"] ];
        label.textColor = [APPSTYLE colorForType:@"Correct_Answer"];//[UIColor greenColor];
        numberOfCorrectWords ++;
      }else{
        [imageSign setFrame:CGRectMake(count+(label.frame.size.width/2),3,9,9)];
        [imageSign setImage:[UIImage imageNamed:@"ic_x"] ];
        label.textColor = [APPSTYLE colorForType:@"Incorrect_Answer"];;
        haveWrongWord = YES;
      }
      [mainScroll addSubview:imageSign];
      count+= label.frame.size.width + 3;
      [mainScroll addSubview:label];
    }
    
    if(haveWrongWord){
      [AUDIO_MANAGER playWrongSound];
      //[self.viewTitleLabel setTitleForFramePresentation:Localized(@"T126")];
    }else{
      [AUDIO_MANAGER playRightSound];
      //[self.viewTitleLabel setTitleForFramePresentation:Localized(@"T128")];
    }
    
    //add points
    if(numberOfTry == 1){
      [self.vehicleDelegate addPoints:kParam010 * numberOfCorrectWords isLastAnswer:YES];
    }else if(numberOfTry == 2){
      [self.vehicleDelegate addPoints:kParam011 * numberOfCorrectWords isLastAnswer:YES];
    }else{
      [self.vehicleDelegate addPoints:kParam012 * numberOfCorrectWords isLastAnswer:YES];
    }
    [self.vehicleDelegate updatePoints:0];
    
    [self.viewTitleLabel setTitleForFramePresentation:Localized(@"T126")];
    [AUDIO_MANAGER setDelegate:nil];
    //UIscrollview center alignment
    float topInset = 0;
    float sideInset = (self.frame.size.width - count) / 2.0;
    if (sideInset < 0.0) sideInset = 0.0;
    [mainScroll setContentInset:UIEdgeInsetsMake(topInset, sideInset, -topInset, -sideInset)];
  }
  if(numberOfCorrectWords == [wordsFull count]){
    //[recordAgainButton setHidden:YES];
  }
  
}

- (void)disableVehicleInteractions {
  [AUDIO_MANAGER setDelegate:nil];
  [stopRecordSoundTimer invalidate];
  stopRecordSoundTimer = nil;
  [super disableVehicleInteractions];
  [APPSTYLE applyIcon:@"ic_mic" toButton:recorderButton];
  soundPlay = NO;
  [profileSoundButton setImage:[UIImage imageNamed:@"ic_play"] forState:UIControlStateNormal];
}

#pragma mark button blink
- (void)bounce {
  if (AUDIO_MANAGER.recordingStatus == kAudioActive) {
    [APPSTYLE applyIcon:(isRecordingImage ? @"ic_mic" : @"ic_mic2") toButton:recorderButton];
    isRecordingImage = !isRecordingImage;
    [self performSelector:@selector(bounce) withObject:0 afterDelay:0.250];
  }
  else {
    [APPSTYLE applyIcon:@"ic_mic" toButton:recorderButton];
  }
}
@end
