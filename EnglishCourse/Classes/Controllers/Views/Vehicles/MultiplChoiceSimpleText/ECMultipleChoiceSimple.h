//
//  ECMultipleChoiceSimple.h
//  TransitionsTest
//
//  Created by Dimitar Shopovski on 11/13/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"
#import "CustomVehicleButton.h"

#import "UIView+Extras.h"

@interface ECMultipleChoiceSimple : ECVehicleDisplay <CustomVehicleButtonDelegate, FeedbackViewDelegate>

@property (nonatomic, strong) UIImageView *imageQuestion;

@property (nonatomic, strong) NSMutableArray *arrayPossibleAnswers;

@property (nonatomic, readwrite) NSInteger nPosibleAttempts;

@property (nonatomic, strong) CustomVehicleButton *currentActiveAnswer;

- (void)initializeElements;
- (void)drawGUI;

@end
