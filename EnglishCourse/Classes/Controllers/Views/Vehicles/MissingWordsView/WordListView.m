//
//  WordListView.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/13/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "WordListView.h"
#import "LMBallonTip.h"
#import "MissingWordView.h"

@implementation WordListView


#pragma mark - loadGUI

- (void)loadGUI
{
    [self loadWordsScrollView];
    [self bindGUI];
}

- (void)loadWordsScrollView {

    if(wordsScrollView) {
        [wordsScrollView removeFromSuperview];
    }
    
    wordsScrollView = [UIScrollView new];
    [wordsScrollView setHeight:kScrollViewHeight];
    [wordsScrollView setX:kWordsSpacing];
    [wordsScrollView setShowsHorizontalScrollIndicator:NO];
    [wordsScrollView setShowsVerticalScrollIndicator:NO];
    [wordsScrollView setBackgroundColor:[UIColor clearColor]];
    [wordsScrollView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    [wordsScrollView setTag:6969];
    
    [self addSubview:wordsScrollView];
}


#pragma mark - bindGUI

- (void)bindGUI
{

    float lastElemetPosition = kZeroPointValue;
    for (int wordIndicator = 0; wordIndicator < self.wordList.count; wordIndicator++)
    {

        UIButton *wordButton = [LMHelper createButtonWithTitle:self.wordList[wordIndicator] backgroundColor:[UIColor whiteColor] fontColor:[UIColor blackColor] highlightColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"] highlightFontColor:[UIColor whiteColor] onPosition:lastElemetPosition];
        
        [wordButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
        [wordButton setTitleColor:[UIColor colorWithHexString:@"464242"] forState:UIControlStateNormal];
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(showToolTip:)];
                [wordButton addGestureRecognizer:longPress];
        [wordButton addTarget:self action:@selector(missingWordButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        lastElemetPosition = lastElemetPosition + wordButton.mWidth + kMissingWordButtonPadding;
        
        [wordButton setTag:wordIndicator];
        [wordsScrollView addSubview:wordButton];
        [wordsScrollView setContentSize:CGSizeMake(lastElemetPosition, kZeroPointValue)];
    }
}


#pragma mark - button action

- (void)missingWordButtonAction:(UIButton *)sender
{

    MissingWordContainerViewL1 *viewLayout1 = (MissingWordContainerViewL1 *)self.superview;
    
    MissingWordView *lastQuestionView = [viewLayout1 getLastQuestionView];
    for(LMTagView *tagView in lastQuestionView.tagList.subviews) {
    
        if([tagView isKindOfClass:[LMTagView class]])
            if(tagView.selected)
                self.placeholderPoint = [tagView.label convertPoint:tagView.label.center toView:self.superview.window];
            
    }
    
    self.attemptsCount ++;
    if (self.placeholderWord)
    {
        
        if ([[LMHelper removeCharsFromString:self.placeholderWord] isEqualToString:sender.titleLabel.text]) {
            [self correctAnswerButtonAnimation:sender];
            [self resetTagNumbers];
        }
        else if(![[LMHelper removeCharsFromString:self.placeholderWord] isEqualToString:sender.titleLabel.text] && self.attemptsCount > 2) {
            UIButton *senderButton = [self findCorrectWordButton];
            self.attemptsCount ++;
            [self correctAnswerButtonAnimation:senderButton];
            [self resetTagNumbers];
        }
        else {
            
            [self wrongAnswerButtonAnimation:sender];
        }
    }
    
}


#pragma mark - correctWordAnimation

- (void)correctAnswerButtonAnimation:(UIButton *)button
{
    __weak typeof(self) weakSelf = self;
    
    UIButton *wordButton = [LMHelper createButtonWithTitle:button.titleLabel.text backgroundColor:[UIColor whiteColor] fontColor:[UIColor blackColor] highlightColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"] highlightFontColor:[UIColor whiteColor] onPosition:0];
    
    [wordButton setBackgroundImage:[UIImage imageWithColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"]] forState:UIControlStateNormal];
    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [wordButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
    [wordButton.layer setCornerRadius:3];
    [wordButton.layer setMasksToBounds:YES];
    
    CGPoint absolutePosition = [button.superview convertPoint:button.frame.origin toView:nil];
    [self.window addSubview:wordButton];
    [wordButton setCenter:absolutePosition];
    [UIView animateWithDuration:.5 animations:^{
        
        [wordButton setCenter:weakSelf.placeholderPoint];
        [weakSelf setUserInteractionEnabled:NO];
        
    } completion:^(BOOL finished) {
        
        [weakSelf setPlaceholderWord:nil];
        
        if ([weakSelf.delegate respondsToSelector:@selector(correctWordIndex:)]) {
            [weakSelf.delegate correctWordIndex:weakSelf.wordIndex];
        }
        
        [wordButton removeFromSuperview];
        [weakSelf setUserInteractionEnabled:YES];
        [AUDIO_MANAGER playRightSound];
    }];


    
    if(self.attemptsCount == 1)
        [self.delegate answerPoints:kParam010];
    else if(self.attemptsCount == 2)
        [self.delegate answerPoints:kParam011];
    else
        [self.delegate answerPoints:kParam012];
    
    self.attemptsCount = 0;
}


#pragma mark - wrongAnswerButtonAnimation

- (void)wrongAnswerButtonAnimation:(UIButton *)button
{
    __weak typeof(self) weakSelf = self;
    [weakSelf.delegate feedback:NO];
    UIButton *wordButton = [LMHelper createButtonWithTitle:button.titleLabel.text backgroundColor:/*[UIColor colorWithHexString:@"D90000"]*/[APPSTYLE colorForType:@"Italy_Main_Red_Color"] fontColor:[UIColor whiteColor] highlightColor:[UIColor greenColor] highlightFontColor:[UIColor whiteColor] onPosition:0];

    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [wordButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [wordButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:17]];
    [wordButton setBackgroundImage:[UIImage imageWithColor:/*[UIColor colorFromHexString:@"330066"]*/
                                    [APPSTYLE colorForType:@"Italy_Main_Green_Color"]] forState:UIControlStateNormal];
    [wordButton.layer setCornerRadius:3];
    [wordButton.layer setMasksToBounds:YES];
    
    float senderPosition = (wordsScrollView.contentOffset.x > 0) ? (button.x - wordsScrollView.contentOffset.x) : button.x;
    [wordButton setX:senderPosition andY:self.y + wordButton.mHeight];
    CGPoint firstPosition = wordButton.center;
    [UIView animateWithDuration:.5
                     animations:^{
                         
                         [weakSelf.window addSubview:wordButton];
                         [wordButton setCenter:weakSelf.placeholderPoint];
                         [weakSelf setUserInteractionEnabled:NO];
                         
    }
                     completion:^(BOOL finished)
    {

        [UIView animateWithDuration:.5
                         animations:^{
                          
                             [wordButton setBackgroundImage:[UIImage imageWithColor:/*[UIColor colorFromHexString:@"D90000"]*/[APPSTYLE colorForType:@"Italy_Main_Red_Color"]] forState:UIControlStateNormal];
                             [wordButton setCenter:firstPosition];
                          
                         }
                         completion:^(BOOL finished) {
                             [wordButton removeFromSuperview];
                             [weakSelf setUserInteractionEnabled:YES];
                             [AUDIO_MANAGER playWrongSound];
                             
                             if(weakSelf.attemptsCount == 2) {
                                 weakSelf.attemptsCount ++;
                                 UIButton *correctWordButton = [weakSelf findCorrectWordButton];
                                 [weakSelf correctAnswerButtonAnimation:correctWordButton];
                                 [weakSelf.delegate feedback:NO];
                             }
        }];
        

    }];
}


#pragma mark - resetTagNumbers

- (void)resetTagNumbers
{
    self.attemptsCount = 0;
}


#pragma mark - findCorrectWordButton

- (UIButton *)findCorrectWordButton
{
    UIButton *correctWordButton = [UIButton new];
    for (UIView *buttonView in wordsScrollView.subviews) {
        
        if([buttonView isKindOfClass:[UIButton class]]) {
            
            if([((UIButton *)buttonView).titleLabel.text isEqualToString:[LMHelper removeCharsFromString:self.placeholderWord]]){
            
                correctWordButton = (UIButton *)buttonView;
            }
        }
    }
    
    return correctWordButton;
}


#pragma mark - showToolTip

- (void)showToolTip:(UIGestureRecognizer *)sender
{
    float senderPosition = (wordsScrollView.contentOffset.x > 0) ? (sender.view.x - wordsScrollView.contentOffset.x) : sender.view.x;
    LMBallonTip *toolTip = [LMBallonTip getFromNib];
    [toolTip setTranslatedWord:[self.translationsList objectAtIndex:sender.view.tag]];
    [toolTip setX:senderPosition andY:sender.view.y - toolTip.mHeight - 5];
    [self addSubview:toolTip];
    
    [toolTip setAlpha:kZeroPointValue];

    if(senderPosition <=  0) {
        toolTip.x = 0;
        [toolTip setFlipLeft:YES];
    }
    else if(senderPosition + sender.view.mWidth >  self.superview.mWidth) {
    
        toolTip.x = self.superview.mWidth - toolTip.mWidth;
    }
    
    else if(senderPosition > 0 && senderPosition < self.superview.mWidth) {
    
        toolTip.x = senderPosition;
        
        if(toolTip.x > 0 && toolTip.x < self.superview.mWidth && (toolTip.mWidth + toolTip.x < self.superview.mWidth)) {
            [toolTip setFlipLeft:YES];
        }
        else if(toolTip.x > 0 && toolTip.x < self.superview.mWidth && (toolTip.mWidth + toolTip.x > self.superview.mWidth)) {
            toolTip.x = self.superview.mWidth - toolTip.mWidth;
            [toolTip setFlipLeft:YES];
        }
    }
    
    if(senderPosition + toolTip.mWidth < self.superview.mWidth / 2.0) {
       
        [toolTip setFlipLeft:YES];
    }
    
    [wordsScrollView setUserInteractionEnabled:NO];
    [UIView animateWithDuration:1.0
                          delay:kZeroPointValue
                        options: UIViewAnimationOptionBeginFromCurrentState
                     animations: ^{ toolTip.alpha = 1; }
                     completion: ^(BOOL finished) { toolTip.alpha = kZeroPointValue; [wordsScrollView setUserInteractionEnabled:YES];
    }];
    
    
}


#pragma mark - setter

- (void)setWordList:(NSArray *)wordList
{
    _wordList = wordList;
    [self loadGUI];
}


#pragma mark - removeScrollViewSubviews

- (void)removeScrollViewSubviews
{
    for (UIView *view in wordsScrollView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
}

#pragma mark - 
@end
