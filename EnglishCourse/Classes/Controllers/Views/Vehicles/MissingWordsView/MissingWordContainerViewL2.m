//
//  MissingWordContainerViewL2.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MissingWordContainerViewL2.h"

@implementation MissingWordContainerViewL2




#pragma mark - loadData

- (void)loadData
{
    translations = [NSMutableArray new];
    words = [NSMutableArray new];
    
    fillTheMissingWords =  ((FillTheMissingWords *)self.dictVehicleInfo);
    SegmentMissingWords *segmentWords = [fillTheMissingWords.segmentsArray objectAtIndex:0];
    answer = segmentWords.translation;
    question = segmentWords.text;
    title = @"Change with data from json";
    
    for(WordMissingWords *word in segmentWords.wordsArray) {
        [words addObject:word.word];
        [translations addObject:word.translation];
    }
    
    [self loadGUI];
}


#pragma mark - loadGUI

- (void)loadGUI
{
    wordListFlowView = [[WordListFlowLayout alloc] initWithFrame:self.frame];
    questionsScrollView = [UIScrollView new];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kQestionsPadding, kZeroPointValue, kScrollViewWidth,50)];
    
    
    [self addSubview:titleLabel];
    [self addSubview:questionsScrollView];
    [self addSubview:wordListFlowView];

    [self bindGUI];
    [self layoutGUI];
}


#pragma mark - bindGUI

- (void)bindGUI
{
    [wordListFlowView setArray:words];
    [wordListFlowView setTranslations:translations];
    [titleLabel setText:title];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    [self layoutTitleView];
    [self layoutQuestionsView];
    [self layoutWordsView];
    [self insertNewQuestion];
    [self loadStyles];

}

- (void)layoutWordsView
{
    [wordListFlowView setWidth:kScrollViewWidth];
    [wordListFlowView setHeight:120];
    [wordListFlowView setX:kQestionsPadding];
    [wordListFlowView setY:(self.mHeight - wordListFlowView.mHeight - kQestionsPadding + 5)];
    [wordListFlowView setDelegate:self];
    [wordListFlowView loadGUI];

}

- (void)layoutQuestionsView
{
    [questionsScrollView setWidth:kScrollViewWidth];
    [questionsScrollView setHeight:(self.mHeight - kScrollViewHeight - 100 - titleLabel.mHeight)];
    [questionsScrollView setX:kQestionsPadding];
    [questionsScrollView setY:titleLabel.y + titleLabel.mHeight + 5];
}

- (void)layoutTitleView
{
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
}


#pragma mark - layoutGUI

- (void)loadStyles
{
    [questionsScrollView setBackgroundColor:[APPSTYLE colorForType:@"Question_Background"]];
}



#pragma mark - insert new question view

- (void)insertNewQuestion
{
    MissingWordView *questionView = [[MissingWordView alloc] initWithFrame:CGRectMake(kZeroPointValue, kZeroPointValue, self.mWidth,80)];
    
    [questionView setQuestionString:question];
    [questionView setDelegate:self];
    
    if([self getLastQuestionView]) {
        
        MissingWordView *lastQuestionView = [self getLastQuestionView];
        [questionView setY:lastQuestionView.y];
        [questionView placeCenterAndBelowView:lastQuestionView withPadding:5];
    }
    
    [questionsScrollView addSubview:questionView];
    [questionsScrollView setContentSize:CGSizeMake(kScrollViewWidth, [self getLastQuestionView].mHeight + [self getLastQuestionView].y)];
    [wordListFlowView setWordList:questionView.array];
    [wordListFlowView setAttemptsCount:0];
}


#pragma mark - getLastQuestionView

- (MissingWordView *)getLastQuestionView
{
    MissingWordView *lastView;
    for(id view in questionsScrollView.subviews) {
        
        if([view isKindOfClass:[MissingWordView class]]) {
            lastView = view;
        }
    }
    
    return lastView;
}

#pragma mark - Question view delegate

- (void)selectedPlaceholderAtPoint:(CGPoint)point
                              word:(NSString *)word
                         wordIndex:(NSInteger)index
{
    [wordListFlowView setPlaceholderPoint:point];
    [wordListFlowView setPlaceholderWord:word];
    [wordListFlowView setWordIndex:index];
    [wordListFlowView setAttemptsCount:0];
}

- (void)gameOver:(BOOL)status
{
    if(status) {
        
        [self bringSubviewToFront:self.feedbackView];
        [self.feedbackView setInformation:YES];
        [self.feedbackView showWithAnimation];
        [ self finish];
    }
    
}


#pragma mark - correctWordIndex

- (void)correctWordIndex:(NSInteger)index
{
    MissingWordView *questionView = [self getLastQuestionView];
    [questionView addCorrectWordIndex:index];
    [questionView.tagList resetSelection];
    
}


#pragma mark - finish game

- (void)finish
{
    CGFloat newHeight = questionsScrollView.mHeight + wordListFlowView.mHeight;
    
    [UIView animateWithDuration:1.0 animations:^{
        questionsScrollView.height = newHeight;
        wordListFlowView.alpha = 0;
    }];
}


#pragma mark - BaseView delegates

- (void)updatePoints:(id)sender {
    
}

- (void)changeNavigationButtons:(id)sender {
}


#pragma mark -

@end
