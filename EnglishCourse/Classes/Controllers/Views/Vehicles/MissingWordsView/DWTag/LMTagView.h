//
//  LMTagView.h
//  MissingWord
//
//  Created by Darko Trpevski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "DWTagView.h"
#import "LMHelper.h"

@interface LMTagView : DWTagView

@property(nonatomic) BOOL isEnabled;
@property(nonatomic) BOOL isCorrect;

- (void)setSelected:(BOOL)selected;
- (void)layoutCorrectWord;

@end
