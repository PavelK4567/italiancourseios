//
//  LMTagView.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/12/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMTagView.h"
#import "LMHelper.h"

#define kSelectedColor [UIColor clearColor]
#define CORNER_RADIUS 0.0f
#define LABEL_MARGIN_DEFAULT 5.0f
#define BOTTOM_MARGIN_DEFAULT 5.0f
#define FONT_SIZE_DEFAULT 20.0f
#define HORIZONTAL_PADDING_DEFAULT 0.0f
#define VERTICAL_PADDING_DEFAULT 3.0f
#define BACKGROUND_COLOR [UIColor clearColor]
#define TEXT_COLOR [UIColor blackColor]
#define TEXT_SHADOW_COLOR [UIColor clearColor]
#define TEXT_SHADOW_OFFSET CGSizeMake(0.0f, 0.0f)
#define BORDER_COLOR [UIColor clearColor]
#define BORDER_WIDTH 0.0f
#define HIGHLIGHTED_BACKGROUND_COLOR [UIColor clearColor]
#define DEFAULT_AUTOMATIC_RESIZE YES
#define DEFAULT_SHOW_TAG_MENU NO

@implementation LMTagView


- (id)init
{
    self = [super init];
    if (self) {
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [self.label setTextColor:TEXT_COLOR];
        [self.label setShadowColor:TEXT_SHADOW_COLOR];
        [self.label setShadowOffset:TEXT_SHADOW_OFFSET];

        [self.label setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.label];
        
        self.button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.button setFrame:self.frame];
        [self addSubview:self.button];
        
        [self.layer setMasksToBounds:YES];
        [self.layer setCornerRadius:CORNER_RADIUS];
        [self.layer setBorderColor:BORDER_COLOR.CGColor];
        [self.layer setBorderWidth:BORDER_WIDTH];
    }
    return self;
}


- (void)setSelected:(BOOL)selected {
    
    super.selected = !selected;
    [self layoutSelection];
}

- (void)setIsEnabled:(BOOL)isEnabled {
    
    _isEnabled = isEnabled;
    if(_isEnabled) {
    
        [self setUserInteractionEnabled:YES];
    }
    else {
        
        [self setUserInteractionEnabled:NO];
    }
}

- (void)setIsCorrect:(BOOL)isCorrect
{
    _isCorrect = isCorrect;
    [self layoutCorrectWord];
}

- (void)layoutSelection {
    
    if(self.selected) {
        [self.label setBackgroundColor:kSelectedColor];   
    }
    else {
        [self.label setBackgroundColor:[UIColor clearColor]];
    }
}

- (void)layoutCorrectWord {
    
    if(self.isCorrect) {
        
        [self.label setBackgroundColor:[UIColor clearColor]];
        [APPSTYLE applyStyle:@"Answer_Font" toLabel:self.label];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self.label.text];
        [attributeString addAttribute:NSUnderlineStyleAttributeName
                                value:[NSNumber numberWithInt:1]
                                range:(NSRange){0,[attributeString length]}];
       // self.label.attributedText = attributeString;
        [self.label sizeToFit];
        self.button.width = self.label.mWidth;
        self.width = self.label.mWidth + 10;
    }
}
@end
