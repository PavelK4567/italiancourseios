//
//  SimplePageProfileScreen.h
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/20/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECVehicleDisplay.h"

@interface SimplePageProfileScreen : ECVehicleDisplay

//GUI elements

@property (nonatomic, strong) UIImageView *imgProfile;
@property (nonatomic, strong) UITextView *txtProfileIntroduction;
@property (nonatomic, strong) UIButton *btnProfileSound;
@property (nonatomic, strong) UIScrollView *scrollView;

- (IBAction)playIntroSound:(id)sender;

- (void)initializeElements;
- (void)drawGUI;

@end
