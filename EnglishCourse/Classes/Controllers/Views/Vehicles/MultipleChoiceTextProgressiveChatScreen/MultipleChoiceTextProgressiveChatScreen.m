//
//  MultipleChoiceTextProgressiveChatScreen.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/10/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MultipleChoiceTextProgressiveChatScreen.h"

#define BUBBLE_VIEW 100
#define INITIAL_CELL_SOUND 0

@implementation MultipleChoiceTextProgressiveChatScreen

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame {
  
  self = [super initWithFrame:frame];
  
  if (self) {
      
      CGSize selfSize = self.size;
       
  }
  
  return self;
}

- (void)setupInstance
{
    [self.viewTitleLabel setTitleOftheInstance:((SimplePageBubbleDialog *)self.dictVehicleInfo).title];
    
    self.bubbledScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.viewTitleLabel.mHeight, self.mWidth, self.mHeight - self.viewTitleLabel.mHeight)];
    [self.bubbledScrollView setShowsHorizontalScrollIndicator:NO];
    [self.bubbledScrollView setShowsVerticalScrollIndicator:NO];
    self.bubbledScrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bubbledScrollView];
    
    
    
    
}

- (void)initData {
    
    if (self.isCompleted) {
        //NSLog(@"TEST");
        if (self.isReplayed) {
            self.isCompleted = NO;
        }
    }
  
    self.charactersArray = [[DataService sharedInstance] findAllCharacters];
    self.segmentsArray = ((MultipleChoiceTextProgressiveChat *)self.dictVehicleInfo).segmentsArray;
    
    if (!isViewGenerated) {
        
        self.bubblesDataArray = [NSMutableArray arrayWithCapacity:1];
        
        NSInteger prevCharacterID = -1;
        
        NSInteger cellIndexCounter = 0;
        
        IconPosition iconPosition = LEFT;
        
        NSBubbleData *chatBubbleData = nil;
        
        Character *character = nil;
        
        NSInteger lastAnswerIndex = [self findLastAnswerIndex];
        
        for (SegmentProgressiveChat *segmentData in self.segmentsArray) {
            
            if (prevCharacterID == -1) {
                
                prevCharacterID = segmentData.characterID;
                
            } else if (prevCharacterID != segmentData.characterID) {
                
                iconPosition = (iconPosition == LEFT)?RIGHT:LEFT;
                
                prevCharacterID = segmentData.characterID;
                
            }
            
            character = [DATA_SERVICE findCharacterByID:segmentData.characterID];
            
            chatBubbleData = [NSBubbleData dataWithText:((!segmentData.isAnswer)?segmentData.text:nil)
                                                   date:nil
                                                   type:((iconPosition == LEFT)?BubbleTypeSomeoneElse:BubbleTypeMine)
                                                  index:cellIndexCounter
                                               chatType:PROGRESSIVE_CHAT
                                           answersArray:segmentData.answersArray
                                            audioSample:segmentData.audio
                                           drawAnswered:self.isCompleted];
            
            chatBubbleData.avatar = [UIImage imageFromDocumentsResourceFile:character.image];
            chatBubbleData.colorInHEX = character.color;
            chatBubbleData.audio = segmentData.audio;
            
            if (segmentData.isAnswer && cellIndexCounter == lastAnswerIndex) {
                //NSLog(@"LAST ANSWER");
                chatBubbleData.isLastAnswer = YES;
            }
            
            chatBubbleData.delegate = self;
            
            [self.bubblesDataArray addObject:chatBubbleData];
            
            cellIndexCounter++;
            
        }
        
        isViewGenerated = YES;
        
        [self drawInstance];
        
    }
    
}

- (void)drawInstance {
    
    NSArray * bubblesArray = self.bubblesDataArray;
    
    NSInteger yOffset = 11;
    
    NSInteger startY = 0;
    
    NSInteger tempCounterVisible = 0;
    
    for (NSBubbleData *bubbleData in bubblesArray) {
    
        if (bubbleData.isCellViewVisible) {
            
            startY+=(bubbleData.view.frame.size.height + yOffset);
            tempCounterVisible++;
        }
        
    }
    
    if (tempCounterVisible == 0) {
        startY = yOffset;
    }
    else {
        startY+=yOffset;

    }
    
    NSInteger tagCounter = 0;
    
    for (NSBubbleData *bubbleData in bubblesArray) {
        
        if (!bubbleData.isCellViewVisible) {
            
            CGRect frame = bubbleData.view.frame;
            frame.origin.y = startY;
            
            __block LMBubbleView *bubbleView = [[LMBubbleView alloc] initWithFrame:CGRectMake(0, startY, frame.size.width, frame.size.height)];
            bubbleView.showAvatar = YES;
            bubbleView.delegate = self;
            bubbleView.tag = tagCounter;
            [bubbleView setAlpha:0.0f];
            
            bubbleView.drawAnswered = [bubbleData drawAnswered];

            [bubbleView setDataInternal:bubbleData];
            
            bubbleView.bubbleType = BUBBLE_CHAT;
            
            bubbleData.isCellViewVisible = YES;
            
            startY += bubbleData.view.mHeight + yOffset;
            
            CGSize contentSize = self.size;
            
            CGSize containerSize = CGSizeMake(contentSize.width, startY);
            CGSize containerFrameSize = self.bubbledScrollView.frame.size;
            
            [self.bubbledScrollView setContentSize:containerSize];
            
            if (containerSize.height > ((containerFrameSize.height + 120) - frame.size.height)) {
                CGPoint bottomOffset = CGPointMake(0, self.bubbledScrollView.contentSize.height - (self.bubbledScrollView.bounds.size.height));
                
                if (bubbleData.answerObjectsArray) {
                    bottomOffset.y += yOffset;
                    containerSize.height += yOffset;
                    
                    [self.bubbledScrollView setContentSize:containerSize];
                }
                
                if (self.bubbledScrollView.contentSize.height > (self.bubbledScrollView.bounds.size.height - frame.size.height)) {
                    
                    //NSLog(@"BSV CONTENT HEIGHT:%f BSV BOUNDS HEIGHT:%f FRAME HEIGHT:%f", self.bubbledScrollView.contentSize.height, self.bubbledScrollView.bounds.size.height, frame.size.height);
                    
                    bottomOffset.y += yOffset;
                    
                    [self.bubbledScrollView setContentOffset:bottomOffset animated:YES];
                    
                }
                
            }
            
            [self.bubbledScrollView addSubview:bubbleView];
            
            if (!self.isCompleted) {
                [UIView animateWithDuration:0.20 animations:^{
                    [bubbleView setAlpha:1.0f];
                }];
                
                break;
            } else {
                [bubbleView setAlpha:1.0f];
//                startY += yOffset;
                tagCounter++;
            }
            
        }
        
        if (!self.isCompleted) {
            
            tagCounter++;
            
            if (tagCounter == [bubblesArray count] && !self.isCompleted) {
                [self.bubbledScrollView setContentSize:CGSizeMake(self.frame.size.width, startY + 40)];
                
                self.isCompleted = YES;
                
                 [self.vehicleDelegate changeNavigationButtons:self];
                
                [self.vehicleDelegate addPoints:0 isLastAnswer:YES];
                 [self.vehicleDelegate changeNavigationButtons:nil];
                
                self.isDisplayed = YES;
                
                [self.delegate didFinishWithExecution];
            }
        }
        else {
            if (tagCounter == [bubblesArray count]) {
                NSInteger contentSize = self.bubbledScrollView.contentSize.height;
                contentSize += (tagCounter * (yOffset / 2));
                
                [self.bubbledScrollView setContentSize:CGSizeMake(self.bubbledScrollView.mWidth, contentSize)];
                [self.bubbledScrollView setContentOffset:CGPointMake(0, 0)];
            }
        }
        
        
    }
    
    if (viewCompleted) {
        [self.bubbledScrollView setContentOffset:CGPointZero];
    }
    
}

- (void)drawCompletedInstance {
    
    
    
}

- (void)playInitialSound {
    if (!self.isInitialSoundPlayed) {
        if (self.bubblesDataArray && [self.bubblesDataArray count] > 0) {
            NSBubbleData *bubbleData = self.bubblesDataArray[INITIAL_CELL_SOUND];
            
            if (!bubbleData.drawAnswered) {
                [AUDIO_MANAGER setDelegate:self];
                
                [AUDIO_MANAGER playAudioFromDocumentPath:[bubbleData audio] componentSender:bubbleData.btnProfileSound];
            } else {
                [self drawInstance];
            }
            
        }
        
        self.isInitialSoundPlayed = YES;
    }
}

- (void)resizeAnswersCellWithHeight:(NSInteger)cellIndex cellHeight:(NSInteger)cellHeight
{
    
    [UIView animateWithDuration:0.2 animations:^{
        NSArray *subViewsArray = self.bubbledScrollView.subviews;
        
        LMBubbleView *bubbledView = subViewsArray[[subViewsArray count] - 1];
        
        NSBubbleData *bubbledData = bubbledView.data;
        
        [bubbledView.bubbleImage setHeight:cellHeight];
        [bubbledView.avatarImage setY:(cellHeight - bubbledView.avatarImage.mHeight)];
        
        [bubbledView setHeight:cellHeight];
        
        [bubbledData.view setHeight:cellHeight];
        
    } completion:^(BOOL finished) {
        [self drawInstance];
    }];
    
}

- (void)drawRect:(CGRect)rect
{
    [self.viewTitleLabel setTitleOftheInstance:((SimplePageBubbleDialog *)self.dictVehicleInfo).title];
    
}

- (void)stopPlaying {

    self.isAudioPlaying = NO;
    
    [AUDIO_MANAGER setDelegate:nil];
    
    [AUDIO_MANAGER stopPlaying];
    
    [self.delegate didFinishWithExecution];
    
}

- (void)resetProgressiveChat
{
    isViewGenerated = NO;
    
    self.isInitialSoundPlayed = NO;
    
    self.isDisplayed = NO;
    
//    for (NSBubbleData *bubbleData in self.bubblesDataArray) {
//        bubbleData.isCellViewVisible = NO;
//        
//        if (bubbleData.answerObjectsArray) {
//            bubbleData.isAnswered = NO;
//            bubbleData.isExecuted = NO;
//            
//            NSArray *childsArray = [bubbleData.view subviews];
//            
//            if (childsArray) {
//                for (UIView *view in childsArray)
//                {
//                    ((LMNumberedMarkingLabel *)view).isAnswerExecuted = NO;
//                }
//            }
//        }
//        
//    }
    
    self.bubblesDataArray = nil;
    
    self.bubblesDataArray = [NSMutableArray arrayWithCapacity:1];
    
    NSInteger prevCharacterID = -1;
    
    NSInteger cellIndexCounter = 0;
    
    IconPosition iconPosition = LEFT;
    
    NSBubbleData *chatBubbleData = nil;
    
    Character *character = nil;
    
    NSInteger lastAnswerIndex = [self findLastAnswerIndex];
    
    for (SegmentProgressiveChat *segmentData in self.segmentsArray) {
        
        if (prevCharacterID == -1) {
            
            prevCharacterID = segmentData.characterID;
            
        } else if (prevCharacterID != segmentData.characterID) {
            
            iconPosition = (iconPosition == LEFT)?RIGHT:LEFT;
            
            prevCharacterID = segmentData.characterID;
            
        }
        
        character = [DATA_SERVICE findCharacterByID:segmentData.characterID];
        
        chatBubbleData = [NSBubbleData dataWithText:((!segmentData.isAnswer)?segmentData.text:nil)
                                               date:nil
                                               type:((iconPosition == LEFT)?BubbleTypeSomeoneElse:BubbleTypeMine)
                                              index:cellIndexCounter
                                           chatType:PROGRESSIVE_CHAT
                                       answersArray:segmentData.answersArray
                                        audioSample:segmentData.audio
                                       drawAnswered:self.isCompleted];
        
        chatBubbleData.avatar = [UIImage imageFromDocumentsResourceFile:character.image];
        chatBubbleData.colorInHEX = character.color;
        chatBubbleData.audio = segmentData.audio;
        
        if (segmentData.isAnswer && cellIndexCounter == lastAnswerIndex) {
            //NSLog(@"LAST ANSWER");
            chatBubbleData.isLastAnswer = YES;
        }
        
        chatBubbleData.delegate = self;
        
        [self.bubblesDataArray addObject:chatBubbleData];
        
        cellIndexCounter++;
        
    }
    
    
    
    
    NSArray *subViews = [self.bubbledScrollView subviews];
    
    for (int i = 0; i < [subViews count]; i++) {
        UIView *subView = subViews[i];
        [subView removeFromSuperview];
        subView = nil;
    }
    
    [self.bubbledScrollView setContentSize:CGSizeZero];
    [self.bubbledScrollView setContentOffset:CGPointZero];
    
}

- (void)clearProgressiveChat
{
    
    isViewGenerated = NO;
    
    self.isInitialSoundPlayed = NO;
    
    NSArray *subViews = [self.bubbledScrollView subviews];
    
    for (int i = 0; i < [subViews count]; i++) {
        UIView *subView = subViews[i];
        [subView removeFromSuperview];
        subView = nil;
    }
    
    [self.bubbledScrollView setContentSize:CGSizeZero];
    [self.bubbledScrollView setContentOffset:CGPointZero];
}


#pragma mark - NSBubbleDataDelegate implementation

- (void)playSoundFromObjectAtIndex:(NSInteger)index {
    
    SegmentProgressiveChat *segmentData = self.segmentsArray[index];
    
    DLog(@"Play sound %@ from object at index: %ld", segmentData.audio, (long)index);
    
    [AUDIO_MANAGER playAudioFromDocumentPath:segmentData.audio];
    
}

- (void)playSoundFromObjectAtIndex:(NSInteger)index sender:(id)sender
{
    SegmentProgressiveChat *segmentData = self.segmentsArray[index];
    
    DLog(@"Play sound %@ from object at index: %ld", segmentData.audio, (long)index);
    
    [AUDIO_MANAGER playAudioFromDocumentPath:segmentData.audio componentSender:sender];
}

- (void)resizeAnswersCellWithHeight:(NSInteger)cellHeight {
    
    NSArray *subViewsArray = self.bubbledScrollView.subviews;
    
    LMBubbleView *bubbledView = subViewsArray[[subViewsArray count] - 1];
    
    NSBubbleData *bubbledData = bubbledView.data;
    
    
    [UIView animateWithDuration:0.2 animations:^{
        
        NSInteger yOffset = 11;
    
        NSInteger existingCellHeight = bubbledView.mHeight;
        NSInteger scrollContentHeight = self.bubbledScrollView.contentSize.height;
    
        CGPoint bottomOffset;

            if (cellHeight < bubbledView.avatarImage.mHeight) {
                
                CGFloat avatarHeight = bubbledView.avatarImage.mHeight;
                
                [bubbledView.bubbleImage setHeight:avatarHeight];
                [bubbledView.avatarImage setY:0];
                
                [bubbledView setHeight:avatarHeight];
                
                [bubbledData.view setHeight:avatarHeight];
                [bubbledData.view setY:0];
                for (UIView *vv in bubbledView.customView.subviews) {
                    
                    if  ([vv isKindOfClass:[LMNumberedMarkingLabel class]]) {
                        
                        [vv setHeight:avatarHeight];
                        [[(LMNumberedMarkingLabel *)vv markedTextLabel] setY:vv.mHeight/2 - [(LMNumberedMarkingLabel *)vv markedTextLabel].mHeight/2];
                    }
                    
                }
                
                
                bottomOffset = CGPointMake(0, self.bubbledScrollView.contentSize.height - self.bubbledScrollView.bounds.size.height - bubbledView.mHeight + yOffset + yOffset + ((IS_IPHONE5)?((bubbledView.mHeight / 2) + yOffset):0));
            }
            else {
                
                [bubbledView.bubbleImage setHeight:cellHeight];
                [bubbledView.avatarImage setY:(cellHeight - bubbledView.avatarImage.mHeight)];
                
                [bubbledView setHeight:cellHeight];
                
                [bubbledData.view setHeight:cellHeight];
                
                
                bottomOffset = CGPointMake(0, self.bubbledScrollView.contentSize.height - self.bubbledScrollView.bounds.size.height - bubbledView.mHeight + yOffset + yOffset + ((IS_IPHONE5)?((bubbledView.mHeight / 2) + yOffset):0));
            }
            
            
            if ((self.bubbledScrollView.contentSize.height + yOffset) > self.bubbledScrollView.bounds.size.height) {
                [self.bubbledScrollView setContentSize:CGSizeMake(self.bubbledScrollView.contentSize.width, ((scrollContentHeight - (cellHeight * 2)) + yOffset + (yOffset / 2)))];
                [self.bubbledScrollView setContentOffset:bottomOffset];
            } else {
//                [self.bubbledScrollView setContentSize:CGSizeMake(self.bubbledScrollView.contentSize.width, ((scrollContentHeight - (cellHeight * 2) + (cellHeight / 2))))];
            
                [self.bubbledScrollView setContentSize:CGSizeMake(self.bubbledScrollView.contentSize.width, self.bubbledScrollView.bounds.size.height)];
            }
            
            DLog(@"Offset:%f Content Size:%f", bottomOffset.y, self.bubbledScrollView.mHeight);
            
            
        } completion:^(BOOL finished) {
            
        }];

}

- (void)drawNextBubble {
    [self drawInstance];
}

#pragma mark - LMAudioManagerDelegate implementation

- (void)playbackComplete {
    
    [AUDIO_MANAGER setDelegate:nil];
    
    [self drawInstance];
}

#pragma mark - LMBubbleViewDelegate implementation

- (void)didSelectViewWithFrame:(CGRect)frame
{
    [AUDIO_MANAGER setDelegate:nil];
    
    [AUDIO_MANAGER stopPlaying];
    
    [self drawInstance];
    
}

- (void)drawNextView
{
    [self drawInstance];
}

- (void)returnPointsForCorrentAnswer:(NSInteger)points isLastAnswer:(BOOL)isLastAnswer
{
    self.calculatedPoints += points;
    [self.vehicleDelegate addPoints:(int)points isLastAnswer:isLastAnswer];
}

- (void)savePointsForInstance
{
    self.isCompleted = YES;
}

- (void)disableVehicleInteractions {
    [AUDIO_MANAGER setDelegate:nil];
    [super disableVehicleInteractions];
}

- (void)showLayoutForCompletedInstance {
    
    if (!viewCompleted) {
    
        self.isCompleted = YES;
        
        isViewGenerated = NO;
        viewCompleted = YES;
        
        
        NSArray *subViews = [self.bubbledScrollView subviews];
        
        for (int i = 0; i < [subViews count]; i++) {
            UIView *subView = subViews[i];
            [subView removeFromSuperview];
            subView = nil;
        }
        
        CGPoint point = CGPointMake(0, 0);
        
        [self.bubbledScrollView setContentOffset:point];
        [self.bubbledScrollView setContentSize:CGSizeMake(0, 0)];
        
        [self initData];
        
    }
    
}

- (NSInteger)findLastAnswerIndex
{
    NSInteger index = ([self.segmentsArray count] - 1);
    
    for (; index > 0; index--) {
        
        SegmentProgressiveChat *segmentData  = self.segmentsArray[index];
        
        if ([segmentData isAnswer]) {
            break;
        }
    }
    
    
    
    
    return index;
}


- (BOOL)checkIfMultipleAnswersExists
{
    BOOL isMultipleChoiceAnswerExist = NO;
    
    for (int i = 0; i > [self.segmentsArray count]; i++) {
        
        SegmentProgressiveChat *segmentData  = self.segmentsArray[i];
        
        if ([segmentData isAnswer]) {
            isMultipleChoiceAnswerExist = YES;
            break;
        }
    }
    
    return isMultipleChoiceAnswerExist;
}

@end
