//
//  WritingVehicleScreen.m
//  ECPractices
//
//  Created by Dimitar Shopovski on 11/27/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "WritingVehicleScreen.h"

@implementation WritingVehicleScreen

- (void)drawGUI {
    
    self.btnProfileSound = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnProfileSound.frame = CGRectMake(3, 3, 30, 34);
    [self.btnProfileSound setImage:[UIImage imageNamed:@"ic_small_sound.png"] forState:UIControlStateNormal];
    [self.btnProfileSound addTarget:self action:@selector(playIntroSound:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnProfileSound];
    
    self.imgBoard = [[UIImageView alloc] initWithFrame:CGRectMake(18, 50, 284, 109)];
    self.imgBoard.contentMode = UIViewContentModeScaleAspectFit;
    self.imgBoard.image = [UIImage imageNamed:@"img_board_writing"];
    
    [self addSubview:self.imgBoard];
    
    
    self.txtTranslationText = [[UITextView alloc] initWithFrame:CGRectMake((LM_WIDTH_INSTACE-231)/2, 70, 231, 40)];
    [self.txtTranslationText setBackgroundColor:[UIColor clearColor]];
    self.txtTranslationText.textAlignment = NSTextAlignmentCenter;
    self.txtTranslationText.backgroundColor = [UIColor clearColor];
    self.txtTranslationText.delegate = self;
    [self addSubview:self.txtTranslationText];
    
    self.btnTranslate = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnTranslate.frame = CGRectMake((LM_WIDTH_INSTACE-100)/2, 255, 100, 39);
    [self.btnTranslate setBackgroundColor:[APPSTYLE colorForType:@"Italy_Main_Green_Color"]];
    
    [self.btnTranslate setTitle:Localized(@"T141") forState:UIControlStateNormal];
    [self.btnTranslate addTarget:self action:@selector(translateSentence:) forControlEvents:UIControlEventTouchUpInside];
    
    [APPSTYLE applyStyle:@"Button_Writing_Check" toButton:self.btnTranslate];
    
    [self.btnTranslate.layer setCornerRadius:6];
    [self.btnTranslate.layer setMasksToBounds:YES];
    
    
    //        self.btnTranslate.alpha = 0.8;
    self.btnTranslate.userInteractionEnabled = NO;
    //        self.btnTranslate.hidden = YES;
    
    [self addSubview:self.btnTranslate];
    
    [self bringSubviewToFront:self.feedbackView];
    
    //        self.feedbackView.viewBg.center = CGPointMake(frame.size.width/2, frame.size.height/3*2+(frame.size.height/3)/2);
}

- (void)initializeElements {
        
    //UIFont *fontForTextView;
        
    switch ([(Writing *)self.dictVehicleInfo layout]) {
        case 0:
            self.imgBoard.image = [UIImage imageNamed:@"img_board_writing"];
            [APPSTYLE applyStyle:@"Writing_Board_Font" toTextView:self.txtTranslationText];
            //fontForTextView = [APPSTYLE fontForType:@"Writing_Board_Font"];
            break;
        case 1:
            self.imgBoard.image = [UIImage imageNamed:@"img_note_writing"];
            [APPSTYLE applyStyle:@"Writing_Note_Font" toTextView:self.txtTranslationText];
            //fontForTextView = [APPSTYLE fontForType:@"Writing_Note_Font"];
            break;
        case 2:
            self.imgBoard.image = [UIImage imageNamed:@"img_bubble_writing"];
            [APPSTYLE applyStyle:@"Writing_Bubble_Font" toTextView:self.txtTranslationText];
            //fontForTextView = [APPSTYLE fontForType:@"Writing_Bubble_Font"];
            break;
        default:
            break;
    }
    
    self.txtTranslationText.center = self.imgBoard.center;
    
    NSString *strTextToTranslate = [(Writing *)self.dictVehicleInfo questionText];
    
    [self.viewTitleLabel setTitleOftheInstance:strTextToTranslate];
    
//    self.txtTranslationText.height = fontForTextView.lineHeight * 3;
    
    self.imgBoard.center = CGPointMake(self.imgBoard.center.x, self.viewTitleLabel.frame.origin.y+self.viewTitleLabel.frame.size.height+10+self.imgBoard.frame.size.height/2);
    self.txtTranslationText.center = self.imgBoard.center;
    
    self.btnTranslate.center = CGPointMake(self.imgBoard.center.x, self.imgBoard.frame.origin.y+self.imgBoard.frame.size.height+10+self.btnTranslate.frame.size.height/2);
    
    if (![(Writing *)self.dictVehicleInfo questionAudio]) {
        
        self.btnProfileSound.hidden = YES;
    }
}


- (IBAction)playIntroSound:(id)sender {
        
    [AUDIO_MANAGER playAudioFromDocumentPath:[(Writing*)self.dictVehicleInfo questionAudio] componentSender:self.btnProfileSound];
    
}

- (void)showKeyboard {
    
    if (self.isCompleted) {
        return;
    }
    isWritingActive = YES;
    [self.txtTranslationText becomeFirstResponder];
}

- (IBAction)translateSentence:(id)sender {
    
    isWritingActive = NO;
    [self.txtTranslationText resignFirstResponder];
    
    NSArray *arrayCorrectAnswers = [(Writing *)self.dictVehicleInfo correctAnswers];
        
    if ([self analyzeStringsInArray:self.txtTranslationText.text :arrayCorrectAnswers]) {
        
        self.btnTranslate.hidden = YES;
        self.txtTranslationText.userInteractionEnabled = NO;

//        [self showLayoutForCompletedInstance];
        self.isCompleted = YES;
        
        [self.feedbackView setInformation:YES];
        [self.vehicleDelegate addPoints:kParam010 isLastAnswer:YES];
        
    }
    else {
        
        self.txtTranslationText.editable = NO;
        self.btnTranslate.hidden = YES;
        self.txtTranslationText.userInteractionEnabled = NO;
                
        [self.vehicleDelegate addPointsWithIncorrectAnswer:kParam012];
        self.isCompleted = NO;
        [self.feedbackView setInformation:NO];

    }
    
    [self.feedbackView showWithAnimation];

}

- (BOOL)analyzeStringsInArray:(NSString *)inputString :(NSArray *)arrayStrings {
    
    BOOL isCaseSensitive = [(Writing *)self.dictVehicleInfo isCaseSensitive];
    
    for (NSString *str in arrayStrings) {
        
        if ([self analyzeTwoStrings:[self createRegularString:inputString isCaseSensitive:isCaseSensitive] :[self createRegularString:str isCaseSensitive:isCaseSensitive] :isCaseSensitive]) {
        
            return YES;
        }
        
    }
    
    return NO;
}

- (BOOL)analyzeTwoStrings:(NSString *)inputString :(NSString *)correctString :(BOOL)isCaseSensitive {
    
    return [[self createRegularString:inputString isCaseSensitive:isCaseSensitive] isEqualToString:[self createRegularString:correctString isCaseSensitive:isCaseSensitive]];
}


- (NSString *)createRegularString:(NSString *)inputString isCaseSensitive:(BOOL)isCaseSensitive {
    
    NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890\'"];

    NSString *regularString = [[inputString componentsSeparatedByCharactersInSet:
                              [myCharSet invertedSet]]
                             componentsJoinedByString:@" "];
    
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:&error];
    
    regularString= [regex stringByReplacingMatchesInString:regularString options:0 range:NSMakeRange(0, [regularString length]) withTemplate:@" "];
    
    regularString = [regularString stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (!isCaseSensitive) {
        
        regularString = [regularString lowercaseString];
    }
    
//    NSLog(@"/////-%@-/////", regularString);
    
    return regularString;
}

#pragma mark - UITextView Delegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        if (textView.text.length != 0) {
            [self translateSentence:nil];
            isWritingActive = NO;
        }
        return NO;
    }
    else {
        
        int numLines = textView.contentSize.height/textView.font.lineHeight;
        
        if (numLines != lastNumberOfLines) {
            
            lastNumberOfLines = numLines;
            
            if (lastNumberOfLines <= 3) {
                self.txtTranslationText.height = textView.font.lineHeight * lastNumberOfLines;
                
                int yOffsetPlus = 0;
                if ([(Writing *)self.dictVehicleInfo layout] == 1) {
                    yOffsetPlus=8;
                }
                else if ([(Writing *)self.dictVehicleInfo layout] == 2) {
                    
                    yOffsetPlus=-3;
                }
                self.txtTranslationText.center = CGPointMake(self.imgBoard.center.x, self.imgBoard.center.y+yOffsetPlus);;
            }
            //NSLog(@"Number - %d", lastNumberOfLines);

        }
        
    }
    
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    if ([textView.text length] > 0) {
        
        self.btnTranslate.alpha = 1.0;
        self.btnTranslate.userInteractionEnabled = YES;
        self.btnTranslate.hidden = NO;
    }
    else {
        
//        self.btnTranslate.alpha = 0.8;
        self.btnTranslate.userInteractionEnabled = NO;
//        self.btnTranslate.hidden = YES;
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    isWritingActive = YES;
}


#pragma mark - Reset instance 

- (void)resetTheInstance {
    
    self.btnTranslate.hidden = NO;
    self.btnTranslate.userInteractionEnabled = NO;
    self.txtTranslationText.editable = YES;
    self.txtTranslationText.text = @"";
    self.txtTranslationText.userInteractionEnabled = YES;

}

- (void)showLayoutForCompletedInstance {
    
//    self.txtTranslationText.editable = NO;
    self.btnTranslate.hidden = YES;
    self.txtTranslationText.userInteractionEnabled = NO;

    NSString *strForCompleted = @"";
    
    if (![self.txtTranslationText.text isEqualToString:@""] && [self.txtTranslationText.text length]>0) {
        
        if (self.isCompleted) {
            
            NSArray *arrayPossible = [(Writing *)self.dictVehicleInfo correctAnswers];
            
            for (NSString *possibleAnswers in arrayPossible) {
                
                if ([[possibleAnswers lowercaseString] isEqualToString:[self.txtTranslationText.text lowercaseString]]) {
                    
                    strForCompleted = self.txtTranslationText.text;
                    break;
                }
                
            }
            
        }
        
    }
    
    if (![strForCompleted isEqualToString:@""]) {
        
        self.txtTranslationText.text = strForCompleted;

    }
    else {
        
        self.txtTranslationText.text = [[(Writing *)self.dictVehicleInfo correctAnswers] objectAtIndex:0];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (isWritingActive) {
        
        UITouch *touch = [touches anyObject];
        
        if (touch.view == self) {
            [self.txtTranslationText resignFirstResponder];
            isWritingActive = NO;
        }
    }
}


@end
