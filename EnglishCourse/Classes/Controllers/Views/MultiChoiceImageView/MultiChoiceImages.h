//
//  MultiChoiceImages.h
//  ECControls
//
//  Created by Dimitar Shopovski on 11/18/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultiChoiceImages : UIView

@property float width;
@property (nonatomic, strong) NSMutableArray *arrayInteractiveImages;

- (void)animateLayout;

@end
