//
//  ProgressCircleView.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/16/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "ProgressCircleView.h"

@implementation ProgressCircleView
@synthesize dictProgressColor, progress;


- (void)drawGUI {
    
    if (self.isProfileProgressCircle) {
        
        [self.progressLabel setColorTable: @{
                                             NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                             NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[APPSTYLE colorForType:@"Native_Pink_Color"],
                                             NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[APPSTYLE colorForType:@"Italy_Main_Green_Color"]
                                             }];
    } else {
        
        [self.progressLabel setColorTable:dictProgressColor];
    }
    
    [self.progressLabel setProgress:progress/100.0];

}


#pragma mark - 
#pragma mark - Setter color scheme

- (void)changeDictProgressColors:(NSDictionary *)dict {
    
    dictProgressColor = [NSDictionary dictionaryWithDictionary:dict];
    [self.progressLabel setColorTable:dict];
    [self setNeedsDisplay];

    
}

#pragma mark - Setter for progress
- (void)changeProgress:(float)newProgress {

    [self.progressLabel setProgress:newProgress/100.0];
    self.progress = newProgress;
    [self layoutIfNeeded];
    
}
- (void)changeProfileProgress:(float)newProgress {
  
    self.isProfileProgressCircle = YES;
    [self changeProgress:newProgress];
  
}

- (void)setColorForProfileProgressCircle:(NSDictionary *)dictColorScheme {
    
    self.isProfileProgressCircle = YES;
    dictProgressColor = [NSDictionary dictionaryWithDictionary:dictColorScheme];
    [self.progressLabel setColorTable:dictColorScheme];
    [self layoutSubviews];

}

@end
