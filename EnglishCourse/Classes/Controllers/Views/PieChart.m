//
//  PieChart.m
//  Try_PieChart
//
//  Created by Imran on 23/08/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PieChart.h"


@implementation PieChart
@synthesize m_val1, m_val2;

-(void)setVal1:(float)val1 setVal2:(float)val2
{
    
	self.m_val1=val1;
	self.m_val2=val2;
    [self setNeedsDisplay];

}


- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
        // Initialization code
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    // Drawing code
	float sum=self->m_val1 + self->m_val2;
	float mult=(360/sum);
	
	float startDeg=-90.0;
	float endDeg=90.0;
	
	int x=rect.size.width/2;
	int y=rect.size.height/2;
	int r=rect.size.width/2;
	
	CGContextRef ctx=UIGraphicsGetCurrentContext();
	//CGContextSetRGBStrokeColor(ctx, 1.0, 0.0, 1.0, 0.0);
	//CGContextSetLineWidth(ctx, 2.0);
	
	startDeg=-90.0;
	endDeg=-90+(self->m_val1 * mult);
	if(startDeg != endDeg)
	{
        CGContextSetFillColorWithColor(ctx, [APPSTYLE colorForType:@"Italy_Main_Green_Color"].CGColor);
		CGContextMoveToPoint(ctx, x, y);
		CGContextAddArc(ctx, x, y, r, [self degreesToRadians:startDeg], [self degreesToRadians:endDeg], 0);
		CGContextClosePath(ctx);
		CGContextFillPath(ctx);
	
	
	}
	
	startDeg=endDeg;
	endDeg=endDeg + (self->m_val2 * mult);
	if(startDeg != endDeg)
	{
        
        CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:202.0/255.0 green:181.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor);
		CGContextMoveToPoint(ctx, x, y);
		CGContextAddArc(ctx, x, y, r, [self degreesToRadians:startDeg], [self degreesToRadians:endDeg], 0);
		CGContextClosePath(ctx);
		CGContextFillPath(ctx);
		
		
	}
	
}

- (float)degreesToRadians:(float)degrees {
    
    return (degrees)*M_PI/180.0;
}


@end
