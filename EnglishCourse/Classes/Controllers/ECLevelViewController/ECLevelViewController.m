//
//  ECLevelViewController.m
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//

#import "ECLevelViewController.h"
#import "ECLevelTableViewCell.h"

@interface ECLevelViewController ()

@end

@implementation ECLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tbLevels registerNib:[UINib nibWithNibName:@"ECLevelTableViewCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"Cell"];
    
    self.arrayDataSource = [[NSMutableArray alloc] initWithObjects:
                            [NSDictionary dictionaryWithObjectsAndKeys:@"I am", @"levelName", @(36.0), @"levelProgress", @(YES), @"levelActive", nil],
                            [NSDictionary dictionaryWithObjectsAndKeys:@"Colors", @"levelName", @(76.0), @"levelProgress", @(YES), @"levelActive", nil],
                            [NSDictionary dictionaryWithObjectsAndKeys:@"Numbers", @"levelName", @(81.0), @"levelProgress", @(NO), @"levelActive", nil],
                            [NSDictionary dictionaryWithObjectsAndKeys:@"Family", @"levelName", @(1.0), @"levelProgress", @(YES), @"levelActive", nil],
                            [NSDictionary dictionaryWithObjectsAndKeys:@"I like", @"levelName", @(3.0), @"levelProgress", @(NO), @"levelActive", nil],
                            [NSDictionary dictionaryWithObjectsAndKeys:@"Travel", @"levelName", @(10.0), @"levelProgress", @(NO), @"levelActive", nil], nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.arrayDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *strCell = @"Cell";
    
    ECLevelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:strCell];
    
    if (cell == nil) {
        
        cell = [[ECLevelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:strCell];
    }
    
    
    cell.lblLevelName.text = [NSString stringWithFormat:@"%ld - %@", indexPath.row+1, [[self.arrayDataSource objectAtIndex:indexPath.row] objectForKey:@"levelName"]];
    
    float progress = [[[self.arrayDataSource objectAtIndex:indexPath.row] objectForKey:@"levelProgress"] floatValue];
    
    [cell.kaProgressLabel setProgress:progress/100.0];
    [cell.kaProgressLabel setColorTable: @{
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelFillColor):[UIColor clearColor],
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor colorWithRed:123.0/255.0 green:104.0/255.0 blue:238.0/255.0 alpha:0.6],
                                       NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor magentaColor]
                                       }];
    
    
    if ([[[self.arrayDataSource objectAtIndex:indexPath.row] objectForKey:@"levelActive"] boolValue]) {
        
        cell.imgLevelStatus.image = [UIImage imageNamed:@"star-active.png"];
        cell.lblLevelName.textColor = [UIColor grayColor];
    }
    else {
     
        cell.imgLevelStatus.image = [UIImage imageNamed:@"star-inactive.png"];
        cell.lblLevelName.textColor = [UIColor lightGrayColor];
    }
    return cell;
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
