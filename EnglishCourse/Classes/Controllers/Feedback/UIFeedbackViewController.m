//
//  UIFeedbackViewController.m
//  K1000
//
//  Created by Action-Item on 5/7/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import "UIFeedbackViewController.h"
//#import "UISubscribePopupView.h"


@interface UIFeedbackViewController ()
@end


@implementation UIFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

  self.navigationItem.rightBarButtonItem = nil;
  if (!IS_IPHONE4) {
      _userLevelImageView.width = _userLevelImageView.mWidth + 35;
    _userLevelImageView.centerX = self.view.mWidth * 0.5f;
  }
  [self layoutData];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];

  UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
 // [backButton setFrame:(CGRect) { CGPointZero, kGameNavbarButtonSize }];
  //[backButton setImage:[UIImage imageNamed:@"backButton"]];
  [backButton addTarget:self action:@selector(goHome) forControlEvents:UIControlEventTouchUpInside];

  UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
  if ([[[UIDevice currentDevice] systemVersion] compare:@"6.9.9" options:NSNumericSearch] != NSOrderedAscending) {
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -16;
    self.navigationItem.leftBarButtonItems = @[ negativeSpacer, backItem ];
  } else {
    self.navigationItem.leftBarButtonItem = backItem;
  }
  //[self.navigationController.navigationBar setBackgroundImage:IMAGE(NAVIGATION_BG_IMAGE) forBarMetrics:0];
}

- (void)setUserScore:(double)score andLevel:(int)level;
{
  _userScore = score;
  _userLevel = level;

  [self layoutData];
}

- (void)layoutData
{
  //_registerButton.hidden = !userProfileLogic().isGuestMode;

  if (_userScore < 0 || _userLevel < 0) {
    _userLevelImageView.image = nil;
    _userNameLabel.text = nil;
    _userScoreLabel.text = nil;
    _scoreSummaryDescriptionLabel.text = nil;
    _scoreSummaryTitleLabel.text = nil;
    return;
  }

  //_registerButton.title = Localized(@"T117B");
  //_doneButton.title = Localized(@"OK");
  //_userNameLabel.text = [userProfileLogic().user hasName] ? userProfileLogic().user.name : nil;
  _userScoreLabel.text = [NSString stringWithFormat:@"%@ %d", Localized(@"Score:"), (int)_userScore];

  int userLevel = _userLevel;
  switch (userLevel) {
  case userLevelOne: {
    [_userLevelImageView setImage:[UIImage imageNamed:@"1"]];
    _scoreSummaryTitleLabel.text = Localized(@"Don't Give Up!");
    _scoreSummaryDescriptionLabel.text =
      Localized(@"It takes time to learn a new langauge. With Kantoo 1000, you'll get there soon. Subscribed users have acess from beginning to end!");
    break;
  }
  case userLevelTwo: {
    [_userLevelImageView setImage:[UIImage imageNamed:@"2"]];
    _scoreSummaryTitleLabel.text = Localized(@"Keep Practicing!");
    _scoreSummaryDescriptionLabel.text =
      Localized(@"Practice leads to perfection, and Kantoo 1000 will be with you on every step (of the way). Subscribed users have access to the app and much more!");
    break;
  }
  case userLevelThree: {
    [_userLevelImageView setImage:[UIImage imageNamed:@"3"]];
    _scoreSummaryTitleLabel.text = Localized(@"Good Job!");
    _scoreSummaryDescriptionLabel.text = Localized(@"You're really getting there. Subscribed users have access to all 1000 words. You can be an English pro!");
    break;
  }
  case userLevelFour: {
    [_userLevelImageView setImage:[UIImage imageNamed:@"4"]];
    _scoreSummaryTitleLabel.text = Localized(@"Excellent!");
    _scoreSummaryDescriptionLabel.text =
      Localized(@"You are on your way to real mastery. Subscribed users have access to all 1000 words. Soon, you'll be a superstar!");
    break;
  }
  case userLevelFive: {
    [_userLevelImageView setImage:[UIImage imageNamed:@"5"]];
    _scoreSummaryTitleLabel.text = Localized(@"You Rock!");
    _scoreSummaryDescriptionLabel.text =
      Localized(@"You've become a master. Subscribed users have access to all of Kantoo 1000. We'll help you keep up the good work!");
    break;
  }
  default:
    break;
  }

  /*
      Uncomment for localized feedback
   */

  //  switch (userLevel) {
  //    case userLevelOne:
  //      _userLevelImageView.image = IMAGE(@"1");
  //      _scoreSummaryTitleLabel.text = [@"T118" localized];
  //      _scoreSummaryDescriptionLabel.text = [(userProfileLogic().isGuestMode ? @"T119B" : @"T119") localized];
  //      break;
  //    case userLevelTwo:
  //      _userLevelImageView.image = IMAGE(@"2");
  //      _scoreSummaryTitleLabel.text = [@"T120" localized];
  //      _scoreSummaryDescriptionLabel.text = [(userProfileLogic().isGuestMode ? @"T121B" : @"T121") localized];
  //      break;
  //    case userLevelThree:
  //      _userLevelImageView.image = IMAGE(@"3");
  //      _scoreSummaryTitleLabel.text = [@"T122" localized];
  //      _scoreSummaryDescriptionLabel.text = [(userProfileLogic().isGuestMode ? @"T123B" : @"T123") localized];
  //      break;
  //    case userLevelFour:
  //      _userLevelImageView.image = IMAGE(@"4");
  //      _scoreSummaryTitleLabel.text = [@"T124" localized];
  //      _scoreSummaryDescriptionLabel.text = [(userProfileLogic().isGuestMode ? @"T125B" : @"T125") localized];
  //      break;
  //    case userLevelFive:
  //      _userLevelImageView.image = IMAGE(@"5");
  //      _scoreSummaryTitleLabel.text = [@"T126" localized];
  //      _scoreSummaryDescriptionLabel.text = [(userProfileLogic().isGuestMode ? @"T127B" : @"T127") localized];
  //      break;
  //    default:
  //      break;
  //  }
}

- (void)goHome
{
  //[viewLogic() presentMainScreen];
  [APP_DELEGATE backToLevelPage];
}

- (void)dismissAnimated
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

/* UIButton Delegates */


- (IBAction)doneButtonClicked:(UIButton *)sender
{
  [self dismissAnimated];
}

- (IBAction)registerButtonClicked:(UIButton *)sender
{
 /* UISubscribePopupView *popup = [UISubscribePopupView loadFromNib];
  popup.dismissToLogin = NO;
  popup.allowOther = NO;
  [popup setDelegate:popup];
  [APPDELEGATE.window addSubview:popup];
  [popup show:YES];
  NSString *uuid = [[UIApplication sharedApplication] uniqueInstallationIdentifier];
  [Flurry logEvent:@"onAppleSubscriptionTerms" withParameters:@{ @"uuid" : uuid, @"type" : @"subscription_expired_popup" }];
  [self dismissAnimated];*/
}



@end
