//
//  ConfirmationDialog.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/23/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "ConfirmationDialog.h"
#import "NSString+Encode.h"

@implementation ConfirmationDialog


#pragma mark - awakeFromNib

- (void)awakeFromNib
{
    [super awakeFromNib];
}


#pragma mark - bindGUI

- (void)bindGUI
{
    
    if([self.email isEqualToString:@""]) {
        self.resend = NO;
    }
    else {
        self.resend = YES;
    }
    
    [titleLabel setText:Localized(@"T592")];
    [descriptionLabel setText:Localized(@"T593")];
    [nameLabel setText:Localized(@"T594")];
    [nameValueLabel setText:[NSString stringWithFormat:@"%@ %@",self.name,self.familyName]];
    [emailLabel setText:[NSString stringWithFormat:Localized(@"T595")]];
    [emailValueLabel setText:self.email];
    [approveButton setTitle:Localized(@"T597") forState:UIControlStateNormal];
    [cancelButton setTitle:Localized(@"T596") forState:UIControlStateNormal];
    [nameLabel sizeToFit];
    [emailLabel sizeToFit];
    [nameValueLabel sizeToFit];
    [emailValueLabel sizeToFit];
    
    emailValueLabel.width = descriptionLabel.mWidth;
    nameValueLabel.width = descriptionLabel.mWidth;
    nameLabel.width = descriptionLabel.mWidth;
    emailLabel.width = descriptionLabel.mWidth;
    [self layoutGUI];
}


#pragma mark - layoutGUI

- (void)layoutGUI
{
    [cancelButton.layer setCornerRadius:kDefaultPadding];
    [approveButton.layer setCornerRadius:kDefaultPadding];
    [containerView.layer setCornerRadius:kDefaultPadding];
    
    [cancelButton.layer setBorderWidth:1];
    //[approveButton.layer setBorderWidth:1];
    UIColor *tempColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"];
    [cancelButton.layer setBorderColor:tempColor.CGColor];
    //[approveButton.layer setBorderColor:tempColor.CGColor];
    
    if([self.name isEqualToString:@""] || [self.familyName isEqualToString:@""]) {
    
        [nameValueLabel setHidden:YES];
        [nameLabel setHidden:YES];
        [descriptionLabel setHidden:YES];
        [emailLabel placeBelowView:titleLabel withPadding:kDefaultPadding];
        [emailValueLabel placeBelowView:emailLabel withPadding:kDefaultPadding * 2];
        [containerView setHeight:emailValueLabel.y + emailValueLabel.mHeight + 100];
    }
}


#pragma mark - Show on view

- (void)showAnimated:(BOOL)animated
{
    [self showAnimated:animated completionBlock:nil];
}


- (void)showAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock
{
    if (_shown == YES) return;
    
    _shown = YES;
    
    if (animated)
    {
        self.superview.userInteractionEnabled = YES;
        
        [UIView animateWithDuration:0.35
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.alpha = 1;
                         }
                         completion:^(BOOL finished) {
                             self.superview.window.userInteractionEnabled = YES;
                             
                             if (completionBlock)
                                 completionBlock();
                         }];
    }
    else
    {
        self.alpha = 1;
        
        if (completionBlock)
            completionBlock();
    }
}


- (void)hideAnimated:(BOOL)animated
{
    [self hideAnimated:YES completionBlock:nil];
}


- (void)hideAnimated:(BOOL)animated completionBlock:(void (^)(void))completionBlock;
{
    if (_shown == NO) return;
    
    _shown = NO;
    
    if (animated)
    {
        self.superview.userInteractionEnabled = NO;
        
        [UIView animateWithDuration:0.35
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.alpha = 0;
                         }
                         completion:^(BOOL finished) {
                             self.superview.userInteractionEnabled = YES;
                             
                             if (completionBlock)
                                 completionBlock();
                         }];
    }
    else
    {
        self.alpha = 0;
        
        if (completionBlock)
            completionBlock();
    }
}


#pragma mark - button actions

- (IBAction)hidePopup:(id)sender
{
    [self hideAnimated:YES];
}


#pragma mark - confirmation screen delegate

- (IBAction)approveAction:(id)sender
{
    __weak  ConfirmationDialog *weakSelf = self;
    
    if([ReachabilityHelper reachable]) {
        [API_REQUEST_MANAGER sendDiplomaForUser:[USER_MANAGER userMsisdnString]
                                        groupID:[REQUEST_MANAGER groupID]
                                      contentID:[REQUEST_MANAGER contentID]
                                        levelId:[NSString stringWithFormat:@"%ld",(long)self.levelId]
                                     salutation:@"Mr"
                                      firstName:[self.name encodeString:NSUTF8StringEncoding]
                                       lastName:[self.familyName encodeString:NSUTF8StringEncoding]
                                          email:[self.email encodeString:NSUTF8StringEncoding]
                                         resend:self.resend ? @"true" : @"false"
                                CompletionBlock:^(ASIHTTPRequest *asiHttpRequest, NSError *error, NSInteger responseCode, NSDictionary *responseDictionary) {
                                
                                    if(!error && [[responseDictionary objectForKey:@"status"] intValue]  == 0) {
                      
                                        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                                          message:Localized(@"T598")
                                                                                         delegate:weakSelf
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                        message.tag = 1;
                                        [message show];
                                    
                                    }
                                }];
    }
    
    else {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:Localized(@"T302")
                                                         delegate:weakSelf
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        message.tag = 0;
        [message show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1) {
        
        [self.viewController dismissViewControllerAnimated:NO completion:nil];
        [self.delegate isApproved:YES];
        [self hideAnimated:YES];
    }
}

#pragma mark -

@end
