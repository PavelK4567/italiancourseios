//
//  LevelCompletitionViewController.m
//  EnglishCourse
//
//  Created by Darko Trpevski on 2/18/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LevelCompletitionViewController.h"

@implementation LevelCompletitionViewController


#pragma mark - viewDidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadGUI];
    
    self.isVisited = NO;
}


#pragma mark - viewDidAppear

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.isVisited) {
        [self dismissModalViewControllerWithPushDirection:nil];
    }

}


#pragma mark - loadGUI

- (void)loadGUI
{
    [self bindGUI];
}


#pragma mark - bindGUI

- (void)bindGUI
{
    LevelData *levelData = DATA_SERVICE.dataModel.courseData.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
    if(self.diplomaPoints < kParam043) {

        [createDiplomaButton setTitle:Localized(@"T578") forState:UIControlStateNormal];
        [facebookButton setTitle:Localized(@"T620") forState:UIControlStateNormal];
        [titleLabel setText:[NSString stringWithFormat:Localized(@"T572"),levelData.levelName]];
        [diplomaTextLabel setText:[NSString stringWithFormat:Localized(@"T573"),levelData.levelName,kParam043 - self.diplomaPoints]];
        [titleViewLabel setText:Localized(@"T571")];
        [diplomaImageView setImageFromDocumentsResourceFile:levelData.image];
        [practiceModeButton setTitle:Localized(@"T575") forState:UIControlStateNormal];
        [nextLevelButton setTitle:Localized(@"T574") forState:UIControlStateNormal];

        [self layoutFail];
    }
    
    if(self.numberOfLessons > 0) {
        [createDiplomaButton setTitle:Localized(@"T578") forState:UIControlStateNormal];
        [facebookButton setTitle:Localized(@"T620") forState:UIControlStateNormal];
        [titleLabel setText:Localized(@"T576")];
        [diplomaTextLabel setText:[NSString stringWithFormat:Localized(@"T577"),self.numberOfLessons,levelData.levelName,levelData.levelName]];
        [titleViewLabel setText:Localized(@"T571")];
        [diplomaImageView setImage:[UIImage imageNamed:@"ic_diploma2_lc"]];
        
        [self layoutDiploma];
    }
    
    if(self.diplomaPoints == -1 && self.numberOfLessons == -1) {
    
        [createDiplomaButton setTitle:Localized(@"T578") forState:UIControlStateNormal];
        [facebookButton setTitle:Localized(@"T620") forState:UIControlStateNormal];
        [practiceModeButton setTitle:Localized(@"T574") forState:UIControlStateNormal];
        [nextLevelButton setTitle:Localized(@"T575") forState:UIControlStateNormal];
        [titleLabel setText:Localized(@"T576")];
        [diplomaTextLabel setText:[NSString stringWithFormat:Localized(@"T580"),levelData.levelName]];
        [titleViewLabel setText:Localized(@"T571")];
        [diplomaImageView setImage:[UIImage imageNamed:@"ic_diploma_lc"]];
        
        [self layoutDiploma];
    }
}


#pragma mark - layoutGUI


- (void)layoutFail
{
    [nextLevelButton.layer setCornerRadius:5];
    [facebookButton.layer setCornerRadius:5];
    [practiceModeButton.layer setCornerRadius:5];
    
    practiceModeButton.layer.borderWidth = 1;
    practiceModeButton.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"];
    [practiceModeButton.layer setBorderColor: [APPSTYLE colorForType:@"Italy_Main_Green_Color"].CGColor];
    [practiceModeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [titleLabel setY:0];
    [practiceModeButton setHidden:NO];
    [nextLevelButton setHidden:NO];
    [diplomaTextLabel sizeToFit];
    [diplomaImageView placeCenterAndBelowView:titleLabel withPadding:10];
    [diplomaTextLabel placeCenterAndBelowView:diplomaImageView withPadding:10];
    [facebookButton placeBelowView:diplomaTextLabel withPadding:10];
    [createDiplomaButton setHidden:YES];
    [nextLevelButton placeBelowView:facebookButton withPadding:10];
    [practiceModeButton placeBelowView:facebookButton withPadding:10];
    [nextLevelButton setTitleColor:createDiplomaButton.backgroundColor forState:UIControlStateNormal];
    [facebookButton setHidden:YES];
    nextLevelButton.layer.borderWidth = 1.0f;
    nextLevelButton.layer.borderColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"].CGColor;
    
    
    [containerScrollView setContentSize:CGSizeMake(self.view.mWidth, practiceModeButton.mHeight + practiceModeButton.y + 20)];
    [containerScrollView setScrollEnabled:YES];
    
    if(containerScrollView.contentSize.height <= containerScrollView.mHeight) {
        
        [practiceModeButton setY:containerScrollView.mHeight - practiceModeButton.mHeight - 10];
        [nextLevelButton setY:containerScrollView.mHeight - practiceModeButton.mHeight - 10];
        [facebookButton setY:(nextLevelButton.y - facebookButton.mHeight - 10)];
        [containerScrollView setScrollEnabled:NO];
    }
    
    if([PROGRESS_MANAGER currentLevelIndex] == (PROGRESS_MANAGER.userStatistic.levelsArray.count - 1)) {
    
        [practiceModeButton setHidden:YES];
    }
}

- (void)layoutDiploma
{
    [facebookButton setHidden:NO];
    [createDiplomaButton.layer setCornerRadius:5];
    [facebookButton.layer setCornerRadius:5];
    [titleLabel setY:0];
    [diplomaImageView placeCenterAndBelowView:titleLabel withPadding:10];
    [diplomaTextLabel placeCenterAndBelowView:diplomaImageView withPadding:5];
    [diplomaTextLabel sizeToFit];
    [facebookButton placeBelowView:diplomaTextLabel withPadding:10];
    [createDiplomaButton placeBelowView:facebookButton withPadding:10];
    [practiceModeButton setHidden:YES];
    [nextLevelButton setHidden:YES];

    [createDiplomaButton setHidden:NO];
    [containerScrollView setContentSize:CGSizeMake(self.view.mWidth, createDiplomaButton.mHeight + createDiplomaButton.y + 20)];
    
    [containerScrollView setScrollEnabled:YES];
    if(containerScrollView.contentSize.height <= containerScrollView.mHeight) {
        
        [createDiplomaButton setY:containerScrollView.mHeight - createDiplomaButton.mHeight - 10];
        [facebookButton setY:(createDiplomaButton.y - facebookButton.mHeight - 10)];
        [containerScrollView setScrollEnabled:NO];
    }
}

#pragma mark - button actions

- (IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)shareAction:(id)sender
{
    LevelData *levelData = DATA_SERVICE.dataModel.courseData.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
    
    SLComposeViewController *shareViewController = [SLComposeViewController new];
    shareViewController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [shareViewController setInitialText:levelData.fbLink];
    [self presentViewController:shareViewController animated:YES completion:nil];
}


- (IBAction)practiceAction:(id)sender
{
    
    NSInteger unitIndex    = [PROGRESS_MANAGER getFirstLessonWithoutStar:[PROGRESS_MANAGER currentLevelIndex]];
    NSInteger sectionIndex = [PROGRESS_MANAGER getFirstSectionWithoutStar:[PROGRESS_MANAGER currentLevelIndex]];
    
    [self.delegate practiceMoreInLevel:[PROGRESS_MANAGER currentLevelIndex] section:((sectionIndex == -1) ? [PROGRESS_MANAGER currentSectionIndex] : sectionIndex) unit:(unitIndex == -1) ? [PROGRESS_MANAGER currentUnitIndex] : unitIndex];
    [self dismissModalViewControllerWithPushDirection:nil];
}


- (IBAction)nextLevelAction:(id)sender
{
    [self dismissModalViewControllerWithPushDirection:nil];
    [APP_DELEGATE.lessonController jumpToNewLesson:self];
}


- (IBAction)createDiplomaAction:(id)sender
{
    LevelData *levelData = DATA_SERVICE.dataModel.courseData.levelsArray[PROGRESS_MANAGER.currentLevelIndex];
    UserDetailsViewController *userDetailsViewController = [[UserDetailsViewController alloc] initWithNibName:@"UserDetailsViewController" bundle:nil];
    [userDetailsViewController setLevelId:levelData.levelID];
    self.isVisited = YES;
    userDetailsViewController.userDetailsDelegate = self;
    [self presentViewController:userDetailsViewController animated:YES completion:nil];
}

- (void)closeLevelCompletition
{
    self.isVisited = NO;
}

#pragma mark -
@end
