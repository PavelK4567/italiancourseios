//
//  ECLessonViewController.h
//  KMGame
//
//  Created by Dimitar Shopovski on 11/14/14.
//  Copyright (c) 2014 Dimitar Shopovski. All rights reserved.
//  Updated by Darko

#import <UIKit/UIKit.h>
#import "LessonViewController.h"
#import "Constants.h"
#import "SyncService.h"

@interface LessonSelectionViewController : LMBaseViewController <UITableViewDataSource, UITableViewDelegate, SSZipArchiveDelegate, SyncServiceDelegate>
{
    UIView *moduleView;
//    UIView *customColorView;
}

- (void)reloadLessonSelectionViewController;

@property (weak, nonatomic) IBOutlet UITableView *tbLevels;
@property (nonatomic, strong) NSMutableArray *arrayDataSource;
@property (nonatomic, assign) NSInteger lsvcLevelIndex;

@property (nonatomic, assign) NSInteger maxUnitIndex;
@property (nonatomic, assign) NSInteger maxModuleIndex;

- (void)changeSelectedLesson;
- (void)backFromFinishedLevelGoOnTop;

@end
