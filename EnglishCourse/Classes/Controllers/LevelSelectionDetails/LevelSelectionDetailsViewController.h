//
//  LevelSelectionDetailsViewController.h
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMBaseViewController.h"

@interface LevelSelectionDetailsViewController : LMBaseViewController{
  
  __weak IBOutlet UIImageView *levelImageView;
  __weak IBOutlet UILabel *titleLabel;
  __weak IBOutlet UILabel *screenDescriptionLabel;
  __weak IBOutlet UIButton *startButton;
}

@property (nonatomic, strong) LevelData *levelData;
@property NSInteger levelIndex;

@property (nonatomic, assign) BOOL isFromPlacementTest;

@end
