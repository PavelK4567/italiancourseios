//
//  LevelSelectionViewController.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LevelSelectionViewController.h"
#import "LevelSelectionDetailsViewController.h"
#import "UITestViewController.h"
#import "UIView+K1000Shadow.h"

@interface LevelSelectionViewController ()

@end

@implementation LevelSelectionViewController

- (void)configureUI
{
  [super configureUI];
  DLog(@"configureUI");
  
  //set up scroll
  //MainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, [self.view width], [self.view height])];
  //MainScrollView.backgroundColor = [UIColor clearColor];
  //[self.view addSubview:MainScrollView];
  MainScrollView.contentSize = CGSizeMake(self.view.frame.size.width,MainScrollView.frame.size.height);
  //MainScrollView.backgroundColor = [UIColor clearColor];
  MainScrollView.pagingEnabled = YES;
  MainScrollView.showsVerticalScrollIndicator = YES;
  MainScrollView.alwaysBounceVertical = YES;
  MainScrollView.delaysContentTouches = YES;
  MainScrollView.canCancelContentTouches = YES;
  //MainScrollView.delegate = self;
  [MainScrollView setScrollEnabled:NO];
  [self makeLevelsButton];
  
  [APPSTYLE applyStyle:@"Label_Title" toLabel:titleLabel];
  [APPSTYLE applyStyle:@"Label_Body" toLabel:screenDescriptionLabel];
  //[APPSTYLE applyStyle:@"Button_Level_Selection" toButton:placementTestButton];
}
-(void)makeLevelsButton{
  
    self.arrayCourseLevels = (NSArray *)[[DataService sharedInstance] findAllLevelsForCourse];

    int startYCordinate = 20;
    LevelData *level;
    
    int startNumber = 0;
    
    for (int i=0; i<self.arrayCourseLevels.count; i++) {
        
        level = [self.arrayCourseLevels objectAtIndex:i];
        
        LevelSelectionView *tempView = [LevelSelectionView  loadFromNib];
        [tempView addK1000Shadow];
        tempView.delegate = self;
        tempView.tag = i;
        [tempView setFrame:CGRectMake(placementTestButton.frame.origin.x, startYCordinate, [tempView mWidth], [tempView mHeight])];
        //tempView.backgroundColor = [APPSTYLE colorForType:@"Views_Level_Selection"];
        tempView.backgroundColor = [UIColor whiteColor];
        startYCordinate += [tempView mHeight]+10;
        [MainScrollView addSubview:tempView];
        [tempView setViewData:level :startNumber+1];
        if([PROGRESS_MANAGER  areAllLessonsVisitedInLevel:i]){
          [tempView.chekImageView setHidden:NO];
        }else{
          [tempView.chekImageView setHidden:YES];
        }
        for (Section *section in [(LevelData *)level sectionsArray]) {
            
            startNumber+=section.unitsArray.count;
        }
        
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(pressOnLevelButton:)];
        [tempView addGestureRecognizer:singleFingerTap];
  }
  
}

- (void)configureAppearance {
  
  [super configureAppearance];
  titleLabel.text = Localized(@"T420");
  screenDescriptionLabel.text = Localized(@"T421");
  [APPSTYLE applyTitle:Localized(@"T422") toButton:placementTestButton];
  [APPSTYLE applyStyle:@"Button_Level_Selection" toButton:placementTestButton];
  placementTestButton.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Green_Color"];
  placementTestButton.layer.cornerRadius = 2.0;
  self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}
- (void)configureNavigation {
  [super configureNavigation];
  self.navigationItem.title = Localized(@"T423");
}
- (void)loadData
{
  
}
#pragma mark select Level
- (void)pressOnLevelButton:(UITapGestureRecognizer *)recognizer {
    
    DLog(@"pressOnLevelButton - %ld", (long)recognizer.view.tag);

    [PROGRESS_MANAGER saveCurrentLevelIndex:recognizer.view.tag forUser:[USER_MANAGER userMsisdn]];
    [PROGRESS_MANAGER setCurrentLevelIndex:recognizer.view.tag];

    [APP_DELEGATE buildMainStackWithLevelIndex:recognizer.view.tag];
    
}

#pragma mark LevelSelectionViewDelegate functions

-(void)moreInfoButtonClick:(id)sender{
    
    DLog(@"LevelSelectionViewDelegate");
  
    LevelSelectionView *levelView = (LevelSelectionView *)sender;
    
    LevelSelectionDetailsViewController *viewController = [LevelSelectionDetailsViewController new];
    viewController.levelData = levelView.levelInfo;
    viewController.levelIndex = levelView.tag;
    self.navigationItem.title = @" ";
    [self.navigationController pushViewController:viewController animated:YES];
    
}
#pragma mark - Interface Builder Actions

- (IBAction)placementTestButtonPress:(id)sender {
  UITestViewController *viewController = [UITestViewController new];
    [self setTitle:Localized(@"T470")];
  [self.navigationController pushViewController:viewController animated:YES];
}
#pragma mark old code from dimitar
- (IBAction)openLevel:(id)sender {
  
    UIButton *btnSender = (UIButton *)sender;
    
    
    LessonSelectionViewController *lessonController = [[LessonSelectionViewController alloc] initWithNibName:@"LessonSelectionViewController" bundle:nil];
    
    switch (btnSender.tag) {
        case 1:
            
            break;
        case 2:
            
            break;
        case 3:
            break;
        default:
            break;
    }
    [self presentViewController:lessonController animated:YES completion:nil];
}


@end
