
//
//  MainViewController.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/1/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "MainViewController.h"
#import "ProfileView.h"
#import "CertificateView.h"
#import "LessonSelectionViewController.h"
#import "LevelSelectionViewController.h"
#import "ProductTourViewController.h"
#import "TourViewController.h"
#import "SettingsViewController.h"
#import "AchievementsViewController.h"

#import "SyncService.h"
#import "LMAppDelegate.h"

@interface MainViewController (){
  ProfileView* tempProfileView;
    bool isShowingTutorial;
}

@property(weak, nonatomic) IBOutlet UISegmentedControl *MainSegmentedControl;
@property(weak, nonatomic) IBOutlet UIScrollView *MainScrollView;
@property(strong, nonatomic) LessonSelectionViewController *LessonSelectionController;
@property(strong, nonatomic) AchievementsViewController *AchievementsViewController;

@end

@implementation MainViewController
    
@synthesize tutorial = _tutorial;
@synthesize customCam;
    
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    DLog(@"SYNCING\n");
    
    [[Utilities sharedInstance] appendLogFileWithString:@"> SYNCING\n"];
    
//    if (![[Utilities sharedInstance] checkIfASITmpFolderCreated]) {
//        [[Utilities sharedInstance] createASITmpFolder];
//    }
    
    if (![USER_MANAGER isGuest]) {
        [[Utilities sharedInstance] appendLogFileWithString:@"> REGISTERED USER\n"];
        if (APP_DELEGATE.isCourseDataSynced) {
            [[Utilities sharedInstance] appendLogFileWithString:@"> COURCE IS DOWNLOADED\n"];
            [[SyncService sharedInstance] syncContentForLevel: PROGRESS_MANAGER.currentLevelIndex];
        } else {
            [[Utilities sharedInstance] appendLogFileWithString:@"> REPLACING AND INITAL DOWNLOAD\n"];
            [SyncService sharedInstance].delegate = self;
            [[SyncService sharedInstance] replaceAllTemporaryObjects];
        }
    } else{
        [USER_MANAGER loadUserValuesWithCompletitionBlock:^(NSError *error) {
            
        }];
    }
    
    [self.LessonSelectionController changeSelectedLesson];
    //[tempProfileView setData];
    self.MainSegmentedControl.selectedSegmentIndex = 0;
    [self.MainScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    [self changeSegmentedControlColor];
    
    customCam = nil;
}

- (void)configureUI
{
  
  [super configureUI];
  [USER_MANAGER setLastLoginTime];
  DLog(@"configureUI - MainViewController");
  
    [self initStatsForGuest];
    
  [self.MainSegmentedControl addTarget:self action:@selector(segmentedControlValueDidChange:) forControlEvents:UIControlEventValueChanged];
  self.MainSegmentedControl.layer.cornerRadius = 4;//ready for new design
  //[[UISegmentedControl appearance] setTintColor:[APPSTYLE colorForType:@"Native_Pink_Color"]];
  [[UISegmentedControl appearance] setBackgroundColor:[APPSTYLE colorForType:@"Italy_Main_Grey_Color_3"]];//ready for new design
  [[UISegmentedControl appearance] setTitleTextAttributes:
                                    @{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
  [[UISegmentedControl appearance] setTitleTextAttributes:
                                    @{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateNormal];
  
 
  
  self.MainSegmentedControl.selectedSegmentIndex = 0;
  
  [self changeSegmentedControlColor];
  //set up scroll
  self.MainScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3,self.MainScrollView.frame.size.height);
  self.MainScrollView.backgroundColor = [UIColor clearColor];
  self.MainScrollView.pagingEnabled = YES;
  self.MainScrollView.showsVerticalScrollIndicator = NO;
  self.MainScrollView.alwaysBounceVertical = NO;
  self.MainScrollView.delaysContentTouches = YES;
  self.MainScrollView.canCancelContentTouches = YES;
  self.MainScrollView.delegate = self;
  [self.MainScrollView setScrollEnabled:YES];
  
    if (self.LessonSelectionController == nil) {
        
        self.LessonSelectionController = [LessonSelectionViewController new];
        self.LessonSelectionController.lsvcLevelIndex = self.levelIndexForFirstController;
        self.LessonSelectionController.view.frame = CGRectMake(self.MainScrollView.frame.size.width*0, 0, self.MainScrollView.frame.size.width, self.MainScrollView.frame.size.height);
        [self.MainScrollView addSubview:self.LessonSelectionController.view];
    }


    
    //temp code
  UIScrollView* secondScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.MainScrollView.frame.size.width * 1, 0, self.view.frame.size.width, self.MainScrollView.frame.size.height)];
  secondScrollView.backgroundColor = [UIColor clearColor];
  
  tempProfileView = [ProfileView loadFromNib];
  [tempProfileView setUI];
  [secondScrollView addSubview:tempProfileView];
  [self.MainScrollView addSubview:secondScrollView];
  secondScrollView.contentSize = tempProfileView.frame.size;
  
  
  
  if (self.AchievementsViewController == nil) {
    
    self.AchievementsViewController = [AchievementsViewController new];
    //self.LessonSelectionController.levelIndex = self.levelIndexForFirstController;
    self.AchievementsViewController.view.frame = CGRectMake(self.MainScrollView.frame.size.width*2, 0, self.MainScrollView.frame.size.width, self.MainScrollView.frame.size.height);
    [self.MainScrollView addSubview:self.AchievementsViewController.view];
  }
  
  /*CertificateView* tempCertificateView = [CertificateView loadFromNib];
  tempCertificateView.frame = CGRectMake(self.MainScrollView.frame.size.width * 2, 0, tempCertificateView.frame.size.width, tempCertificateView.frame.size.height);
  [self.MainScrollView addSubview:tempCertificateView];*/
  
}
    
/**
 * Create & present the tutorial screen.
 * This only happens if this is the first time the app is started
 * on the current device.
 */
-(void) buildTutorial {
    if ([[Utilities sharedInstance] isFirstRun]) {
        [self installImages];
        isShowingTutorial = YES;
        self.tutorial = [[TutorialViewController alloc] init];
        [self presentViewController:self.tutorial animated:YES completion:nil];
    } else {
        isShowingTutorial = NO;
        [self presentCamera];
    }
}
    
- (void)installImages {
    UIImage *img = [UIImage imageNamed:@"img_apple"];
    [self saveImageOnAppDir:@"_mela" andImage:img];
    
    img = [UIImage imageNamed:@"img_bee"];
    [self saveImageOnAppDir:@"_ape" andImage:img];
    
    img = [UIImage imageNamed:@"img_beer"];
    [self saveImageOnAppDir:@"_birra" andImage:img];
    
    img = [UIImage imageNamed:@"img_kite"];
    [self saveImageOnAppDir:@"_aquilone" andImage:img];
    
    img = [UIImage imageNamed:@"img_sunflower"];
    [self saveImageOnAppDir:@"_girasole" andImage:img];
}
    
-(void) saveImageOnAppDir: (NSString *) name andImage: (UIImage *) img {
    NSError *error;
    NSData *imageData = UIImagePNGRepresentation(img);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.jpg", name]];
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/MyFolder"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"MyFolder/%@.jpg", name]];
    
    NSLog(@"pre writing to file");
    if (![imageData writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
}
    
- (NSString *)recordingPathForIndex:(int)path order:(NSInteger)order {
    return [[self recordingPath] stringByAppendingPathComponent:
            [NSString stringWithFormat:@"record%ld_%d.lpcm", (long)order,path]];
}
- (NSString *)recordingPath {
    NSString *recordingPathUrlString = [[NSString documentsDir] stringByAppendingPathComponent:@"GuestRecordings"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:recordingPathUrlString]) {
        NSError *error;
        [[NSFileManager defaultManager] createDirectoryAtPath:recordingPathUrlString withIntermediateDirectories:NO attributes:nil error:&error];
    }
    return recordingPathUrlString;
}
    
- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kDataExtracted];
    [defaults setBool:NO forKey:kDataCompleteDownload];
    [defaults synchronize];
    
    [[SyncService sharedInstance] buildInitialDataModel:self];
}
    
- (void)returnBuildStatusWithError:(BOOL)hasError errorMessage:(NSString *)errorMessage {
    //[APP_DELEGATE buildMainStackWithLevelIndex:0];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:kInitialModelGenerated]) {
        [defaults setBool:YES forKey:kInitialModelGenerated];
        [defaults synchronize];
    }
    
}
    
-(void) initStatsForGuest {
    if ([USER_MANAGER isGuest]) {
        [[Utilities sharedInstance] extractInitialData:self];
        UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
        if(!userStatistic) {
            ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
            [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
        }
        
        [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
        
        
        NSLog(@"Was logged as VIVO user - %@", [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"] ? @"Yes" : @"No");
        
        if ([USER_MANAGER isNative]) {
            
            [PROGRESS_MANAGER updateProgressModel];
            
        }
        else if ([USER_MANAGER isGuest] && ![[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"]) {
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userWasLogged"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [PROGRESS_MANAGER updateProgressModel];
            
        }
        
        [AUDIO_MANAGER record:[self recordingPathForIndex:100 order:200]];
        [AUDIO_MANAGER stopRecording];
    }
}

- (void)configureAppearance {
    
  [super configureAppearance];
  [self.MainSegmentedControl setTitle:Localized(@"T356") forSegmentAtIndex:0];
  [self.MainSegmentedControl setTitle:Localized(@"T357") forSegmentAtIndex:1];
  [self.MainSegmentedControl setTitle:Localized(@"T358") forSegmentAtIndex:2];
  
  self.view.backgroundColor = [UIColor whiteColor];
  DLog(@"configureAppearance %@", Localized(@"T001"));
  UIImage *menuImage = [UIImage imageNamed:@"ic_menu"];
  UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
  [menuButton setBackgroundImage:menuImage forState:UIControlStateNormal];
  [menuButton setFrame:CGRectMake(-10, 0, 44, 44)];
  [menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem *menuBarButton = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
  self.navigationItem.leftBarButtonItem =menuBarButton;
  
    //NSString stringWithFormat:Localized(@"T350"),
  self.navigationItem.title = [NSString stringWithFormat:@"%@", [PROGRESS_MANAGER takeCurentLevelName]];
  
    self.lblPoints = [[UILabel alloc] initWithFrame:CGRectMake(0, 4, 100, 48)];
    self.lblPoints.textAlignment = NSTextAlignmentCenter;
    [self.lblPoints setTextColor:[UIColor whiteColor]];
    [self.lblPoints setFont:[UIFont systemFontOfSize:12.0]];
//    self.lblPoints.text = @"2000\npuntos";

    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn]];
    LevelStatistic *currentLevelStatistics = (LevelStatistic *)[userStatistic.levelsArray objectAtIndex:[PROGRESS_MANAGER currentLevelIndex]];
    self.lblPoints.text = [NSString stringWithFormat:@"%ld\nPONTOS", currentLevelStatistics.score];

    self.lblPoints.numberOfLines = 0;
    [self.lblPoints sizeToFit];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.lblPoints];
    
    UIImage *camMenuImage = [UIImage imageNamed:@"ic_cam_nav"];
    UIButton *camMenuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [camMenuButton setBackgroundImage:camMenuImage forState:UIControlStateNormal];
    [camMenuButton setFrame:CGRectMake(-10, 0, 24, 24)];
    [camMenuButton addTarget:self action:@selector(openCamera) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *camMenuBarButton = [[UIBarButtonItem alloc] initWithCustomView:camMenuButton];
    self.navigationItem.rightBarButtonItems = @[rightItem, camMenuBarButton];
    
    //self.navigationItem.rightBarButtonItem = rightItem;
    
    
  if([[NSUserDefaults standardUserDefaults] boolForKey:@"Demonstration"]){
    //[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"Demonstration"];
    [self showDemo];
  }
}
    
-(void) openCamera {
    if ([USER_MANAGER isGuest]) {
        //[UIAlertView alertWithCause:kAlertViewNoGusets];
        [[[UIAlertView alloc] initWithTitle:@"" message:Localized(@"T662") delegate:self cancelButtonTitle:@"Login" otherButtonTitles:Localized(@"T510"), nil] show];
    } else {
        [self buildTutorial];
    }
    //    customCam = [[CameraViewController alloc] init];
    //    [self presentViewController:customCam animated:YES completion:nil];
}
    
-(void) presentCamera {
    isShowingTutorial = NO;
    NSUserDefaults *df = [NSUserDefaults standardUserDefaults];
    int count = (int)[df integerForKey:@"imagesTaken"];
    NSDate *date = (NSDate *)[df objectForKey:@"dateTaken"];
    
    if (date == nil) {
        customCam = [[CameraViewController alloc] init];
        [customCam setLng:@"it"];
        [customCam setPhoneNum:[USER_MANAGER userMsisdn]];
        [self presentViewController:customCam animated:YES completion:nil];
        return;
    }
    
    NSTimeInterval secondsBetween = [[NSDate date] timeIntervalSinceDate:date];
    
    int numberOfDays = secondsBetween / 86400;
    if (numberOfDays < 30) {
        if (count < 100) {
            customCam = [[CameraViewController alloc] init];
            [customCam setLng:@"it"];
            [customCam setPhoneNum:[USER_MANAGER userMsisdn]];
            [self presentViewController:customCam animated:YES completion:nil];
        } else {
            [UIAlertView alertWithCause:kAlertViewCameraDisabled];
        }
    }
}
    
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        //[APP_DELEGATE buildLoginStack];
        [APP_DELEGATE presentLoginStack];
    }
}

- (void)loadData
{
  [super loadData];
  [tempProfileView setData];
  [self.AchievementsViewController setData];
  

}


#pragma mark - Reload content in first segment

- (void)checkForChanges {
    
    NSInteger currentLevelIndex = self.LessonSelectionController.lsvcLevelIndex;

    self.LessonSelectionController.lsvcLevelIndex = self.levelIndexForFirstController;
        
    if (currentLevelIndex != self.levelIndexForFirstController) {
    
        [self.LessonSelectionController performSelectorOnMainThread:@selector(reloadLessonSelectionViewController) withObject:nil waitUntilDone:YES];
        
    }
}

- (void)goOnTop {
    
    [self.LessonSelectionController backFromFinishedLevelGoOnTop];
}


#pragma mark Segmented control functions
-(void)changeSegmentedControlColor{
  
  //return;
  //ready for new design
  for (int i=0; i<[self.MainSegmentedControl.subviews count]; i++)
  {
    [[self.MainSegmentedControl.subviews objectAtIndex:i] setTintColor:nil];
    if (![[self.MainSegmentedControl.subviews objectAtIndex:i]isSelected])
    {
      UIColor *tintcolor=[APPSTYLE colorForType:@"Italy_Main_Grey_Color_3"];
      [[self.MainSegmentedControl.subviews objectAtIndex:i] setTintColor:tintcolor];
    }
    else
    {
      UIColor *tintcolor=[APPSTYLE colorForType:@"Italy_Main_Grey_Color"];
      [[self.MainSegmentedControl.subviews objectAtIndex:i] setTintColor:tintcolor];
    }
  }
}
- (void)segmentedControlValueDidChange:(UISegmentedControl *)segment
{
  [self changeSegmentedControlColor];
  //[AUDIO_MANAGER playSound:kSoundResults];
  [self.MainScrollView setContentOffset:CGPointMake(self.view.frame.size.width*segment.selectedSegmentIndex, 0) animated:YES];
  switch (segment.selectedSegmentIndex) {
    case 0:
    {
      // action for the 1 button (Current)
      break;
    }
    case 1:
    {
      // action for the 2 button (Current)
      [tempProfileView setData];
      break;
    }
    case 2:
    {
      // action for the 3 button (Current)
      break;
    }
  }
}
#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
  DLog(@"scrollView.contentOffset.x  %f",scrollView.contentOffset.x );
  if(scrollView.contentOffset.x <= 0){
    self.MainSegmentedControl.selectedSegmentIndex = 0;
  }
  else if(scrollView.contentOffset.x == self.MainScrollView.frame.size.width*1){
    self.MainSegmentedControl.selectedSegmentIndex = 1;
    [tempProfileView setData];
  }
  else if(scrollView.contentOffset.x >= self.MainScrollView.frame.size.width*2){
    self.MainSegmentedControl.selectedSegmentIndex = 2;
  }
  [self changeSegmentedControlColor];
}
#pragma mark Menu
-(void)showMenu{
  [self makeMenu];
}
- (void)makeMenu
{

  if (menuIsOpen) {
    [upMenu closeWithCompletion:^{
        [self afterCloseMenu];
    }];
    return;
  }
  if (!upMenu) {
    [self createMenu];
  }else{
    if([USER_MANAGER isGuest] || [USER_MANAGER isNative]){
      if ([upMenu.items count] >3) {
        upMenu = nil;
        [self createMenu];
      }
    }else{
      if ([upMenu.items count] <4) {
        upMenu = nil;
        [self createMenu];
      }
    }
    
  }
  CGRect frame = CGRectMake(0, 0, 150, 220);
  
  if (!upMenuTempView) {
    
    upMenuTempView = [[UIView alloc] initWithFrame:frame];
    upMenuTempView.backgroundColor = [UIColor clearColor];
    //you must change frame after anim bounse end - in not call this function tab for tapView not work on this part
    [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(menuStopBounse) userInfo:nil repeats:NO];
    [self.view addSubview:upMenuTempView];
  }
  
  [upMenu showInView:upMenuTempView];
  menuIsOpen = YES;
  
  tapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
  tapView.backgroundColor = [UIColor clearColor];
  [self.view insertSubview:tapView belowSubview:upMenuTempView];
  tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(focusAtTap:)];
  [tapView addGestureRecognizer:tap];

}
-(void)menuStopBounse{
  [upMenuTempView setFrame:CGRectMake(0, 0, 150, 202)];
}
-(void)createMenu{
  DLog(@"createMenu");
  REMenuItem *demoItem = [[REMenuItem alloc] initWithTitle:Localized(@"T351")
                                                  subtitle:@""
                                                     image:[UIImage imageNamed:@"ic_demo"]
                                          highlightedImage:[UIImage imageNamed:@"ic_demo"]
                                                    action:^(REMenuItem *item)
                          {
                            DLog(@"Item: %@", item);
                            [self afterCloseMenu];
                            [self showDemo];
                          }];
  
  REMenuItem *levelItem = [[REMenuItem alloc] initWithTitle:Localized(@"T352")
                                                   subtitle:@""
                                                      image:[UIImage imageNamed:@"ic_level"]
                                           highlightedImage:[UIImage imageNamed:@"ic_level"]
                                                     action:^(REMenuItem *item)
                           {
                             DLog(@"DLog: %@", item);
                             [self afterCloseMenu];
                             [self showLevelSelection];
                           }];
  
  REMenuItem *settingsItem = [[REMenuItem alloc] initWithTitle:Localized(@"T353")
                                                      subtitle:nil
                                                         image:[UIImage imageNamed:@"ic_settings"]
                                              highlightedImage:[UIImage imageNamed:@"ic_settings"]
                                                        action:^(REMenuItem *item)
                              {
                                DLog(@"Item: %@", item);
                                [self afterCloseMenu];
                                [self showSetting];
                              }];
  
  NSString *logOutString = Localized(@"T354");
  if([USER_MANAGER isGuest]){
    logOutString = Localized(@"T355");
  }
  REMenuItem *logoutItem = [[REMenuItem alloc] initWithTitle:logOutString
                                                       image:[UIImage imageNamed:@"ic_logout"]
                                            highlightedImage:[UIImage imageNamed:@"ic_logout"]
                                                      action:^(REMenuItem *item)
                            {
                              DLog(@"Item: %@", item);
                              if ([USER_MANAGER isGuest]) {
                                [APP_DELEGATE buildLoginStack];
                                if(DOWNLOAD_MANAGER.isManagerExecutingRequests) {
                                  [DOWNLOAD_MANAGER cancelRequestsExecution];
                                  [PROGRESS_MANAGER deleteStatistic];
                                }
                              }else{
                                [self showLogOutAlert];
                              }
                              
                              
                              
                              [self afterCloseMenu];
                              
                              
                            }];
  
  
 
    if([USER_MANAGER isGuest] || [USER_MANAGER isNative] ){
        upMenu = [[REMenu alloc] initWithItems:@[ demoItem, levelItem, settingsItem]];
    }else{
      upMenu = [[REMenu alloc] initWithItems:@[ demoItem, levelItem, settingsItem,logoutItem]];
    }
  
    upMenu.backgroundColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];//old color Purple_Main_Color
    upMenu.shadowColor = [UIColor whiteColor];
    upMenu.textColor = [UIColor whiteColor];
    upMenu.separatorColor = [UIColor whiteColor];
    upMenu.textOffset = CGSizeMake(40, 0);
    upMenu.textShadowColor = [UIColor clearColor];
    upMenu.textAlignment = NSTextAlignmentLeft;
    upMenu.font = [UIFont systemFontOfSize:14.0];
  //}
}

-(void)showLogOutAlert{
  UIAlertView *tempAlert = [UIAlertView alertViewWithTitle:@""
                                                   message:Localized(@"T370")
                                         cancelButtonTitle:Localized(@"T371")
                                         otherButtonTitles:@[Localized(@"T372")]
                                                 onDismiss: ^(int buttonIndex) {
                                                   if (buttonIndex == -1){
                                                     
                                                   }else if (buttonIndex == 0){
                                                     [USER_MANAGER logOutUser];
                                                     [APP_DELEGATE buildLoginStack];
                                                     if(DOWNLOAD_MANAGER.isManagerExecutingRequests) {
                                                       [DOWNLOAD_MANAGER cancelRequestsExecution];
                                                       [PROGRESS_MANAGER deleteStatistic];
                                                     }
                                                   }
                                                 }
                                                  onCancel: ^{
                                                    
                                                  }];
  [tempAlert show];
}
-(void)afterCloseMenu{
  
  DLog(@"Close menu");
  if (upMenuTempView) {
    [self.view removeGestureRecognizer:tap];
    menuIsOpen = NO;
    
    [upMenuTempView removeFromSuperview];
    upMenuTempView = nil;
    
    [tapView  removeFromSuperview];
    tapView = nil;
  }
}
#pragma mark - Focusing

- (void)focusAtTap:(UIGestureRecognizer *)gestureRecognizer
{
  if(upMenu)
  {
    if(upMenu.isOpen){
      [upMenu closeWithCompletion:^{
        
          [self afterCloseMenu];
        
      }];
    }else{
      [self afterCloseMenu];
    }
  }else{
    [self afterCloseMenu];
  }
  //[self.view removeGestureRecognizer:tap];
}

-(void)showDemo{
  TourViewController *controler = [TourViewController new];
  UINavigationController *navCon = [[UINavigationController alloc] initWithRootViewController:controler];
  
  if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
    navCon.providesPresentationContextTransitionStyle = YES;
    navCon.definesPresentationContext = YES;
    navCon.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:navCon animated:YES completion:nil];
    menuIsOpen = NO;
  }else{
    [self presentViewController:navCon animated:YES completion:^{
      [navCon dismissViewControllerAnimated:NO completion:^{
        [APP_DELEGATE window].rootViewController.modalPresentationStyle = UIModalPresentationCurrentContext;
        [self presentViewController:navCon animated:NO completion:nil];
        [APP_DELEGATE window].rootViewController.modalPresentationStyle = UIModalPresentationFullScreen;
      }];
    }];
    
  }
}

- (void)showSetting {
    
    SettingsViewController *controler = [SettingsViewController new];
    self.navigationItem.title = @" ";
    [self.navigationController pushViewController:controler animated:YES];

}

- (void)showLevelSelection {
    
    LevelSelectionViewController *controler = [LevelSelectionViewController new];
    self.navigationItem.title = @" ";
    [self.navigationController pushViewController:controler animated:YES];

}

#pragma mark -
#pragma mark - SyncServiceDelegate implementation

- (void)returnSyncStatusWithError:(BOOL)hasError cause:(EAlertViewCause)cause
{
    if (hasError) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIAlertView alertWithCause:cause];
        });
        
    }
}

#pragma mark -
#pragma mark - SyncServiceDelegate implementation

- (void)refreshUnitsData
{
    self.LessonSelectionController.arrayDataSource = nil;
    self.LessonSelectionController.arrayDataSource = (NSMutableArray *)[[DataService sharedInstance] findAllModulesFromLevel:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
}

@end
