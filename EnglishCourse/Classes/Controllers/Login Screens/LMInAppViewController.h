//
//  LMInAppViewController.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 6/8/16.
//  Copyright © 2016 LaMark. All rights reserved.
//

#import "LMBaseViewController.h"
#import "PurchaseManager.h"

#import "Constants.h"
#import "Utilities.h"
#import "SyncService.h"
#import "SSZipArchive.h"

@interface LMInAppViewController : LMBaseViewController<SSZipArchiveDelegate, SyncServiceDelegate>

@property (nonatomic, assign) BOOL isInitialDataGenerated;

@end
