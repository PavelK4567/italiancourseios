//
//  LMInAppViewController.m
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 6/8/16.
//  Copyright © 2016 LaMark. All rights reserved.
//

#import "LMInAppViewController.h"

@interface LMInAppViewController () {
    
    IBOutlet UIButton *subscriptionPopUpsubscribeButton;
    IBOutlet UILabel *subscriptionPopUpTitle;
    IBOutlet UILabel *subscriptionPopUpDescription;
    
    BOOL isReExtractingInProcess;

}

- (IBAction)inAppPurchaseAction:(id)sender;

@end

@implementation LMInAppViewController

- (id)initWithNibName:(NSString *)nib bundle:(NSBundle *)budnle
{
    self = [super initWithNibName:nib bundle:budnle];
    if (self) {
        //self.title = Localized(@"Login");
    }
    return self;
}

- (void)configureAppearance
{
    [super configureAppearance];
    
    [APPSTYLE applyStyle:@"Button_Login_Page" toButton:subscriptionPopUpsubscribeButton];
    [APPSTYLE applyBorderColor:[APPSTYLE otherViewColorForKey:@"Italy_Main_Green_Color"] toButton:subscriptionPopUpsubscribeButton];

    subscriptionPopUpsubscribeButton.backgroundColor = [APPSTYLE otherViewColorForKey:@"Italy_Main_Green_Color"];
    
    [APPSTYLE applyStyle:@"Login_Label_Body" toLabel:subscriptionPopUpTitle];
    
    self.view.backgroundColor = [APPSTYLE colorForType:@"New_Vehicle_Background"];
}

- (void)configureUI
{
    [super configureUI];
    
    [APPSTYLE applyTitle:Localized(@"T017.1") toButton:subscriptionPopUpsubscribeButton];
    
    subscriptionPopUpTitle.text = Localized(@"T003.1");
    
    
}

- (void)configureObservers
{
    [super configureObservers];
}

- (void)configureNavigation
{
    [super configureNavigation];
    self.navigationItem.title = Localized(@"T069");
    // self.navigationController.navigationBarHidden = YES;
}

- (void)configureData
{
    [super configureData];
}

- (void)inAppPurchaseAction:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (![defaults objectForKey:kDataExtracted]) {
        return;
    }
    [AUDIO_MANAGER stopRecording];
    [self subscribe:nil];
}

#pragma mark - In App subscribe

- (IBAction)subscribe:(id)sender
{
    if (![ReachabilityHelper reachable]) {
        [UIAlertView alertWithCause:kAlertNoInternetConnectionWhenClickingCameraButton];
        return;
    }
    [self addActivityIndicator];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:0];
    
    if (![PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
        [PURCHASE_MANAGER requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            if (success) {
                NSArray *filtered = [products filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"productIdentifier BEGINSWITH[cd] %@", kMonthlyPurchaseId]];
                if (filtered && [filtered count] != 0) {
                    SKProduct *product = filtered[0];
                    [PURCHASE_MANAGER buyProduct:product];
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:@"No userID", @"userID", nil];
                    [Flurry logEvent:kOnAppleSubscriptionTerms withParameters:articleParams];
                } else {
                    [[NSNotificationCenter defaultCenter] removeObserver:self];
                    [self removeActivityIndicator];
                }
            } else {
                [[NSNotificationCenter defaultCenter] removeObserver:self];
                [self removeActivityIndicator];
            }
        }];
    } else {
        [self removeActivityIndicator];
    }
}
#pragma mark - Product Purchased

- (void)productPurchased:(NSString *)productId
{
    DLog(@"productPurchased %@", productId);
    [self removeActivityIndicator];
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
    if ([PURCHASE_MANAGER productPurchased:kMonthlyPurchaseId]) {
        DLog(@"YES ");
        [[NSNotificationCenter defaultCenter] removeObserver:self]; // dali samo za yes da go trgam?
        //[self addActivityIndicator];
        NSString *uid_icloud = [SDCloudUserDefaults stringForKey:@"uid"];
        if (uid_icloud && ![uid_icloud isEqualToString:@""]) {
            [USER_MANAGER initializeWithNativeUser];
            [USER_MANAGER makeBillingForNativeUserWithCompletitionBlock:^(NSError *error) {
                [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
                    [[NSUserDefaults standardUserDefaults] setObject:@YES forKey:kUserManagerInitialized];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self initProgressModel];
                    [self removeActivityIndicator];
                    [APP_DELEGATE buildMainStack];
                }];
            }];
            
            return;
        }
        //[self addActivityIndicator];
        [USER_MANAGER initializeWithNativeUser];
        [USER_MANAGER registerNativeUserValuesWithCompletitionBlock:^(NSError *error) {
            if(error){
                [UIAlertView alertWithCause:kAlertServerUnreachable];
            }
            else {
                [USER_MANAGER setIsUserRegister:YES];
            }
            [USER_MANAGER loadNativeUserValuesWithCompletitionBlock:^(NSError *error) {
                if(error){
                    [UIAlertView alertWithCause:kAlertServerUnreachable];
                }
                
                
                [self removeActivityIndicator];
                [self initProgressModel];
                [APP_DELEGATE buildMainStack];
                
            }];
        }];
    } else {
        DLog(@"NO ");
    }
}

#pragma mark -
#pragma mark - SSZipArchive delegate

- (void)zipArchiveDidUnzipArchiveAtPath:(NSString *)path zipInfo:(unz_global_info)zipInfo unzippedPath:(NSString *)unzippedPath {
    
    if (isReExtractingInProcess) {
        [self removeActivityIndicator];
        
        isReExtractingInProcess = NO;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:YES forKey:kDataExtracted];
    [defaults setBool:NO forKey:kDataCompleteDownload];
    [defaults synchronize];
    
    [[SyncService sharedInstance] buildInitialDataModel:self];
}

#pragma mark -
#pragma mark - SyncService delegate

- (void)returnBuildStatusWithError:(BOOL)hasError errorMessage:(NSString *)errorMessage {
    //[APP_DELEGATE buildMainStackWithLevelIndex:0];
    
    [self initProgressModel];
    
    self.isInitialDataGenerated = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults objectForKey:kInitialModelGenerated]) {
        [defaults setBool:YES forKey:kInitialModelGenerated];
        [defaults synchronize];
    }
    
}


#pragma mark - initProgressModel

- (void)initProgressModel
{
    
    UserStatistic *userStatistic = [PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN];
    if(!userStatistic) {
        ProgressMenager *progressModel = [[ProgressMenager alloc] initWithMSISDN:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN userLevels:[DataService sharedInstance].dataModel.courseData.levelsArray];
        [PROGRESS_MANAGER saveUserStatistic:progressModel.userStatistic];
    }
    [[ProgressMenager sharedInstance] setUserStatistic:[PROGRESS_MANAGER getProgressForUser:[USER_MANAGER userMsisdn] ? [USER_MANAGER userMsisdn] : kGuestUserMSISDN]];
    
    
    NSLog(@"Was logged as VIVO user - %@", [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"] ? @"Yes" : @"No");
    
    if ([USER_MANAGER isNative]) {
        
        [PROGRESS_MANAGER updateProgressModel];
        
    }
    else if ([USER_MANAGER isGuest] && [[NSUserDefaults standardUserDefaults] boolForKey:@"userWasLogged"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userWasLogged"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [PROGRESS_MANAGER updateProgressModel];
        
    }
}


- (BOOL)shouldAutorotate {
    
    return NO;
}



@end
