//
//  LMAppDelegate.m
//  EnglishCourse
//
//  Created by Kiril Kiroski on 12/24/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMAppDelegate.h"
#import "LevelSelectionViewController.h"
#import "LMLoginViewController.h"
//#import <Fabric/Fabric.h>
//#import <Crashlytics/Crashlytics.h>
//#import <SplunkMint-iOS/SplunkMint-iOS.h>//old mint
#import <SplunkMint/SplunkMint.h>

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSLog(@"chekCloudData>>> objectForKey:uid %@ ", [SDCloudUserDefaults stringForKey:@"uid"]);
  NSLog(@"chekCloudData>>>kMonthlyPurchaseId %@ ", [SDCloudUserDefaults objectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]]);
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  
  /*
   // clean save data
   OSStatus errorCode = noErr;
   
   static NSString *const UIApplication_UIID_Key = @"uniqueInstallationIdentifier";
   static NSString *const UIApplication_UserUIID_Key = @"uniqueUserIdentifier";
   
   NSDictionary *query = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id)kSecClassGenericPassword, (__bridge id)kSecClass, UIApplication_UIID_Key,
   (__bridge id)kSecAttrGeneric, UIApplication_UIID_Key, (__bridge id)kSecAttrAccount,
   [[NSBundle mainBundle] bundleIdentifier], (__bridge id)kSecAttrService, nil];
   errorCode = SecItemDelete((__bridge CFDictionaryRef)query);
   
   NSMutableDictionary *queryDelete = [NSMutableDictionary dictionary];
   [queryDelete setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
   [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrGeneric];
   [queryDelete setObject:UIApplication_UserUIID_Key forKey:(__bridge id)kSecAttrAccount];
   [queryDelete setObject:[[NSBundle mainBundle] bundleIdentifier] forKey:(__bridge id)kSecAttrService];
   
   errorCode = SecItemDelete((__bridge CFDictionaryRef)queryDelete);
   
   [SDCloudUserDefaults removeObjectForKey:@"uid"];
   [SDCloudUserDefaults removeObjectForKey:@"uuid"];
   [SDCloudUserDefaults removeObjectForKey:UIApplication_UIID_Key];
   [SDCloudUserDefaults removeObjectForKey:UIApplication_UserUIID_Key];
   [SDCloudUserDefaults removeObjectForKey:[kMonthlyPurchaseId stringByAppendingString:@"_"]];
   [SDCloudUserDefaults removeObjectForKey:kMonthlyPurchaseId];
   [SDCloudUserDefaults synchronize];
   return YES;
   // end clean save data
   /*
   */
   
  [self configureAplicationSettings];
  [self configureReachability];
  [self configureRootController];
  [self.window makeKeyAndVisible];

  //[Fabric with:@[CrashlyticsKit]];
  // [[Mint sharedInstance] initAndStartSession:@"2a2ead0c"];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    //[NOTIFICATION_MANAGER makeNotifications:kParam050];//Feature #4872
    //reason: Notification-the notification should be removed from the app
    
    CFTimeInterval elapsedTime = CACurrentMediaTime() - self.startTime;
    [PROGRESS_MANAGER addPlayTime:elapsedTime];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [PROGRESS_MANAGER addPlayTime:0];
    [NOTIFICATION_MANAGER cancelAllNotifications];
    self.startTime = CACurrentMediaTime();
    self.startSessionDate = [NSDate date];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[SyncService sharedInstance] continueSyncing];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    //[NOTIFICATION_MANAGER makeNotifications:kParam050];
  
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark Build Root ViewControllers
- (void)configureRootController
{
    [DATA_SERVICE loadDataModel];
    
    if ([USER_MANAGER processAutologin]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if (![defaults objectForKey:kDataExtracted] || ![defaults boolForKey:kInitialModelGenerated] || !DATA_SERVICE.dataModel) {
            [self continueAsGuest];
            //[self buildLoginStack];
        }else{
            [self buildMainStack];
        }
    } else {
        //[self buildLoginStack];
        [self continueAsGuest];
    }
}
    
-(void) continueAsGuest {
    [USER_MANAGER initializeWithGuest];
    [USER_MANAGER setGuestStatus];
    [self buildMainStack];
}
    
-(void) presentLoginStack {
    LMLoginViewController *loginController = [LMLoginViewController new];
    self.navigationController.navigationItem.title = @" ";
    [self.navigationController pushViewController:loginController animated:YES];
}

- (void)buildLoginStack
{
  LMLoginViewController *loginController = [LMLoginViewController new];
  self.navigationController = [[UINavigationController alloc] initWithRootViewController:loginController];
  self.baseController = loginController;
  self.window.rootViewController = self.navigationController;
}

- (void)buildLoadingStack
{
  UIViewController *firstController = [UIViewController new];
  self.navigationController = [[UINavigationController alloc] initWithRootViewController:firstController];
  //self.baseController = firstController;
  self.window.rootViewController = self.navigationController;
}

- (void)buildMainStack
{
    [DATA_SERVICE loadDataModel];
//    [PROGRESS_MANAGER updateProgressModel];
    [APP_DELEGATE buildMainStackWithLevelIndex:[PROGRESS_MANAGER getCurrentLevelIndexForUser:[USER_MANAGER userMsisdn]]];
    
}

- (void)backToLevelPage {
    
    if (self.mainController == nil) {
        self.mainController = [MainViewController new];
        self.mainController.levelIndexForFirstController = [PROGRESS_MANAGER currentLevelIndex];

    }
    else {
        
        self.mainController.levelIndexForFirstController = [PROGRESS_MANAGER currentLevelIndex];
        
    }
    
    if (self.navigationController == nil) {
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainController];
    }
    else {
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:self.mainController, nil]];
    }

    [self.mainController checkForChanges];

    
    self.baseController = self.mainController;
    self.window.rootViewController = self.navigationController;
}

#pragma mark - Back to level new level after finish previous level

- (void)backToLevelPageAfterFinishThePrevious {
    
    if (self.mainController == nil) {
        self.mainController = [MainViewController new];
        self.mainController.levelIndexForFirstController = [PROGRESS_MANAGER currentLevelIndex];
        
    }
    else {
        
        self.mainController.levelIndexForFirstController = [PROGRESS_MANAGER currentLevelIndex];
        
    }
    
    if (self.navigationController == nil) {
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainController];
    }
    else {
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:self.mainController, nil]];
    }
    
    
    [self.mainController goOnTop];
    
  
}

- (void)buildMainStackWithLevelIndex:(NSInteger)levelIndex {
    
    NSLog(@"BUILD MAIN STACK LEVEL INDEX");
    
//    self.currentLevelIndex = levelIndex;
    BOOL exist = NO;
    
    [PROGRESS_MANAGER setCurrentLevelIndex:levelIndex];
    
    if (self.mainController == nil) {
        self.mainController = [MainViewController new];
    }
    else {

        self.mainController.levelIndexForFirstController = levelIndex;
        exist = YES;
    }
    
    if (self.navigationController == nil) {
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainController];
    }
    else {
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:self.mainController, nil]];
    }
    
    if (exist) {
        
        [self.mainController performSelectorOnMainThread:@selector(checkForChanges) withObject:nil waitUntilDone:YES];

    }
    
    self.baseController = self.mainController;
    self.window.rootViewController = self.navigationController;

}

- (void)openLevelSelectionAfterFinishingLevels {
    
    if (self.mainController == nil) {
        self.mainController = [MainViewController new];
    }
    
    
    if (self.navigationController == nil) {
        
        LevelSelectionViewController *controler = [LevelSelectionViewController new];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.mainController];
        [self.navigationController pushViewController:controler animated:YES];
    }
    else {
        
        LevelSelectionViewController *controler = [LevelSelectionViewController new];
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:self.mainController, nil]];
        [self.navigationController pushViewController:controler animated:YES];
    }
    
    
    self.baseController = self.mainController;
    self.window.rootViewController = self.navigationController;
}
- (void)buildLessonStack:(NSArray *)arrayLevelData withIndex:(NSIndexPath *)indexPath :(NSInteger)levelIndex{
    
}
- (void)buildLessonStack:(NSArray *)arrayLevelData withLevel:(NSInteger)levelIndex section:(NSInteger)section unit:(NSInteger)unit
{
    //NSLog(@"BUILD LESSON STACK");
    
    
    BOOL isReturningInSameLesson = YES;
    
    if ([PROGRESS_MANAGER currentSectionIndex] == section && [PROGRESS_MANAGER currentUnitIndex] == unit) {
        
        isReturningInSameLesson = NO;
//        [PROGRESS_MANAGER setLastVisitedInstance:[PROGRESS_MANAGER currentLevelIndex] section:section unit:unit instanceIndex:0];

    }
    else {
        

    }

    [PROGRESS_MANAGER setCurrentSectionIndex:section];
    [PROGRESS_MANAGER setCurrentUnitIndex:unit];
    
    BOOL exist = NO;
    
    if (self.lessonController == nil) {
        self.lessonController = [LessonViewController new];
        self.lessonController.arrayLevelDataSource = arrayLevelData;
        self.lessonController.currentIndex = [NSIndexPath indexPathForRow:unit inSection:section];
        self.lessonController.currentLevelIndex = levelIndex;
    }
    else {
        
        self.lessonController.arrayLevelDataSource = arrayLevelData;
        self.lessonController.currentIndex = [NSIndexPath indexPathForRow:unit inSection:section];
        self.lessonController.currentLevelIndex = levelIndex;
        
        exist = YES;
    }
    
    
    if (self.navigationController == nil) {
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:self.lessonController];
    }
    else {
        [self.navigationController setViewControllers:[NSArray arrayWithObjects:self.lessonController, nil]];
    }
    
    if (exist) {
        
        //to do : check if different index
        [self.lessonController refreshContentWithStartPage:isReturningInSameLesson];
    }
    
    self.baseController = self.lessonController;
    self.window.rootViewController = self.navigationController;
}

- (void)buildSplashStack
{
    SplashScreenViewController *splashScreenViewController = [SplashScreenViewController new];
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:splashScreenViewController];
    self.baseController = splashScreenViewController;
    self.window.rootViewController = self.navigationController;
}

#pragma mark - Imlpementation
- (void)configureAplicationSettings
{
  [AUDIO_MANAGER setEnabled:[[NSUserDefaults standardUserDefaults] boolForKey:@"SoundEffectsEnabled"]];
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  if (![[[defaults dictionaryRepresentation] allKeys] containsObject:@"SoundEffectsEnabled"]) {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SoundEffectsEnabled"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"NotificationEnabled"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"Demonstration"];
    [AUDIO_MANAGER setEnabled:[[NSUserDefaults standardUserDefaults] boolForKey:@"SoundEffectsEnabled"]];
    
    NSDate* now = [NSDate date];
    [[NSUserDefaults standardUserDefaults] setObject:now forKey:kStartAplicationDate];
    [[NSUserDefaults standardUserDefaults] synchronize];
  }
}
-(void)scheduleLocalNotification{
  DLog(@"scheduleLocalNotification");
}


#pragma mark - configureReachability

- (void)configureReachability
{
    
    Reachability *reach = [Reachability reachabilityWithHostName:@"http://google.com"];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [reach startNotifier];
}

#pragma mark - reachabilityChanged

- (void)reachabilityChanged:(NSNotification *)notification
{
    Reachability *localReachability = [notification object];
  
    if ([localReachability isReachable]) {
      //NSLog(@"INTERNET ON");
        NSMutableArray *requestList = [API_REQUEST_MANAGER getRequestList];
        if(requestList.count > 0) {
            [API_REQUEST_MANAGER setRequestList:requestList];
            [API_REQUEST_MANAGER doNetworkOperations];
        }
    }
    else {
      //NSLog(@"INTERNET OFF");
    }
}


@end
