//
//  LMViewMacros.h
//  EnglishCourse
//
//  Created by Darko Trpevski on 12/3/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#ifndef EnglishCourse_LMViewMacros_h
#define EnglishCourse_LMViewMacros_h

#define LM_VEHICLE_RECT CGRectMake(LM_WIDTH*page, 0, LM_WIDTH, LM_SCROLL_HEIGHT)
#define LM_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define LM_SCROLL_HEIGHT (([UIScreen mainScreen].bounds.size.height)-64-49)
#define LM_DEMO_SCROLL_HEIGHT (([UIScreen mainScreen].bounds.size.height))


#endif
