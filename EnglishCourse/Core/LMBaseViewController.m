//  LMBaseViewController.m
//  Created by Kiril Kiroski on 12/2/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//


#import "LMBaseViewController.h"


@interface LMBaseViewController ()

@property(strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property(strong, nonatomic) UIView *activityIndicatorBacground;

@end


@implementation LMBaseViewController

+ (id) new
{
  return [[self class] newFromXibNamed:NSStringFromClass([self class])];
}

+ (id)newFromXibNamed:(NSString *)xib
{
  return [(LMBaseViewController *)[[self class] alloc] initWithNibName:xib bundle:0];
}


- (id)initWithNibName:(NSString *)xib bundle:(NSBundle *)bundle
{
  BOOL use568H = NO;
  if (IS_WIDESCREEN) {
    NSString *path = [[NSBundle mainBundle] pathForResource:[xib stringByAppendingString:@"-568h"] ofType:@"nib"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
      use568H = YES;
    }
  }
  self = [super initWithNibName:use568H ? [xib stringByAppendingString:@"-568h"] : xib bundle:bundle];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  [self configureUI];
  [self configureData];
  [self setNeedsStatusBarAppearanceUpdate];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self configureAppearance];
  [self configureNavigation];
  [self configureObservers];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  [self loadData];
  [self layout];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self dismissObservers];
}

- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
}

#pragma mark - Implementation

- (void)configureAppearance
{
  UIImage *bgImage = [UIImage imageNamed:@"bg.png"];
  self.view.backgroundColor = [UIColor colorWithPatternImage:bgImage];
  
  if ([UIScreen mainScreen].bounds.size.height == 568.0) {
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg-568h"]];
  }
  
  //!TODO : remove if you have image :@"bg.png"
  self.view.backgroundColor = [UIColor whiteColor];
}

- (void)configureUI
{
  self.viewWidth = self.view.frame.size.width;
  self.viewHeight = self.view.frame.size.height;
}

- (void)configureObservers
{
}

- (void)configureNavigation
{
 /* self.navigationItem.title = Localized(@"T069");
  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0") || SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0")) {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar-iOS7"]
                                                 forBarPosition:UIBarPositionTopAttached
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setTranslucent:NO];
  
  } else {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar-iOS6"] forBarMetrics:UIBarMetricsDefault];
  }
*/
  self.navigationController.navigationBar.tintColor =[UIColor whiteColor]; ;//old color[APPSTYLE otherViewColorForKey:@"Purple_Main_Color"];
  if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
  }
  self.navigationController.navigationBar.titleTextAttributes = @{
    NSFontAttributeName : [UIFont fontWithName:@"HelveticaNeue-Bold" size:18.0],
    NSForegroundColorAttributeName : [UIColor whiteColor]//old color [APPSTYLE colorForType:@"Purple_Main_Color"]
  };
  self.navigationController.navigationBar.barTintColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];
  self.navigationController.navigationBar.translucent = NO;
  //self.navigationController.navigationBar.topItem.title = Localized(@"T069");
  //[self.navigationController setNavigationBarHidden:NO animated:YES];
  [UIBarButtonItem.appearance setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
  //////
  self.navigationController.navigationBar.layer.shadowRadius = 0;
  self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0, 0.8);
  self.navigationController.navigationBar.layer.shadowOpacity = 0.8;
  self.navigationController.navigationBar.layer.shadowColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"].CGColor;
  UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.navigationController.navigationBar.bounds];
  self.navigationController.navigationBar.layer.shadowPath = path.CGPath;
  ////
    
}


- (void)makeDoneBtn
{
  UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
  doneButton.style = UIBarButtonItemStyleBordered;
  self.navigationItem.rightBarButtonItem = doneButton;
}


- (void)configureData
{
}

- (void)loadData
{
}

- (void)layout
{
}

- (void)dismissObservers
{
}

#pragma mark - Status Bar Style

- (UIStatusBarStyle)preferredStatusBarStyle
{
  return UIStatusBarStyleDefault;
}

#pragma mark - done Action
- (void)doneAction
{
}

#pragma mark - show Settings
- (void)showSettings
{
}




#pragma mark - ActivityIndicator
- (void)addActivityIndicator
{
  if (!self.activityIndicator) {
    self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityIndicatorBacground.userInteractionEnabled = YES;
    self.activityIndicatorBacground.backgroundColor = [UIColor darkGrayColor];
    self.activityIndicatorBacground.alpha = 0.5;
    [self.view addSubview:self.activityIndicatorBacground];


    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.activityIndicator.color = [APPSTYLE otherViewColorForKey:@"Italy_Main_Green_Color"];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.activityIndicator startAnimating];
  }
}
- (void)addActivityIndicatorForFramePresent
{
  if (!self.activityIndicator) {
    self.activityIndicatorBacground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    self.activityIndicatorBacground.userInteractionEnabled = YES;
    self.activityIndicatorBacground.backgroundColor = [UIColor clearColor];
    self.activityIndicatorBacground.alpha = 0.5;
    [self.view addSubview:self.activityIndicatorBacground];
    
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:self.activityIndicator];
    self.activityIndicator.transform = CGAffineTransformMakeScale(1.3, 1.3);
    //self.activityIndicator.color = [APPSTYLE otherViewColorForKey:@"Button_Login_Frame"];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2+160);
    [self.activityIndicator startAnimating];
    
    UILabel *info = [[UILabel alloc] initWithFrame:CGRectMake(180, 0, 160, 50)];
    info.center = CGPointMake(180, self.view.frame.size.height / 2+160);
    info.x = 180;
    /*if (IS_IPHONE4)
    {
      info.y -= 20;
    }*/
    info.textColor = [UIColor blackColor];
    info.text = @"Processando...";
    [self.activityIndicatorBacground addSubview:info];
  }
}

- (void)removeActivityIndicator
{
  if (self.activityIndicator) {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    [self.activityIndicatorBacground removeFromSuperview];
    self.activityIndicator = nil;
    self.activityIndicatorBacground = nil;
  }
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {

    return UIInterfaceOrientationMaskPortrait;
    
}

- (BOOL)shouldAutorotate {
    
  return NO;
    
}
/*- (BOOL)prefersStatusBarHidden {
  return NO;
}*/
@end
