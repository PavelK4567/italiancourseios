//
//  UIImage+DocumentsPath.h
//  EnglishCourse
//
//  Created by Dimitar Shopovski on 12/23/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (DocumentsPath)

+ (UIImage *)imageFromDocumentsResourceFile:(NSString *)fileName;

@end
