//
//  UIView+K1000Shadow.h
//  K1000
//
//  Created by Action Item on 4/13/14.
//  Copyright (c) 2014 hAcx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (K1000Shadow)

- (void)addK1000Shadow;

- (void)addVehicleBaseShadow;

@end
