//
//  NSDate-Expanded.m
//  YES
//
//  Created by action item on 1/14/10.
//  Copyright 2010 RosTelecom. All rights reserved.
//

#import "NSDate-Expanded.h"

#define DATE_COMPONENTS (NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
#define CURRENT_CALENDAR [NSCalendar currentCalendar]
#define NUMBER_OF_SECONDS_IN_A_DAY 86400

@implementation NSDate (Expanded)

static NSDateFormatter *gDateFormatter = NULL;





- (BOOL)isBefore:(NSDate*)other
{
	BOOL retVal = [self timeIntervalSinceDate:other] < 0;
	return retVal;
}

- (BOOL)isAfter:(NSDate*)other
{
	return [self timeIntervalSinceDate:other] > 0;
}

- (BOOL)isSameDay:(NSDate*)other
{
    if (other == nil) return NO; // or it will crash

    NSCalendar* calendar = [NSCalendar currentCalendar];

    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:self];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:other];

    return [comp1 day] == [comp2 day] &&
	[comp1 month] == [comp2 month] &&
	[comp1 year]  == [comp2 year];
}

- (int)daysFrom1970
{
	NSTimeInterval since = [self timeIntervalSince1970];
	return since/(60*60*24);
}

- (int)daysFromDate:(NSDate *)date
{
	NSTimeInterval since = [self timeIntervalSinceDate:date];
	return since/(60*60*24);
}

- (BOOL)is7DaysFromNow
{
	NSDate *seven = [[NSDate date] dateByAddingTimeInterval:86400*7];
	return [seven isSameDay:self];
}

- (BOOL)isTomorrow
{
	NSDate *tomorrow = [[NSDate date] dateByAddingTimeInterval:86400];
	return [tomorrow isSameDay:self];
}

- (BOOL)isYesterday
{
    NSDate *yesterday = [[NSDate date] dateByAddingTimeInterval:-86400];
	return [yesterday isSameDay:self];
}

- (NSDate *)dateAtEndOfDay
{
	NSDateComponents *components = [CURRENT_CALENDAR components:DATE_COMPONENTS fromDate:self];
	[components setHour:23];
	[components setMinute:59];
	[components setSecond:59];
	return [CURRENT_CALENDAR dateFromComponents:components];
}

#pragma mark - Implementation of methods, methods bellow return nil or 1

+ (NSInteger)dayOfWeekOfDate:(NSDate *)date {
    
    return 0;
}
+ (NSDate *)dateWithStringFormat:(NSString *)format dateAsString:(NSString *)dateAsString {
    
    return nil;
}

+ (NSDate *)addMinutesToDate:(NSDate *)date minutes:(NSInteger)minutes {
    
    return nil;
}

+ (NSDate *)addSecondsToDate:(NSDate *)date seconds:(NSInteger)seconds {
    
    return nil;
}

+ (NSDate *)addHoursToDate:(NSDate *)date hours:(NSInteger)hours {
    
    return nil;
}

+ (NSDate *)addDaysToDate:(NSDate *)date days:(NSInteger)days {
    
    return nil;
}

+ (NSDate *)subtractDaysFromDate:(NSDate *)date days:(NSInteger)days {
    
    return nil;
}


- (BOOL)isDateBetween:(NSDate *)firstDate secondDate:(NSDate *)secondDate {
    
    return NO;
}

- (NSString *)getDateAsPhpString {
    
    return nil;
}


- (NSString *)getDateAsStringWithFormat:(NSString *)format {
    
    return nil;
}

- (NSString *)getDateAsString {
    
    return nil;
}

- (NSString *)getTimeAsStringWithSeconds {
    
    return nil;
}

- (NSString *)getTimeAsString {
    
    return nil;
}

- (NSString *)stringWithFormat:(NSString *)format {
    
    return nil;
}

- (NSInteger)dayOfWeek {
    
    return 0;
}

- (NSInteger)dayOfMonth {
    
    return 0;
}

- (NSDate *)dateAtStartOfDay {
    
    return nil;
}



- (NSString *)asUTCString:(NSString *)format {
    
    return nil;
}


- (NSDate *)getFirstDayOfWeek {
    
    return nil;
}


- (int)totalYearsFromTime {
    
    return 1;
}

- (int)totalMonthsFromTime {
    
    return 1;
}

- (int)totalDaysFromTime {
    
    return 1;
}

- (int)totalHoursFromTime {
    
    return 1;
}

- (int)totalMinutesFromTime {
    
    return 1;
}

- (int)totalSecondsFromTime {
    
    return 1;
}





- (NSInteger)minutesBeforeDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)minutesAfterDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)hoursBeforeDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)hoursAfterDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)daysBeforeDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)daysAfterDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)weeksAfterDate:(NSDate *)aDate {
    
    return 1;
}

- (NSInteger)monthsAfterDate:(NSDate *)aDate {
    
    return 1;
}


+ (NSDate *)getFirstDayOfCurrentMonth {
    
    return nil;
}

+ (NSUInteger)getCurrentDay {
    
    return 1;
}

+ (NSString *)getCurrentMonthName {
    
    return nil;
}

+ (NSString *)getDayOfWeekName:(NSInteger)dayFromMonthStart {
    
    return nil;
}


+ (NSString *)getDayOfWeekNameByDate:(NSDate *)currentDate {
    
    return nil;
}

+ (NSString *)getMonthNameByDate:(NSDate *)currentDate {
    
    return nil;
}


+ (NSDate *)getDateOfDayInCurrentMonth:(int)day {
    
    return nil;
}


+ (NSTimeInterval)getPreciseDiffByServerTime:(NSDate *)serverTime {
    
    return 0;
}

+ (NSTimeInterval)getDiffByServerTime:(NSDate *)serverTime {
    
    return 0;
}


+ (NSDate *)currentZeroDate {
    
    return nil;
}

+ (NSDate *)combineDateAndTime:(NSDate *)date time:(NSDate *)time {
    
    return nil;
}

- (NSTimeInterval)secondsBeforeDate:(NSDate *)aDate {
    
    return 0;
}

- (NSTimeInterval)secondsAfterDate:(NSDate *)aDate {
    
    return 0;
}



@end
