//  NSDate+Extras.m
//  Created by Dimitar Tasev on 20140214.
//  Copyright (c) 2014 Dimitar Tasev. All rights reserved.


#import "NSDate+Extras.h"


@implementation NSDate (Extras)

+ (NSString *)timeAgo:(NSDate *)referentDate {
  NSDateComponents *components = [[NSCalendar currentCalendar]
    components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
      fromDate:referentDate
        toDate:[NSDate date]
       options:0];
  NSString *stringTimeAgo = 0;
  if (components.year != 0) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoYear"), components.year];
  } else if (components.month) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoMonth"), components.month];
  } else if (components.day) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoDay"), components.day];
  } else if (components.hour) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoHour"), components.hour];
  } else if (components.minute) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoMinute"), components.minute];
  } else if (components.second) {
    stringTimeAgo = [NSString stringWithFormat:Localized(@"Date.TimeAgoSecond"), components.second];
  }

  return stringTimeAgo;
}

#pragma mark -

+ (NSDate *)dateFromString:(NSString *)string withFormat:(NSString *)theFormat {
  if (string == nil)
    return nil;

  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  [dateFormatter setDateFormat:theFormat];
  return [dateFormatter dateFromString:string];
}

+ (NSString *)stringFromDate:(NSDate *)date withFormat:(NSString *)theFormat {
  if (date == nil)
    return nil;

  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  [dateFormatter setDateFormat:theFormat];
  return [dateFormatter stringFromDate:date];
}

+ (NSString *)stringFromDateAutoupdatingCurrentLocale:(NSDate *)date {
  if (date == nil) {
    return nil;
  }

  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  [dateFormatter setDateStyle:NSDateFormatterShortStyle];
  [dateFormatter setLocale:[NSLocale autoupdatingCurrentLocale]];
  return [dateFormatter stringFromDate:date];
}

#pragma mark -

- (NSDate *)dateToBeginningOfDay {
  unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
  NSDateComponents *parts = [[NSCalendar currentCalendar] components:flags fromDate:self];
  [parts setHour:0];
  [parts setMinute:0];
  [parts setSecond:0];
  return [[NSCalendar currentCalendar] dateFromComponents:parts];
}

- (NSDate *)dateToEndOfDay {
  unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
  NSDateComponents *parts = [[NSCalendar currentCalendar] components:flags fromDate:self];
  [parts setHour:23];
  [parts setMinute:59];
  [parts setSecond:59];
  return [[NSCalendar currentCalendar] dateFromComponents:parts];
}

- (BOOL)beforeDate:(NSDate *)date {
  return [self compare:date] == NSOrderedAscending;
}

- (BOOL)beforeOrEqualToDate:(NSDate *)date {
  return [self compare:date] != NSOrderedDescending;
}

- (BOOL)afterDate:(NSDate *)date {
  return [self compare:date] == NSOrderedDescending;
}

- (BOOL)afterOrEqualToDate:(NSDate *)date {
  return [self compare:date] != NSOrderedAscending;
}

- (NSString *)stringWithFormat:(NSString *)format {
  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  dateFormatter.dateFormat = format;
  return [dateFormatter stringFromDate:self];
}

#pragma mark -

+ (NSDateFormatter *)fullFormatter {
  static NSDateFormatter *__formatter = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    __formatter = [NSDateFormatter new];
    [__formatter setDateFormat:@"yyyy-MM-ddTHH:mm:ssZ"];
  });
  return __formatter;
}

+ (NSDateFormatter *)dateFormatter {
  static NSDateFormatter *__formatter = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    __formatter = [NSDateFormatter new];
    [__formatter setDateFormat:@"dd.MM.yyyy"];
  });
  return __formatter;
}

+ (NSDateFormatter *)timeFormatter {
  static NSDateFormatter *__formatter = 0;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    __formatter = [NSDateFormatter new];
    [__formatter setDateFormat:@"HH:mm"];
  });
  return __formatter;
}

@end
