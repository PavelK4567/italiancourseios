//
//  LMBubbleView.h
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSBubbleData.h"

#import "UIColor+Extras.h"
#import "UIImage+Extras.h"
#import "UIView+Extras.h"

#import "LMAudioManager.h"

@protocol LMBubbleViewDelegate <NSObject>

- (void)didSelectViewWithFrame:(CGRect)frame;
- (void)drawNextView;

@end

@interface LMBubbleView : UIView<LMAudioManagerDelegate>

@property (nonatomic, strong) UIView *customView;
@property (nonatomic, strong) UIImageView *bubbleImage;
@property (nonatomic, strong) UIImageView *avatarImage;
@property (nonatomic, strong) UIImageView *arrowImage;

@property (nonatomic, strong) NSBubbleData *data;
@property (nonatomic) BOOL showAvatar;
@property (nonatomic, assign) BOOL isSelectable;

@property (nonatomic, assign) BOOL isExecuted;

@property (nonatomic, weak) id<LMBubbleViewDelegate>delegate;

@property (nonatomic, assign) BOOL drawAnswered;

@property (nonatomic, assign) BubbleType bubbleType;

- (void)resizeToSize:(CGFloat)height;
- (void)setDataInternal:(NSBubbleData *)value;

@end