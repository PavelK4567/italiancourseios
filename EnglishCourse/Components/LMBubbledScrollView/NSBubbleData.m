;//
//  NSBubbleData.m
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "NSBubbleData.h"
#import <QuartzCore/QuartzCore.h>

#define CONTAINER_VIEW_TAG 110

#define TRY_FIRST 1
#define TRY_SECOND 2
#define TRY_THIRD 3

@implementation NSBubbleData

#pragma mark - Text bubble

const UIEdgeInsets textInsetsMine = {5, 15, 11, 10};
const UIEdgeInsets textInsetsSomeone = {5, 15, 11, 10};

+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithText:text date:date type:type];
   
}

+ (id)dataWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type index:(NSInteger)index chatType:(ChatType)chatType answersArray:(NSArray *)answersArray audioSample:(NSString *)audioSample drawAnswered:(BOOL)drawAnswered
{
    //text = @"Buenos días, yo soy Rafael, el director del programa. Buenos días, yo soy Rafael, el director del programa.";
    return [[NSBubbleData alloc] initWithText:text date:date type:type index:index chatType:chatType answersArray:answersArray audioSample:audioSample drawAnswered:drawAnswered];
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type index:(NSInteger)index chatType:(ChatType)chatType answersArray:(NSArray *)answersArray audioSample:(NSString *)audioSample drawAnswered:(BOOL)drawAnswered
{
    self.indexPosition = index;
    self.chatType = chatType;
    self.answerObjectsArray = answersArray;
    
    self.drawAnswered = drawAnswered;
    
    UIView *viewElement = nil;
    
    UIFont *font = [UIFont systemFontOfSize:14.0f];
    CGSize size = CGSizeMake(240, 46);

    if (self.chatType == SIMPLE_CHAT) {
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 0)];
        [containerView setTag:CONTAINER_VIEW_TAG];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, size.height)];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.text = (text ? text : @"");
        label.font = font;
        label.backgroundColor = [UIColor clearColor];
        
        [label sizeToFit];
        
          label.height = label.mHeight + 20;
        
        [containerView addSubview:label];
        
        [containerView setHeight:label.mHeight];
        
//        CGSize labelTextSize = [label boundingRectWithSize:CGSizeMake(240, MAXFLOAT)
//                                                       options:NSStringDrawingUsesLineFragmentOrigin
//                                                    attributes:@{
//                                                                 NSFontAttributeName : font
//                                                                 }
//                                                       context:nil].size;
        
        viewElement = containerView;
    }
    else if (self.chatType == PROGRESSIVE_CHAT)
    {
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 0)];
        [containerView setTag:CONTAINER_VIEW_TAG];
        
        if (self.answerObjectsArray == nil || [self.answerObjectsArray count] == 0) {
            self.btnProfileSound = [[UIImageView alloc] initWithFrame:CGRectMake(0, 9, kSpeakerImageWidth, kSpeakerImageHeight)];
            self.btnProfileSound.userInteractionEnabled = YES;
            self.btnProfileSound.image = [UIImage imageNamed:@"ic_small_sound2.png"];
            //btnProfileSound.animationImages = @[[UIImage imageNamed:@"ic_small_sound2.png"], [UIImage imageNamed:@"ic_small_sound.png"]];
            
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playIntroSound:)];
            [self.btnProfileSound addGestureRecognizer:tapGesture];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(28, 0, 208, size.height)];
            label.numberOfLines = 0;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            label.text = (text ? text : @"");
            label.font = font;
            label.backgroundColor = [UIColor clearColor];
            
            [label sizeToFit];
            
          //  label.height += 20;
             label.height = label.mHeight + 20;
            
            [containerView addSubview:self.btnProfileSound];
            [containerView addSubview:label];
            
            [containerView setHeight:label.mHeight];
            
        }
        else {
            if ([self.answerObjectsArray count] > 0) {
                
                NSInteger yPosition = 0;
                
                NSInteger answerNumber = 1;
                
                for (Answer *answer in self.answerObjectsArray) {
                    
                    LMNumberedMarkingLabel *numberedMarkedLabel = [[LMNumberedMarkingLabel alloc] init];
                    numberedMarkedLabel.frame = CGRectMake(0, yPosition, 240, kPotentialAnswerHeight);
                    numberedMarkedLabel.delegate = self;
                    numberedMarkedLabel.potentialAnswer = answer;
                    
                    numberedMarkedLabel.indexLabel.text = [NSString stringWithFormat:@"%ld",(long)answerNumber];
                    [numberedMarkedLabel.indexLabel sizeToFit];
                    
                    numberedMarkedLabel.markedTextLabel.text = answer.text;
                    [numberedMarkedLabel.markedTextLabel sizeToFit];
                    
                    numberedMarkedLabel.viewIndex = (answerNumber - 1);
                    
                    numberedMarkedLabel.audioForPlay = audioSample;
                    
                    numberedMarkedLabel.labelTag = answerNumber;
                    numberedMarkedLabel.willExecute = YES;
                    
                    numberedMarkedLabel.indexLabel.y = (numberedMarkedLabel.mHeight / 2) - (numberedMarkedLabel.indexLabel.mHeight / 2);
                    
                    [containerView addSubview:numberedMarkedLabel];
                    
                    numberedMarkedLabel.indexLabel.center = CGPointMake(numberedMarkedLabel.indexLabel.center.x, numberedMarkedLabel.markedTextLabel.center.y);
                    
                    numberedMarkedLabel.imgSoundBtn.center = CGPointMake(numberedMarkedLabel.indexLabel.center.x, numberedMarkedLabel.markedTextLabel.center.y);
                    
//                    numberedMarkedLabel.height =numberedMarkedLabel.markedTextLabel.height;

                    
                    
                    CGFloat currentHeight = numberedMarkedLabel.markedTextLabel.mHeight;
                    
//                    if (answerNumber == 1) {
//                        
//                        [numberedMarkedLabel setBackgroundColor:[UIColor redColor]];
//                    }
//                    else if (answerNumber == 2) {
//                        
//                        [numberedMarkedLabel setBackgroundColor:[UIColor orangeColor]];
//                        
//                    }
//                    else {
//                        [numberedMarkedLabel setBackgroundColor:[UIColor greenColor]];
//                        
//                    }

                    
                    yPosition += (currentHeight + kPotentialAnswerPadding);
                    answerNumber++;
                    
                    
//                    DLog(@"HEIGHT numbered marked label - %f", numberedMarkedLabel.height);
                    
                    if ((self.isAnswered && answer.isCorrect) || (answer.isCorrect && self.drawAnswered)) {
                        [numberedMarkedLabel drawAnswered];
                        [numberedMarkedLabel sizeToAnswered];
                        
                        [containerView setHeight:(numberedMarkedLabel.mHeight + kPotentialAnswerPadding)];
                        
                        break;
                    }
                    
                }
                
                if (!self.drawAnswered) {
                    [containerView setHeight:(yPosition + kPotentialAnswerPadding)];
                    
//                    for (UIView *vv in containerView.subviews) {
//                        
//                        if (([vv isKindOfClass:[LMNumberedMarkingLabel class]])) {
//                            
//                            vv.height = [(LMNumberedMarkingLabel *)vv markedTextLabel].height;
//                        }
//                        
//                    }
                }
                
                
            }
        }
        
        viewElement = containerView;
    }
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    
//    for (UIView *vv in viewElement.subviews) {
//        
//        NSLog(@"----------- sv = %f,  %@", vv.height, [vv class]);
//        if (([vv isKindOfClass:[LMNumberedMarkingLabel class]])) {
//            
//            DLog(@"Height of subview - %f", [(LMNumberedMarkingLabel *)vv markedTextLabel].height)
//        }
//        
//    }
    
    return [self initWithView:viewElement date:date type:type insets:insets];
}

- (id)initWithText:(NSString *)text date:(NSDate *)date type:(NSBubbleType)type
{
    UIFont *font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    CGSize size = [(text ? text : @"") sizeWithFont:font constrainedToSize:CGSizeMake(240, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 240, size.height)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.text = (text ? text : @"");
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    
    UIEdgeInsets insets = (type == BubbleTypeMine ? textInsetsMine : textInsetsSomeone);
    return [self initWithView:label date:date type:type insets:insets];
}

#pragma mark - LMNumberedMarkingLabelDelegate

- (void)didPressItemAtIndex:(NSInteger)itemIndex correctAnswer:(BOOL)correctAnswer sender:(id)sender {
    DLog(@"Answer at index: %ld" ,(long)itemIndex);
    
    self.guessingAnswerCounter ++;
    
    if (correctAnswer && !self.isExecuted) {
        
        self.isExecuted = YES;
        
        [self.delegate resizeAnswersCellWithHeight:(kPotentialAnswerHeight + kPotentialAnswerPadding + kPotentialAnswerPadding + kPotentialAnswerPadding + kPotentialAnswerPadding)];
        
        if (self.guessingAnswerCounter == TRY_FIRST) {
            self.points = kParam010;
        } else if (self.guessingAnswerCounter == TRY_SECOND) {
            self.points = kParam011;
        } else if (self.guessingAnswerCounter >= TRY_THIRD) {
            self.points = kParam012;
        }
        
        [self.delegate returnPointsForCorrentAnswer:self.points isLastAnswer:self.isLastAnswer];
        
        [AUDIO_MANAGER setDelegate:self];
        
//        NSArray *arrayOfLabels = self.view.subviews;
//        
//        LMNumberedMarkingLabel *numberedLabel = (LMNumberedMarkingLabel *)arrayOfLabels[itemIndex];
        
        LMNumberedMarkingLabel *numberedLabel = (LMNumberedMarkingLabel *)sender;
        
        [AUDIO_MANAGER playAudioFromDocumentPath:self.audio componentSender:numberedLabel.imgSoundBtn];
    }
    
}

- (void)didPressItemAtIndex:(NSInteger)itemIndex correctAnswer:(BOOL)correctAnswer sender:(id)sender answerSender:(id)labelSender {
    
//    DLog(@"Answer at index: %ld" ,(long)itemIndex);
    
    self.guessingAnswerCounter ++;
    
    if (correctAnswer && !self.isExecuted) {
        
        self.isExecuted = YES;
        
        float heightAnswer = [(UILabel *)labelSender mHeight];
        [self.delegate resizeAnswersCellWithHeight:(heightAnswer + kPotentialAnswerPadding + kPotentialAnswerPadding + kPotentialAnswerPadding + kPotentialAnswerPadding)];
                
        if (self.guessingAnswerCounter == TRY_FIRST) {
            self.points = kParam010;
        } else if (self.guessingAnswerCounter == TRY_SECOND) {
            self.points = kParam011;
        } else if (self.guessingAnswerCounter >= TRY_THIRD) {
            self.points = kParam012;
        }
        
        [self.delegate returnPointsForCorrentAnswer:self.points isLastAnswer:self.isLastAnswer];
        
        [AUDIO_MANAGER setDelegate:self];
                
        LMNumberedMarkingLabel *numberedLabel = (LMNumberedMarkingLabel *)sender;
        numberedLabel.imgSoundBtn.center = CGPointMake(numberedLabel.indexLabel.center.x, numberedLabel.markedTextLabel.center.y);

        [AUDIO_MANAGER playAudioFromDocumentPath:self.audio componentSender:numberedLabel.imgSoundBtn];
        
        if (correctAnswer) {
            
            self.isAnswered = YES;
        }
    }

}



- (void)executedItemWithTag:(NSInteger)itemTag
{
    NSArray *childsArray = self.view.subviews;
    
    for (UIView *childView in childsArray) {
        
        LMNumberedMarkingLabel *labeledView = (LMNumberedMarkingLabel *)childView;
        
        //if (labeledView.labelTag != itemTag) {
            labeledView.willExecute = NO;
        //}
        
    }
}

- (void)enableLabelsForExecuting
{
    NSArray *childsArray = self.view.subviews;
    
    for (UIView *childView in childsArray) {
        
        LMNumberedMarkingLabel *labeledView = (LMNumberedMarkingLabel *)childView;
        
        labeledView.willExecute = YES;
        
    }
}

#pragma mark - Image bubble

const UIEdgeInsets imageInsetsMine = {11, 13, 16, 22};
const UIEdgeInsets imageInsetsSomeone = {11, 18, 16, 14};

+ (id)dataWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
    return [[NSBubbleData alloc] initWithImage:image date:date type:type];
   
}

- (id)initWithImage:(UIImage *)image date:(NSDate *)date type:(NSBubbleType)type
{
    CGSize size = image.size;
    if (size.width > 240)
    {
        size.height /= (size.width / 240);
        size.width = 240;
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    imageView.image = image;
    imageView.layer.cornerRadius = 5.0;
    imageView.layer.masksToBounds = YES;

    UIEdgeInsets insets = (type == BubbleTypeMine ? imageInsetsMine : imageInsetsSomeone);
    return [self initWithView:imageView date:date type:type insets:insets];       
}

- (IBAction)playIntroSound:(id)sender {
    
    //[((UIImageView *)((UIGestureRecognizer *)sender).view) startAnimating];
    
    //[self.delegate playSoundFromObjectAtIndex:self.indexPosition];
    [self.delegate playSoundFromObjectAtIndex:self.indexPosition sender:(UIImageView *)((UIGestureRecognizer *)sender).view];
    
}

- (void)playSoundAtIndex:(NSInteger)index {
    [self.delegate playSoundFromObjectAtIndex:index];
}

#pragma mark - Custom view bubble

+ (id)dataWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets
{
    return [[NSBubbleData alloc] initWithView:view date:date type:type insets:insets];
  
}
#pragma mark - LMAudioManager implementation

- (void)playbackComplete
{
    [self.delegate drawNextBubble];
}

- (id)initWithView:(UIView *)view date:(NSDate *)date type:(NSBubbleType)type insets:(UIEdgeInsets)insets  
{
    self = [super init];
    if (self)
    {
        _view = view;
        _date = date;

        _type = type;
        _insets = insets;
    }
    return self;
}

@end
