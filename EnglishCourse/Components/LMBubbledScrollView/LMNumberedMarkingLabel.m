//
//  LMNumberedMarkingLabel.m
//  LMBubbledScrollView
//
//  Created by Zarko Popovski on 1/9/15.
//  Copyright (c) 2015 LaMark. All rights reserved.
//

#import "LMNumberedMarkingLabel.h"

@implementation LMNumberedMarkingLabel

- (id)initWithFrame:(CGRect)frame {
    
  self = [super initWithFrame:frame];
  if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"LMNumberedMarkingLabelView" owner:self options:nil] objectAtIndex:0];
      
      UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playSoundAction:)];
      [self.imgSoundBtn addGestureRecognizer:tapGesture];
      
    }
    
    return self;
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UIView *superView = self.superview;
    
    if (self.willExecute && [superView.subviews count] > 1) {
    
        [self.delegate executedItemWithTag:self.labelTag];
        
        if (self.potentialAnswer.isCorrect) {
            
            if (!self.isAnswerExecuted) {
                self.indexLabel.textColor = [UIColor colorWithRed:3.0/255.0 green:145.0/255.0 blue:71.0/255.0 alpha:1.0];
                self.markedTextLabel.textColor = [UIColor colorWithRed:3.0/255.0 green:145.0/255.0 blue:71.0/255.0 alpha:1.0];
                self.indexLabel.font = [UIFont boldSystemFontOfSize:13.0f];
                [self.indexLabel sizeToFit];

                self.markedTextLabel.font = [UIFont boldSystemFontOfSize:13.0f];
                self.markedTextLabel.width = 224.0;
                [self.markedTextLabel sizeToFit];
                
                AUDIO_MANAGER.delegate = self;
                
                DLog(@"playRightSound - %@", self.audioForPlay);
                [AUDIO_MANAGER playRightSound];
                
                self.isAnswerExecuted = YES;
            }
            else {
            
                DLog(@"Auido for play - %@", self.audioForPlay);
                [AUDIO_MANAGER playAudioFromDocumentPath:self.audioForPlay componentSender:self.imgSoundBtn];
            }
        }
        else {
            
            AUDIO_MANAGER.delegate = nil;
            [AUDIO_MANAGER playWrongSound];
            
            if (!self.isTimerExecuted) {
                NSString *indexName = self.indexLabel.text;
                
                self.indexLabel.text = @"X";
                [self.indexLabel sizeToFit];
                self.indexLabel.textColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];;
                self.markedTextLabel.textColor = [APPSTYLE colorForType:@"Italy_Main_Red_Color"];//
                self.markedTextLabel.font = [UIFont boldSystemFontOfSize:14.0f];
                self.indexLabel.font = [UIFont boldSystemFontOfSize:14.0f];

                
                int64_t delayInSeconds = 1;
                
                dispatch_time_t dispatchTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                
                dispatch_after(dispatchTime, dispatch_get_main_queue(), ^(void){
                    
                    self.indexLabel.text = indexName;
                    
                    self.indexLabel.textColor = [UIColor blackColor];
                    self.markedTextLabel.textColor = [UIColor blackColor];
                    self.markedTextLabel.font = [UIFont systemFontOfSize:14.0f];
                    self.indexLabel.font = [UIFont systemFontOfSize:14.0f];

                    self.isTimerExecuted = NO;
                    
                    [self.delegate enableLabelsForExecuting];
                    
                });
                
                self.isTimerExecuted = YES;
                
            }
            
            [self.delegate didPressItemAtIndex:self.viewIndex correctAnswer:NO sender:self];
            
        }
        
    } else {
        UIView *superView = self.superview;
        
        if ([superView.subviews count] == 1) {
            [AUDIO_MANAGER playAudioFromDocumentPath:self.audioForPlay componentSender:self.imgSoundBtn];
        }
    }
}

- (void)changeStatus {
    self.validStatus = !self.validStatus;
}

- (IBAction)playSoundAction:(id)sender {
    [AUDIO_MANAGER playAudioFromDocumentPath:self.audioForPlay componentSender:self.imgSoundBtn];
}

- (void)sizeToAnswered
{
    UIView *superView = self.superview;
    
    for (UIView *childView in superView.subviews) {
        if (((LMNumberedMarkingLabel *)childView).viewIndex != self.viewIndex) {
            [childView removeFromSuperview];
        }
    }
    
    [self setY:0];
}

- (void)drawAnswered
{
    self.indexLabel.hidden = YES;
    self.btnSound.hidden = NO;
    self.imgSoundBtn.hidden = NO;
    
    self.imgSoundBtn.y = (self.mHeight / 2) - (self.imgSoundBtn.mHeight / 2);
    
    self.markedTextLabel.textColor = [UIColor colorWithRed:3.0/255.0 green:145.0/255.0 blue:71.0/255.0 alpha:1.0];
    
    self.markedTextLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    
    [self bringSubviewToFront:self.imgSoundBtn];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark -
#pragma mark - LMAudioManagerDelegate implementation

- (void)playbackComplete
{
    UIView *superView = self.superview;
    
    if ([superView.subviews count] > 1) {
        
        for (UIView *childView in superView.subviews) {
            if (((LMNumberedMarkingLabel *)childView).viewIndex != self.viewIndex) {
                [childView removeFromSuperview];
            }
        }
        
        [self setY:0];
        
        self.imgSoundBtn.y = ((self.mHeight / 3) / 2) - (self.imgSoundBtn.mHeight / 2) - 5;
        
        self.indexLabel.hidden = YES;
        self.imgSoundBtn.hidden = NO;
        self.btnSound.hidden = NO;
        
        [self bringSubviewToFront:self.imgSoundBtn];
        
//        [self.delegate didPressItemAtIndex:self.viewIndex correctAnswer:YES sender:self];

        [self.delegate didPressItemAtIndex:self.viewIndex correctAnswer:YES sender:self answerSender:self.markedTextLabel];
    }
    
}

@end
