//  ASIHTTPRequestFactory.h
//  Created by Dimitar Tasev on 20140401.



#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "ASIDownloadCache.h"



@interface ASIHTTPRequestFactory : NSObject

@property (nonatomic, assign) NSString *languageCode;
@property (nonatomic, assign) NSDictionary *configuration;

+ (id)sharedInstance;

- (void)setAuthorization:(NSDictionary *)headers;
- (void)setTimeoutInterval:(NSTimeInterval)timeout;

- (ASIHTTPRequest *)plainRequestWithURL:(NSString *)url;
- (ASIHTTPRequest *)authRequestWithURL:(NSString *)url;
- (ASIHTTPRequest *)gzipRequestWithURL:(NSString *)url;
- (ASIFormDataRequest *)plainFormRequestWithURL:(NSString *)url;
- (ASIFormDataRequest *)authFormRequestWithURL:(NSString *)url;

- (ASIHTTPRequest *)plainRequestWithURL:(NSString *)url success:(void(^)(void))success failure:(void(^)(void))failure;
- (ASIHTTPRequest *)authRequestWithURL:(NSString *)url success:(void(^)(void))success failure:(void(^)(void))failure;
- (ASIHTTPRequest *)gzipRequestWithURL:(NSString *)url success:(void(^)(void))success failure:(void(^)(void))failure;
- (ASIFormDataRequest *)plainFormRequestWithURL:(NSString *)url success:(void(^)(void))success failure:(void(^)(void))failure;
- (ASIFormDataRequest *)authFormRequestWithURL:(NSString *)url success:(void(^)(void))success failure:(void(^)(void))failure;

@end

#define REQUEST_FACTORY ((ASIHTTPRequestFactory *)[ASIHTTPRequestFactory sharedInstance])
